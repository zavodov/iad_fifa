rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1524600402-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1524600402" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1524600402-layer-602023636" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="602023636" data-review-reference-id="602023636">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024">\
                     <svg:g width="601" height="1024"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.29, 0.78, 22.59, 1.03 Q 32.88, 1.38, 43.17, 1.24 Q\
                        53.47, 1.18, 63.76, 0.59 Q 74.05, 0.51, 84.34, 0.58 Q 94.64, 0.06, 104.93, 0.41 Q 115.22, 1.18, 125.52, 0.81 Q 135.81, 0.80,\
                        146.10, 1.35 Q 156.40, 1.33, 166.69, 1.75 Q 176.98, 0.75, 187.28, 0.58 Q 197.57, 0.79, 207.86, 0.91 Q 218.16, 0.97, 228.45,\
                        1.09 Q 238.74, 0.73, 249.03, 0.42 Q 259.33, 0.32, 269.62, 0.49 Q 279.91, 0.75, 290.21, 0.59 Q 300.50, 0.45, 310.79, 0.31 Q\
                        321.09, 0.34, 331.38, 1.73 Q 341.67, 1.41, 351.97, 0.52 Q 362.26, 0.38, 372.55, 0.44 Q 382.84, 0.17, 393.14, 0.58 Q 403.43,\
                        0.85, 413.72, 1.11 Q 424.02, 0.97, 434.31, 0.34 Q 444.60, 0.29, 454.90, 0.69 Q 465.19, 0.76, 475.48, 0.68 Q 485.78, 1.10,\
                        496.07, 0.97 Q 506.36, 0.90, 516.65, 0.71 Q 526.95, 0.62, 537.24, 1.08 Q 547.53, 1.87, 557.83, 0.85 Q 568.12, 1.60, 578.41,\
                        0.74 Q 588.71, 0.85, 599.38, 1.62 Q 599.63, 11.79, 599.70, 21.90 Q 600.11, 31.93, 600.28, 41.96 Q 599.00, 52.00, 598.61, 62.00\
                        Q 597.96, 72.00, 598.52, 82.00 Q 599.61, 92.00, 599.42, 102.00 Q 599.55, 112.00, 599.59, 122.00 Q 599.02, 132.00, 600.14,\
                        142.00 Q 600.00, 152.00, 600.47, 162.00 Q 600.06, 172.00, 599.36, 182.00 Q 599.09, 192.00, 599.43, 202.00 Q 599.92, 212.00,\
                        599.13, 222.00 Q 599.74, 232.00, 599.14, 242.00 Q 599.31, 252.00, 600.00, 262.00 Q 600.03, 272.00, 599.87, 282.00 Q 600.04,\
                        292.00, 600.69, 302.00 Q 600.24, 312.00, 600.43, 322.00 Q 600.87, 332.00, 600.76, 342.00 Q 600.46, 352.00, 600.18, 362.00\
                        Q 599.82, 372.00, 600.44, 382.00 Q 600.56, 392.00, 600.17, 402.00 Q 600.20, 412.00, 600.34, 422.00 Q 600.49, 432.00, 600.63,\
                        442.00 Q 600.59, 452.00, 600.15, 462.00 Q 599.51, 472.00, 598.91, 482.00 Q 599.29, 492.00, 600.19, 502.00 Q 599.62, 512.00,\
                        599.03, 522.00 Q 600.04, 532.00, 600.38, 542.00 Q 600.20, 552.00, 599.82, 562.00 Q 599.95, 572.00, 599.36, 582.00 Q 600.00,\
                        592.00, 599.40, 602.00 Q 599.51, 612.00, 599.17, 622.00 Q 599.49, 632.00, 599.25, 642.00 Q 599.06, 652.00, 600.06, 662.00\
                        Q 599.47, 672.00, 598.80, 682.00 Q 598.93, 692.00, 599.32, 702.00 Q 599.30, 712.00, 598.36, 722.00 Q 598.27, 732.00, 598.69,\
                        742.00 Q 598.87, 752.00, 598.67, 762.00 Q 598.90, 772.00, 598.40, 782.00 Q 598.72, 792.00, 598.80, 802.00 Q 599.41, 812.00,\
                        599.00, 822.00 Q 598.81, 832.00, 598.53, 842.00 Q 598.87, 852.00, 599.70, 862.00 Q 599.71, 872.00, 599.79, 882.00 Q 600.51,\
                        892.00, 600.25, 902.00 Q 599.99, 912.00, 599.58, 922.00 Q 599.63, 932.00, 598.77, 942.00 Q 599.58, 952.00, 599.21, 962.00\
                        Q 599.74, 972.00, 599.53, 982.00 Q 600.21, 992.00, 599.99, 1002.00 Q 598.73, 1012.00, 599.14, 1022.14 Q 588.91, 1022.60, 578.57,\
                        1023.07 Q 568.14, 1022.33, 557.84, 1022.46 Q 547.53, 1021.97, 537.24, 1022.26 Q 526.95, 1021.77, 516.66, 1022.38 Q 506.36,\
                        1022.52, 496.07, 1023.25 Q 485.78, 1023.64, 475.48, 1022.95 Q 465.19, 1023.41, 454.90, 1022.63 Q 444.60, 1023.12, 434.31,\
                        1023.90 Q 424.02, 1023.95, 413.72, 1023.27 Q 403.43, 1023.32, 393.14, 1023.19 Q 382.84, 1022.77, 372.55, 1023.70 Q 362.26,\
                        1023.83, 351.97, 1023.70 Q 341.67, 1023.58, 331.38, 1023.48 Q 321.09, 1023.26, 310.79, 1023.17 Q 300.50, 1022.98, 290.21,\
                        1022.47 Q 279.91, 1022.52, 269.62, 1023.03 Q 259.33, 1023.23, 249.03, 1023.33 Q 238.74, 1023.16, 228.45, 1022.97 Q 218.16,\
                        1022.54, 207.86, 1022.55 Q 197.57, 1022.67, 187.28, 1022.64 Q 176.98, 1022.49, 166.69, 1022.39 Q 156.40, 1022.80, 146.10,\
                        1023.13 Q 135.81, 1023.39, 125.52, 1023.30 Q 115.22, 1023.44, 104.93, 1023.10 Q 94.64, 1022.98, 84.34, 1022.79 Q 74.05, 1021.64,\
                        63.76, 1021.86 Q 53.47, 1022.78, 43.17, 1022.50 Q 32.88, 1023.08, 22.59, 1023.39 Q 12.29, 1023.29, 1.43, 1022.57 Q 1.47, 1012.18,\
                        2.02, 1002.00 Q 1.69, 992.02, 0.94, 982.03 Q 0.52, 972.02, 0.68, 962.01 Q 0.41, 952.01, 0.45, 942.00 Q 1.24, 932.00, 1.00,\
                        922.00 Q 0.51, 912.00, 0.75, 902.00 Q 1.92, 892.00, 2.17, 882.00 Q 1.33, 872.00, 1.30, 862.00 Q 1.71, 852.00, 1.73, 842.00\
                        Q 1.83, 832.00, 1.30, 822.00 Q 1.37, 812.00, 2.09, 802.00 Q 2.63, 792.00, 1.58, 782.00 Q 1.58, 772.00, 2.51, 762.00 Q 1.98,\
                        752.00, 2.27, 742.00 Q 2.14, 732.00, 0.69, 722.00 Q 1.54, 712.00, 2.19, 702.00 Q 2.72, 692.00, 3.05, 682.00 Q 2.29, 672.00,\
                        1.30, 662.00 Q 0.12, 652.00, 0.05, 642.00 Q -0.22, 632.00, -0.02, 622.00 Q 0.83, 612.00, 2.11, 602.00 Q 1.97, 592.00, 1.95,\
                        582.00 Q 1.53, 572.00, 1.44, 562.00 Q 1.25, 552.00, 1.16, 542.00 Q 1.45, 532.00, 2.41, 522.00 Q 2.24, 512.00, 2.41, 502.00\
                        Q 2.95, 492.00, 1.76, 482.00 Q 1.85, 472.00, 0.79, 462.00 Q 0.79, 452.00, 1.25, 442.00 Q 1.56, 432.00, 1.97, 422.00 Q 2.03,\
                        412.00, 2.61, 402.00 Q 2.09, 392.00, 2.49, 382.00 Q 1.40, 372.00, 2.71, 362.00 Q 3.12, 352.00, 2.36, 342.00 Q 1.75, 332.00,\
                        1.29, 322.00 Q 1.31, 312.00, 0.77, 302.00 Q 0.82, 292.00, 0.16, 282.00 Q 0.36, 272.00, 0.24, 262.00 Q 0.69, 252.00, 1.17,\
                        242.00 Q 0.97, 232.00, 2.16, 222.00 Q 1.76, 212.00, 2.01, 202.00 Q 0.87, 192.00, 1.70, 182.00 Q 2.41, 172.00, 3.12, 162.00\
                        Q 2.51, 152.00, 2.16, 142.00 Q 2.74, 132.00, 1.97, 122.00 Q 1.54, 112.00, 0.36, 102.00 Q 0.18, 92.00, 0.75, 82.00 Q 0.95,\
                        72.00, 1.33, 62.00 Q 0.93, 52.00, 1.55, 42.00 Q 1.24, 32.00, 1.62, 22.00 Q 2.00, 12.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 7.82, 10.20, 12.86, 18.85 Q 18.22, 27.32, 22.56, 36.39 Q 28.27,\
                        44.65, 33.49, 53.20 Q 38.13, 62.09, 42.74, 70.99 Q 48.04, 79.50, 53.49, 87.92 Q 58.80, 96.41, 64.21, 104.85 Q 67.50, 114.53,\
                        73.69, 122.51 Q 79.15, 130.93, 83.52, 139.97 Q 88.30, 148.78, 93.80, 157.16 Q 99.27, 165.57, 104.38, 174.18 Q 109.88, 182.57,\
                        114.24, 191.62 Q 119.74, 200.01, 125.06, 208.50 Q 128.82, 217.90, 133.51, 226.76 Q 139.41, 234.92, 144.93, 243.29 Q 150.37,\
                        251.71, 155.53, 260.30 Q 160.56, 268.96, 165.75, 277.53 Q 170.85, 286.15, 175.55, 295.00 Q 180.01, 304.00, 185.30, 312.50\
                        Q 190.37, 321.14, 194.72, 330.20 Q 199.33, 339.11, 204.99, 347.40 Q 210.63, 355.70, 215.79, 364.29 Q 219.90, 373.49, 224.59,\
                        382.35 Q 229.90, 390.85, 235.34, 399.27 Q 241.18, 407.46, 246.26, 416.09 Q 251.29, 424.75, 256.54, 433.28 Q 261.43, 442.02,\
                        266.30, 450.78 Q 271.44, 459.38, 276.27, 468.16 Q 280.83, 477.09, 285.88, 485.74 Q 290.97, 494.37, 296.55, 502.71 Q 301.74,\
                        511.27, 306.91, 519.85 Q 311.80, 528.60, 316.84, 537.25 Q 321.63, 546.05, 326.83, 554.62 Q 331.79, 563.32, 337.05, 571.84\
                        Q 342.02, 580.54, 347.00, 589.23 Q 351.60, 598.15, 356.20, 607.06 Q 361.39, 615.62, 366.26, 624.38 Q 371.17, 633.11, 377.23,\
                        641.17 Q 382.24, 649.84, 386.63, 658.88 Q 391.61, 667.57, 396.61, 676.25 Q 401.97, 684.71, 407.60, 693.02 Q 412.28, 701.89,\
                        417.77, 710.28 Q 422.53, 719.11, 427.55, 727.77 Q 432.48, 736.49, 437.91, 744.92 Q 442.75, 753.69, 447.95, 762.25 Q 453.16,\
                        770.81, 458.58, 779.24 Q 463.86, 787.76, 468.95, 796.38 Q 473.79, 805.16, 478.96, 813.73 Q 484.00, 822.39, 488.62, 831.29\
                        Q 493.80, 839.86, 499.12, 848.35 Q 503.73, 857.26, 509.20, 865.66 Q 513.99, 874.47, 519.42, 882.89 Q 524.42, 891.57, 529.37,\
                        900.28 Q 534.46, 908.90, 539.73, 917.43 Q 544.92, 925.99, 549.44, 934.95 Q 554.78, 943.43, 558.51, 952.85 Q 563.56, 961.51,\
                        568.74, 970.08 Q 574.01, 978.60, 578.96, 987.30 Q 583.78, 996.09, 589.59, 1004.30 Q 593.94, 1013.35, 599.00, 1022.00" style="\
                        fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 1022.00 Q 5.20, 1012.26, 10.77, 1003.90 Q 16.20, 995.47, 21.34, 986.86\
                        Q 26.40, 978.20, 31.55, 969.61 Q 36.84, 961.09, 41.91, 952.44 Q 46.68, 943.61, 51.72, 934.95 Q 56.67, 926.23, 61.80, 917.61\
                        Q 66.94, 909.01, 72.22, 900.49 Q 77.10, 891.72, 82.69, 883.39 Q 87.49, 874.58, 92.49, 865.89 Q 97.40, 857.15, 102.42, 848.47\
                        Q 108.01, 840.13, 112.40, 831.08 Q 117.75, 822.60, 122.69, 813.87 Q 127.90, 805.31, 133.23, 796.82 Q 139.01, 788.58, 144.12,\
                        779.96 Q 148.56, 770.94, 153.86, 762.43 Q 158.87, 753.74, 164.19, 745.24 Q 169.37, 736.66, 174.19, 727.87 Q 179.20, 719.18,\
                        184.29, 710.55 Q 189.90, 702.21, 194.72, 693.42 Q 199.76, 684.76, 204.35, 675.83 Q 209.46, 667.21, 215.12, 658.90 Q 220.08,\
                        650.19, 224.65, 641.25 Q 229.60, 632.53, 235.24, 624.22 Q 239.31, 614.98, 244.73, 606.54 Q 249.40, 597.65, 255.10, 589.38\
                        Q 260.21, 580.76, 264.97, 571.93 Q 270.05, 563.28, 276.02, 555.16 Q 281.21, 546.59, 286.38, 538.00 Q 291.55, 529.41, 295.55,\
                        520.13 Q 301.06, 511.74, 305.88, 502.95 Q 311.14, 494.41, 315.62, 485.42 Q 320.56, 476.69, 325.66, 468.07 Q 330.89, 459.51,\
                        336.75, 451.33 Q 341.45, 442.46, 346.69, 433.91 Q 351.64, 425.20, 356.60, 416.48 Q 361.57, 407.78, 366.65, 399.14 Q 371.73,\
                        390.49, 378.13, 382.63 Q 383.18, 373.97, 387.89, 365.11 Q 392.81, 356.37, 397.75, 347.65 Q 402.58, 338.86, 407.08, 329.88\
                        Q 412.54, 321.46, 417.19, 312.56 Q 422.64, 304.14, 428.57, 295.99 Q 434.02, 287.57, 438.83, 278.77 Q 443.42, 269.84, 448.68,\
                        261.31 Q 453.21, 252.34, 458.16, 243.62 Q 463.86, 235.34, 469.40, 226.97 Q 473.92, 218.00, 477.87, 208.70 Q 482.81, 199.97,\
                        488.53, 191.71 Q 492.77, 182.57, 498.43, 174.27 Q 503.90, 165.86, 509.57, 157.56 Q 514.90, 149.07, 519.61, 140.21 Q 524.53,\
                        131.47, 529.42, 122.72 Q 534.90, 114.31, 539.75, 105.53 Q 544.99, 96.98, 550.89, 88.83 Q 555.90, 80.14, 561.17, 71.61 Q 566.44,\
                        63.08, 571.58, 54.47 Q 576.79, 45.91, 582.11, 37.41 Q 586.15, 28.16, 590.74, 19.23 Q 595.92, 10.65, 601.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1178807388" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1178807388" data-review-reference-id="1178807388">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1178807388-700202742" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="700202742" data-review-reference-id="700202742">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.83, 71.50, 5.71, 61.00 Q 5.16, 50.50, 5.06, 40.00 Q 5.40, 29.50,\
                                 5.49, 19.00 Q 4.68, 17.50, 4.99, 15.53 Q 6.55, 14.65, 6.86, 13.51 Q 7.47, 12.52, 8.42, 11.43 Q 9.08, 10.23, 10.08, 9.48 Q\
                                 11.50, 9.59, 12.58, 9.03 Q 14.01, 8.21, 15.88, 8.38 Q 29.12, 8.50, 42.28, 7.75 Q 55.47, 7.67, 68.66, 8.37 Q 81.83, 8.82, 95.04,\
                                 8.77 Q 96.51, 9.45, 98.41, 8.89 Q 99.24, 9.95, 100.50, 9.93 Q 101.21, 11.06, 102.30, 11.70 Q 103.17, 12.52, 103.65, 13.61\
                                 Q 104.90, 14.23, 105.74, 15.24 Q 106.35, 16.79, 107.20, 18.59 Q 106.93, 29.32, 106.82, 39.92 Q 106.97, 50.46, 107.07, 60.98\
                                 Q 106.75, 71.49, 105.97, 82.96 Q 95.35, 83.04, 85.12, 82.82 Q 75.08, 83.27, 65.04, 83.32 Q 55.02, 83.40, 45.01, 83.39 Q 35.00,\
                                 82.96, 25.00, 82.39 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'700202742\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'700202742\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="700202742_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="700202742_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-2085045111" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="2085045111" data-review-reference-id="2085045111">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 4.80, 71.50, 5.29, 61.00 Q 5.75, 50.50, 6.09, 40.00 Q 6.77, 29.50,\
                                 6.72, 19.00 Q 7.15, 17.50, 6.67, 15.92 Q 6.97, 14.81, 7.71, 13.87 Q 8.40, 12.95, 8.69, 11.69 Q 9.80, 11.22, 11.17, 11.28 Q\
                                 11.79, 10.12, 12.61, 9.10 Q 13.96, 8.10, 15.74, 7.61 Q 29.11, 8.40, 42.29, 7.94 Q 55.48, 8.22, 68.66, 8.73 Q 81.84, 9.37,\
                                 95.08, 8.49 Q 96.56, 9.25, 98.15, 9.59 Q 99.03, 10.43, 99.86, 11.31 Q 100.83, 11.85, 102.51, 11.48 Q 103.98, 11.94, 104.34,\
                                 13.19 Q 105.09, 14.13, 106.12, 15.07 Q 106.74, 16.64, 107.08, 18.62 Q 105.96, 29.41, 105.75, 39.97 Q 106.59, 50.46, 106.51,\
                                 60.98 Q 105.19, 71.50, 105.58, 82.58 Q 95.15, 82.44, 85.07, 82.52 Q 74.96, 81.41, 64.98, 81.29 Q 55.01, 82.89, 45.00, 81.90\
                                 Q 35.00, 82.48, 25.00, 81.30 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'2085045111\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'2085045111\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="2085045111_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="2085045111_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1567203740" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1567203740" data-review-reference-id="1567203740">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.37, 71.50, 6.34, 61.00 Q 6.82, 50.50, 6.79, 40.00 Q 6.74, 29.50,\
                                 6.79, 19.00 Q 6.65, 17.50, 6.78, 15.95 Q 7.43, 14.97, 8.38, 14.16 Q 8.96, 13.21, 9.56, 12.54 Q 10.58, 12.30, 11.22, 11.36\
                                 Q 11.77, 10.08, 12.74, 9.40 Q 14.66, 9.91, 15.87, 8.31 Q 29.17, 9.07, 42.34, 9.22 Q 55.51, 9.51, 68.67, 8.92 Q 81.83, 8.46,\
                                 94.98, 9.12 Q 96.29, 10.38, 97.60, 11.08 Q 98.82, 10.92, 99.53, 12.00 Q 100.67, 12.18, 102.28, 11.71 Q 103.06, 12.60, 103.81,\
                                 13.51 Q 104.00, 14.73, 104.29, 15.87 Q 105.23, 17.22, 106.21, 18.78 Q 106.24, 29.39, 105.15, 39.99 Q 105.03, 50.50, 105.59,\
                                 60.99 Q 105.99, 71.49, 105.30, 82.30 Q 95.02, 82.05, 85.08, 82.59 Q 75.07, 83.10, 65.04, 83.21 Q 55.02, 83.18, 45.01, 83.20\
                                 Q 35.00, 83.20, 25.00, 82.73 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1567203740\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1567203740\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1567203740_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1567203740_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1136023639" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1136023639" data-review-reference-id="1136023639">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 7.23, 71.50, 7.03, 61.00 Q 7.82, 50.50, 7.26, 40.00 Q 7.02, 29.50,\
                                 7.58, 19.00 Q 7.20, 17.50, 6.47, 15.87 Q 6.01, 14.46, 6.66, 13.42 Q 7.84, 12.69, 8.63, 11.64 Q 9.83, 11.26, 10.91, 10.85 Q\
                                 12.08, 10.65, 12.93, 9.84 Q 14.32, 9.04, 15.80, 7.92 Q 29.02, 7.44, 42.26, 7.27 Q 55.46, 7.06, 68.64, 6.97 Q 81.82, 6.94,\
                                 95.35, 6.82 Q 96.93, 7.76, 98.65, 8.23 Q 99.82, 8.59, 100.91, 9.05 Q 101.78, 9.89, 102.87, 11.12 Q 103.50, 12.28, 104.13,\
                                 13.32 Q 103.64, 14.92, 104.05, 15.98 Q 104.60, 17.46, 104.93, 19.01 Q 105.49, 29.46, 105.77, 39.97 Q 105.84, 50.48, 106.22,\
                                 60.99 Q 106.14, 71.49, 105.70, 82.70 Q 95.37, 83.11, 85.19, 83.34 Q 75.09, 83.40, 65.04, 83.28 Q 55.02, 83.32, 45.01, 83.47\
                                 Q 35.01, 83.63, 25.00, 83.41 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1136023639\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1136023639\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1136023639_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1136023639_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1962094851" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1962094851" data-review-reference-id="1962094851">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.37, 71.50, 5.33, 61.00 Q 5.99, 50.50, 5.58, 40.00 Q 5.88, 29.50,\
                                 6.46, 19.00 Q 6.35, 17.50, 5.75, 15.70 Q 6.45, 14.62, 7.25, 13.68 Q 8.19, 12.86, 8.95, 11.95 Q 10.11, 11.65, 11.05, 11.08\
                                 Q 11.76, 10.06, 12.63, 9.15 Q 14.16, 8.61, 15.86, 8.25 Q 29.09, 8.20, 42.31, 8.49 Q 55.47, 7.86, 68.66, 7.99 Q 81.83, 8.14,\
                                 95.13, 8.19 Q 96.71, 8.65, 98.30, 9.18 Q 99.15, 10.15, 100.27, 10.42 Q 101.33, 10.81, 102.70, 11.29 Q 103.54, 12.25, 104.11,\
                                 13.33 Q 104.52, 14.44, 105.39, 15.39 Q 105.20, 17.23, 105.89, 18.84 Q 106.22, 29.39, 105.75, 39.97 Q 106.46, 50.47, 105.03,\
                                 61.00 Q 105.61, 71.50, 105.67, 82.67 Q 95.16, 82.48, 85.21, 83.47 Q 75.05, 82.79, 65.03, 82.85 Q 55.03, 83.78, 45.00, 82.34\
                                 Q 35.00, 81.99, 25.00, 82.45 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1962094851\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1962094851\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1962094851_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1962094851_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1178807388-1094007743" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1094007743" data-review-reference-id="1094007743">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.44, 71.50, 5.47, 61.00 Q 5.23, 50.50, 5.51, 40.00 Q 5.10, 29.50,\
                                 4.75, 19.00 Q 5.22, 17.50, 5.17, 15.57 Q 5.67, 14.33, 6.07, 13.17 Q 6.42, 12.03, 7.34, 10.38 Q 9.02, 10.15, 10.40, 10.01 Q\
                                 11.22, 9.08, 12.31, 8.43 Q 13.84, 7.79, 15.66, 7.13 Q 28.99, 7.09, 42.24, 6.96 Q 55.45, 6.70, 68.65, 7.11 Q 81.82, 6.87, 95.34,\
                                 6.86 Q 96.98, 7.54, 98.46, 8.75 Q 99.64, 9.03, 100.10, 10.79 Q 100.94, 11.63, 102.37, 11.63 Q 103.02, 12.62, 103.87, 13.47\
                                 Q 104.62, 14.38, 105.14, 15.50 Q 105.75, 17.02, 106.51, 18.72 Q 106.47, 29.37, 105.96, 39.96 Q 106.32, 50.47, 106.40, 60.98\
                                 Q 106.61, 71.49, 105.75, 82.75 Q 95.37, 83.10, 85.12, 82.85 Q 75.07, 83.02, 65.04, 83.38 Q 55.02, 83.15, 45.01, 82.98 Q 35.00,\
                                 83.15, 25.00, 82.91 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1094007743\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1094007743\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1094007743_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1094007743_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-766730" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="766730" data-review-reference-id="766730">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:600px;" width="600" height="465">\
                     <svg:g width="600" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 2.27, 22.55, 2.25 Q 32.83, 2.08, 43.10, 1.86 Q 53.38, 1.40,\
                        63.66, 1.13 Q 73.93, 1.28, 84.21, 1.00 Q 94.48, 1.00, 104.76, 1.47 Q 115.03, 1.37, 125.31, 1.17 Q 135.59, 1.47, 145.86, 1.54\
                        Q 156.14, 1.11, 166.41, 0.90 Q 176.69, 0.35, 186.97, 0.77 Q 197.24, 1.31, 207.52, 1.43 Q 217.79, 1.62, 228.07, 1.73 Q 238.34,\
                        1.29, 248.62, 1.19 Q 258.90, 1.34, 269.17, 1.28 Q 279.45, 1.62, 289.72, 1.70 Q 300.00, 0.68, 310.28, 1.19 Q 320.55, 0.86,\
                        330.83, 1.01 Q 341.10, -0.36, 351.38, 0.34 Q 361.66, -0.16, 371.93, 0.82 Q 382.21, 1.41, 392.48, 1.36 Q 402.76, 1.88, 413.03,\
                        1.42 Q 423.31, 1.95, 433.59, 1.55 Q 443.86, 2.48, 454.14, 2.30 Q 464.41, 1.54, 474.69, 1.67 Q 484.97, 1.66, 495.24, 1.42 Q\
                        505.52, 0.71, 515.79, 1.37 Q 526.07, 1.17, 536.34, 2.05 Q 546.62, 1.47, 556.90, 1.77 Q 567.17, 1.98, 577.45, 1.60 Q 587.72,\
                        0.98, 598.70, 1.30 Q 597.66, 12.13, 598.03, 22.04 Q 598.27, 32.05, 598.48, 42.07 Q 598.11, 52.11, 598.37, 62.13 Q 599.82,\
                        72.15, 600.35, 82.17 Q 600.01, 92.19, 599.25, 102.22 Q 599.65, 112.24, 599.51, 122.26 Q 599.14, 132.28, 598.75, 142.30 Q 598.85,\
                        152.33, 598.67, 162.35 Q 598.60, 172.37, 598.92, 182.39 Q 597.99, 192.41, 598.22, 202.43 Q 598.37, 212.46, 598.58, 222.48\
                        Q 598.64, 232.50, 598.10, 242.52 Q 598.53, 252.54, 598.83, 262.57 Q 598.09, 272.59, 597.31, 282.61 Q 597.85, 292.63, 597.56,\
                        302.65 Q 598.23, 312.67, 598.57, 322.70 Q 598.42, 332.72, 598.22, 342.74 Q 598.47, 352.76, 597.86, 362.78 Q 596.79, 372.80,\
                        598.61, 382.83 Q 599.02, 392.85, 599.69, 402.87 Q 599.93, 412.89, 599.86, 422.91 Q 599.62, 432.93, 599.73, 442.96 Q 599.25,\
                        452.98, 598.46, 463.46 Q 588.10, 464.12, 577.64, 464.36 Q 567.25, 464.10, 556.94, 464.49 Q 546.63, 463.83, 536.35, 463.67\
                        Q 526.07, 464.41, 515.79, 463.63 Q 505.52, 463.23, 495.24, 463.49 Q 484.97, 464.77, 474.69, 464.53 Q 464.41, 463.16, 454.14,\
                        462.71 Q 443.86, 462.99, 433.59, 463.43 Q 423.31, 463.31, 413.03, 463.23 Q 402.76, 463.26, 392.48, 463.26 Q 382.21, 462.96,\
                        371.93, 463.07 Q 361.66, 462.98, 351.38, 463.15 Q 341.10, 464.48, 330.83, 465.24 Q 320.55, 465.31, 310.28, 465.43 Q 300.00,\
                        465.42, 289.72, 464.97 Q 279.45, 463.75, 269.17, 463.60 Q 258.90, 464.18, 248.62, 464.68 Q 238.34, 464.88, 228.07, 464.48\
                        Q 217.79, 464.40, 207.52, 464.11 Q 197.24, 463.88, 186.97, 463.69 Q 176.69, 463.72, 166.41, 464.27 Q 156.14, 464.30, 145.86,\
                        463.68 Q 135.59, 462.54, 125.31, 463.02 Q 115.03, 462.62, 104.76, 463.70 Q 94.48, 463.93, 84.21, 463.58 Q 73.93, 464.12, 63.66,\
                        463.82 Q 53.38, 463.68, 43.10, 463.32 Q 32.83, 464.15, 22.55, 463.60 Q 12.28, 464.38, 1.33, 463.67 Q 1.49, 453.15, 1.51, 443.03\
                        Q 1.30, 432.98, 0.72, 422.95 Q 0.32, 412.92, 0.48, 402.88 Q 0.48, 392.85, 0.73, 382.83 Q 1.80, 372.80, 1.77, 362.78 Q 1.13,\
                        352.76, 0.94, 342.74 Q 1.19, 332.72, 1.42, 322.70 Q 1.25, 312.67, 1.24, 302.65 Q 0.99, 292.63, 1.08, 282.61 Q 1.01, 272.59,\
                        0.95, 262.57 Q 1.22, 252.54, 1.62, 242.52 Q 2.46, 232.50, 1.89, 222.48 Q 1.62, 212.46, 1.89, 202.43 Q 3.01, 192.41, 3.00,\
                        182.39 Q 1.69, 172.37, 0.84, 162.35 Q 1.19, 152.33, 1.29, 142.30 Q 1.04, 132.28, 1.31, 122.26 Q 0.90, 112.24, 0.68, 102.22\
                        Q -0.07, 92.20, -0.29, 82.17 Q 0.33, 72.15, 0.00, 62.13 Q -0.49, 52.11, 0.50, 42.09 Q 0.81, 32.07, 1.61, 22.04 Q 2.00, 12.02,\
                        2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1149758023" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1149758023" data-review-reference-id="1149758023">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:599px;" width="599" height="80">\
                     <svg:g width="599" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.26, 2.98, 22.52, 2.60 Q 32.78, 2.66, 43.03, 1.83 Q 53.29, 2.32,\
                        63.55, 2.76 Q 73.81, 2.37, 84.07, 1.87 Q 94.33, 1.38, 104.59, 1.47 Q 114.84, 1.56, 125.10, 1.48 Q 135.36, 1.60, 145.62, 1.65\
                        Q 155.88, 1.60, 166.14, 1.53 Q 176.40, 1.52, 186.66, 1.65 Q 196.91, 1.88, 207.17, 1.59 Q 217.43, 2.32, 227.69, 1.48 Q 237.95,\
                        1.34, 248.21, 1.40 Q 258.47, 1.83, 268.72, 1.70 Q 278.98, 0.90, 289.24, 2.35 Q 299.50, 1.44, 309.76, 1.73 Q 320.02, 1.60,\
                        330.28, 1.75 Q 340.53, 1.78, 350.79, 1.96 Q 361.05, 2.16, 371.31, 2.39 Q 381.57, 3.36, 391.83, 2.71 Q 402.09, 2.09, 412.34,\
                        2.21 Q 422.60, 3.21, 432.86, 3.24 Q 443.12, 2.32, 453.38, 2.34 Q 463.64, 1.97, 473.90, 2.03 Q 484.15, 1.81, 494.41, 1.78 Q\
                        504.67, 2.42, 514.93, 2.69 Q 525.19, 2.22, 535.45, 2.63 Q 545.71, 1.68, 555.97, 0.66 Q 566.22, 2.03, 576.48, 3.32 Q 586.74,\
                        1.43, 597.06, 1.94 Q 597.48, 14.51, 597.40, 27.28 Q 597.68, 39.95, 597.68, 52.64 Q 597.42, 65.33, 597.11, 78.11 Q 586.78,\
                        78.13, 576.59, 78.76 Q 566.20, 77.70, 555.95, 77.63 Q 545.70, 77.47, 535.44, 77.48 Q 525.19, 77.65, 514.93, 77.66 Q 504.67,\
                        77.43, 494.41, 77.10 Q 484.15, 78.23, 473.90, 78.36 Q 463.64, 78.81, 453.38, 78.59 Q 443.12, 78.49, 432.86, 78.20 Q 422.60,\
                        77.12, 412.34, 77.03 Q 402.09, 77.37, 391.83, 77.58 Q 381.57, 78.26, 371.31, 78.75 Q 361.05, 77.75, 350.79, 78.23 Q 340.53,\
                        78.43, 330.28, 77.98 Q 320.02, 77.87, 309.76, 78.05 Q 299.50, 78.86, 289.24, 78.35 Q 278.98, 78.57, 268.72, 78.63 Q 258.47,\
                        78.65, 248.21, 78.54 Q 237.95, 78.26, 227.69, 78.44 Q 217.43, 78.39, 207.17, 78.98 Q 196.91, 77.91, 186.66, 77.96 Q 176.40,\
                        77.86, 166.14, 78.36 Q 155.88, 79.53, 145.62, 79.49 Q 135.36, 79.21, 125.10, 79.02 Q 114.84, 78.54, 104.59, 77.04 Q 94.33,\
                        78.44, 84.07, 78.82 Q 73.81, 78.29, 63.55, 77.69 Q 53.29, 78.67, 43.03, 78.97 Q 32.78, 79.18, 22.52, 79.63 Q 12.26, 79.42,\
                        1.06, 78.94 Q 1.12, 65.63, 1.35, 52.76 Q 1.43, 40.04, 1.00, 27.37 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1304250031" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1304250031" data-review-reference-id="1304250031">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:600px;" width="600" height="80">\
                     <svg:g width="600" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 3.51, 22.55, 2.18 Q 32.83, 1.38, 43.10, 1.37 Q 53.38, 1.31,\
                        63.66, 1.18 Q 73.93, 0.87, 84.21, 1.73 Q 94.48, 2.34, 104.76, 2.64 Q 115.03, 2.55, 125.31, 2.80 Q 135.59, 3.22, 145.86, 2.07\
                        Q 156.14, 2.46, 166.41, 1.81 Q 176.69, 1.29, 186.97, 1.47 Q 197.24, 2.45, 207.52, 2.12 Q 217.79, 1.82, 228.07, 2.24 Q 238.34,\
                        1.47, 248.62, 1.55 Q 258.90, 1.13, 269.17, 2.50 Q 279.45, 2.28, 289.72, 1.40 Q 300.00, 2.26, 310.28, 2.67 Q 320.55, 2.25,\
                        330.83, 1.55 Q 341.10, 1.10, 351.38, 1.28 Q 361.66, 1.40, 371.93, 1.46 Q 382.21, 3.05, 392.48, 2.72 Q 402.76, 2.41, 413.03,\
                        2.09 Q 423.31, 2.03, 433.59, 2.14 Q 443.86, 2.20, 454.14, 2.38 Q 464.41, 2.04, 474.69, 1.49 Q 484.97, 1.35, 495.24, 1.32 Q\
                        505.52, 0.59, 515.79, 0.22 Q 526.07, 0.44, 536.34, 1.77 Q 546.62, 2.85, 556.90, 1.63 Q 567.17, 2.16, 577.45, 1.23 Q 587.72,\
                        1.15, 597.86, 2.14 Q 598.02, 14.66, 597.91, 27.35 Q 598.89, 39.94, 599.08, 52.63 Q 600.10, 65.30, 599.02, 79.02 Q 588.29,\
                        79.71, 577.70, 79.81 Q 567.30, 79.87, 556.95, 79.72 Q 546.65, 79.97, 536.36, 80.18 Q 526.08, 80.22, 515.80, 79.92 Q 505.52,\
                        79.48, 495.24, 78.07 Q 484.97, 78.33, 474.69, 78.67 Q 464.41, 78.48, 454.14, 78.81 Q 443.86, 79.49, 433.59, 79.27 Q 423.31,\
                        79.73, 413.03, 79.23 Q 402.76, 78.76, 392.48, 78.79 Q 382.21, 79.40, 371.93, 78.36 Q 361.66, 78.49, 351.38, 79.39 Q 341.10,\
                        78.86, 330.83, 78.88 Q 320.55, 78.99, 310.28, 79.03 Q 300.00, 79.05, 289.72, 79.25 Q 279.45, 79.76, 269.17, 79.86 Q 258.90,\
                        79.95, 248.62, 79.73 Q 238.34, 79.92, 228.07, 79.43 Q 217.79, 79.51, 207.52, 79.36 Q 197.24, 79.80, 186.97, 78.91 Q 176.69,\
                        78.52, 166.41, 77.72 Q 156.14, 78.15, 145.86, 79.12 Q 135.59, 79.04, 125.31, 77.45 Q 115.03, 77.66, 104.76, 78.77 Q 94.48,\
                        79.87, 84.21, 79.43 Q 73.93, 77.77, 63.66, 77.57 Q 53.38, 77.50, 43.10, 77.37 Q 32.83, 77.88, 22.55, 77.81 Q 12.28, 79.28,\
                        1.42, 78.58 Q 1.13, 65.62, 1.68, 52.71 Q 0.69, 40.09, 0.36, 27.39 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-376351437" style="position: absolute; left: 115px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="376351437" data-review-reference-id="376351437">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60">\
                     <svg:g width="200" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.89, 0.11, 23.78, 0.02 Q 34.67, -0.09, 45.56, 0.03 Q\
                        56.44, 0.36, 67.33, 0.09 Q 78.22, -0.04, 89.11, 0.09 Q 100.00, 0.59, 110.89, 1.44 Q 121.78, 1.13, 132.67, 0.63 Q 143.56, 0.39,\
                        154.44, 0.51 Q 165.33, 0.43, 176.22, 0.71 Q 187.11, 1.09, 198.09, 1.91 Q 197.41, 16.20, 197.55, 30.06 Q 197.73, 44.02, 197.98,\
                        57.98 Q 187.20, 58.29, 176.29, 58.51 Q 165.38, 58.75, 154.46, 58.62 Q 143.56, 58.34, 132.67, 58.01 Q 121.78, 58.32, 110.89,\
                        59.03 Q 100.00, 57.85, 89.11, 58.73 Q 78.22, 58.28, 67.33, 58.62 Q 56.44, 59.20, 45.56, 58.30 Q 34.67, 57.55, 23.78, 57.09\
                        Q 12.89, 56.15, 2.77, 57.23 Q 2.60, 43.80, 2.63, 29.91 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 11.69, 5.17, 21.83, 6.78 Q 31.87, 8.76, 41.76, 11.23 Q 51.53,\
                        14.15, 61.46, 16.49 Q 71.24, 19.35, 81.08, 22.01 Q 90.53, 26.06, 100.49, 28.27 Q 110.29, 31.10, 120.13, 33.74 Q 129.77, 37.10,\
                        139.68, 39.51 Q 149.54, 42.11, 159.37, 44.82 Q 169.03, 48.09, 178.52, 51.98 Q 188.20, 55.20, 198.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.26, 52.94, 21.24, 50.40 Q 31.31, 48.24, 41.09, 45.00 Q 51.00,\
                        42.22, 61.05, 39.95 Q 70.76, 36.51, 80.77, 34.08 Q 90.66, 31.23, 100.68, 28.86 Q 110.88, 27.12, 120.82, 24.45 Q 130.61, 21.27,\
                        140.15, 17.22 Q 150.03, 14.35, 159.94, 11.57 Q 169.97, 9.24, 179.65, 5.67 Q 190.10, 4.80, 200.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-483149906" style="position: absolute; left: 343px; top: 20px; width: 242px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="483149906" data-review-reference-id="483149906">\
            <div class="stencil-wrapper" style="width: 242px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Коэффициенты </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-718547274" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="718547274" data-review-reference-id="718547274">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1545702130" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1545702130" data-review-reference-id="1545702130">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 80px;width:86px;" width="86" height="80">\
                     <svg:g width="86" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 15.17, 3.42, 28.33, 3.46 Q 41.50, 3.35, 54.67, 2.93 Q 67.83, 2.57,\
                        80.85, 2.15 Q 81.30, 14.07, 81.26, 26.30 Q 81.40, 38.47, 80.92, 50.67 Q 81.10, 62.83, 81.33, 75.33 Q 68.24, 76.23, 54.87,\
                        76.44 Q 41.57, 76.00, 28.37, 76.28 Q 15.19, 76.30, 1.62, 75.38 Q 2.21, 62.76, 1.51, 50.74 Q 0.29, 38.61, 0.21, 26.39 Q 2.00,\
                        14.17, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 82.00, 4.00 Q 83.62, 16.33, 83.33, 28.67 Q 83.20, 41.00, 82.59, 53.33 Q 82.00,\
                        65.67, 82.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 83.00, 5.00 Q 84.40, 17.33, 84.49, 29.67 Q 84.64, 42.00, 84.31, 54.33 Q 83.00,\
                        66.67, 83.00, 79.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 84.00, 6.00 Q 85.83, 18.33, 85.52, 30.67 Q 85.27, 43.00, 85.35, 55.33 Q 84.00,\
                        67.67, 84.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 76.00 Q 14.00, 76.98, 24.00, 75.21 Q 34.00, 74.13, 44.00, 73.50 Q 54.00,\
                        73.81, 64.00, 73.86 Q 74.00, 76.00, 84.00, 76.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 77.00 Q 15.00, 76.82, 25.00, 76.61 Q 35.00, 76.47, 45.00, 76.30 Q 55.00,\
                        76.12, 65.00, 76.83 Q 75.00, 77.00, 85.00, 77.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 78.00 Q 16.00, 77.33, 26.00, 78.02 Q 36.00, 78.70, 46.00, 78.82 Q 56.00,\
                        79.77, 66.00, 79.01 Q 76.00, 78.00, 86.00, 78.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-1524600402-layer-1545702130button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-1524600402-layer-1545702130button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-1524600402-layer-1545702130button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:82px;height:76px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				=<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 86px; height: 80px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1524600402-layer-1545702130\', \'interaction299074184\', {"button":"left","id":"action309743741","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction420937191","options":"reloadOnly","target":"page106900939","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1524600402-layer-2058537725" style="position: absolute; left: 0px; top: 350px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="2058537725" data-review-reference-id="2058537725">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-1524600402-layer-2058537725-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:600px;" width="600" height="395">\
                     <svg:g id="__containerId__-1524600402-layer-2058537725svg" width="600" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.39, 22.55, 1.13 Q 32.83, 0.93, 43.10, 0.92 Q 53.38, 0.39,\
                        63.66, 0.69 Q 73.93, 0.76, 84.21, 0.51 Q 94.48, 0.73, 104.76, 0.10 Q 115.03, 0.88, 125.31, 0.84 Q 135.59, 1.82, 145.86, 1.23\
                        Q 156.14, 0.94, 166.41, 1.21 Q 176.69, 1.69, 186.97, 1.66 Q 197.24, 0.76, 207.52, 0.40 Q 217.79, 1.29, 228.07, 1.91 Q 238.34,\
                        1.55, 248.62, 1.45 Q 258.90, 1.36, 269.17, 2.20 Q 279.45, 1.86, 289.72, 1.45 Q 300.00, 1.76, 310.28, 1.27 Q 320.55, 1.20,\
                        330.83, 1.46 Q 341.10, 1.66, 351.38, 1.87 Q 361.66, 1.71, 371.93, 1.66 Q 382.21, 0.21, 392.48, 1.22 Q 402.76, 0.49, 413.03,\
                        0.67 Q 423.31, 0.45, 433.59, 0.72 Q 443.86, 0.81, 454.14, 0.70 Q 464.41, 0.69, 474.69, 0.27 Q 484.97, 1.49, 495.24, 0.92 Q\
                        505.52, 1.42, 515.79, 1.24 Q 526.07, 1.83, 536.34, 2.73 Q 546.62, 2.00, 556.90, 1.56 Q 567.17, 0.90, 577.45, 1.52 Q 587.72,\
                        0.48, 598.05, 1.95 Q 598.69, 12.06, 598.76, 22.47 Q 599.07, 32.80, 598.98, 43.13 Q 597.81, 53.45, 598.36, 63.73 Q 597.77,\
                        74.03, 598.17, 84.32 Q 597.55, 94.61, 597.62, 104.89 Q 597.93, 115.18, 597.81, 125.47 Q 597.69, 135.76, 598.00, 146.05 Q 599.85,\
                        156.34, 600.44, 166.63 Q 600.18, 176.92, 599.07, 187.21 Q 599.20, 197.50, 599.69, 207.79 Q 599.41, 218.08, 599.35, 228.37\
                        Q 599.32, 238.66, 599.77, 248.95 Q 598.55, 259.24, 598.64, 269.53 Q 598.76, 279.82, 598.98, 290.11 Q 599.35, 300.39, 599.22,\
                        310.68 Q 598.92, 320.97, 599.07, 331.26 Q 599.75, 341.55, 599.76, 351.84 Q 598.79, 362.13, 597.55, 372.42 Q 598.34, 382.71,\
                        598.38, 393.38 Q 587.94, 393.64, 577.49, 393.29 Q 567.17, 392.96, 556.89, 392.84 Q 546.61, 392.05, 536.34, 392.37 Q 526.06,\
                        391.99, 515.79, 392.13 Q 505.52, 393.55, 495.24, 393.57 Q 484.97, 393.92, 474.69, 393.92 Q 464.41, 394.49, 454.14, 394.48\
                        Q 443.86, 393.85, 433.59, 393.23 Q 423.31, 394.67, 413.03, 394.57 Q 402.76, 393.82, 392.48, 393.38 Q 382.21, 393.92, 371.93,\
                        394.33 Q 361.66, 394.56, 351.38, 393.86 Q 341.10, 393.81, 330.83, 394.04 Q 320.55, 394.48, 310.28, 394.90 Q 300.00, 393.49,\
                        289.72, 393.04 Q 279.45, 393.82, 269.17, 393.36 Q 258.90, 392.87, 248.62, 392.44 Q 238.34, 393.04, 228.07, 393.16 Q 217.79,\
                        392.78, 207.52, 392.99 Q 197.24, 392.26, 186.97, 392.32 Q 176.69, 393.38, 166.41, 392.48 Q 156.14, 393.11, 145.86, 393.62\
                        Q 135.59, 394.49, 125.31, 393.92 Q 115.03, 392.70, 104.76, 393.15 Q 94.48, 393.95, 84.21, 395.08 Q 73.93, 395.21, 63.66, 395.40\
                        Q 53.38, 395.42, 43.10, 395.61 Q 32.83, 394.11, 22.55, 394.30 Q 12.28, 393.58, 1.57, 393.43 Q 0.29, 383.28, 0.34, 372.66 Q\
                        0.60, 362.22, 0.55, 351.89 Q 0.43, 341.58, 1.15, 331.27 Q 1.52, 320.98, 1.37, 310.69 Q 0.55, 300.40, 0.61, 290.11 Q 1.80,\
                        279.82, 2.58, 269.53 Q 2.57, 259.24, 2.08, 248.95 Q 1.08, 238.66, 0.98, 228.37 Q 0.85, 218.08, 0.77, 207.79 Q 0.58, 197.50,\
                        0.42, 187.21 Q 0.78, 176.92, 0.66, 166.63 Q 0.51, 156.34, 0.35, 146.05 Q 0.17, 135.76, 0.28, 125.47 Q 0.29, 115.18, 0.38,\
                        104.89 Q 0.25, 94.61, -0.14, 84.32 Q -0.34, 74.03, 0.09, 63.74 Q 0.80, 53.45, 1.42, 43.16 Q 1.53, 32.87, 1.17, 22.58 Q 2.00,\
                        12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1524600402-layer-2058537725-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-1524600402-layer-2058537725-accordion", "596", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-1524600402-layer-1291856583" style="position: absolute; left: 0px; top: 380px; width: 598px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="1291856583" data-review-reference-id="1291856583">\
            <div class="stencil-wrapper" style="width: 598px; height: 273px">\
               <div title="">\
                  <svg:svg xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 273px;width:598px;" width="598" height="273">\
                     <svg:g x="0" y="0" width="598" height="273" style="stroke:black;stroke-width:1px;fill:white;"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.24, 0.46, 22.48, 0.60 Q 32.72, 0.75, 42.97, 0.67 Q 53.21, 0.65,\
                        63.45, 0.93 Q 73.69, 0.65, 83.93, 0.74 Q 94.17, 0.60, 104.41, 0.59 Q 114.66, 1.47, 124.90, 1.61 Q 135.14, 0.90, 145.38, 0.81\
                        Q 155.62, 1.07, 165.86, 0.96 Q 176.10, 0.75, 186.34, 0.74 Q 196.59, 0.58, 206.83, 0.54 Q 217.07, 1.27, 227.31, 2.11 Q 237.55,\
                        2.42, 247.79, 2.47 Q 258.03, 2.13, 268.28, 1.29 Q 278.52, 2.14, 288.76, 1.64 Q 299.00, 2.37, 309.24, 2.10 Q 319.48, 1.78,\
                        329.72, 1.55 Q 339.97, 1.63, 350.21, 1.43 Q 360.45, 1.37, 370.69, 1.64 Q 380.93, 1.56, 391.17, 1.38 Q 401.41, 1.55, 411.66,\
                        1.12 Q 421.90, 1.13, 432.14, 1.79 Q 442.38, 2.43, 452.62, 2.28 Q 462.86, 3.52, 473.10, 2.90 Q 483.35, 2.08, 493.59, 3.00 Q\
                        503.83, 2.95, 514.07, 2.47 Q 524.31, 2.32, 534.55, 1.47 Q 544.79, 1.75, 555.03, 2.79 Q 565.28, 1.75, 575.52, 1.47 Q 585.76,\
                        2.26, 595.41, 2.59 Q 595.05, 12.66, 595.77, 22.72 Q 595.87, 33.05, 596.59, 43.37 Q 596.02, 53.73, 596.97, 64.07 Q 596.56,\
                        74.42, 595.82, 84.77 Q 596.31, 95.12, 596.70, 105.46 Q 597.15, 115.81, 596.95, 126.15 Q 596.98, 136.50, 596.54, 146.85 Q 597.05,\
                        157.19, 596.31, 167.54 Q 596.79, 177.88, 596.55, 188.23 Q 596.35, 198.58, 596.47, 208.92 Q 596.49, 219.27, 597.26, 229.62\
                        Q 596.81, 239.96, 597.28, 250.31 Q 596.01, 260.65, 595.95, 270.95 Q 585.67, 270.75, 575.45, 270.50 Q 565.29, 271.20, 555.06,\
                        271.64 Q 544.80, 271.17, 534.55, 270.29 Q 524.31, 271.04, 514.07, 271.13 Q 503.83, 272.33, 493.59, 272.69 Q 483.35, 271.61,\
                        473.10, 270.76 Q 462.86, 270.55, 452.62, 270.68 Q 442.38, 270.64, 432.14, 271.30 Q 421.90, 271.01, 411.66, 271.26 Q 401.41,\
                        271.95, 391.17, 272.07 Q 380.93, 272.29, 370.69, 272.47 Q 360.45, 271.82, 350.21, 271.24 Q 339.97, 271.86, 329.72, 272.52\
                        Q 319.48, 272.29, 309.24, 271.75 Q 299.00, 271.34, 288.76, 270.84 Q 278.52, 270.15, 268.28, 269.80 Q 258.03, 269.21, 247.79,\
                        269.89 Q 237.55, 269.45, 227.31, 270.47 Q 217.07, 270.65, 206.83, 270.51 Q 196.59, 270.88, 186.34, 271.25 Q 176.10, 271.41,\
                        165.86, 270.27 Q 155.62, 270.89, 145.38, 272.11 Q 135.14, 271.76, 124.90, 271.44 Q 114.66, 270.82, 104.41, 270.58 Q 94.17,\
                        270.92, 83.93, 271.45 Q 73.69, 271.42, 63.45, 271.62 Q 53.21, 271.92, 42.97, 271.98 Q 32.72, 271.67, 22.48, 271.72 Q 12.24,\
                        271.89, 1.82, 271.18 Q 1.24, 260.91, 1.97, 250.31 Q 1.18, 240.02, 1.08, 229.65 Q 1.45, 219.28, 2.24, 208.92 Q 2.02, 198.58,\
                        1.13, 188.23 Q 0.93, 177.89, 1.33, 167.54 Q 2.06, 157.19, 1.90, 146.85 Q 1.67, 136.50, 1.78, 126.15 Q 1.76, 115.81, 1.77,\
                        105.46 Q 1.54, 95.12, 1.16, 84.77 Q 1.16, 74.42, 1.71, 64.08 Q 1.12, 53.73, 0.91, 43.38 Q 1.08, 33.04, 1.89, 22.69 Q 2.00,\
                        12.35, 2.00, 2.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 193.00, 0.00 Q 192.82, 10.50, 194.32,\
                        21.00 Q 195.13, 31.50, 195.51, 42.00 Q 194.86, 52.50, 194.95, 63.00 Q 194.85, 73.50, 195.34, 84.00 Q 195.26, 94.50, 194.59,\
                        105.00 Q 193.74, 115.50, 194.31, 126.00 Q 194.43, 136.50, 194.35, 147.00 Q 194.78, 157.50, 195.06, 168.00 Q 195.27, 178.50,\
                        195.02, 189.00 Q 194.38, 199.50, 194.13, 210.00 Q 193.72, 220.50, 192.55, 231.00 Q 192.67, 241.50, 193.51, 252.00 Q 193.00,\
                        262.50, 193.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 301.00, 0.00 Q 300.72, 10.50,\
                        300.54, 21.00 Q 299.99, 31.50, 300.33, 42.00 Q 299.91, 52.50, 299.86, 63.00 Q 299.94, 73.50, 300.91, 84.00 Q 301.95, 94.50,\
                        300.37, 105.00 Q 300.39, 115.50, 300.95, 126.00 Q 301.57, 136.50, 300.80, 147.00 Q 300.48, 157.50, 300.52, 168.00 Q 300.74,\
                        178.50, 301.66, 189.00 Q 300.18, 199.50, 300.49, 210.00 Q 300.57, 220.50, 301.43, 231.00 Q 300.77, 241.50, 301.52, 252.00\
                        Q 301.00, 262.50, 301.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 409.00, 0.00 Q 408.40,\
                        10.50, 408.34, 21.00 Q 408.12, 31.50, 409.00, 42.00 Q 408.80, 52.50, 408.95, 63.00 Q 409.21, 73.50, 411.17, 84.00 Q 411.36,\
                        94.50, 410.42, 105.00 Q 409.29, 115.50, 409.21, 126.00 Q 408.25, 136.50, 408.17, 147.00 Q 409.06, 157.50, 410.42, 168.00 Q\
                        410.89, 178.50, 410.64, 189.00 Q 410.51, 199.50, 410.63, 210.00 Q 409.91, 220.50, 409.83, 231.00 Q 409.49, 241.50, 409.72,\
                        252.00 Q 409.00, 262.50, 409.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 517.00, 0.00\
                        Q 515.99, 10.50, 515.94, 21.00 Q 516.60, 31.50, 516.20, 42.00 Q 516.36, 52.50, 517.13, 63.00 Q 516.97, 73.50, 516.35, 84.00\
                        Q 515.48, 94.50, 516.45, 105.00 Q 518.17, 115.50, 518.02, 126.00 Q 517.58, 136.50, 517.61, 147.00 Q 517.85, 157.50, 517.12,\
                        168.00 Q 516.96, 178.50, 515.74, 189.00 Q 516.41, 199.50, 516.54, 210.00 Q 517.64, 220.50, 518.71, 231.00 Q 518.52, 241.50,\
                        517.92, 252.00 Q 517.00, 262.50, 517.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00,\
                        39.00 Q 10.31, 38.07, 20.62, 38.00 Q 30.93, 37.67, 41.24, 38.69 Q 51.55, 38.04, 61.86, 38.44 Q 72.17, 39.51, 82.48, 39.82\
                        Q 92.79, 39.00, 103.10, 38.16 Q 113.41, 37.28, 123.72, 37.22 Q 134.03, 37.61, 144.34, 37.75 Q 154.66, 38.04, 164.97, 38.74\
                        Q 175.28, 38.67, 185.59, 38.16 Q 195.90, 38.15, 206.21, 38.01 Q 216.52, 37.71, 226.83, 37.47 Q 237.14, 37.44, 247.45, 37.33\
                        Q 257.76, 37.27, 268.07, 37.49 Q 278.38, 37.41, 288.69, 37.38 Q 299.00, 37.27, 309.31, 37.20 Q 319.62, 37.70, 329.93, 37.97\
                        Q 340.24, 37.58, 350.55, 37.95 Q 360.86, 37.65, 371.17, 37.56 Q 381.48, 37.28, 391.79, 37.38 Q 402.10, 37.41, 412.41, 37.58\
                        Q 422.72, 37.90, 433.03, 38.18 Q 443.34, 38.13, 453.65, 37.99 Q 463.97, 37.97, 474.28, 37.85 Q 484.59, 37.64, 494.90, 37.54\
                        Q 505.21, 37.62, 515.52, 37.76 Q 525.83, 38.26, 536.14, 38.39 Q 546.45, 38.43, 556.76, 38.12 Q 567.07, 37.88, 577.38, 37.92\
                        Q 587.69, 39.00, 598.00, 39.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 78.00 Q 10.31, 76.70,\
                        20.62, 77.22 Q 30.93, 76.77, 41.24, 77.70 Q 51.55, 77.92, 61.86, 77.11 Q 72.17, 77.86, 82.48, 78.32 Q 92.79, 78.37, 103.10,\
                        78.46 Q 113.41, 78.12, 123.72, 77.32 Q 134.03, 77.46, 144.34, 77.97 Q 154.66, 77.84, 164.97, 77.40 Q 175.28, 77.03, 185.59,\
                        77.52 Q 195.90, 77.54, 206.21, 77.83 Q 216.52, 77.36, 226.83, 77.27 Q 237.14, 77.18, 247.45, 77.16 Q 257.76, 77.82, 268.07,\
                        77.54 Q 278.38, 78.09, 288.69, 77.81 Q 299.00, 77.56, 309.31, 77.10 Q 319.62, 77.13, 329.93, 77.04 Q 340.24, 76.94, 350.55,\
                        76.85 Q 360.86, 77.14, 371.17, 78.10 Q 381.48, 78.02, 391.79, 78.14 Q 402.10, 77.78, 412.41, 77.88 Q 422.72, 77.82, 433.03,\
                        77.26 Q 443.34, 76.85, 453.65, 76.50 Q 463.97, 77.02, 474.28, 76.43 Q 484.59, 76.99, 494.90, 77.09 Q 505.21, 77.70, 515.52,\
                        78.27 Q 525.83, 77.53, 536.14, 77.50 Q 546.45, 78.09, 556.76, 78.32 Q 567.07, 77.95, 577.38, 77.88 Q 587.69, 78.00, 598.00,\
                        78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 117.00 Q 10.31, 115.41, 20.62, 115.76 Q 30.93,\
                        116.42, 41.24, 116.18 Q 51.55, 115.05, 61.86, 115.73 Q 72.17, 115.50, 82.48, 116.10 Q 92.79, 115.91, 103.10, 114.84 Q 113.41,\
                        115.32, 123.72, 115.76 Q 134.03, 115.43, 144.34, 115.30 Q 154.66, 117.33, 164.97, 116.80 Q 175.28, 117.49, 185.59, 116.66\
                        Q 195.90, 115.65, 206.21, 114.92 Q 216.52, 114.94, 226.83, 114.83 Q 237.14, 114.68, 247.45, 115.09 Q 257.76, 114.71, 268.07,\
                        114.91 Q 278.38, 114.75, 288.69, 115.19 Q 299.00, 116.24, 309.31, 117.72 Q 319.62, 118.37, 329.93, 116.40 Q 340.24, 116.71,\
                        350.55, 116.90 Q 360.86, 117.45, 371.17, 116.21 Q 381.48, 115.66, 391.79, 115.79 Q 402.10, 115.44, 412.41, 115.18 Q 422.72,\
                        115.56, 433.03, 115.50 Q 443.34, 115.35, 453.65, 115.27 Q 463.97, 115.09, 474.28, 115.33 Q 484.59, 115.43, 494.90, 116.28\
                        Q 505.21, 115.00, 515.52, 115.53 Q 525.83, 115.27, 536.14, 116.08 Q 546.45, 115.68, 556.76, 115.66 Q 567.07, 115.64, 577.38,\
                        115.44 Q 587.69, 117.00, 598.00, 117.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 156.00\
                        Q 10.31, 156.41, 20.62, 156.20 Q 30.93, 156.03, 41.24, 155.68 Q 51.55, 155.36, 61.86, 155.69 Q 72.17, 156.54, 82.48, 155.68\
                        Q 92.79, 155.50, 103.10, 155.60 Q 113.41, 155.51, 123.72, 155.54 Q 134.03, 155.40, 144.34, 156.34 Q 154.66, 155.56, 164.97,\
                        155.12 Q 175.28, 154.63, 185.59, 155.85 Q 195.90, 156.14, 206.21, 155.70 Q 216.52, 155.62, 226.83, 155.39 Q 237.14, 156.34,\
                        247.45, 155.31 Q 257.76, 155.15, 268.07, 155.23 Q 278.38, 155.55, 288.69, 156.05 Q 299.00, 155.58, 309.31, 154.56 Q 319.62,\
                        154.47, 329.93, 154.96 Q 340.24, 154.99, 350.55, 154.78 Q 360.86, 155.14, 371.17, 156.77 Q 381.48, 157.24, 391.79, 157.16\
                        Q 402.10, 157.08, 412.41, 156.74 Q 422.72, 157.20, 433.03, 156.58 Q 443.34, 156.67, 453.65, 155.62 Q 463.97, 155.91, 474.28,\
                        156.67 Q 484.59, 155.91, 494.90, 155.45 Q 505.21, 155.14, 515.52, 155.23 Q 525.83, 155.57, 536.14, 155.64 Q 546.45, 155.29,\
                        556.76, 155.29 Q 567.07, 155.68, 577.38, 155.21 Q 587.69, 156.00, 598.00, 156.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 0.00, 195.00 Q 10.31, 194.83, 20.62, 194.76 Q 30.93, 194.66, 41.24, 194.51 Q 51.55, 194.90, 61.86, 194.29 Q 72.17, 194.27,\
                        82.48, 194.07 Q 92.79, 194.39, 103.10, 195.05 Q 113.41, 194.17, 123.72, 194.03 Q 134.03, 194.13, 144.34, 193.83 Q 154.66,\
                        194.03, 164.97, 193.40 Q 175.28, 194.15, 185.59, 194.47 Q 195.90, 195.43, 206.21, 195.34 Q 216.52, 195.34, 226.83, 194.42\
                        Q 237.14, 194.14, 247.45, 194.07 Q 257.76, 194.24, 268.07, 193.83 Q 278.38, 193.78, 288.69, 193.67 Q 299.00, 194.04, 309.31,\
                        194.54 Q 319.62, 193.87, 329.93, 193.48 Q 340.24, 194.18, 350.55, 193.98 Q 360.86, 194.32, 371.17, 193.88 Q 381.48, 194.35,\
                        391.79, 195.01 Q 402.10, 195.44, 412.41, 194.82 Q 422.72, 194.56, 433.03, 195.25 Q 443.34, 195.00, 453.65, 194.52 Q 463.97,\
                        194.34, 474.28, 194.66 Q 484.59, 193.72, 494.90, 194.22 Q 505.21, 193.74, 515.52, 193.76 Q 525.83, 194.25, 536.14, 193.61\
                        Q 546.45, 193.83, 556.76, 193.63 Q 567.07, 194.34, 577.38, 193.59 Q 587.69, 195.00, 598.00, 195.00" style=" fill:none;"/><svg:path\
                        class=" svg_unselected_element" d="M 0.00, 234.00 Q 10.31, 234.30, 20.62, 234.04 Q 30.93, 233.93, 41.24, 232.72 Q 51.55, 232.36,\
                        61.86, 233.07 Q 72.17, 233.17, 82.48, 233.10 Q 92.79, 232.49, 103.10, 232.44 Q 113.41, 232.26, 123.72, 232.41 Q 134.03, 232.11,\
                        144.34, 232.10 Q 154.66, 232.08, 164.97, 232.63 Q 175.28, 233.56, 185.59, 232.51 Q 195.90, 233.13, 206.21, 232.54 Q 216.52,\
                        232.81, 226.83, 232.14 Q 237.14, 232.14, 247.45, 233.06 Q 257.76, 232.40, 268.07, 232.18 Q 278.38, 232.07, 288.69, 232.05\
                        Q 299.00, 231.85, 309.31, 231.82 Q 319.62, 232.03, 329.93, 232.68 Q 340.24, 233.20, 350.55, 234.43 Q 360.86, 233.30, 371.17,\
                        232.82 Q 381.48, 233.19, 391.79, 233.60 Q 402.10, 233.46, 412.41, 232.84 Q 422.72, 233.15, 433.03, 233.94 Q 443.34, 233.58,\
                        453.65, 233.12 Q 463.97, 232.70, 474.28, 232.91 Q 484.59, 233.35, 494.90, 233.97 Q 505.21, 233.29, 515.52, 232.91 Q 525.83,\
                        232.96, 536.14, 232.76 Q 546.45, 232.69, 556.76, 233.13 Q 567.07, 232.84, 577.38, 232.67 Q 587.69, 234.00, 598.00, 234.00"\
                        style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div style="height: 273px;width:598px; position:absolute; top: 0px; left: 0px;"><span style=\'position: absolute; top: 5px;left: 10px;width:163px;\'><span style=\'position: relative;\'>BK</span></span><span\
                     style=\'position: absolute; top: 5px;left: 203px;width:78px;\'><span style=\'position: relative;\'>1</span></span><span style=\'position:\
                     absolute; top: 5px;left: 311px;width:78px;\'><span style=\'position: relative;\'>X</span></span><span style=\'position: absolute;\
                     top: 5px;left: 419px;width:78px;\'><span style=\'position: relative;\'>2</span></span><span style=\'position: absolute; top: 5px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 44px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>1X ставка</span></span><span style=\'position: absolute; top: 44px;left: 203px;width:78px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 311px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 44px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position:\
                     absolute; top: 83px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Winline</span></span><span style=\'position:\
                     absolute; top: 83px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute;\
                     top: 83px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     83px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 83px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 122px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>Лига ставок</span></span><span style=\'position: absolute; top: 122px;left: 203px;width:78px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left: 311px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 122px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position:\
                     absolute; top: 161px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Фонбет</span></span><span style=\'position:\
                     absolute; top: 161px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute;\
                     top: 161px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     161px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 161px;left:\
                     527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 200px;left: 10px;width:163px;\'><span\
                     style=\'position: relative;\'>Леон</span></span><span style=\'position: absolute; top: 200px;left: 203px;width:78px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 200px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 200px;left: 419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 200px;left: 527px;width:51px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute;\
                     top: 239px;left: 10px;width:163px;\'><span style=\'position: relative;\'>Pinnacle</span></span><span style=\'position: absolute;\
                     top: 239px;left: 203px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     239px;left: 311px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 239px;left:\
                     419px;width:78px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 239px;left: 527px;width:51px;\'><span\
                     style=\'position: relative;\'>...</span></span>\
                  </div>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1524600402"] .border-wrapper, body[data-current-page-id="1524600402"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1524600402"] .border-wrapper, body.has-frame[data-current-page-id="1524600402"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1524600402"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1524600402"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1524600402",\
      			"name": "coefficients",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-600-1024" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:657px;height:1048px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.17, 2.24, 52.33, 1.97 Q 62.50, 1.76, 72.67, 1.44 Q 82.83,\
            1.20, 93.00, 1.56 Q 103.17, 1.59, 113.33, 1.49 Q 123.50, 1.30, 133.67, 1.15 Q 143.83, 1.22, 154.00, 1.73 Q 164.17, 2.40, 174.33,\
            1.43 Q 184.50, 1.93, 194.67, 1.70 Q 204.83, 1.87, 215.00, 1.86 Q 225.17, 1.45, 235.33, 1.60 Q 245.50, 2.03, 255.67, 2.01 Q\
            265.83, 1.88, 276.00, 2.21 Q 286.17, 2.84, 296.33, 3.16 Q 306.50, 2.92, 316.67, 2.22 Q 326.83, 3.02, 337.00, 2.48 Q 347.17,\
            3.00, 357.33, 2.00 Q 367.50, 2.57, 377.67, 2.69 Q 387.83, 2.88, 398.00, 2.99 Q 408.17, 1.89, 418.33, 2.06 Q 428.50, 2.00,\
            438.67, 2.17 Q 448.83, 2.69, 459.00, 3.25 Q 469.17, 2.70, 479.33, 3.05 Q 489.50, 3.04, 499.67, 2.42 Q 509.83, 2.76, 520.00,\
            2.73 Q 530.17, 3.12, 540.33, 3.14 Q 550.50, 2.68, 560.67, 3.56 Q 570.83, 3.66, 581.00, 2.72 Q 591.17, 3.22, 601.33, 1.87 Q\
            611.50, 1.98, 621.67, 2.91 Q 631.83, 3.02, 642.13, 2.87 Q 642.06, 13.07, 641.77, 23.21 Q 641.26, 33.31, 642.86, 43.33 Q 643.14,\
            53.42, 641.90, 63.53 Q 641.13, 73.62, 641.60, 83.71 Q 642.80, 93.79, 641.41, 103.88 Q 640.26, 113.97, 641.81, 124.06 Q 642.50,\
            134.15, 643.50, 144.24 Q 643.91, 154.32, 643.33, 164.41 Q 643.77, 174.50, 643.00, 184.59 Q 642.29, 194.68, 642.68, 204.76\
            Q 642.19, 214.85, 642.02, 224.94 Q 642.57, 235.03, 641.42, 245.12 Q 641.91, 255.21, 641.43, 265.29 Q 642.18, 275.38, 641.51,\
            285.47 Q 641.28, 295.56, 641.73, 305.65 Q 642.47, 315.74, 642.89, 325.82 Q 642.53, 335.91, 641.72, 346.00 Q 641.66, 356.09,\
            642.09, 366.18 Q 642.54, 376.26, 642.35, 386.35 Q 642.05, 396.44, 642.34, 406.53 Q 642.77, 416.62, 642.99, 426.71 Q 642.67,\
            436.79, 642.54, 446.88 Q 642.84, 456.97, 642.77, 467.06 Q 643.39, 477.15, 643.66, 487.24 Q 643.27, 497.32, 641.39, 507.41\
            Q 640.84, 517.50, 642.27, 527.59 Q 643.02, 537.68, 642.48, 547.76 Q 642.83, 557.85, 642.68, 567.94 Q 643.49, 578.03, 643.42,\
            588.12 Q 643.60, 598.21, 642.99, 608.29 Q 642.14, 618.38, 641.79, 628.47 Q 641.70, 638.56, 641.40, 648.65 Q 640.81, 658.74,\
            640.93, 668.82 Q 641.91, 678.91, 642.89, 689.00 Q 643.95, 699.09, 642.48, 709.18 Q 641.86, 719.27, 640.94, 729.35 Q 641.14,\
            739.44, 640.82, 749.53 Q 640.58, 759.62, 641.79, 769.71 Q 643.23, 779.79, 643.71, 789.88 Q 643.07, 799.97, 642.80, 810.06\
            Q 642.31, 820.15, 641.28, 830.24 Q 640.97, 840.32, 642.28, 850.41 Q 642.82, 860.50, 643.09, 870.59 Q 643.76, 880.68, 644.22,\
            890.77 Q 643.80, 900.85, 644.52, 910.94 Q 643.42, 921.03, 642.82, 931.12 Q 643.71, 941.21, 644.34, 951.29 Q 644.42, 961.38,\
            643.68, 971.47 Q 643.07, 981.56, 642.93, 991.65 Q 643.26, 1001.74, 643.31, 1011.82 Q 643.29, 1021.91, 642.32, 1032.32 Q 631.90,\
            1032.20, 621.73, 1032.44 Q 611.61, 1033.63, 601.40, 1033.96 Q 591.18, 1033.07, 581.01, 1033.17 Q 570.84, 1032.90, 560.67,\
            1033.24 Q 550.50, 1031.82, 540.33, 1031.67 Q 530.17, 1032.43, 520.00, 1032.79 Q 509.83, 1032.32, 499.67, 1032.50 Q 489.50,\
            1032.67, 479.33, 1032.87 Q 469.17, 1033.04, 459.00, 1032.94 Q 448.83, 1033.06, 438.67, 1032.70 Q 428.50, 1033.20, 418.33,\
            1033.10 Q 408.17, 1032.84, 398.00, 1032.79 Q 387.83, 1032.38, 377.67, 1031.77 Q 367.50, 1031.63, 357.33, 1031.74 Q 347.17,\
            1031.58, 337.00, 1031.85 Q 326.83, 1032.04, 316.67, 1032.48 Q 306.50, 1032.35, 296.33, 1032.31 Q 286.17, 1031.07, 276.00,\
            1032.05 Q 265.83, 1031.38, 255.67, 1031.94 Q 245.50, 1032.63, 235.33, 1032.58 Q 225.17, 1032.94, 215.00, 1033.24 Q 204.83,\
            1032.59, 194.67, 1032.60 Q 184.50, 1032.71, 174.33, 1032.51 Q 164.17, 1032.64, 154.00, 1032.73 Q 143.83, 1031.71, 133.67,\
            1031.66 Q 123.50, 1031.74, 113.33, 1032.48 Q 103.17, 1033.16, 93.00, 1033.47 Q 82.83, 1033.11, 72.67, 1033.16 Q 62.50, 1032.06,\
            52.33, 1031.31 Q 42.17, 1031.24, 32.52, 1031.48 Q 32.12, 1021.87, 31.49, 1011.90 Q 30.65, 1001.83, 30.32, 991.70 Q 30.79,\
            981.58, 31.18, 971.48 Q 31.70, 961.38, 30.84, 951.30 Q 31.34, 941.21, 31.48, 931.12 Q 31.35, 921.03, 30.36, 910.94 Q 30.94,\
            900.85, 30.07, 890.77 Q 29.91, 880.68, 30.13, 870.59 Q 30.59, 860.50, 31.62, 850.41 Q 31.79, 840.32, 31.79, 830.24 Q 31.25,\
            820.15, 31.84, 810.06 Q 31.37, 799.97, 31.54, 789.88 Q 32.31, 779.79, 31.35, 769.71 Q 32.36, 759.62, 32.29, 749.53 Q 32.05,\
            739.44, 32.02, 729.35 Q 32.40, 719.27, 32.05, 709.18 Q 31.06, 699.09, 31.70, 689.00 Q 30.92, 678.91, 30.53, 668.82 Q 31.55,\
            658.74, 32.54, 648.65 Q 32.15, 638.56, 32.95, 628.47 Q 32.23, 618.38, 32.66, 608.29 Q 32.37, 598.21, 31.29, 588.12 Q 30.96,\
            578.03, 31.57, 567.94 Q 32.11, 557.85, 32.27, 547.76 Q 32.74, 537.68, 31.82, 527.59 Q 32.35, 517.50, 31.24, 507.41 Q 30.60,\
            497.32, 30.82, 487.24 Q 31.17, 477.15, 31.85, 467.06 Q 31.36, 456.97, 31.45, 446.88 Q 31.36, 436.79, 31.23, 426.71 Q 31.34,\
            416.62, 31.66, 406.53 Q 31.00, 396.44, 32.07, 386.35 Q 32.15, 376.26, 30.80, 366.18 Q 31.32, 356.09, 30.47, 346.00 Q 30.92,\
            335.91, 30.50, 325.82 Q 30.67, 315.74, 30.39, 305.65 Q 30.35, 295.56, 30.66, 285.47 Q 30.52, 275.38, 31.43, 265.29 Q 30.31,\
            255.21, 29.99, 245.12 Q 29.94, 235.03, 30.99, 224.94 Q 30.84, 214.85, 30.92, 204.76 Q 31.74, 194.68, 31.08, 184.59 Q 31.39,\
            174.50, 30.91, 164.41 Q 29.98, 154.32, 30.23, 144.24 Q 30.54, 134.15, 30.56, 124.06 Q 30.86, 113.97, 31.04, 103.88 Q 30.93,\
            93.79, 30.99, 83.71 Q 30.92, 73.62, 30.48, 63.53 Q 30.28, 53.44, 30.59, 43.35 Q 30.78, 33.26, 30.56, 23.18 Q 32.00, 13.09,\
            32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.17, 6.13, 43.33, 6.09 Q 53.50, 5.95, 63.67, 5.73 Q 73.83,\
            5.52, 84.00, 5.46 Q 94.17, 5.38, 104.33, 5.38 Q 114.50, 5.57, 124.67, 5.32 Q 134.83, 5.21, 145.00, 5.16 Q 155.17, 5.01, 165.33,\
            5.27 Q 175.50, 5.27, 185.67, 5.32 Q 195.83, 5.25, 206.00, 5.49 Q 216.17, 5.38, 226.33, 6.02 Q 236.50, 5.72, 246.67, 5.92 Q\
            256.83, 5.54, 267.00, 5.14 Q 277.17, 5.03, 287.33, 4.82 Q 297.50, 4.79, 307.67, 5.23 Q 317.83, 5.57, 328.00, 6.34 Q 338.17,\
            6.38, 348.33, 5.99 Q 358.50, 5.87, 368.67, 5.89 Q 378.83, 6.08, 389.00, 6.18 Q 399.17, 6.07, 409.33, 6.57 Q 419.50, 7.34,\
            429.67, 7.80 Q 439.83, 7.15, 450.00, 6.98 Q 460.17, 6.30, 470.33, 7.13 Q 480.50, 5.84, 490.67, 6.54 Q 500.83, 6.42, 511.00,\
            6.46 Q 521.17, 6.23, 531.33, 6.50 Q 541.50, 5.70, 551.67, 5.68 Q 561.83, 5.53, 572.00, 5.60 Q 582.17, 5.78, 592.33, 5.78 Q\
            602.50, 6.89, 612.67, 6.78 Q 622.83, 7.55, 633.03, 6.97 Q 633.54, 16.91, 633.17, 27.15 Q 633.71, 37.22, 633.85, 47.33 Q 633.70,\
            57.43, 632.81, 67.53 Q 633.10, 77.62, 632.85, 87.71 Q 633.32, 97.79, 633.94, 107.88 Q 633.70, 117.97, 634.21, 128.06 Q 633.51,\
            138.15, 634.02, 148.24 Q 632.49, 158.32, 633.16, 168.41 Q 633.20, 178.50, 632.21, 188.59 Q 632.41, 198.68, 631.53, 208.76\
            Q 632.08, 218.85, 631.98, 228.94 Q 632.17, 239.03, 632.22, 249.12 Q 632.76, 259.21, 633.19, 269.29 Q 632.52, 279.38, 631.55,\
            289.47 Q 631.77, 299.56, 633.17, 309.65 Q 633.52, 319.74, 633.14, 329.82 Q 632.73, 339.91, 633.32, 350.00 Q 633.02, 360.09,\
            632.75, 370.18 Q 633.45, 380.26, 633.34, 390.35 Q 633.50, 400.44, 633.04, 410.53 Q 633.54, 420.62, 633.65, 430.71 Q 633.54,\
            440.79, 633.74, 450.88 Q 633.99, 460.97, 634.05, 471.06 Q 633.74, 481.15, 634.18, 491.24 Q 633.75, 501.32, 634.35, 511.41\
            Q 634.94, 521.50, 634.51, 531.59 Q 633.45, 541.68, 633.38, 551.76 Q 634.47, 561.85, 633.56, 571.94 Q 632.99, 582.03, 632.28,\
            592.12 Q 632.03, 602.21, 632.55, 612.29 Q 633.13, 622.38, 633.37, 632.47 Q 633.40, 642.56, 632.94, 652.65 Q 633.78, 662.74,\
            634.22, 672.82 Q 634.12, 682.91, 634.14, 693.00 Q 634.03, 703.09, 633.92, 713.18 Q 632.75, 723.26, 632.17, 733.35 Q 632.69,\
            743.44, 632.99, 753.53 Q 633.23, 763.62, 632.48, 773.71 Q 633.82, 783.79, 633.01, 793.88 Q 633.51, 803.97, 633.65, 814.06\
            Q 633.92, 824.15, 633.53, 834.24 Q 634.12, 844.32, 634.43, 854.41 Q 634.37, 864.50, 633.46, 874.59 Q 633.16, 884.68, 633.71,\
            894.77 Q 632.65, 904.85, 631.56, 914.94 Q 631.90, 925.03, 632.91, 935.12 Q 633.63, 945.21, 633.46, 955.29 Q 633.34, 965.38,\
            632.97, 975.47 Q 633.14, 985.56, 634.27, 995.65 Q 633.76, 1005.74, 632.38, 1015.82 Q 632.77, 1025.91, 633.18, 1036.18 Q 622.96,\
            1036.37, 612.67, 1036.02 Q 602.49, 1035.88, 592.34, 1036.33 Q 582.18, 1036.73, 572.00, 1035.77 Q 561.83, 1035.95, 551.67,\
            1036.50 Q 541.50, 1037.20, 531.33, 1036.96 Q 521.17, 1036.49, 511.00, 1036.79 Q 500.83, 1035.99, 490.67, 1036.06 Q 480.50,\
            1036.07, 470.33, 1035.45 Q 460.17, 1035.70, 450.00, 1036.18 Q 439.83, 1036.86, 429.67, 1036.72 Q 419.50, 1036.46, 409.33,\
            1036.50 Q 399.17, 1036.86, 389.00, 1036.54 Q 378.83, 1036.06, 368.67, 1036.85 Q 358.50, 1036.90, 348.33, 1036.02 Q 338.17,\
            1035.80, 328.00, 1035.69 Q 317.83, 1036.05, 307.67, 1036.24 Q 297.50, 1036.51, 287.33, 1036.23 Q 277.17, 1036.40, 267.00,\
            1036.29 Q 256.83, 1036.09, 246.67, 1035.79 Q 236.50, 1036.17, 226.33, 1035.94 Q 216.17, 1036.59, 206.00, 1035.72 Q 195.83,\
            1035.60, 185.67, 1035.66 Q 175.50, 1036.41, 165.33, 1036.47 Q 155.17, 1036.82, 145.00, 1036.86 Q 134.83, 1036.83, 124.67,\
            1036.94 Q 114.50, 1036.92, 104.33, 1035.74 Q 94.17, 1036.35, 84.00, 1036.81 Q 73.83, 1036.89, 63.67, 1036.38 Q 53.50, 1036.52,\
            43.33, 1037.01 Q 33.17, 1037.44, 22.17, 1036.83 Q 22.21, 1026.18, 21.55, 1016.03 Q 22.11, 1005.80, 22.79, 995.65 Q 23.43,\
            985.55, 23.52, 975.47 Q 22.93, 965.38, 22.25, 955.30 Q 22.22, 945.21, 23.63, 935.12 Q 24.58, 925.03, 23.48, 914.94 Q 23.71,\
            904.85, 23.38, 894.77 Q 23.25, 884.68, 23.29, 874.59 Q 22.98, 864.50, 21.84, 854.41 Q 21.46, 844.32, 21.43, 834.24 Q 22.16,\
            824.15, 21.89, 814.06 Q 21.67, 803.97, 21.83, 793.88 Q 21.27, 783.79, 21.85, 773.71 Q 21.14, 763.62, 20.97, 753.53 Q 20.78,\
            743.44, 21.29, 733.35 Q 22.10, 723.26, 22.33, 713.18 Q 22.21, 703.09, 21.96, 693.00 Q 21.46, 682.91, 21.47, 672.82 Q 21.23,\
            662.74, 21.18, 652.65 Q 21.88, 642.56, 22.95, 632.47 Q 24.42, 622.38, 24.87, 612.29 Q 23.92, 602.21, 23.36, 592.12 Q 22.85,\
            582.03, 22.86, 571.94 Q 23.69, 561.85, 22.71, 551.76 Q 23.81, 541.68, 24.47, 531.59 Q 23.87, 521.50, 22.68, 511.41 Q 22.59,\
            501.32, 23.14, 491.24 Q 22.94, 481.15, 22.46, 471.06 Q 22.65, 460.97, 22.44, 450.88 Q 21.59, 440.79, 21.72, 430.71 Q 21.07,\
            420.62, 21.39, 410.53 Q 20.67, 400.44, 21.31, 390.35 Q 21.44, 380.26, 21.16, 370.18 Q 21.08, 360.09, 20.94, 350.00 Q 21.65,\
            339.91, 21.23, 329.82 Q 21.53, 319.74, 21.59, 309.65 Q 21.58, 299.56, 22.15, 289.47 Q 22.76, 279.38, 23.63, 269.29 Q 23.47,\
            259.21, 23.93, 249.12 Q 24.88, 239.03, 22.66, 228.94 Q 21.90, 218.85, 22.04, 208.76 Q 21.86, 198.68, 21.69, 188.59 Q 21.87,\
            178.50, 21.20, 168.41 Q 21.74, 158.32, 21.00, 148.24 Q 22.32, 138.15, 21.85, 128.06 Q 22.01, 117.97, 22.19, 107.88 Q 21.38,\
            97.79, 21.44, 87.71 Q 21.26, 77.62, 20.64, 67.53 Q 21.50, 57.44, 21.79, 47.35 Q 21.54, 37.26, 22.25, 27.18 Q 23.00, 17.09,\
            23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.17, 11.28, 60.33, 10.79 Q 70.50, 10.52, 80.67, 10.32 Q 90.83,\
            11.25, 101.00, 10.60 Q 111.17, 10.68, 121.33, 9.85 Q 131.50, 9.83, 141.67, 10.00 Q 151.83, 9.70, 162.00, 9.72 Q 172.17, 9.71,\
            182.33, 10.98 Q 192.50, 10.21, 202.67, 10.51 Q 212.83, 10.33, 223.00, 9.55 Q 233.17, 9.13, 243.33, 9.11 Q 253.50, 9.04, 263.67,\
            9.53 Q 273.83, 10.17, 284.00, 10.09 Q 294.17, 9.19, 304.33, 9.27 Q 314.50, 10.16, 324.67, 10.53 Q 334.83, 10.54, 345.00, 10.63\
            Q 355.17, 10.71, 365.33, 11.50 Q 375.50, 10.87, 385.67, 11.21 Q 395.83, 10.32, 406.00, 9.83 Q 416.17, 9.54, 426.33, 8.82 Q\
            436.50, 8.72, 446.67, 9.02 Q 456.83, 8.83, 467.00, 8.81 Q 477.17, 9.60, 487.33, 8.96 Q 497.50, 9.71, 507.67, 9.46 Q 517.83,\
            9.56, 528.00, 9.57 Q 538.17, 9.27, 548.33, 9.16 Q 558.50, 9.30, 568.67, 9.52 Q 578.83, 9.52, 589.00, 9.68 Q 599.17, 9.21,\
            609.33, 9.02 Q 619.50, 9.01, 629.67, 9.60 Q 639.83, 10.50, 650.37, 10.63 Q 650.45, 20.94, 650.30, 31.13 Q 649.58, 41.29, 650.02,\
            51.35 Q 650.21, 61.44, 650.72, 71.52 Q 650.23, 81.62, 649.95, 91.71 Q 650.04, 101.79, 649.81, 111.88 Q 650.95, 121.97, 649.82,\
            132.06 Q 650.96, 142.15, 650.44, 152.24 Q 651.27, 162.32, 652.16, 172.41 Q 652.16, 182.50, 652.26, 192.59 Q 651.47, 202.68,\
            650.99, 212.76 Q 651.86, 222.85, 651.14, 232.94 Q 651.47, 243.03, 651.70, 253.12 Q 651.21, 263.21, 651.17, 273.29 Q 651.30,\
            283.38, 651.68, 293.47 Q 651.50, 303.56, 649.67, 313.65 Q 650.47, 323.74, 650.97, 333.82 Q 651.03, 343.91, 650.91, 354.00\
            Q 650.71, 364.09, 650.91, 374.18 Q 650.99, 384.26, 650.80, 394.35 Q 650.49, 404.44, 650.71, 414.53 Q 650.36, 424.62, 650.86,\
            434.71 Q 650.79, 444.79, 650.67, 454.88 Q 651.26, 464.97, 650.69, 475.06 Q 651.01, 485.15, 650.84, 495.24 Q 650.48, 505.32,\
            650.65, 515.41 Q 651.29, 525.50, 651.49, 535.59 Q 651.61, 545.68, 651.64, 555.76 Q 651.78, 565.85, 651.51, 575.94 Q 650.84,\
            586.03, 650.30, 596.12 Q 650.03, 606.21, 650.35, 616.29 Q 650.41, 626.38, 650.13, 636.47 Q 650.71, 646.56, 650.25, 656.65\
            Q 650.46, 666.74, 650.04, 676.82 Q 650.01, 686.91, 650.74, 697.00 Q 650.75, 707.09, 650.46, 717.18 Q 650.66, 727.27, 650.63,\
            737.35 Q 650.11, 747.44, 650.19, 757.53 Q 651.07, 767.62, 650.98, 777.71 Q 651.26, 787.79, 651.35, 797.88 Q 651.12, 807.97,\
            650.86, 818.06 Q 651.74, 828.15, 651.92, 838.24 Q 651.20, 848.32, 650.33, 858.41 Q 650.85, 868.50, 652.09, 878.59 Q 652.34,\
            888.68, 650.54, 898.77 Q 650.53, 908.85, 650.98, 918.94 Q 650.60, 929.03, 651.40, 939.12 Q 651.35, 949.21, 651.75, 959.29\
            Q 651.92, 969.38, 651.47, 979.47 Q 650.82, 989.56, 650.68, 999.65 Q 650.41, 1009.74, 649.86, 1019.82 Q 650.33, 1029.91, 650.49,\
            1040.49 Q 640.02, 1040.57, 629.58, 1039.37 Q 619.54, 1040.60, 609.34, 1040.24 Q 599.18, 1041.15, 589.00, 1040.64 Q 578.83,\
            1039.90, 568.67, 1040.50 Q 558.50, 1041.64, 548.33, 1041.21 Q 538.17, 1041.90, 528.00, 1041.86 Q 517.83, 1041.91, 507.67,\
            1041.52 Q 497.50, 1040.74, 487.33, 1040.70 Q 477.17, 1039.07, 467.00, 1037.65 Q 456.83, 1039.03, 446.67, 1039.43 Q 436.50,\
            1040.71, 426.33, 1040.80 Q 416.17, 1040.60, 406.00, 1040.58 Q 395.83, 1040.57, 385.67, 1041.00 Q 375.50, 1041.41, 365.33,\
            1041.43 Q 355.17, 1041.42, 345.00, 1041.39 Q 334.83, 1040.90, 324.67, 1040.65 Q 314.50, 1040.84, 304.33, 1040.52 Q 294.17,\
            1041.06, 284.00, 1040.21 Q 273.83, 1040.89, 263.67, 1040.69 Q 253.50, 1040.33, 243.33, 1039.87 Q 233.17, 1039.15, 223.00,\
            1040.40 Q 212.83, 1040.17, 202.67, 1040.48 Q 192.50, 1039.75, 182.33, 1040.38 Q 172.17, 1040.59, 162.00, 1040.93 Q 151.83,\
            1040.72, 141.67, 1040.09 Q 131.50, 1040.10, 121.33, 1039.94 Q 111.17, 1039.64, 101.00, 1039.81 Q 90.83, 1039.83, 80.67, 1039.46\
            Q 70.50, 1040.68, 60.33, 1040.00 Q 50.17, 1040.13, 39.56, 1040.44 Q 39.22, 1030.17, 39.53, 1019.89 Q 39.08, 1009.80, 39.15,\
            999.68 Q 39.09, 989.57, 38.91, 979.48 Q 38.75, 969.39, 38.95, 959.30 Q 39.35, 949.21, 39.27, 939.12 Q 39.49, 929.03, 39.56,\
            918.94 Q 39.22, 908.85, 39.50, 898.77 Q 39.81, 888.68, 39.86, 878.59 Q 39.75, 868.50, 39.92, 858.41 Q 40.12, 848.32, 39.72,\
            838.24 Q 40.03, 828.15, 40.23, 818.06 Q 39.27, 807.97, 39.02, 797.88 Q 39.78, 787.79, 39.86, 777.71 Q 40.16, 767.62, 40.11,\
            757.53 Q 40.58, 747.44, 40.56, 737.35 Q 40.75, 727.27, 40.26, 717.18 Q 39.47, 707.09, 39.01, 697.00 Q 39.00, 686.91, 39.05,\
            676.82 Q 39.53, 666.74, 39.42, 656.65 Q 39.12, 646.56, 39.79, 636.47 Q 39.82, 626.38, 39.79, 616.29 Q 41.04, 606.21, 40.74,\
            596.12 Q 40.32, 586.03, 40.10, 575.94 Q 39.37, 565.85, 38.88, 555.76 Q 39.24, 545.68, 39.84, 535.59 Q 40.01, 525.50, 39.78,\
            515.41 Q 39.14, 505.32, 38.97, 495.24 Q 38.90, 485.15, 39.17, 475.06 Q 39.69, 464.97, 39.64, 454.88 Q 39.67, 444.79, 39.16,\
            434.71 Q 39.01, 424.62, 38.84, 414.53 Q 38.70, 404.44, 39.25, 394.35 Q 40.09, 384.26, 40.77, 374.18 Q 40.91, 364.09, 40.44,\
            354.00 Q 38.76, 343.91, 38.08, 333.82 Q 38.03, 323.74, 37.83, 313.65 Q 38.73, 303.56, 40.84, 293.47 Q 41.71, 283.38, 40.71,\
            273.29 Q 39.69, 263.21, 39.95, 253.12 Q 40.36, 243.03, 40.18, 232.94 Q 39.48, 222.85, 39.10, 212.76 Q 39.10, 202.68, 39.64,\
            192.59 Q 40.20, 182.50, 39.04, 172.41 Q 39.44, 162.32, 38.63, 152.24 Q 39.12, 142.15, 39.59, 132.06 Q 38.83, 121.97, 40.23,\
            111.88 Q 40.04, 101.79, 39.89, 91.71 Q 39.32, 81.62, 40.31, 71.53 Q 40.12, 61.44, 40.79, 51.35 Q 40.34, 41.26, 39.98, 31.18\
            Q 40.00, 21.09, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');