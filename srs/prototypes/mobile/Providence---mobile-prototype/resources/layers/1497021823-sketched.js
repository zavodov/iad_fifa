rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1497021823-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1497021823" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1497021823-layer-1971788974" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1971788974" data-review-reference-id="1971788974">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024">\
                     <svg:g width="601" height="1024"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.29, 2.61, 22.59, 2.11 Q 32.88, 2.72, 43.17, 1.33 Q\
                        53.47, 2.66, 63.76, 2.86 Q 74.05, 1.84, 84.34, 0.93 Q 94.64, 1.25, 104.93, 2.90 Q 115.22, 3.56, 125.52, 2.79 Q 135.81, 2.76,\
                        146.10, 1.84 Q 156.40, 1.09, 166.69, 1.33 Q 176.98, 0.65, 187.28, 1.24 Q 197.57, 1.73, 207.86, 2.37 Q 218.16, 2.58, 228.45,\
                        2.75 Q 238.74, 2.31, 249.03, 1.16 Q 259.33, 1.87, 269.62, 1.87 Q 279.91, 2.27, 290.21, 1.50 Q 300.50, 0.97, 310.79, 0.72 Q\
                        321.09, 1.81, 331.38, 1.65 Q 341.67, 0.53, 351.97, 1.63 Q 362.26, 1.36, 372.55, 1.61 Q 382.84, 1.26, 393.14, 1.01 Q 403.43,\
                        1.31, 413.72, 0.79 Q 424.02, 1.11, 434.31, 1.19 Q 444.60, 1.33, 454.90, 1.40 Q 465.19, 1.20, 475.48, 0.96 Q 485.78, 1.14,\
                        496.07, 0.54 Q 506.36, 0.40, 516.65, 2.08 Q 526.95, 2.50, 537.24, 1.87 Q 547.53, 1.86, 557.83, 2.19 Q 568.12, 1.71, 578.41,\
                        1.37 Q 588.71, 0.60, 599.83, 1.17 Q 600.42, 11.53, 600.89, 21.73 Q 599.68, 31.95, 599.19, 41.99 Q 598.04, 52.02, 599.59, 62.00\
                        Q 600.62, 71.99, 601.25, 82.00 Q 600.75, 92.00, 599.74, 102.00 Q 600.00, 112.00, 599.27, 122.00 Q 599.47, 132.00, 599.44,\
                        142.00 Q 599.72, 152.00, 600.11, 162.00 Q 600.52, 172.00, 600.54, 182.00 Q 599.90, 192.00, 599.88, 202.00 Q 600.30, 212.00,\
                        600.93, 222.00 Q 600.88, 232.00, 600.62, 242.00 Q 600.59, 252.00, 600.70, 262.00 Q 600.33, 272.00, 600.51, 282.00 Q 600.95,\
                        292.00, 600.93, 302.00 Q 601.03, 312.00, 600.66, 322.00 Q 600.18, 332.00, 600.47, 342.00 Q 601.07, 352.00, 599.83, 362.00\
                        Q 599.50, 372.00, 600.06, 382.00 Q 600.41, 392.00, 600.07, 402.00 Q 599.18, 412.00, 598.67, 422.00 Q 598.23, 432.00, 598.19,\
                        442.00 Q 598.29, 452.00, 598.40, 462.00 Q 598.78, 472.00, 599.49, 482.00 Q 599.64, 492.00, 600.03, 502.00 Q 599.88, 512.00,\
                        599.55, 522.00 Q 597.95, 532.00, 598.81, 542.00 Q 599.30, 552.00, 599.27, 562.00 Q 599.40, 572.00, 599.25, 582.00 Q 600.06,\
                        592.00, 599.93, 602.00 Q 600.20, 612.00, 600.49, 622.00 Q 599.78, 632.00, 598.86, 642.00 Q 599.86, 652.00, 600.69, 662.00\
                        Q 600.63, 672.00, 599.64, 682.00 Q 599.95, 692.00, 600.29, 702.00 Q 600.68, 712.00, 600.40, 722.00 Q 600.45, 732.00, 600.55,\
                        742.00 Q 601.66, 752.00, 600.40, 762.00 Q 599.84, 772.00, 599.67, 782.00 Q 600.56, 792.00, 600.40, 802.00 Q 600.02, 812.00,\
                        599.36, 822.00 Q 599.46, 832.00, 600.72, 842.00 Q 600.62, 852.00, 600.46, 862.00 Q 600.59, 872.00, 600.73, 882.00 Q 600.83,\
                        892.00, 601.11, 902.00 Q 601.12, 912.00, 601.18, 922.00 Q 601.37, 932.00, 600.55, 942.00 Q 600.71, 952.00, 600.51, 962.00\
                        Q 600.58, 972.00, 600.74, 982.00 Q 600.86, 992.00, 600.89, 1002.00 Q 600.16, 1012.00, 599.81, 1022.82 Q 589.09, 1023.16, 578.56,\
                        1023.01 Q 568.15, 1022.42, 557.85, 1022.70 Q 547.54, 1022.60, 537.25, 1022.56 Q 526.95, 1023.25, 516.66, 1023.27 Q 506.36,\
                        1023.07, 496.07, 1022.48 Q 485.78, 1022.66, 475.48, 1022.62 Q 465.19, 1022.87, 454.90, 1022.77 Q 444.60, 1022.96, 434.31,\
                        1023.34 Q 424.02, 1022.92, 413.72, 1023.26 Q 403.43, 1022.80, 393.14, 1023.76 Q 382.84, 1023.42, 372.55, 1023.20 Q 362.26,\
                        1022.99, 351.97, 1022.96 Q 341.67, 1023.86, 331.38, 1023.74 Q 321.09, 1023.11, 310.79, 1021.71 Q 300.50, 1020.64, 290.21,\
                        1020.95 Q 279.91, 1021.48, 269.62, 1021.98 Q 259.33, 1023.05, 249.03, 1023.23 Q 238.74, 1022.07, 228.45, 1022.77 Q 218.16,\
                        1021.25, 207.86, 1021.91 Q 197.57, 1021.24, 187.28, 1022.38 Q 176.98, 1022.03, 166.69, 1022.15 Q 156.40, 1023.23, 146.10,\
                        1022.82 Q 135.81, 1022.69, 125.52, 1022.66 Q 115.22, 1023.51, 104.93, 1022.14 Q 94.64, 1022.23, 84.34, 1021.11 Q 74.05, 1021.24,\
                        63.76, 1021.51 Q 53.47, 1021.02, 43.17, 1021.78 Q 32.88, 1021.88, 22.59, 1022.92 Q 12.29, 1022.50, 1.53, 1022.47 Q 1.54, 1012.15,\
                        0.65, 1002.19 Q 1.32, 992.05, 2.54, 981.98 Q 2.48, 971.99, 1.75, 962.00 Q 0.44, 952.01, 0.81, 942.00 Q 0.30, 932.00, 0.76,\
                        922.00 Q 1.37, 912.00, 0.56, 902.00 Q 1.34, 892.00, 1.77, 882.00 Q 1.64, 872.00, 0.65, 862.00 Q 1.17, 852.00, 0.28, 842.00\
                        Q 0.75, 832.00, 0.38, 822.00 Q 0.35, 812.00, 0.58, 802.00 Q 0.20, 792.00, 0.39, 782.00 Q 0.07, 772.00, 1.22, 762.00 Q 0.70,\
                        752.00, 0.80, 742.00 Q 0.76, 732.00, 0.59, 722.00 Q 0.49, 712.00, 0.30, 702.00 Q 0.11, 692.00, 0.10, 682.00 Q 0.97, 672.00,\
                        0.17, 662.00 Q 0.35, 652.00, 0.92, 642.00 Q 0.79, 632.00, 0.56, 622.00 Q 0.47, 612.00, 0.86, 602.00 Q 0.93, 592.00, 1.39,\
                        582.00 Q 0.90, 572.00, 1.41, 562.00 Q 0.95, 552.00, -0.32, 542.00 Q -0.07, 532.00, 1.03, 522.00 Q 0.82, 512.00, 0.43, 502.00\
                        Q 1.17, 492.00, 0.56, 482.00 Q 1.08, 472.00, 0.40, 462.00 Q 0.52, 452.00, 0.96, 442.00 Q 1.54, 432.00, 1.61, 422.00 Q 1.49,\
                        412.00, 1.69, 402.00 Q 1.73, 392.00, 1.46, 382.00 Q 0.98, 372.00, 0.42, 362.00 Q 0.06, 352.00, 1.06, 342.00 Q 1.35, 332.00,\
                        1.43, 322.00 Q 1.39, 312.00, 1.05, 302.00 Q 0.70, 292.00, 0.67, 282.00 Q 0.62, 272.00, 0.47, 262.00 Q 0.81, 252.00, 0.71,\
                        242.00 Q 0.46, 232.00, 0.76, 222.00 Q 0.31, 212.00, 0.24, 202.00 Q 0.02, 192.00, -0.06, 182.00 Q -0.23, 172.00, 1.50, 162.00\
                        Q 1.30, 152.00, 1.45, 142.00 Q 1.03, 132.00, 1.04, 122.00 Q 0.98, 112.00, 0.86, 102.00 Q 0.69, 92.00, 0.83, 82.00 Q 1.15,\
                        72.00, 1.29, 62.00 Q 1.18, 52.00, 1.41, 42.00 Q 1.22, 32.00, 1.65, 22.00 Q 2.00, 12.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 8.58, 9.75, 13.04, 18.75 Q 17.54, 27.72, 23.26, 35.98 Q 29.24,\
                        44.09, 33.48, 53.21 Q 38.75, 61.73, 43.92, 70.31 Q 49.02, 78.93, 54.43, 87.37 Q 59.08, 96.25, 63.50, 105.27 Q 68.65, 113.86,\
                        74.48, 122.05 Q 79.90, 130.49, 84.76, 139.24 Q 89.42, 148.12, 94.39, 156.82 Q 99.45, 165.47, 104.66, 174.02 Q 109.67, 182.69,\
                        114.84, 191.27 Q 120.02, 199.84, 124.84, 208.63 Q 129.94, 217.25, 135.02, 225.88 Q 140.18, 234.47, 145.52, 242.95 Q 150.57,\
                        251.60, 155.55, 260.28 Q 160.42, 269.04, 165.32, 277.78 Q 169.67, 286.84, 175.12, 295.25 Q 179.54, 304.27, 185.18, 312.58\
                        Q 190.40, 321.12, 195.28, 329.87 Q 200.38, 338.49, 205.19, 347.28 Q 210.35, 355.87, 215.43, 364.50 Q 220.64, 373.06, 225.73,\
                        381.68 Q 230.78, 390.33, 235.98, 398.90 Q 241.06, 407.53, 245.54, 416.51 Q 250.30, 425.33, 255.46, 433.91 Q 260.50, 442.57,\
                        265.44, 451.28 Q 270.75, 459.78, 276.14, 468.23 Q 281.52, 476.69, 286.94, 485.12 Q 291.63, 493.98, 296.50, 502.73 Q 301.77,\
                        511.26, 306.38, 520.16 Q 311.75, 528.62, 316.37, 537.53 Q 321.83, 545.93, 326.45, 554.84 Q 330.75, 563.93, 335.77, 572.59\
                        Q 340.32, 581.54, 346.61, 589.46 Q 351.66, 598.11, 356.49, 606.89 Q 361.84, 615.36, 367.71, 623.53 Q 373.21, 631.92, 378.24,\
                        640.58 Q 382.72, 649.56, 387.31, 658.48 Q 392.30, 667.16, 397.36, 675.81 Q 402.15, 684.61, 407.43, 693.12 Q 412.93, 701.51,\
                        418.11, 710.08 Q 422.25, 719.26, 427.90, 727.56 Q 432.72, 736.35, 438.33, 744.67 Q 443.46, 753.27, 448.48, 761.94 Q 453.16,\
                        770.81, 458.56, 779.25 Q 463.36, 788.04, 468.61, 796.58 Q 473.97, 805.05, 478.86, 813.79 Q 483.71, 822.56, 488.13, 831.58\
                        Q 493.50, 840.04, 497.61, 849.24 Q 502.62, 857.91, 507.57, 866.62 Q 513.39, 874.82, 518.34, 883.52 Q 522.93, 892.44, 527.86,\
                        901.16 Q 533.01, 909.75, 538.61, 918.08 Q 543.34, 926.92, 549.06, 935.18 Q 554.33, 943.70, 559.48, 952.29 Q 564.45, 960.98,\
                        569.65, 969.55 Q 574.74, 978.17, 579.37, 987.07 Q 584.71, 995.55, 589.41, 1004.40 Q 593.94, 1013.35, 599.00, 1022.00" style="\
                        fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 1022.00 Q 6.41, 1012.96, 11.34, 1004.23 Q 16.80, 995.81, 21.57, 986.99\
                        Q 26.59, 978.32, 32.17, 969.96 Q 37.55, 961.50, 41.90, 952.43 Q 47.35, 944.00, 51.86, 935.03 Q 57.68, 926.82, 62.69, 918.14\
                        Q 68.14, 909.71, 73.48, 901.22 Q 78.39, 892.48, 82.78, 883.44 Q 88.05, 874.91, 93.37, 866.40 Q 98.38, 857.72, 102.96, 848.79\
                        Q 108.34, 840.32, 113.65, 831.82 Q 118.67, 823.14, 122.85, 813.97 Q 127.66, 805.16, 132.94, 796.64 Q 139.36, 788.79, 143.87,\
                        779.81 Q 149.03, 771.21, 154.25, 762.65 Q 159.25, 753.96, 163.80, 745.01 Q 168.97, 736.42, 173.87, 727.68 Q 178.86, 718.98,\
                        184.09, 710.43 Q 188.95, 701.66, 194.72, 693.42 Q 199.66, 684.70, 205.05, 676.24 Q 210.16, 667.61, 215.03, 658.85 Q 220.21,\
                        650.26, 224.34, 641.07 Q 229.79, 632.64, 234.43, 623.74 Q 239.84, 615.29, 244.42, 606.36 Q 249.97, 597.99, 255.12, 589.39\
                        Q 259.89, 580.57, 265.10, 572.00 Q 270.59, 563.60, 276.79, 555.62 Q 280.77, 546.33, 285.42, 537.44 Q 290.61, 528.85, 296.59,\
                        520.75 Q 301.60, 512.06, 306.27, 503.18 Q 311.49, 494.62, 316.30, 485.82 Q 321.51, 477.25, 325.97, 468.25 Q 331.48, 459.86,\
                        336.69, 451.29 Q 341.83, 442.69, 346.51, 433.81 Q 351.21, 424.94, 356.29, 416.30 Q 361.41, 407.69, 366.59, 399.10 Q 371.13,\
                        390.14, 376.63, 381.75 Q 382.02, 373.28, 388.04, 365.19 Q 393.24, 356.62, 397.92, 347.74 Q 402.98, 339.10, 408.73, 330.84\
                        Q 413.96, 322.29, 418.88, 313.55 Q 424.27, 305.10, 428.71, 296.08 Q 433.55, 287.30, 439.26, 279.02 Q 443.86, 270.10, 448.01,\
                        260.91 Q 453.60, 252.57, 458.69, 243.94 Q 463.50, 235.13, 468.11, 226.21 Q 473.05, 217.49, 478.67, 209.17 Q 484.06, 200.71,\
                        488.34, 191.60 Q 493.19, 182.82, 499.35, 174.81 Q 505.03, 166.52, 509.57, 157.56 Q 514.36, 148.75, 519.74, 140.28 Q 524.49,\
                        131.45, 528.98, 122.46 Q 534.17, 113.88, 539.05, 105.12 Q 544.66, 96.79, 550.42, 88.55 Q 554.46, 79.30, 559.10, 70.39 Q 564.79,\
                        62.11, 570.49, 53.83 Q 574.99, 44.85, 580.63, 36.54 Q 585.99, 28.06, 591.36, 19.59 Q 595.92, 10.65, 601.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1942985619" style="position: absolute; left: 0px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1942985619" data-review-reference-id="1942985619">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1942985619-1853931572" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1853931572" data-review-reference-id="1853931572">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.65, 71.50, 5.34, 61.00 Q 5.25, 50.50, 5.13, 40.00 Q 5.14, 29.50,\
                                 6.01, 19.00 Q 5.49, 17.50, 5.37, 15.62 Q 5.55, 14.29, 6.26, 13.25 Q 6.88, 12.25, 8.40, 11.42 Q 8.94, 10.03, 10.23, 9.72 Q\
                                 11.60, 9.78, 12.44, 8.72 Q 13.94, 8.04, 15.63, 7.01 Q 29.00, 7.13, 42.25, 7.05 Q 55.47, 7.67, 68.65, 7.91 Q 81.83, 8.50, 95.03,\
                                 8.84 Q 96.65, 8.89, 98.52, 8.58 Q 99.74, 8.79, 100.50, 9.92 Q 101.34, 10.79, 102.24, 11.76 Q 103.65, 12.17, 103.68, 13.59\
                                 Q 103.96, 14.75, 105.01, 15.56 Q 106.05, 16.91, 106.63, 18.70 Q 106.46, 29.37, 106.12, 39.95 Q 105.98, 50.48, 106.28, 60.99\
                                 Q 106.04, 71.49, 105.16, 82.16 Q 95.05, 82.14, 85.09, 82.64 Q 75.08, 83.17, 65.03, 82.98 Q 55.02, 83.24, 45.01, 83.43 Q 35.01,\
                                 83.45, 25.00, 83.58 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1853931572\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1853931572\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1853931572_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1853931572_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">RFPL\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-165592102" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="165592102" data-review-reference-id="165592102">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.90, 71.50, 7.27, 61.00 Q 6.96, 50.50, 7.57, 40.00 Q 7.87, 29.50,\
                                 7.10, 19.00 Q 6.20, 17.50, 6.45, 15.87 Q 8.01, 15.18, 7.83, 13.93 Q 8.74, 13.11, 8.96, 11.96 Q 9.74, 11.14, 10.71, 10.52 Q\
                                 11.71, 9.97, 12.71, 9.34 Q 14.52, 9.54, 16.12, 9.66 Q 29.20, 9.39, 42.31, 8.41 Q 55.49, 8.66, 68.67, 9.45 Q 81.84, 9.91, 94.97,\
                                 9.20 Q 96.64, 8.92, 97.96, 10.11 Q 98.81, 10.93, 100.07, 10.84 Q 100.88, 11.75, 102.31, 11.68 Q 102.88, 12.73, 103.44, 13.73\
                                 Q 103.13, 15.20, 103.65, 16.15 Q 104.59, 17.47, 104.38, 19.11 Q 105.86, 29.42, 105.12, 39.99 Q 106.04, 50.48, 105.72, 60.99\
                                 Q 105.33, 71.50, 105.38, 82.38 Q 95.26, 82.77, 85.09, 82.66 Q 75.04, 82.62, 65.02, 82.60 Q 55.01, 82.38, 45.01, 82.72 Q 35.00,\
                                 82.33, 25.00, 83.05 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'165592102\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'165592102\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="165592102_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="165592102_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-468989596" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="468989596" data-review-reference-id="468989596">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.33, 71.50, 6.77, 61.00 Q 6.34, 50.50, 7.01, 40.00 Q 5.80, 29.50,\
                                 5.47, 19.00 Q 5.88, 17.50, 6.07, 15.78 Q 7.45, 14.98, 7.48, 13.78 Q 8.43, 12.97, 8.29, 11.31 Q 9.71, 11.10, 10.40, 10.01 Q\
                                 10.89, 8.47, 12.28, 8.35 Q 14.45, 9.37, 16.07, 9.41 Q 29.11, 8.37, 42.33, 8.98 Q 55.49, 8.71, 68.67, 9.50 Q 81.83, 8.09, 95.20,\
                                 7.77 Q 96.57, 9.20, 98.00, 10.01 Q 98.78, 11.01, 100.28, 10.39 Q 101.77, 9.90, 103.58, 10.40 Q 104.34, 11.68, 105.05, 12.77\
                                 Q 104.87, 14.25, 104.42, 15.81 Q 103.63, 17.83, 104.35, 19.12 Q 105.68, 29.44, 105.45, 39.98 Q 105.68, 50.48, 105.02, 61.00\
                                 Q 105.87, 71.50, 105.70, 82.70 Q 95.22, 82.66, 85.03, 82.20 Q 75.05, 82.81, 65.05, 83.71 Q 55.03, 83.80, 45.01, 83.80 Q 35.01,\
                                 83.43, 25.00, 83.22 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'468989596\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'468989596\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="468989596_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="468989596_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-266350341" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="266350341" data-review-reference-id="266350341">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 5.34, 71.50, 5.52, 61.00 Q 6.00, 50.50, 6.28, 40.00 Q 7.64, 29.50,\
                                 6.81, 19.00 Q 6.84, 17.50, 6.49, 15.88 Q 7.94, 15.16, 8.79, 14.34 Q 8.73, 13.11, 8.77, 11.77 Q 10.03, 11.54, 11.40, 11.66\
                                 Q 12.41, 11.24, 13.04, 10.08 Q 14.45, 9.37, 15.96, 8.80 Q 29.21, 9.45, 42.33, 8.91 Q 55.49, 8.43, 68.67, 8.93 Q 81.83, 8.83,\
                                 95.13, 8.17 Q 96.66, 8.84, 98.06, 9.83 Q 99.00, 10.50, 99.92, 11.18 Q 100.60, 12.33, 101.72, 12.28 Q 103.19, 12.50, 103.36,\
                                 13.78 Q 105.08, 14.13, 104.87, 15.62 Q 106.11, 16.88, 106.71, 18.68 Q 105.88, 29.42, 105.78, 39.96 Q 105.94, 50.48, 105.96,\
                                 60.99 Q 105.28, 71.50, 105.51, 82.51 Q 95.02, 82.05, 85.05, 82.35 Q 75.07, 83.03, 65.03, 83.04 Q 55.02, 83.49, 45.01, 83.80\
                                 Q 35.01, 83.28, 25.00, 82.63 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'266350341\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'266350341\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="266350341_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="266350341_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-2065274472" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="2065274472" data-review-reference-id="2065274472">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 6.66, 71.50, 6.40, 61.00 Q 6.28, 50.50, 5.98, 40.00 Q 5.86, 29.50,\
                                 5.79, 19.00 Q 5.92, 17.50, 5.93, 15.75 Q 6.22, 14.53, 6.65, 13.42 Q 7.14, 12.37, 7.94, 10.97 Q 8.95, 10.05, 10.72, 10.54 Q\
                                 11.41, 9.43, 12.62, 9.12 Q 14.16, 8.62, 15.81, 7.96 Q 29.07, 7.91, 42.28, 7.91 Q 55.47, 7.69, 68.65, 7.37 Q 81.82, 7.23, 95.21,\
                                 7.69 Q 96.76, 8.44, 98.58, 8.43 Q 99.36, 9.68, 99.92, 11.17 Q 101.15, 11.20, 102.20, 11.79 Q 103.43, 12.33, 103.33, 13.80\
                                 Q 103.89, 14.78, 104.69, 15.70 Q 105.48, 17.12, 105.33, 18.94 Q 105.39, 29.46, 105.83, 39.96 Q 105.62, 50.49, 105.86, 60.99\
                                 Q 105.51, 71.50, 105.44, 82.43 Q 94.94, 81.83, 85.12, 82.86 Q 75.05, 82.79, 65.04, 83.11 Q 55.02, 83.25, 45.00, 82.57 Q 35.00,\
                                 81.85, 25.00, 81.76 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'2065274472\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'2065274472\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="2065274472_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="2065274472_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1942985619-1052746445" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1052746445" data-review-reference-id="1052746445">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 91px;width:111px;" width="105" height="86">\
                           <svg:g id="target" width="108" height="80" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 82.00 Q 4.55, 71.50, 4.51, 61.00 Q 5.12, 50.50, 5.01, 40.00 Q 5.20, 29.50,\
                                 5.57, 19.00 Q 5.07, 17.50, 5.43, 15.63 Q 6.64, 14.69, 6.89, 13.52 Q 8.05, 12.79, 8.74, 11.74 Q 9.64, 11.01, 10.39, 9.99 Q\
                                 11.34, 9.31, 12.59, 9.05 Q 14.40, 9.24, 15.89, 8.41 Q 29.01, 7.30, 42.25, 7.05 Q 55.48, 8.05, 68.66, 8.02 Q 81.83, 7.93, 95.18,\
                                 7.89 Q 96.81, 8.25, 98.20, 9.46 Q 99.42, 9.53, 100.45, 10.03 Q 101.49, 10.50, 102.85, 11.14 Q 103.64, 12.18, 104.42, 13.15\
                                 Q 105.25, 14.04, 105.77, 15.23 Q 106.23, 16.84, 106.81, 18.67 Q 106.89, 29.33, 106.91, 39.91 Q 107.05, 50.45, 106.90, 60.98\
                                 Q 105.96, 71.49, 105.04, 82.04 Q 95.20, 82.59, 85.04, 82.26 Q 75.03, 82.51, 65.03, 82.83 Q 55.01, 82.50, 45.01, 82.75 Q 35.00,\
                                 82.77, 25.00, 83.03 Q 15.00, 82.00, 5.00, 82.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1052746445\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1052746445\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1052746445_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 74px;width:104px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:31px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1052746445_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 80px;width:107px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:38px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1506471845" style="position: absolute; left: 0px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1506471845" data-review-reference-id="1506471845">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:600px;" width="600" height="465">\
                     <svg:g width="600" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, -0.14, 22.55, -0.24 Q 32.83, -0.33, 43.10, 0.12 Q 53.38,\
                        -0.02, 63.66, 0.15 Q 73.93, 1.01, 84.21, 1.07 Q 94.48, 0.74, 104.76, 0.13 Q 115.03, 0.69, 125.31, 0.52 Q 135.59, 1.72, 145.86,\
                        2.12 Q 156.14, 1.92, 166.41, 1.89 Q 176.69, 2.20, 186.97, 1.42 Q 197.24, 0.42, 207.52, 0.65 Q 217.79, 1.37, 228.07, 1.68 Q\
                        238.34, 1.73, 248.62, 1.30 Q 258.90, 1.91, 269.17, 2.50 Q 279.45, 2.67, 289.72, 2.28 Q 300.00, 3.64, 310.28, 1.93 Q 320.55,\
                        1.81, 330.83, 1.55 Q 341.10, 1.30, 351.38, 1.70 Q 361.66, 1.81, 371.93, 1.78 Q 382.21, 0.99, 392.48, 0.44 Q 402.76, 0.23,\
                        413.03, 0.03 Q 423.31, 0.46, 433.59, 0.62 Q 443.86, 0.31, 454.14, 0.88 Q 464.41, 0.66, 474.69, 0.65 Q 484.97, 0.62, 495.24,\
                        0.24 Q 505.52, 0.07, 515.79, 1.24 Q 526.07, 0.84, 536.34, 1.13 Q 546.62, 1.45, 556.90, 1.28 Q 567.17, 0.61, 577.45, 0.96 Q\
                        587.72, 1.59, 597.41, 2.59 Q 597.91, 12.05, 599.02, 21.90 Q 599.39, 31.97, 599.03, 42.05 Q 599.15, 52.09, 599.36, 62.12 Q\
                        599.53, 72.15, 599.02, 82.17 Q 599.55, 92.19, 599.63, 102.22 Q 599.80, 112.24, 599.91, 122.26 Q 599.46, 132.28, 599.27, 142.30\
                        Q 598.87, 152.33, 599.05, 162.35 Q 599.38, 172.37, 599.42, 182.39 Q 599.72, 192.41, 599.40, 202.43 Q 599.59, 212.46, 599.12,\
                        222.48 Q 598.46, 232.50, 598.50, 242.52 Q 598.70, 252.54, 598.80, 262.57 Q 599.22, 272.59, 599.02, 282.61 Q 599.28, 292.63,\
                        597.83, 302.65 Q 598.82, 312.67, 599.12, 322.70 Q 598.51, 332.72, 599.40, 342.74 Q 599.05, 352.76, 599.83, 362.78 Q 599.66,\
                        372.80, 599.52, 382.83 Q 599.98, 392.85, 599.89, 402.87 Q 598.37, 412.89, 598.46, 422.91 Q 599.47, 432.93, 599.86, 442.96\
                        Q 599.76, 452.98, 598.91, 463.91 Q 588.23, 464.51, 577.71, 464.84 Q 567.30, 464.97, 556.95, 464.76 Q 546.64, 464.34, 536.36,\
                        464.83 Q 526.08, 465.02, 515.80, 464.76 Q 505.52, 465.20, 495.24, 465.32 Q 484.97, 464.28, 474.69, 463.32 Q 464.41, 463.21,\
                        454.14, 463.98 Q 443.86, 464.22, 433.59, 464.61 Q 423.31, 464.43, 413.03, 464.46 Q 402.76, 463.68, 392.48, 463.55 Q 382.21,\
                        463.48, 371.93, 463.77 Q 361.66, 463.68, 351.38, 464.28 Q 341.10, 464.79, 330.83, 464.45 Q 320.55, 464.64, 310.28, 463.77\
                        Q 300.00, 464.18, 289.72, 464.17 Q 279.45, 464.26, 269.17, 464.37 Q 258.90, 464.55, 248.62, 464.33 Q 238.34, 464.45, 228.07,\
                        464.54 Q 217.79, 464.18, 207.52, 463.72 Q 197.24, 463.24, 186.97, 462.97 Q 176.69, 462.87, 166.41, 462.64 Q 156.14, 462.03,\
                        145.86, 463.31 Q 135.59, 463.42, 125.31, 463.48 Q 115.03, 463.48, 104.76, 463.60 Q 94.48, 463.99, 84.21, 463.71 Q 73.93, 462.89,\
                        63.66, 463.31 Q 53.38, 463.27, 43.10, 464.00 Q 32.83, 463.47, 22.55, 463.42 Q 12.28, 462.79, 2.22, 462.78 Q 2.31, 452.88,\
                        1.61, 443.01 Q 1.23, 432.99, 1.57, 422.93 Q 1.41, 412.90, 1.19, 402.88 Q 0.48, 392.85, 0.61, 382.83 Q 0.81, 372.81, 1.22,\
                        362.78 Q 2.34, 352.76, 1.03, 342.74 Q 1.40, 332.72, 2.53, 322.70 Q 2.29, 312.67, 1.22, 302.65 Q 0.39, 292.63, 0.42, 282.61\
                        Q 1.08, 272.59, 1.62, 262.57 Q 2.32, 252.54, 0.46, 242.52 Q -0.03, 232.50, 0.85, 222.48 Q 0.98, 212.46, -0.41, 202.43 Q 0.06,\
                        192.41, 0.30, 182.39 Q 1.47, 172.37, 0.98, 162.35 Q 1.22, 152.33, -0.24, 142.30 Q -0.56, 132.28, -0.30, 122.26 Q 1.13, 112.24,\
                        1.75, 102.22 Q 2.15, 92.20, 2.19, 82.17 Q 1.11, 72.15, 0.93, 62.13 Q 0.11, 52.11, 0.47, 42.09 Q 1.19, 32.07, 0.83, 22.04 Q\
                        2.00, 12.02, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1561898673" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1561898673" data-review-reference-id="1561898673">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:599px;" width="599" height="80">\
                     <svg:g width="599" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.26, 1.03, 22.52, 1.66 Q 32.78, 1.35, 43.03, 2.51 Q 53.29, 1.86,\
                        63.55, 1.51 Q 73.81, 1.64, 84.07, 2.82 Q 94.33, 2.76, 104.59, 2.38 Q 114.84, 2.64, 125.10, 2.85 Q 135.36, 3.49, 145.62, 1.75\
                        Q 155.88, 1.88, 166.14, 2.03 Q 176.40, 2.10, 186.66, 1.66 Q 196.91, 1.59, 207.17, 2.62 Q 217.43, 2.74, 227.69, 3.31 Q 237.95,\
                        2.53, 248.21, 2.62 Q 258.47, 2.73, 268.72, 2.88 Q 278.98, 2.83, 289.24, 1.23 Q 299.50, 1.29, 309.76, 1.51 Q 320.02, 1.33,\
                        330.28, 0.77 Q 340.53, 0.51, 350.79, 0.26 Q 361.05, 0.33, 371.31, 0.08 Q 381.57, -0.09, 391.83, 0.03 Q 402.09, 0.33, 412.34,\
                        1.37 Q 422.60, 0.99, 432.86, 1.56 Q 443.12, 1.80, 453.38, 2.44 Q 463.64, 3.02, 473.90, 2.05 Q 484.15, 2.18, 494.41, 2.28 Q\
                        504.67, 2.10, 514.93, 1.64 Q 525.19, 1.40, 535.45, 2.00 Q 545.71, 2.44, 555.97, 2.31 Q 566.22, 1.56, 576.48, 1.07 Q 586.74,\
                        1.26, 597.46, 1.54 Q 597.97, 14.34, 598.24, 27.16 Q 598.36, 39.91, 598.51, 52.62 Q 598.56, 65.31, 597.85, 78.85 Q 587.16,\
                        79.26, 576.73, 79.74 Q 566.34, 79.76, 556.03, 80.00 Q 545.73, 79.38, 535.46, 79.15 Q 525.19, 79.32, 514.93, 79.07 Q 504.67,\
                        79.43, 494.41, 79.18 Q 484.16, 79.28, 473.90, 79.45 Q 463.64, 79.38, 453.38, 79.47 Q 443.12, 78.96, 432.86, 78.26 Q 422.60,\
                        78.62, 412.34, 79.22 Q 402.09, 78.96, 391.83, 78.46 Q 381.57, 78.27, 371.31, 78.24 Q 361.05, 78.88, 350.79, 78.59 Q 340.53,\
                        77.18, 330.28, 77.60 Q 320.02, 77.45, 309.76, 78.10 Q 299.50, 77.85, 289.24, 77.33 Q 278.98, 78.18, 268.72, 78.62 Q 258.47,\
                        79.87, 248.21, 79.84 Q 237.95, 79.76, 227.69, 79.88 Q 217.43, 79.56, 207.17, 79.33 Q 196.91, 79.19, 186.66, 78.72 Q 176.40,\
                        79.48, 166.14, 78.49 Q 155.88, 78.19, 145.62, 79.01 Q 135.36, 79.50, 125.10, 79.34 Q 114.84, 79.20, 104.59, 79.25 Q 94.33,\
                        78.43, 84.07, 77.81 Q 73.81, 78.19, 63.55, 78.41 Q 53.29, 78.98, 43.03, 79.09 Q 32.78, 79.47, 22.52, 79.29 Q 12.26, 79.16,\
                        1.61, 78.39 Q 1.23, 65.59, 1.38, 52.75 Q 0.91, 40.07, 0.77, 27.37 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1877215721" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1877215721" data-review-reference-id="1877215721">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:600px;" width="600" height="80">\
                     <svg:g width="600" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 2.94, 22.55, 2.37 Q 32.83, 1.76, 43.10, 1.10 Q 53.38, 1.42,\
                        63.66, 0.93 Q 73.93, 1.02, 84.21, 1.10 Q 94.48, 1.43, 104.76, 2.20 Q 115.03, 1.96, 125.31, 1.05 Q 135.59, 2.25, 145.86, 1.17\
                        Q 156.14, 1.52, 166.41, 0.85 Q 176.69, 0.76, 186.97, 1.35 Q 197.24, 1.62, 207.52, 1.68 Q 217.79, 1.84, 228.07, 1.35 Q 238.34,\
                        0.65, 248.62, 0.19 Q 258.90, 0.95, 269.17, 1.89 Q 279.45, 0.67, 289.72, 0.84 Q 300.00, 0.93, 310.28, 1.14 Q 320.55, 0.97,\
                        330.83, 0.16 Q 341.10, 0.47, 351.38, -0.15 Q 361.66, 0.36, 371.93, 0.19 Q 382.21, 0.90, 392.48, 0.28 Q 402.76, 1.35, 413.03,\
                        1.09 Q 423.31, 0.06, 433.59, 0.81 Q 443.86, 0.33, 454.14, 0.51 Q 464.41, 0.45, 474.69, 1.01 Q 484.97, 0.64, 495.24, 0.57 Q\
                        505.52, 0.57, 515.79, 0.59 Q 526.07, 1.93, 536.34, 1.82 Q 546.62, 1.30, 556.90, 0.55 Q 567.17, 1.22, 577.45, 1.31 Q 587.72,\
                        1.27, 598.49, 1.51 Q 599.01, 14.33, 599.01, 27.19 Q 598.44, 39.97, 598.81, 52.64 Q 599.20, 65.31, 598.55, 78.55 Q 587.78,\
                        78.17, 577.52, 78.53 Q 567.22, 78.70, 556.93, 79.05 Q 546.64, 79.02, 536.35, 79.13 Q 526.07, 78.81, 515.80, 79.64 Q 505.52,\
                        79.48, 495.24, 78.46 Q 484.97, 78.30, 474.69, 79.13 Q 464.41, 79.02, 454.14, 78.02 Q 443.86, 78.49, 433.59, 78.47 Q 423.31,\
                        78.95, 413.03, 79.43 Q 402.76, 78.31, 392.48, 77.90 Q 382.21, 77.94, 371.93, 78.44 Q 361.66, 78.74, 351.38, 78.21 Q 341.10,\
                        78.77, 330.83, 78.65 Q 320.55, 78.14, 310.28, 77.87 Q 300.00, 78.13, 289.72, 78.67 Q 279.45, 79.16, 269.17, 78.36 Q 258.90,\
                        77.90, 248.62, 78.52 Q 238.34, 79.02, 228.07, 79.50 Q 217.79, 79.39, 207.52, 78.38 Q 197.24, 79.05, 186.97, 79.48 Q 176.69,\
                        78.16, 166.41, 78.83 Q 156.14, 78.93, 145.86, 79.86 Q 135.59, 79.77, 125.31, 79.53 Q 115.03, 79.59, 104.76, 79.90 Q 94.48,\
                        79.59, 84.21, 79.62 Q 73.93, 79.76, 63.66, 79.78 Q 53.38, 79.95, 43.10, 79.45 Q 32.83, 79.18, 22.55, 79.59 Q 12.28, 79.54,\
                        1.34, 78.66 Q 1.39, 65.54, 0.77, 52.84 Q 0.98, 40.07, 1.95, 27.34 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-648635862" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="648635862" data-review-reference-id="648635862">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60">\
                     <svg:g width="200" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.89, -0.26, 23.78, 0.05 Q 34.67, 0.14, 45.56, 0.56 Q\
                        56.44, 0.56, 67.33, 0.62 Q 78.22, 1.41, 89.11, 1.42 Q 100.00, 1.00, 110.89, 1.28 Q 121.78, 1.37, 132.67, 1.14 Q 143.56, 1.43,\
                        154.44, 2.29 Q 165.33, 2.26, 176.22, 2.02 Q 187.11, 1.56, 198.70, 1.30 Q 198.83, 15.72, 199.04, 29.85 Q 199.15, 43.92, 198.46,\
                        58.46 Q 187.29, 58.55, 176.33, 58.80 Q 165.39, 58.83, 154.46, 58.52 Q 143.57, 58.74, 132.66, 56.64 Q 121.77, 56.96, 110.89,\
                        57.12 Q 100.00, 57.85, 89.11, 57.49 Q 78.22, 57.39, 67.33, 58.12 Q 56.44, 58.97, 45.56, 58.92 Q 34.67, 58.85, 23.78, 58.80\
                        Q 12.89, 59.05, 1.44, 58.56 Q 1.08, 44.31, 0.85, 30.16 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.50, 2.35, 22.25, 5.33 Q 32.11, 7.91, 41.79, 11.14 Q 51.40,\
                        14.62, 61.45, 16.51 Q 71.26, 19.28, 80.87, 22.76 Q 90.38, 26.59, 100.11, 29.63 Q 110.13, 31.64, 120.07, 33.96 Q 129.94, 36.51,\
                        139.73, 39.34 Q 149.49, 42.29, 159.03, 46.01 Q 168.85, 48.71, 178.56, 51.84 Q 188.20, 55.20, 198.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.61, 54.17, 21.64, 51.85 Q 31.79, 49.91, 41.68, 47.08 Q 51.94,\
                        45.55, 61.70, 42.26 Q 71.45, 38.93, 81.28, 35.87 Q 91.49, 34.17, 101.44, 31.54 Q 110.93, 27.32, 120.88, 24.67 Q 130.75, 21.78,\
                        140.96, 20.08 Q 150.62, 16.41, 160.52, 13.62 Q 170.51, 11.15, 180.43, 8.40 Q 190.10, 4.80, 200.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1061433121" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1061433121" data-review-reference-id="1061433121">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-1795695749" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1795695749" data-review-reference-id="1795695749">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 80px;width:86px;" width="86" height="80">\
                     <svg:g width="86" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 15.17, 2.40, 28.33, 2.09 Q 41.50, 1.95, 54.67, 2.00 Q 67.83, 2.22,\
                        81.10, 1.90 Q 81.12, 14.13, 81.43, 26.27 Q 81.71, 38.45, 81.09, 50.66 Q 81.53, 62.83, 81.33, 75.33 Q 68.01, 75.53, 54.70,\
                        75.22 Q 41.49, 74.86, 28.37, 76.04 Q 15.17, 75.52, 1.34, 75.66 Q 1.64, 62.95, 1.79, 50.70 Q 1.54, 38.53, 1.90, 26.34 Q 2.00,\
                        14.17, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 82.00, 4.00 Q 81.39, 16.33, 80.66, 28.67 Q 80.33, 41.00, 80.90, 53.33 Q 82.00,\
                        65.67, 82.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 83.00, 5.00 Q 84.00, 17.33, 83.43, 29.67 Q 83.72, 42.00, 82.75, 54.33 Q 83.00,\
                        66.67, 83.00, 79.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 84.00, 6.00 Q 84.17, 18.33, 83.58, 30.67 Q 83.76, 43.00, 83.82, 55.33 Q 84.00,\
                        67.67, 84.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 76.00 Q 14.00, 74.86, 24.00, 75.16 Q 34.00, 75.43, 44.00, 75.70 Q 54.00,\
                        76.27, 64.00, 75.41 Q 74.00, 76.00, 84.00, 76.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 77.00 Q 15.00, 76.78, 25.00, 76.87 Q 35.00, 76.83, 45.00, 77.48 Q 55.00,\
                        77.48, 65.00, 77.46 Q 75.00, 77.00, 85.00, 77.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 78.00 Q 16.00, 76.41, 26.00, 76.40 Q 36.00, 76.71, 46.00, 76.56 Q 56.00,\
                        76.80, 66.00, 77.04 Q 76.00, 78.00, 86.00, 78.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-1497021823-layer-1795695749button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-1497021823-layer-1795695749button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-1497021823-layer-1795695749button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:82px;height:76px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				=<br />  \
                     			</button></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-538977365" style="position: absolute; left: 417px; top: 20px; width: 174px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="538977365" data-review-reference-id="538977365">\
            <div class="stencil-wrapper" style="width: 174px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Статистика</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1497021823-layer-656263660" style="position: absolute; left: 0px; top: 355px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="656263660" data-review-reference-id="656263660">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-1497021823-layer-656263660-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:600px;" width="600" height="395">\
                     <svg:g id="__containerId__-1497021823-layer-656263660svg" width="600" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.20, 22.55, 1.45 Q 32.83, 2.02, 43.10, 1.73 Q 53.38, 1.47,\
                        63.66, 2.04 Q 73.93, 1.43, 84.21, 1.80 Q 94.48, 1.66, 104.76, 1.71 Q 115.03, 2.16, 125.31, 1.92 Q 135.59, 1.69, 145.86, 0.36\
                        Q 156.14, 1.30, 166.41, 1.39 Q 176.69, 1.78, 186.97, 1.58 Q 197.24, 1.66, 207.52, 2.03 Q 217.79, 2.63, 228.07, 1.88 Q 238.34,\
                        0.54, 248.62, 0.57 Q 258.90, 0.31, 269.17, 0.95 Q 279.45, 1.39, 289.72, 0.22 Q 300.00, 1.10, 310.28, 1.42 Q 320.55, 1.96,\
                        330.83, 0.83 Q 341.10, 0.94, 351.38, 1.28 Q 361.66, 2.49, 371.93, 1.95 Q 382.21, 2.13, 392.48, 2.04 Q 402.76, 3.03, 413.03,\
                        2.62 Q 423.31, 2.15, 433.59, 2.66 Q 443.86, 1.96, 454.14, 2.40 Q 464.41, 1.62, 474.69, 1.01 Q 484.97, 1.00, 495.24, 1.30 Q\
                        505.52, 1.13, 515.79, 0.61 Q 526.07, 1.08, 536.34, 1.15 Q 546.62, 2.44, 556.90, 1.52 Q 567.17, 0.11, 577.45, -0.21 Q 587.72,\
                        1.27, 598.55, 1.45 Q 598.86, 12.00, 599.24, 22.40 Q 599.59, 32.76, 598.53, 43.14 Q 598.77, 53.44, 598.76, 63.73 Q 598.51,\
                        74.02, 598.94, 84.31 Q 599.44, 94.60, 599.27, 104.89 Q 598.82, 115.18, 598.58, 125.47 Q 597.72, 135.76, 598.62, 146.05 Q 598.11,\
                        156.34, 598.34, 166.63 Q 598.65, 176.92, 598.77, 187.21 Q 597.52, 197.50, 597.19, 207.79 Q 598.56, 218.08, 598.10, 228.37\
                        Q 598.72, 238.66, 598.92, 248.95 Q 598.59, 259.24, 598.13, 269.53 Q 598.56, 279.82, 599.45, 290.11 Q 599.55, 300.39, 598.88,\
                        310.68 Q 597.90, 320.97, 598.66, 331.26 Q 599.40, 341.55, 599.02, 351.84 Q 598.85, 362.13, 599.53, 372.42 Q 599.64, 382.71,\
                        598.75, 393.75 Q 588.07, 394.05, 577.62, 394.20 Q 567.22, 393.79, 556.93, 393.97 Q 546.63, 393.83, 536.35, 393.58 Q 526.07,\
                        394.19, 515.80, 394.68 Q 505.52, 393.73, 495.24, 393.05 Q 484.97, 392.82, 474.69, 394.08 Q 464.41, 394.41, 454.14, 394.88\
                        Q 443.86, 394.96, 433.59, 395.08 Q 423.31, 393.82, 413.03, 393.34 Q 402.76, 393.90, 392.48, 394.21 Q 382.21, 394.50, 371.93,\
                        394.68 Q 361.66, 394.64, 351.38, 395.00 Q 341.10, 394.91, 330.83, 395.05 Q 320.55, 394.18, 310.28, 392.87 Q 300.00, 392.94,\
                        289.72, 393.38 Q 279.45, 393.69, 269.17, 393.91 Q 258.90, 393.54, 248.62, 394.06 Q 238.34, 394.47, 228.07, 394.21 Q 217.79,\
                        394.52, 207.52, 393.68 Q 197.24, 393.79, 186.97, 393.47 Q 176.69, 393.59, 166.41, 393.66 Q 156.14, 393.88, 145.86, 394.09\
                        Q 135.59, 393.70, 125.31, 392.72 Q 115.03, 391.82, 104.76, 393.27 Q 94.48, 394.83, 84.21, 393.00 Q 73.93, 393.44, 63.66, 394.24\
                        Q 53.38, 395.05, 43.10, 395.12 Q 32.83, 394.75, 22.55, 395.24 Q 12.28, 393.97, 1.82, 393.18 Q 2.13, 382.67, 2.16, 372.40 Q\
                        1.15, 362.19, 0.50, 351.89 Q 0.49, 341.58, 1.68, 331.27 Q 1.28, 320.98, 0.66, 310.69 Q 0.37, 300.40, 0.55, 290.11 Q 0.81,\
                        279.82, 0.76, 269.53 Q 0.68, 259.24, 0.72, 248.95 Q 0.68, 238.66, 0.63, 228.37 Q 0.51, 218.08, 0.62, 207.79 Q 0.62, 197.50,\
                        0.87, 187.21 Q 0.71, 176.92, 0.43, 166.63 Q 1.27, 156.34, 1.52, 146.05 Q 0.89, 135.76, 1.07, 125.47 Q 0.42, 115.18, 0.85,\
                        104.89 Q 0.99, 94.61, 0.90, 84.32 Q 0.62, 74.03, 0.77, 63.74 Q 0.75, 53.45, -0.09, 43.16 Q 0.33, 32.87, 0.45, 22.58 Q 2.00,\
                        12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 596px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-1497021823-layer-656263660-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:590px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-1497021823-layer-656263660-accordion", "596", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-1497021823-layer-200451967" style="position: absolute; left: 5px; top: 390px; width: 595px; height: 265px" data-interactive-element-type="static.chart" class="chart stencil mobile-interaction-potential-trigger " data-stencil-id="200451967" data-review-reference-id="200451967">\
            <div class="stencil-wrapper" style="width: 595px; height: 265px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 265px;width:595px;" viewBox="0 0 595 265" width="595" height="265">\
                     <svg:g width="595" height="265">\
                        <svg:g transform="scale(3.3055555555555554, 1.7666666666666666)"><svg:path id="defaultID" class=" svg_unselected_element" d="M 0.00, 0.00 Q 10.00, -2.70, 20.00, -2.31 Q 30.00, -1.93, 40.00,\
                           -1.81 Q 50.00, -1.60, 60.00, -1.33 Q 70.00, -0.63, 80.00, -0.85 Q 90.00, -0.53, 100.00, -0.90 Q 110.00, -1.16, 120.00, -1.36\
                           Q 130.00, -1.09, 140.00, -0.70 Q 150.00, -0.90, 160.00, -1.16 Q 170.00, -0.93, 180.16, -0.16 Q 180.62, 10.51, 180.80, 21.31\
                           Q 181.10, 32.07, 180.60, 42.84 Q 181.02, 53.56, 181.13, 64.28 Q 181.04, 75.00, 180.86, 85.71 Q 180.18, 96.43, 180.45, 107.14\
                           Q 180.97, 117.86, 180.37, 128.57 Q 180.99, 139.29, 180.42, 150.42 Q 170.18, 150.54, 160.05, 150.36 Q 150.00, 149.94, 139.99,\
                           149.69 Q 130.00, 150.09, 120.00, 149.89 Q 110.00, 150.90, 100.00, 149.83 Q 90.00, 150.49, 80.00, 150.86 Q 70.00, 150.68, 60.00,\
                           149.89 Q 50.00, 149.42, 40.00, 150.67 Q 30.00, 151.37, 20.00, 151.16 Q 10.00, 150.74, -0.55, 150.55 Q -0.79, 139.55, -0.75,\
                           128.68 Q -0.40, 117.88, -0.54, 107.16 Q -0.67, 96.44, -1.07, 85.72 Q -0.91, 75.00, -1.78, 64.29 Q -1.48, 53.57, -0.21, 42.86\
                           Q -1.01, 32.14, -0.96, 21.43 Q 0.00, 10.71, 0.00, -0.00" style="fill:white;stroke:none;"/>\
                           <svg:g name="line" style="fill:none;stroke:black;stroke-width:1px;"><svg:path class=" svg_unselected_element" d="M 9.00, 3.00 Q 10.84, 14.42, 10.56, 25.83 Q 10.60, 37.25, 10.55, 48.67 Q 10.91,\
                              60.08, 11.37, 71.50 Q 10.18, 82.92, 11.40, 94.33 Q 10.39, 105.75, 10.79, 117.17 Q 10.93, 128.58, 10.29, 138.71 Q 20.64, 139.14,\
                              31.76, 139.69 Q 43.13, 139.16, 54.51, 137.50 Q 65.81, 138.65, 77.15, 138.98 Q 88.50, 138.97, 99.86, 138.49 Q 111.22, 138.18,\
                              122.57, 138.71 Q 133.93, 137.86, 145.29, 137.69 Q 156.64, 138.20, 168.00, 139.55 Q 172.00, 140.00, 176.00, 140.00" style="\
                              fill:none;"/><svg:path class=" svg_unselected_element" d="M 167.00, 135.00 Q 172.15, 137.19, 177.21, 140.00 Q 172.00, 142.50, 167.00, 145.00"\
                              style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 13.00 Q 7.56, 8.53, 9.00, 3.39 Q 11.50, 8.00, 14.00, 13.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 9.00, 140.00 Q 18.19, 125.02, 28.92, 111.00 Q 37.48, 104.46, 46.01, 97.30 Q\
                              50.09, 101.82, 53.90, 105.69 Q 60.69, 97.09, 67.47, 88.45 Q 70.38, 84.91, 73.72, 81.85 Q 74.90, 77.96, 75.40, 73.64 Q 78.79,\
                              70.35, 82.01, 66.95 Q 86.74, 81.70, 92.05, 96.07 Q 94.41, 85.96, 96.39, 75.57 Q 100.76, 70.40, 104.93, 65.44 Q 107.93, 57.76,\
                              110.49, 49.69 Q 116.62, 43.58, 122.01, 36.74 Q 124.38, 41.14, 127.00, 45.00 Q 128.47, 47.79, 129.36, 50.40 Q 134.05, 50.78,\
                              138.94, 50.89 Q 144.88, 39.88, 151.04, 29.06 Q 154.87, 27.24, 159.05, 26.03 Q 158.88, 20.52, 159.82, 15.08 Q 163.50, 14.00,\
                              166.00, 12.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 10.00, 140.00 Q 13.45, 123.37, 17.87, 106.94 Q 22.95, 98.69, 29.11, 91.05 Q\
                              31.65, 82.05, 33.92, 72.74 Q 39.03, 76.38, 44.19, 80.24 Q 47.83, 67.68, 51.45, 55.65 Q 56.55, 53.61, 61.75, 51.72 Q 66.68,\
                              41.85, 74.05, 33.42 Q 79.61, 43.03, 85.98, 50.68 Q 92.25, 46.30, 99.02, 42.95 Q 105.38, 50.54, 110.21, 59.82 Q 116.13, 64.38,\
                              121.13, 69.93 Q 124.21, 84.11, 128.24, 97.89 Q 134.62, 107.95, 139.00, 118.16 Q 142.04, 110.04, 147.34, 102.50 Q 150.79, 113.84,\
                              153.43, 125.45 Q 159.50, 130.50, 165.00, 136.00" style=" fill:none;"/>\
                           </svg:g>\
                        </svg:g>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1497021823"] .border-wrapper, body[data-current-page-id="1497021823"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1497021823"] .border-wrapper, body.has-frame[data-current-page-id="1497021823"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1497021823"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1497021823",\
      			"name": "stats",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-600-1024" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:657px;height:1048px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.17, 1.76, 52.33, 1.52 Q 62.50, 2.29, 72.67, 1.53 Q 82.83,\
            1.74, 93.00, 1.93 Q 103.17, 1.34, 113.33, 1.30 Q 123.50, 1.98, 133.67, 2.53 Q 143.83, 3.00, 154.00, 2.34 Q 164.17, 1.87, 174.33,\
            1.63 Q 184.50, 1.55, 194.67, 2.05 Q 204.83, 2.32, 215.00, 3.07 Q 225.17, 2.56, 235.33, 2.76 Q 245.50, 3.09, 255.67, 2.06 Q\
            265.83, 1.81, 276.00, 1.44 Q 286.17, 1.21, 296.33, 1.27 Q 306.50, 0.99, 316.67, 0.83 Q 326.83, 0.71, 337.00, 0.59 Q 347.17,\
            0.58, 357.33, 1.64 Q 367.50, 1.96, 377.67, 1.94 Q 387.83, 2.24, 398.00, 3.12 Q 408.17, 3.19, 418.33, 3.04 Q 428.50, 2.16,\
            438.67, 2.82 Q 448.83, 3.42, 459.00, 3.14 Q 469.17, 3.70, 479.33, 2.54 Q 489.50, 2.40, 499.67, 2.47 Q 509.83, 2.73, 520.00,\
            2.97 Q 530.17, 2.09, 540.33, 1.96 Q 550.50, 2.14, 560.67, 1.89 Q 570.83, 1.91, 581.00, 2.24 Q 591.17, 1.25, 601.33, 2.15 Q\
            611.50, 1.44, 621.67, 1.56 Q 631.83, 1.71, 642.89, 2.11 Q 643.23, 12.68, 643.72, 22.93 Q 643.22, 33.18, 642.86, 43.33 Q 643.03,\
            53.42, 642.86, 63.52 Q 642.82, 73.61, 642.57, 83.70 Q 642.19, 93.79, 642.68, 103.88 Q 642.49, 113.97, 642.34, 124.06 Q 643.06,\
            134.15, 642.45, 144.24 Q 643.26, 154.32, 641.83, 164.41 Q 641.70, 174.50, 643.20, 184.59 Q 642.92, 194.68, 643.12, 204.76\
            Q 642.31, 214.85, 642.81, 224.94 Q 643.34, 235.03, 642.87, 245.12 Q 641.62, 255.21, 641.53, 265.29 Q 642.43, 275.38, 642.54,\
            285.47 Q 642.57, 295.56, 642.42, 305.65 Q 641.88, 315.74, 641.31, 325.82 Q 641.59, 335.91, 641.79, 346.00 Q 641.34, 356.09,\
            641.43, 366.18 Q 642.04, 376.26, 642.31, 386.35 Q 642.93, 396.44, 642.40, 406.53 Q 642.31, 416.62, 642.32, 426.71 Q 642.48,\
            436.79, 642.55, 446.88 Q 642.31, 456.97, 642.32, 467.06 Q 642.11, 477.15, 641.95, 487.24 Q 642.54, 497.32, 643.37, 507.41\
            Q 642.56, 517.50, 642.24, 527.59 Q 642.10, 537.68, 642.94, 547.76 Q 643.32, 557.85, 642.76, 567.94 Q 642.74, 578.03, 642.44,\
            588.12 Q 643.28, 598.21, 643.22, 608.29 Q 643.38, 618.38, 642.93, 628.47 Q 642.45, 638.56, 643.18, 648.65 Q 643.21, 658.74,\
            642.32, 668.82 Q 642.12, 678.91, 642.78, 689.00 Q 643.33, 699.09, 643.67, 709.18 Q 643.09, 719.27, 642.92, 729.35 Q 642.78,\
            739.44, 642.78, 749.53 Q 642.51, 759.62, 642.54, 769.71 Q 641.74, 779.79, 642.07, 789.88 Q 642.94, 799.97, 643.04, 810.06\
            Q 642.75, 820.15, 642.00, 830.24 Q 641.90, 840.32, 642.24, 850.41 Q 643.53, 860.50, 643.50, 870.59 Q 642.98, 880.68, 642.63,\
            890.77 Q 643.15, 900.85, 642.96, 910.94 Q 642.41, 921.03, 640.95, 931.12 Q 641.03, 941.21, 641.21, 951.29 Q 642.35, 961.38,\
            643.67, 971.47 Q 643.69, 981.56, 642.91, 991.65 Q 642.81, 1001.74, 643.02, 1011.82 Q 642.21, 1021.91, 642.01, 1032.01 Q 631.93,\
            1032.30, 621.77, 1032.74 Q 611.53, 1032.46, 601.37, 1033.02 Q 591.19, 1033.20, 581.01, 1032.82 Q 570.84, 1032.48, 560.67,\
            1032.61 Q 550.50, 1032.76, 540.33, 1032.30 Q 530.17, 1031.96, 520.00, 1031.96 Q 509.83, 1032.44, 499.67, 1032.61 Q 489.50,\
            1033.03, 479.33, 1033.43 Q 469.17, 1033.08, 459.00, 1033.82 Q 448.83, 1033.96, 438.67, 1033.28 Q 428.50, 1032.50, 418.33,\
            1031.98 Q 408.17, 1031.88, 398.00, 1031.67 Q 387.83, 1031.98, 377.67, 1032.33 Q 367.50, 1032.68, 357.33, 1032.24 Q 347.17,\
            1031.81, 337.00, 1031.89 Q 326.83, 1032.07, 316.67, 1032.29 Q 306.50, 1032.41, 296.33, 1032.13 Q 286.17, 1032.38, 276.00,\
            1033.20 Q 265.83, 1032.84, 255.67, 1032.62 Q 245.50, 1032.33, 235.33, 1031.99 Q 225.17, 1032.26, 215.00, 1032.43 Q 204.83,\
            1032.41, 194.67, 1032.81 Q 184.50, 1032.85, 174.33, 1032.59 Q 164.17, 1033.20, 154.00, 1031.48 Q 143.83, 1032.22, 133.67,\
            1031.91 Q 123.50, 1031.73, 113.33, 1031.64 Q 103.17, 1031.21, 93.00, 1031.22 Q 82.83, 1031.49, 72.67, 1031.89 Q 62.50, 1032.62,\
            52.33, 1032.78 Q 42.17, 1033.19, 31.86, 1032.14 Q 32.30, 1021.81, 32.29, 1011.78 Q 32.89, 1001.68, 32.18, 991.64 Q 31.64,\
            981.57, 31.70, 971.47 Q 30.22, 961.39, 31.00, 951.30 Q 30.90, 941.21, 31.86, 931.12 Q 32.53, 921.03, 32.01, 910.94 Q 32.12,\
            900.85, 32.60, 890.77 Q 31.75, 880.68, 31.03, 870.59 Q 30.70, 860.50, 31.42, 850.41 Q 31.38, 840.32, 30.73, 830.24 Q 30.56,\
            820.15, 31.59, 810.06 Q 31.73, 799.97, 32.12, 789.88 Q 31.71, 779.79, 31.27, 769.71 Q 30.89, 759.62, 31.09, 749.53 Q 31.30,\
            739.44, 31.29, 729.35 Q 31.35, 719.27, 31.17, 709.18 Q 31.41, 699.09, 30.40, 689.00 Q 30.27, 678.91, 30.18, 668.82 Q 29.93,\
            658.74, 29.63, 648.65 Q 29.61, 638.56, 30.58, 628.47 Q 30.20, 618.38, 30.59, 608.29 Q 30.02, 598.21, 30.62, 588.12 Q 30.65,\
            578.03, 30.96, 567.94 Q 31.30, 557.85, 31.35, 547.76 Q 31.52, 537.68, 31.24, 527.59 Q 32.11, 517.50, 32.31, 507.41 Q 32.94,\
            497.32, 31.66, 487.24 Q 31.41, 477.15, 30.87, 467.06 Q 30.44, 456.97, 30.65, 446.88 Q 31.10, 436.79, 31.26, 426.71 Q 31.71,\
            416.62, 32.24, 406.53 Q 32.32, 396.44, 32.51, 386.35 Q 33.15, 376.26, 32.29, 366.18 Q 31.09, 356.09, 30.54, 346.00 Q 30.21,\
            335.91, 30.06, 325.82 Q 30.35, 315.74, 31.19, 305.65 Q 31.58, 295.56, 31.44, 285.47 Q 30.44, 275.38, 30.40, 265.29 Q 30.70,\
            255.21, 31.02, 245.12 Q 30.50, 235.03, 31.22, 224.94 Q 31.61, 214.85, 32.01, 204.76 Q 32.75, 194.68, 32.10, 184.59 Q 32.07,\
            174.50, 31.10, 164.41 Q 30.70, 154.32, 30.19, 144.24 Q 30.10, 134.15, 30.47, 124.06 Q 30.24, 113.97, 30.31, 103.88 Q 30.46,\
            93.79, 30.77, 83.71 Q 30.98, 73.62, 31.76, 63.53 Q 30.63, 53.44, 30.61, 43.35 Q 30.60, 33.26, 30.27, 23.18 Q 32.00, 13.09,\
            32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.17, 6.18, 43.33, 5.81 Q 53.50, 5.66, 63.67, 5.64 Q 73.83,\
            5.51, 84.00, 5.19 Q 94.17, 5.71, 104.33, 5.49 Q 114.50, 5.85, 124.67, 6.09 Q 134.83, 4.94, 145.00, 4.87 Q 155.17, 5.83, 165.33,\
            5.58 Q 175.50, 5.50, 185.67, 5.60 Q 195.83, 5.83, 206.00, 5.99 Q 216.17, 5.87, 226.33, 6.34 Q 236.50, 4.97, 246.67, 5.29 Q\
            256.83, 5.24, 267.00, 5.06 Q 277.17, 5.58, 287.33, 6.13 Q 297.50, 6.09, 307.67, 5.71 Q 317.83, 5.58, 328.00, 5.52 Q 338.17,\
            6.19, 348.33, 6.20 Q 358.50, 6.15, 368.67, 6.48 Q 378.83, 6.98, 389.00, 6.83 Q 399.17, 6.06, 409.33, 6.98 Q 419.50, 6.67,\
            429.67, 7.48 Q 439.83, 6.64, 450.00, 5.93 Q 460.17, 5.96, 470.33, 6.54 Q 480.50, 5.75, 490.67, 5.89 Q 500.83, 5.47, 511.00,\
            6.02 Q 521.17, 5.91, 531.33, 5.87 Q 541.50, 5.69, 551.67, 5.60 Q 561.83, 5.72, 572.00, 5.66 Q 582.17, 5.17, 592.33, 5.47 Q\
            602.50, 4.89, 612.67, 4.73 Q 622.83, 5.00, 633.92, 6.08 Q 633.88, 16.80, 634.01, 27.03 Q 633.92, 37.20, 633.74, 47.33 Q 634.06,\
            57.42, 633.86, 67.52 Q 634.07, 77.61, 634.10, 87.70 Q 633.67, 97.79, 633.72, 107.88 Q 633.55, 117.97, 633.48, 128.06 Q 633.74,\
            138.15, 634.56, 148.24 Q 633.44, 158.32, 633.61, 168.41 Q 634.25, 178.50, 634.84, 188.59 Q 634.86, 198.68, 634.17, 208.76\
            Q 634.36, 218.85, 634.39, 228.94 Q 634.61, 239.03, 634.12, 249.12 Q 634.61, 259.21, 633.66, 269.29 Q 632.89, 279.38, 632.22,\
            289.47 Q 634.17, 299.56, 633.99, 309.65 Q 633.17, 319.74, 633.52, 329.82 Q 633.65, 339.91, 633.81, 350.00 Q 633.26, 360.09,\
            633.47, 370.18 Q 632.82, 380.26, 633.66, 390.35 Q 633.74, 400.44, 633.84, 410.53 Q 633.88, 420.62, 634.10, 430.71 Q 634.20,\
            440.79, 633.77, 450.88 Q 633.82, 460.97, 633.59, 471.06 Q 633.45, 481.15, 633.98, 491.24 Q 634.37, 501.32, 634.72, 511.41\
            Q 633.88, 521.50, 633.58, 531.59 Q 634.40, 541.68, 634.74, 551.76 Q 634.85, 561.85, 634.97, 571.94 Q 634.85, 582.03, 634.38,\
            592.12 Q 634.45, 602.21, 634.10, 612.29 Q 634.62, 622.38, 633.57, 632.47 Q 633.44, 642.56, 634.01, 652.65 Q 634.01, 662.74,\
            634.25, 672.82 Q 633.88, 682.91, 633.43, 693.00 Q 633.99, 703.09, 634.35, 713.18 Q 633.65, 723.26, 634.14, 733.35 Q 634.07,\
            743.44, 634.34, 753.53 Q 633.86, 763.62, 634.02, 773.71 Q 633.14, 783.79, 633.43, 793.88 Q 633.34, 803.97, 633.04, 814.06\
            Q 633.37, 824.15, 633.30, 834.24 Q 633.14, 844.32, 633.16, 854.41 Q 633.64, 864.50, 633.24, 874.59 Q 633.28, 884.68, 633.93,\
            894.77 Q 632.80, 904.85, 632.74, 914.94 Q 632.58, 925.03, 633.43, 935.12 Q 633.78, 945.21, 634.10, 955.29 Q 634.17, 965.38,\
            634.37, 975.47 Q 634.16, 985.56, 634.30, 995.65 Q 634.30, 1005.74, 634.11, 1015.82 Q 633.43, 1025.91, 633.61, 1036.61 Q 623.11,\
            1036.83, 612.81, 1036.97 Q 602.60, 1037.49, 592.38, 1037.32 Q 582.18, 1036.93, 572.01, 1036.71 Q 561.83, 1036.26, 551.67,\
            1037.14 Q 541.50, 1036.38, 531.33, 1036.16 Q 521.17, 1035.87, 511.00, 1035.77 Q 500.83, 1036.34, 490.67, 1035.93 Q 480.50,\
            1036.00, 470.33, 1036.55 Q 460.17, 1037.15, 450.00, 1036.56 Q 439.83, 1035.07, 429.67, 1036.67 Q 419.50, 1037.70, 409.33,\
            1037.53 Q 399.17, 1036.99, 389.00, 1037.04 Q 378.83, 1037.46, 368.67, 1037.26 Q 358.50, 1036.89, 348.33, 1036.76 Q 338.17,\
            1037.09, 328.00, 1037.13 Q 317.83, 1035.67, 307.67, 1036.25 Q 297.50, 1036.62, 287.33, 1036.96 Q 277.17, 1037.00, 267.00,\
            1037.10 Q 256.83, 1037.17, 246.67, 1036.99 Q 236.50, 1037.19, 226.33, 1037.03 Q 216.17, 1037.15, 206.00, 1037.34 Q 195.83,\
            1037.51, 185.67, 1037.13 Q 175.50, 1036.28, 165.33, 1036.20 Q 155.17, 1035.96, 145.00, 1036.56 Q 134.83, 1036.34, 124.67,\
            1036.19 Q 114.50, 1036.30, 104.33, 1036.07 Q 94.17, 1036.10, 84.00, 1036.44 Q 73.83, 1036.47, 63.67, 1036.91 Q 53.50, 1037.62,\
            43.33, 1037.43 Q 33.17, 1037.18, 22.57, 1036.43 Q 22.27, 1026.16, 23.09, 1015.81 Q 23.04, 1005.73, 22.37, 995.67 Q 22.46,\
            985.57, 21.79, 975.48 Q 21.48, 965.39, 21.08, 955.30 Q 20.88, 945.21, 21.66, 935.12 Q 20.94, 925.03, 21.60, 914.94 Q 22.36,\
            904.85, 22.47, 894.77 Q 22.27, 884.68, 21.93, 874.59 Q 22.37, 864.50, 22.24, 854.41 Q 21.43, 844.32, 21.51, 834.24 Q 22.25,\
            824.15, 23.40, 814.06 Q 23.17, 803.97, 22.42, 793.88 Q 21.94, 783.79, 21.36, 773.71 Q 21.77, 763.62, 21.18, 753.53 Q 22.07,\
            743.44, 21.16, 733.35 Q 21.00, 723.26, 22.11, 713.18 Q 21.42, 703.09, 22.37, 693.00 Q 22.53, 682.91, 21.96, 672.82 Q 21.63,\
            662.74, 21.33, 652.65 Q 21.38, 642.56, 21.78, 632.47 Q 21.46, 622.38, 21.09, 612.29 Q 21.36, 602.21, 21.98, 592.12 Q 22.08,\
            582.03, 21.15, 571.94 Q 21.51, 561.85, 21.79, 551.76 Q 21.78, 541.68, 21.40, 531.59 Q 21.97, 521.50, 22.87, 511.41 Q 23.49,\
            501.32, 23.02, 491.24 Q 21.88, 481.15, 22.63, 471.06 Q 22.93, 460.97, 23.03, 450.88 Q 22.46, 440.79, 22.87, 430.71 Q 23.29,\
            420.62, 22.91, 410.53 Q 23.48, 400.44, 22.82, 390.35 Q 23.76, 380.26, 22.58, 370.18 Q 22.53, 360.09, 21.93, 350.00 Q 22.11,\
            339.91, 22.78, 329.82 Q 23.10, 319.74, 23.38, 309.65 Q 21.98, 299.56, 22.10, 289.47 Q 22.58, 279.38, 23.12, 269.29 Q 22.70,\
            259.21, 22.22, 249.12 Q 22.33, 239.03, 21.78, 228.94 Q 22.85, 218.85, 21.77, 208.76 Q 22.08, 198.68, 22.14, 188.59 Q 22.75,\
            178.50, 21.88, 168.41 Q 21.90, 158.32, 22.18, 148.24 Q 22.24, 138.15, 22.90, 128.06 Q 22.14, 117.97, 22.71, 107.88 Q 22.20,\
            97.79, 22.44, 87.71 Q 22.07, 77.62, 21.98, 67.53 Q 22.17, 57.44, 21.54, 47.35 Q 22.96, 37.26, 22.28, 27.18 Q 23.00, 17.09,\
            23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.17, 11.10, 60.33, 11.30 Q 70.50, 11.87, 80.67, 11.54 Q 90.83,\
            10.79, 101.00, 11.30 Q 111.17, 10.69, 121.33, 10.83 Q 131.50, 10.80, 141.67, 10.46 Q 151.83, 11.20, 162.00, 11.47 Q 172.17,\
            10.61, 182.33, 11.06 Q 192.50, 11.04, 202.67, 11.50 Q 212.83, 12.15, 223.00, 10.91 Q 233.17, 10.57, 243.33, 10.43 Q 253.50,\
            10.62, 263.67, 11.70 Q 273.83, 11.03, 284.00, 11.51 Q 294.17, 10.40, 304.33, 10.39 Q 314.50, 10.65, 324.67, 10.65 Q 334.83,\
            12.10, 345.00, 11.10 Q 355.17, 9.38, 365.33, 10.76 Q 375.50, 11.38, 385.67, 11.31 Q 395.83, 10.59, 406.00, 10.15 Q 416.17,\
            9.78, 426.33, 10.34 Q 436.50, 10.59, 446.67, 10.10 Q 456.83, 9.62, 467.00, 9.73 Q 477.17, 10.88, 487.33, 11.38 Q 497.50, 11.09,\
            507.67, 11.17 Q 517.83, 11.74, 528.00, 11.39 Q 538.17, 11.28, 548.33, 10.05 Q 558.50, 11.15, 568.67, 10.27 Q 578.83, 10.38,\
            589.00, 10.70 Q 599.17, 10.35, 609.33, 10.48 Q 619.50, 10.74, 629.67, 11.28 Q 639.83, 10.77, 650.46, 10.54 Q 651.13, 20.71,\
            651.76, 30.93 Q 650.88, 41.21, 650.07, 51.35 Q 648.97, 61.46, 650.02, 71.53 Q 650.16, 81.62, 650.64, 91.70 Q 651.62, 101.79,\
            652.02, 111.88 Q 652.12, 121.97, 652.04, 132.06 Q 652.17, 142.15, 651.85, 152.24 Q 651.82, 162.32, 651.45, 172.41 Q 651.42,\
            182.50, 651.56, 192.59 Q 651.29, 202.68, 651.26, 212.76 Q 651.62, 222.85, 651.84, 232.94 Q 651.81, 243.03, 651.52, 253.12\
            Q 650.66, 263.21, 649.95, 273.29 Q 649.62, 283.38, 649.68, 293.47 Q 650.51, 303.56, 650.14, 313.65 Q 650.51, 323.74, 649.88,\
            333.82 Q 648.95, 343.91, 649.45, 354.00 Q 648.76, 364.09, 650.01, 374.18 Q 649.72, 384.26, 649.59, 394.35 Q 649.48, 404.44,\
            651.36, 414.53 Q 651.26, 424.62, 649.29, 434.71 Q 649.18, 444.79, 649.40, 454.88 Q 649.24, 464.97, 649.64, 475.06 Q 649.58,\
            485.15, 650.44, 495.24 Q 651.15, 505.32, 651.84, 515.41 Q 652.09, 525.50, 652.39, 535.59 Q 651.91, 545.68, 651.89, 555.76\
            Q 652.28, 565.85, 650.81, 575.94 Q 650.04, 586.03, 650.54, 596.12 Q 650.65, 606.21, 650.51, 616.29 Q 650.52, 626.38, 650.91,\
            636.47 Q 651.13, 646.56, 651.74, 656.65 Q 651.18, 666.74, 650.69, 676.82 Q 650.96, 686.91, 651.24, 697.00 Q 650.33, 707.09,\
            650.62, 717.18 Q 651.63, 727.27, 652.24, 737.35 Q 651.73, 747.44, 651.45, 757.53 Q 651.71, 767.62, 651.43, 777.71 Q 651.56,\
            787.79, 651.71, 797.88 Q 651.73, 807.97, 651.76, 818.06 Q 651.49, 828.15, 651.69, 838.24 Q 651.77, 848.32, 651.74, 858.41\
            Q 651.91, 868.50, 652.15, 878.59 Q 652.14, 888.68, 651.73, 898.77 Q 651.70, 908.85, 651.60, 918.94 Q 650.80, 929.03, 650.70,\
            939.12 Q 650.80, 949.21, 650.76, 959.29 Q 650.92, 969.38, 650.71, 979.47 Q 650.89, 989.56, 650.03, 999.65 Q 650.00, 1009.74,\
            650.16, 1019.82 Q 650.68, 1029.91, 650.13, 1040.13 Q 639.86, 1040.08, 629.77, 1040.70 Q 619.55, 1040.79, 609.36, 1040.90 Q\
            599.19, 1041.16, 589.01, 1041.08 Q 578.84, 1041.02, 568.67, 1041.12 Q 558.50, 1040.39, 548.33, 1040.32 Q 538.17, 1040.31,\
            528.00, 1040.70 Q 517.83, 1040.25, 507.67, 1040.20 Q 497.50, 1040.73, 487.33, 1039.68 Q 477.17, 1040.16, 467.00, 1039.19 Q\
            456.83, 1038.97, 446.67, 1040.70 Q 436.50, 1039.95, 426.33, 1039.48 Q 416.17, 1038.92, 406.00, 1039.74 Q 395.83, 1040.18,\
            385.67, 1040.28 Q 375.50, 1039.48, 365.33, 1039.27 Q 355.17, 1040.73, 345.00, 1041.37 Q 334.83, 1041.07, 324.67, 1041.04 Q\
            314.50, 1041.05, 304.33, 1040.38 Q 294.17, 1040.83, 284.00, 1040.56 Q 273.83, 1040.22, 263.67, 1041.61 Q 253.50, 1040.87,\
            243.33, 1041.27 Q 233.17, 1040.19, 223.00, 1040.30 Q 212.83, 1040.75, 202.67, 1040.63 Q 192.50, 1040.21, 182.33, 1040.02 Q\
            172.17, 1041.02, 162.00, 1039.62 Q 151.83, 1040.27, 141.67, 1040.36 Q 131.50, 1040.83, 121.33, 1040.70 Q 111.17, 1040.11,\
            101.00, 1041.26 Q 90.83, 1041.07, 80.67, 1041.82 Q 70.50, 1041.21, 60.33, 1041.11 Q 50.17, 1041.85, 39.33, 1040.67 Q 38.71,\
            1030.34, 38.30, 1020.07 Q 38.01, 1009.87, 38.63, 999.69 Q 38.23, 989.59, 39.16, 979.48 Q 38.39, 969.39, 38.45, 959.30 Q 37.75,\
            949.21, 38.99, 939.12 Q 39.50, 929.03, 39.23, 918.94 Q 39.82, 908.85, 38.92, 898.77 Q 39.30, 888.68, 39.35, 878.59 Q 38.91,\
            868.50, 38.72, 858.41 Q 38.77, 848.32, 38.66, 838.24 Q 39.03, 828.15, 38.88, 818.06 Q 38.49, 807.97, 38.72, 797.88 Q 38.60,\
            787.79, 38.32, 777.71 Q 38.22, 767.62, 39.11, 757.53 Q 39.50, 747.44, 38.75, 737.35 Q 39.40, 727.27, 38.78, 717.18 Q 39.10,\
            707.09, 38.48, 697.00 Q 38.30, 686.91, 38.37, 676.82 Q 38.39, 666.74, 38.18, 656.65 Q 38.18, 646.56, 38.07, 636.47 Q 38.35,\
            626.38, 37.74, 616.29 Q 38.04, 606.21, 38.43, 596.12 Q 37.72, 586.03, 38.91, 575.94 Q 39.33, 565.85, 38.65, 555.76 Q 38.53,\
            545.68, 39.05, 535.59 Q 39.29, 525.50, 38.81, 515.41 Q 38.71, 505.32, 38.40, 495.24 Q 38.84, 485.15, 38.84, 475.06 Q 38.93,\
            464.97, 38.94, 454.88 Q 39.66, 444.79, 39.30, 434.71 Q 39.09, 424.62, 38.51, 414.53 Q 38.40, 404.44, 38.42, 394.35 Q 39.34,\
            384.26, 39.20, 374.18 Q 39.04, 364.09, 38.98, 354.00 Q 38.43, 343.91, 38.26, 333.82 Q 38.31, 323.74, 38.42, 313.65 Q 38.83,\
            303.56, 39.36, 293.47 Q 39.47, 283.38, 39.36, 273.29 Q 38.78, 263.21, 38.55, 253.12 Q 38.90, 243.03, 39.02, 232.94 Q 38.94,\
            222.85, 39.14, 212.76 Q 39.18, 202.68, 39.02, 192.59 Q 38.92, 182.50, 38.90, 172.41 Q 38.71, 162.32, 38.53, 152.24 Q 38.54,\
            142.15, 39.40, 132.06 Q 39.13, 121.97, 39.39, 111.88 Q 39.31, 101.79, 40.16, 91.71 Q 40.06, 81.62, 40.09, 71.53 Q 39.95, 61.44,\
            40.19, 51.35 Q 39.96, 41.26, 39.41, 31.18 Q 40.00, 21.09, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');