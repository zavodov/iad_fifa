rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page620342037-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page620342037" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page620342037-layer-700942915" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 1024px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="700942915" data-review-reference-id="700942915">\
            <div class="stencil-wrapper" style="width: 275px; height: 1024px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 1024px;width:275px;" width="275" height="1024">\
                     <svg:g width="275" height="1024"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.63, 22.85, 0.45 Q 33.27, 0.36, 43.69, 0.20 Q 54.12, 0.83,\
                        64.54, 0.95 Q 74.96, 0.90, 85.38, 0.60 Q 95.81, 0.64, 106.23, 0.91 Q 116.65, 0.20, 127.08, 1.01 Q 137.50, 1.11, 147.92, 0.90\
                        Q 158.35, 1.19, 168.77, 0.96 Q 179.19, 1.54, 189.62, 0.99 Q 200.04, 0.90, 210.46, 1.55 Q 220.88, 1.72, 231.31, 0.49 Q 241.73,\
                        0.85, 252.15, 1.70 Q 262.58, 1.80, 273.12, 1.88 Q 273.27, 11.91, 273.53, 21.92 Q 273.65, 31.96, 272.94, 42.00 Q 272.96, 52.00,\
                        273.23, 62.00 Q 272.94, 72.00, 272.75, 82.00 Q 273.05, 92.00, 273.58, 102.00 Q 274.45, 112.00, 274.14, 122.00 Q 274.41, 132.00,\
                        274.28, 142.00 Q 273.60, 152.00, 273.89, 162.00 Q 273.62, 172.00, 274.12, 182.00 Q 274.04, 192.00, 273.95, 202.00 Q 274.14,\
                        212.00, 274.30, 222.00 Q 274.39, 232.00, 274.68, 242.00 Q 274.18, 252.00, 274.93, 262.00 Q 274.77, 272.00, 274.53, 282.00\
                        Q 275.06, 292.00, 274.59, 302.00 Q 274.83, 312.00, 275.00, 322.00 Q 274.14, 332.00, 274.11, 342.00 Q 274.11, 352.00, 274.08,\
                        362.00 Q 274.20, 372.00, 273.62, 382.00 Q 273.77, 392.00, 273.76, 402.00 Q 273.52, 412.00, 274.39, 422.00 Q 273.56, 432.00,\
                        274.16, 442.00 Q 273.17, 452.00, 273.63, 462.00 Q 274.36, 472.00, 273.84, 482.00 Q 273.67, 492.00, 273.88, 502.00 Q 274.13,\
                        512.00, 274.24, 522.00 Q 274.18, 532.00, 274.04, 542.00 Q 273.37, 552.00, 272.84, 562.00 Q 272.77, 572.00, 273.41, 582.00\
                        Q 273.85, 592.00, 272.99, 602.00 Q 273.95, 612.00, 273.67, 622.00 Q 274.00, 632.00, 272.91, 642.00 Q 272.77, 652.00, 273.30,\
                        662.00 Q 273.63, 672.00, 273.68, 682.00 Q 273.73, 692.00, 273.91, 702.00 Q 273.51, 712.00, 273.55, 722.00 Q 273.78, 732.00,\
                        274.00, 742.00 Q 273.94, 752.00, 272.86, 762.00 Q 272.53, 772.00, 274.22, 782.00 Q 274.75, 792.00, 273.96, 802.00 Q 273.17,\
                        812.00, 273.69, 822.00 Q 274.71, 832.00, 274.51, 842.00 Q 274.27, 852.00, 273.62, 862.00 Q 273.89, 872.00, 273.91, 882.00\
                        Q 274.20, 892.00, 274.57, 902.00 Q 274.03, 912.00, 273.50, 922.00 Q 273.85, 932.00, 273.75, 942.00 Q 273.61, 952.00, 273.94,\
                        962.00 Q 274.07, 972.00, 274.08, 982.00 Q 274.20, 992.00, 274.28, 1002.00 Q 273.90, 1012.00, 273.20, 1022.20 Q 262.70, 1022.35,\
                        252.24, 1022.60 Q 241.77, 1022.64, 231.33, 1022.72 Q 220.89, 1022.26, 210.46, 1022.08 Q 200.04, 1022.64, 189.62, 1023.35 Q\
                        179.19, 1022.80, 168.77, 1022.06 Q 158.35, 1022.30, 147.92, 1022.70 Q 137.50, 1022.00, 127.08, 1021.07 Q 116.65, 1020.61,\
                        106.23, 1021.55 Q 95.81, 1022.97, 85.38, 1023.07 Q 74.96, 1022.90, 64.54, 1022.51 Q 54.12, 1022.14, 43.69, 1021.66 Q 33.27,\
                        1021.20, 22.85, 1022.22 Q 12.42, 1022.39, 1.74, 1022.26 Q 1.35, 1012.22, 1.20, 1002.11 Q 1.02, 992.07, 0.95, 982.03 Q 1.12,\
                        972.01, 1.15, 962.01 Q 1.25, 952.00, 1.90, 942.00 Q 1.95, 932.00, 1.63, 922.00 Q 1.06, 912.00, 1.22, 902.00 Q 1.28, 892.00,\
                        0.17, 882.00 Q 0.68, 872.00, 0.71, 862.00 Q 1.20, 852.00, 2.14, 842.00 Q 1.68, 832.00, 0.56, 822.00 Q 1.61, 812.00, 1.18,\
                        802.00 Q 0.42, 792.00, 0.42, 782.00 Q 1.32, 772.00, 1.45, 762.00 Q 1.09, 752.00, 2.10, 742.00 Q 1.88, 732.00, 1.63, 722.00\
                        Q 1.25, 712.00, 1.09, 702.00 Q 1.11, 692.00, 0.95, 682.00 Q 0.73, 672.00, 0.76, 662.00 Q 0.65, 652.00, 0.49, 642.00 Q 1.14,\
                        632.00, 1.27, 622.00 Q 1.42, 612.00, 1.26, 602.00 Q 1.09, 592.00, 0.96, 582.00 Q 1.21, 572.00, 1.39, 562.00 Q 1.39, 552.00,\
                        1.48, 542.00 Q 1.70, 532.00, 3.04, 522.00 Q 4.17, 512.00, 3.21, 502.00 Q 2.64, 492.00, 1.00, 482.00 Q 0.69, 472.00, 0.45,\
                        462.00 Q 0.41, 452.00, 1.51, 442.00 Q 2.45, 432.00, 2.58, 422.00 Q 3.09, 412.00, 1.65, 402.00 Q 1.16, 392.00, 0.65, 382.00\
                        Q 0.52, 372.00, 1.30, 362.00 Q 1.08, 352.00, 1.65, 342.00 Q 1.96, 332.00, 2.08, 322.00 Q 2.04, 312.00, 1.51, 302.00 Q 1.79,\
                        292.00, 2.29, 282.00 Q 1.75, 272.00, 1.93, 262.00 Q 1.46, 252.00, 1.01, 242.00 Q 2.22, 232.00, 1.54, 222.00 Q 2.25, 212.00,\
                        2.76, 202.00 Q 1.39, 192.00, 1.88, 182.00 Q 1.65, 172.00, 1.02, 162.00 Q 1.11, 152.00, 1.11, 142.00 Q 0.81, 132.00, 0.73,\
                        122.00 Q 1.39, 112.00, 0.33, 102.00 Q 0.69, 92.00, 0.13, 82.00 Q 0.19, 72.00, 0.16, 62.00 Q 0.51, 52.00, 0.13, 42.00 Q 1.03,\
                        32.00, 0.63, 22.00 Q 2.00, 12.00, 2.00, 2.00" style=" fill:#C1C1C1;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-55349585" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="55349585" data-review-reference-id="55349585">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px;width:275px;" width="275" height="160">\
                     <svg:g width="275" height="160"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 2.85, 22.85, 2.94 Q 33.27, 2.07, 43.69, 2.78 Q 54.12, 2.04,\
                        64.54, 0.71 Q 74.96, 0.35, 85.38, 1.13 Q 95.81, 1.60, 106.23, 0.98 Q 116.65, 0.58, 127.08, 0.31 Q 137.50, 0.07, 147.92, 0.09\
                        Q 158.35, 0.20, 168.77, 0.36 Q 179.19, 0.58, 189.62, 0.63 Q 200.04, 0.36, 210.46, 0.24 Q 220.88, 0.29, 231.31, 0.11 Q 241.73,\
                        -0.05, 252.15, 0.28 Q 262.58, 0.18, 273.89, 1.11 Q 274.17, 12.75, 274.50, 24.07 Q 274.66, 35.32, 274.22, 46.53 Q 273.94, 57.70,\
                        274.17, 68.85 Q 274.16, 80.00, 273.74, 91.14 Q 274.00, 102.28, 273.26, 113.43 Q 274.29, 124.57, 273.59, 135.71 Q 274.19, 146.86,\
                        273.61, 158.61 Q 262.84, 158.79, 252.30, 158.99 Q 241.81, 159.24, 231.35, 159.33 Q 220.92, 159.93, 210.47, 158.79 Q 200.05,\
                        159.73, 189.62, 158.36 Q 179.19, 158.90, 168.77, 158.95 Q 158.35, 158.83, 147.92, 158.82 Q 137.50, 159.25, 127.08, 158.28\
                        Q 116.65, 159.13, 106.23, 159.31 Q 95.81, 159.45, 85.38, 158.74 Q 74.96, 158.42, 64.54, 158.10 Q 54.12, 157.73, 43.69, 157.21\
                        Q 33.27, 158.11, 22.85, 157.35 Q 12.42, 159.19, 1.66, 158.34 Q 1.87, 146.90, 1.85, 135.74 Q 1.42, 124.61, 0.72, 113.47 Q 1.04,\
                        102.30, 1.67, 91.15 Q 0.36, 80.01, 0.05, 68.86 Q 0.58, 57.72, 1.51, 46.57 Q 1.48, 35.43, 0.67, 24.29 Q 2.00, 13.14, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-155199587" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="155199587" data-review-reference-id="155199587">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="155199587-210611091" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="210611091" data-review-reference-id="210611091">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85">\
                           <svg:g width="85" height="85"><svg:path class=" svg_unselected_element" d="M 82.00, 44.00 Q 82.99, 44.81, 81.15, 54.65 Q 76.76, 63.67, 70.66, 71.56 Q 62.69,\
                              77.62, 53.20, 80.88 Q 43.32, 81.84, 33.43, 81.13 Q 24.40, 76.64, 16.05, 71.14 Q 10.54, 62.59, 7.24, 53.15 Q 7.00, 43.24, 7.43,\
                              33.52 Q 10.38, 23.95, 17.25, 16.38 Q 25.68, 10.90, 34.77, 7.02 Q 44.62, 5.60, 54.63, 6.52 Q 63.84, 10.85, 71.30, 17.65 Q 77.11,\
                              25.67, 80.67, 34.88 Q 81.99, 44.66, 80.53, 54.47" style=" fill:white;"/>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="155199587-1100735333" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1100735333" data-review-reference-id="1100735333">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="155199587-1849949533" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1849949533" data-review-reference-id="1849949533">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-616829308" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="616829308" data-review-reference-id="616829308">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.49, 22.62, 0.68 Q 32.92, 0.99, 43.23, 1.61 Q 53.54, 1.36,\
                        63.85, 1.15 Q 74.15, 1.16, 84.46, 1.24 Q 94.77, 0.86, 105.08, 0.61 Q 115.38, 0.67, 125.69, 0.55 Q 136.00, 0.67, 146.31, 0.81\
                        Q 156.62, 0.96, 166.92, 0.87 Q 177.23, 0.90, 187.54, 0.91 Q 197.85, 0.75, 208.15, 0.58 Q 218.46, 0.64, 228.77, 0.57 Q 239.08,\
                        0.65, 249.38, 0.60 Q 259.69, 1.27, 270.53, 1.47 Q 271.12, 12.38, 270.87, 23.38 Q 270.43, 34.22, 270.11, 45.11 Q 259.87, 45.55,\
                        249.45, 45.51 Q 239.12, 45.65, 228.80, 45.95 Q 218.47, 45.72, 208.16, 45.71 Q 197.85, 45.73, 187.54, 45.77 Q 177.23, 45.64,\
                        166.92, 45.88 Q 156.62, 45.97, 146.31, 45.80 Q 136.00, 45.82, 125.69, 46.14 Q 115.38, 46.17, 105.08, 45.96 Q 94.77, 46.34,\
                        84.46, 45.45 Q 74.15, 45.82, 63.85, 44.56 Q 53.54, 44.98, 43.23, 45.16 Q 32.92, 46.18, 22.62, 46.20 Q 12.31, 45.71, 1.54,\
                        45.46 Q 1.56, 34.40, 1.32, 23.60 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 269.65, 15.00, 270.05, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.53, 16.00, 273.23, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.65, 17.00, 273.69, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.17, 24.69, 45.97 Q 35.04, 45.80, 45.38, 45.88 Q 55.73,\
                        46.27, 66.08, 46.35 Q 76.42, 46.75, 86.77, 46.52 Q 97.12, 45.14, 107.46, 46.57 Q 117.81, 46.62, 128.15, 46.63 Q 138.50, 45.57,\
                        148.85, 47.38 Q 159.19, 47.55, 169.54, 47.18 Q 179.88, 46.11, 190.23, 45.14 Q 200.58, 46.17, 210.92, 46.73 Q 221.27, 46.59,\
                        231.62, 46.94 Q 241.96, 47.38, 252.31, 46.33 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 48.59, 25.69, 48.72 Q 36.04, 48.14, 46.38, 47.97 Q 56.73,\
                        46.91, 67.08, 47.06 Q 77.42, 46.95, 87.77, 46.91 Q 98.12, 46.91, 108.46, 45.73 Q 118.81, 46.00, 129.15, 46.64 Q 139.50, 46.47,\
                        149.85, 46.07 Q 160.19, 46.42, 170.54, 45.62 Q 180.88, 46.27, 191.23, 46.38 Q 201.58, 45.69, 211.92, 44.78 Q 222.27, 44.79,\
                        232.62, 45.14 Q 242.96, 45.29, 253.31, 45.45 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.71, 26.69, 46.61 Q 37.04, 47.27, 47.38, 46.85 Q 57.73,\
                        46.87, 68.08, 47.49 Q 78.42, 48.36, 88.77, 47.78 Q 99.12, 46.56, 109.46, 46.89 Q 119.81, 47.01, 130.15, 46.57 Q 140.50, 46.36,\
                        150.85, 47.73 Q 161.19, 48.00, 171.54, 48.08 Q 181.88, 47.56, 192.23, 46.79 Q 202.58, 46.31, 212.92, 46.57 Q 223.27, 46.00,\
                        233.62, 45.91 Q 243.96, 45.67, 254.31, 45.77 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-616829308button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-616829308button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-616829308button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Коэффициенты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-616829308\', \'94925087\', {"button":"left","id":"564703494","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"927521707","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-1530046147" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1530046147" data-review-reference-id="1530046147">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.43, 22.62, 0.60 Q 32.92, 0.52, 43.23, 0.10 Q 53.54, -0.20,\
                        63.85, 0.16 Q 74.15, 0.22, 84.46, 0.09 Q 94.77, -0.07, 105.08, -0.26 Q 115.38, -0.25, 125.69, 0.87 Q 136.00, 0.74, 146.31,\
                        0.60 Q 156.62, 0.40, 166.92, 0.37 Q 177.23, 0.23, 187.54, 0.05 Q 197.85, -0.06, 208.15, -0.23 Q 218.46, -0.25, 228.77, 0.15\
                        Q 239.08, 1.03, 249.38, 1.60 Q 259.69, 1.46, 270.31, 1.69 Q 270.85, 12.47, 270.71, 23.40 Q 270.47, 34.22, 270.15, 45.15 Q\
                        259.86, 45.54, 249.42, 45.27 Q 239.13, 45.80, 228.80, 46.02 Q 218.48, 46.54, 208.16, 46.44 Q 197.85, 46.70, 187.54, 46.16\
                        Q 177.23, 46.25, 166.92, 46.45 Q 156.62, 46.63, 146.31, 46.23 Q 136.00, 46.34, 125.69, 46.24 Q 115.38, 46.16, 105.08, 45.25\
                        Q 94.77, 44.78, 84.46, 44.44 Q 74.15, 44.63, 63.85, 44.78 Q 53.54, 45.34, 43.23, 45.65 Q 32.92, 46.22, 22.62, 46.72 Q 12.31,\
                        45.64, 1.68, 45.32 Q 2.07, 34.23, 2.14, 23.48 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.81, 15.00, 271.97, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.30, 16.00, 273.36, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.80, 17.00, 274.45, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.25, 24.69, 44.79 Q 35.04, 45.12, 45.38, 45.41 Q 55.73,\
                        45.48, 66.08, 45.36 Q 76.42, 45.55, 86.77, 45.78 Q 97.12, 45.50, 107.46, 45.40 Q 117.81, 45.63, 128.15, 44.81 Q 138.50, 44.61,\
                        148.85, 45.06 Q 159.19, 45.47, 169.54, 45.88 Q 179.88, 45.29, 190.23, 45.60 Q 200.58, 45.51, 210.92, 45.37 Q 221.27, 45.34,\
                        231.62, 45.31 Q 241.96, 45.04, 252.31, 44.26 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.80, 25.69, 48.06 Q 36.04, 47.98, 46.38, 48.01 Q 56.73,\
                        47.13, 67.08, 47.38 Q 77.42, 47.91, 87.77, 47.88 Q 98.12, 46.27, 108.46, 46.80 Q 118.81, 46.67, 129.15, 46.53 Q 139.50, 46.36,\
                        149.85, 47.07 Q 160.19, 47.39, 170.54, 47.44 Q 180.88, 46.98, 191.23, 46.22 Q 201.58, 47.08, 211.92, 47.65 Q 222.27, 47.15,\
                        232.62, 46.94 Q 242.96, 46.49, 253.31, 46.15 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.19, 26.69, 47.35 Q 37.04, 47.31, 47.38, 47.24 Q 57.73,\
                        47.27, 68.08, 47.52 Q 78.42, 47.32, 88.77, 47.19 Q 99.12, 47.42, 109.46, 47.19 Q 119.81, 48.41, 130.15, 47.97 Q 140.50, 47.56,\
                        150.85, 47.77 Q 161.19, 47.01, 171.54, 47.04 Q 181.88, 46.95, 192.23, 47.25 Q 202.58, 46.81, 212.92, 46.78 Q 223.27, 47.70,\
                        233.62, 48.97 Q 243.96, 49.41, 254.31, 48.42 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-1530046147button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-1530046147button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-1530046147button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Изменения<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-1530046147\', \'1378270368\', {"button":"left","id":"1902298370","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1634128386","options":"reloadOnly","target":"492670737","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-15105015" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="15105015" data-review-reference-id="15105015">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.57, 22.62, 2.54 Q 32.92, 2.92, 43.23, 2.63 Q 53.54, 2.26,\
                        63.85, 2.01 Q 74.15, 1.68, 84.46, 2.00 Q 94.77, 2.10, 105.08, 2.00 Q 115.38, 1.81, 125.69, 1.39 Q 136.00, 2.31, 146.31, 0.61\
                        Q 156.62, 1.04, 166.92, 1.20 Q 177.23, 2.09, 187.54, 1.02 Q 197.85, 0.40, 208.15, 0.50 Q 218.46, 1.37, 228.77, 1.08 Q 239.08,\
                        0.63, 249.38, -0.12 Q 259.69, 0.41, 270.19, 1.81 Q 271.09, 12.39, 271.50, 23.29 Q 271.09, 34.18, 270.25, 45.25 Q 259.94, 45.77,\
                        249.55, 46.24 Q 239.14, 46.08, 228.78, 45.45 Q 218.46, 45.17, 208.16, 46.11 Q 197.85, 46.39, 187.54, 46.12 Q 177.23, 46.85,\
                        166.92, 47.06 Q 156.62, 46.08, 146.31, 46.22 Q 136.00, 47.02, 125.69, 46.18 Q 115.38, 46.29, 105.08, 45.52 Q 94.77, 46.58,\
                        84.46, 46.86 Q 74.15, 46.94, 63.85, 45.74 Q 53.54, 45.21, 43.23, 44.84 Q 32.92, 45.22, 22.62, 46.57 Q 12.31, 47.02, 1.41,\
                        45.59 Q 0.49, 34.75, 1.45, 23.58 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 270.79, 15.00, 271.89, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 271.60, 16.00, 271.16, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 272.66, 17.00, 271.98, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.42, 24.69, 45.64 Q 35.04, 45.54, 45.38, 46.51 Q 55.73,\
                        45.20, 66.08, 46.22 Q 76.42, 46.26, 86.77, 46.23 Q 97.12, 45.11, 107.46, 44.79 Q 117.81, 44.78, 128.15, 46.02 Q 138.50, 46.01,\
                        148.85, 46.30 Q 159.19, 45.78, 169.54, 46.62 Q 179.88, 46.53, 190.23, 44.88 Q 200.58, 44.59, 210.92, 44.69 Q 221.27, 44.81,\
                        231.62, 46.14 Q 241.96, 46.10, 252.31, 46.80 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.26, 25.69, 48.14 Q 36.04, 47.95, 46.38, 47.99 Q 56.73,\
                        48.13, 67.08, 46.96 Q 77.42, 46.57, 87.77, 46.52 Q 98.12, 47.13, 108.46, 48.22 Q 118.81, 47.78, 129.15, 47.83 Q 139.50, 47.48,\
                        149.85, 46.77 Q 160.19, 45.75, 170.54, 44.87 Q 180.88, 44.97, 191.23, 44.92 Q 201.58, 46.62, 211.92, 46.66 Q 222.27, 46.05,\
                        232.62, 46.15 Q 242.96, 46.21, 253.31, 46.39 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.56, 26.69, 47.75 Q 37.04, 47.22, 47.38, 46.98 Q 57.73,\
                        46.84, 68.08, 47.08 Q 78.42, 47.65, 88.77, 47.28 Q 99.12, 46.96, 109.46, 46.78 Q 119.81, 46.84, 130.15, 47.11 Q 140.50, 47.23,\
                        150.85, 47.89 Q 161.19, 47.52, 171.54, 47.91 Q 181.88, 46.91, 192.23, 47.49 Q 202.58, 46.64, 212.92, 46.84 Q 223.27, 47.03,\
                        233.62, 46.87 Q 243.96, 47.39, 254.31, 47.35 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-15105015button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-15105015button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-15105015button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Статистика<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-15105015\', \'650710198\', {"button":"left","id":"456027254","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"2084877047","options":"reloadOnly","target":"1497021823","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-1303829118" style="position: absolute; left: 0px; top: 974px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1303829118" data-review-reference-id="1303829118">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 0.75, 22.62, 0.67 Q 32.92, 1.11, 43.23, 0.86 Q 53.54, 0.81,\
                        63.85, 1.53 Q 74.15, 1.59, 84.46, 0.90 Q 94.77, 1.50, 105.08, 0.70 Q 115.38, 1.01, 125.69, 1.68 Q 136.00, 2.11, 146.31, 2.35\
                        Q 156.62, 1.95, 166.92, 1.26 Q 177.23, 1.42, 187.54, 2.03 Q 197.85, 2.04, 208.15, 1.23 Q 218.46, 0.85, 228.77, 1.19 Q 239.08,\
                        1.22, 249.38, 1.27 Q 259.69, 1.20, 270.40, 1.60 Q 270.16, 12.70, 269.99, 23.50 Q 270.29, 34.23, 270.16, 45.16 Q 259.79, 45.30,\
                        249.48, 45.71 Q 239.13, 45.89, 228.80, 45.99 Q 218.48, 46.25, 208.16, 45.88 Q 197.85, 45.87, 187.54, 45.31 Q 177.23, 45.80,\
                        166.92, 44.96 Q 156.62, 45.23, 146.31, 45.67 Q 136.00, 45.58, 125.69, 46.25 Q 115.38, 45.90, 105.08, 46.57 Q 94.77, 46.15,\
                        84.46, 45.84 Q 74.15, 45.61, 63.85, 46.00 Q 53.54, 46.57, 43.23, 46.13 Q 32.92, 45.69, 22.62, 45.20 Q 12.31, 46.12, 2.34,\
                        44.66 Q 2.03, 34.24, 2.29, 23.46 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.07, 15.00, 272.23, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.06, 16.00, 272.38, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.97, 17.00, 274.93, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 43.71, 24.69, 43.68 Q 35.04, 43.56, 45.38, 43.94 Q 55.73,\
                        44.51, 66.08, 43.66 Q 76.42, 43.79, 86.77, 44.16 Q 97.12, 45.19, 107.46, 45.84 Q 117.81, 44.88, 128.15, 44.26 Q 138.50, 44.10,\
                        148.85, 44.76 Q 159.19, 44.82, 169.54, 44.76 Q 179.88, 45.39, 190.23, 45.48 Q 200.58, 46.05, 210.92, 45.84 Q 221.27, 45.22,\
                        231.62, 45.20 Q 241.96, 45.07, 252.31, 45.12 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.67, 25.69, 46.16 Q 36.04, 45.98, 46.38, 45.47 Q 56.73,\
                        45.89, 67.08, 45.66 Q 77.42, 45.44, 87.77, 44.85 Q 98.12, 44.37, 108.46, 45.57 Q 118.81, 46.04, 129.15, 45.18 Q 139.50, 44.83,\
                        149.85, 45.48 Q 160.19, 45.57, 170.54, 45.23 Q 180.88, 44.91, 191.23, 44.58 Q 201.58, 45.12, 211.92, 46.22 Q 222.27, 46.81,\
                        232.62, 46.91 Q 242.96, 46.69, 253.31, 45.83 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.87, 26.69, 46.71 Q 37.04, 46.72, 47.38, 47.32 Q 57.73,\
                        47.02, 68.08, 46.95 Q 78.42, 47.41, 88.77, 47.82 Q 99.12, 47.48, 109.46, 47.37 Q 119.81, 47.80, 130.15, 47.72 Q 140.50, 47.14,\
                        150.85, 48.32 Q 161.19, 47.96, 171.54, 48.60 Q 181.88, 49.05, 192.23, 48.34 Q 202.58, 47.93, 212.92, 48.13 Q 223.27, 48.71,\
                        233.62, 49.22 Q 243.96, 48.20, 254.31, 48.94 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-1303829118button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-1303829118button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-1303829118button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Выход<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-1303829118\', \'1149937220\', {"button":"left","id":"954677153","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1292871436","options":"reloadOnly","target":"1168452877","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-429602996" style="position: absolute; left: 0px; top: 928px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="429602996" data-review-reference-id="429602996">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.39, 22.62, 1.34 Q 32.92, 1.20, 43.23, 1.65 Q 53.54, 1.53,\
                        63.85, 0.83 Q 74.15, 1.22, 84.46, 1.80 Q 94.77, 1.69, 105.08, 1.98 Q 115.38, 2.15, 125.69, 1.20 Q 136.00, 1.86, 146.31, 1.49\
                        Q 156.62, 1.68, 166.92, 2.12 Q 177.23, 2.29, 187.54, 1.98 Q 197.85, 1.86, 208.15, 2.87 Q 218.46, 3.00, 228.77, 3.22 Q 239.08,\
                        2.02, 249.38, 1.39 Q 259.69, 1.74, 269.92, 2.08 Q 270.10, 12.72, 270.24, 23.47 Q 269.98, 34.25, 270.63, 45.63 Q 259.90, 45.64,\
                        249.41, 45.18 Q 239.12, 45.73, 228.78, 45.39 Q 218.46, 45.11, 208.16, 45.75 Q 197.85, 45.60, 187.54, 45.06 Q 177.23, 44.40,\
                        166.92, 44.11 Q 156.62, 45.12, 146.31, 44.78 Q 136.00, 44.08, 125.69, 44.04 Q 115.38, 44.75, 105.08, 44.87 Q 94.77, 44.83,\
                        84.46, 45.25 Q 74.15, 44.83, 63.85, 44.86 Q 53.54, 44.08, 43.23, 44.75 Q 32.92, 44.74, 22.62, 45.15 Q 12.31, 45.54, 1.86,\
                        45.14 Q 1.59, 34.39, 2.92, 23.37 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.41, 15.00, 272.44, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.28, 16.00, 273.37, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.64, 17.00, 273.80, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 46.44, 24.69, 46.40 Q 35.04, 46.75, 45.38, 46.86 Q 55.73,\
                        46.96, 66.08, 47.31 Q 76.42, 47.12, 86.77, 46.08 Q 97.12, 46.09, 107.46, 45.97 Q 117.81, 47.42, 128.15, 47.21 Q 138.50, 47.09,\
                        148.85, 47.07 Q 159.19, 46.53, 169.54, 45.31 Q 179.88, 44.39, 190.23, 45.14 Q 200.58, 44.68, 210.92, 44.45 Q 221.27, 44.93,\
                        231.62, 46.65 Q 241.96, 46.87, 252.31, 46.39 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 45.06, 25.69, 44.96 Q 36.04, 44.88, 46.38, 44.69 Q 56.73,\
                        45.65, 67.08, 45.39 Q 77.42, 45.17, 87.77, 45.07 Q 98.12, 45.20, 108.46, 45.94 Q 118.81, 45.74, 129.15, 45.50 Q 139.50, 46.51,\
                        149.85, 45.18 Q 160.19, 45.89, 170.54, 45.36 Q 180.88, 44.85, 191.23, 45.58 Q 201.58, 46.56, 211.92, 46.92 Q 222.27, 47.01,\
                        232.62, 46.66 Q 242.96, 46.38, 253.31, 46.18 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.41, 26.69, 47.39 Q 37.04, 47.24, 47.38, 47.21 Q 57.73,\
                        47.22, 68.08, 46.84 Q 78.42, 47.04, 88.77, 46.97 Q 99.12, 47.07, 109.46, 47.25 Q 119.81, 47.98, 130.15, 46.94 Q 140.50, 47.65,\
                        150.85, 47.04 Q 161.19, 46.85, 171.54, 46.83 Q 181.88, 46.65, 192.23, 46.77 Q 202.58, 47.35, 212.92, 47.57 Q 223.27, 47.42,\
                        233.62, 48.59 Q 243.96, 48.11, 254.31, 46.89 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-429602996button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-429602996button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-429602996button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Настройки<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-429602996\', \'1259313999\', {"button":"left","id":"1798539582","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1306453401","options":"reloadOnly","target":"1985076138","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-2017854117" style="position: absolute; left: 0px; top: 880px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="2017854117" data-review-reference-id="2017854117">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 48px;width:275px;" width="275" height="48">\
                     <svg:g width="275" height="48"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.55, 22.62, 2.00 Q 32.92, 2.31, 43.23, 3.10 Q 53.54, 3.49,\
                        63.85, 2.85 Q 74.15, 2.39, 84.46, 2.18 Q 94.77, 2.96, 105.08, 2.42 Q 115.38, 3.51, 125.69, 3.18 Q 136.00, 3.18, 146.31, 2.46\
                        Q 156.62, 2.16, 166.92, 1.41 Q 177.23, 1.16, 187.54, 1.62 Q 197.85, 1.88, 208.15, 1.64 Q 218.46, 1.58, 228.77, 2.80 Q 239.08,\
                        1.72, 249.38, 1.86 Q 259.69, 1.52, 270.24, 1.76 Q 270.47, 12.09, 271.05, 22.35 Q 270.85, 32.69, 270.55, 43.55 Q 259.99, 43.94,\
                        249.56, 44.31 Q 239.20, 44.91, 228.84, 45.37 Q 218.50, 45.51, 208.17, 45.30 Q 197.85, 44.71, 187.54, 44.43 Q 177.23, 44.81,\
                        166.92, 44.15 Q 156.62, 43.90, 146.31, 44.26 Q 136.00, 42.17, 125.69, 43.11 Q 115.38, 44.34, 105.08, 44.67 Q 94.77, 44.11,\
                        84.46, 43.61 Q 74.15, 43.48, 63.85, 43.24 Q 53.54, 42.51, 43.23, 42.86 Q 32.92, 42.94, 22.62, 42.84 Q 12.31, 42.74, 1.71,\
                        43.29 Q 1.32, 32.98, 2.39, 22.44 Q 2.00, 12.25, 2.00, 2.00" style=" fill:#a7a77c;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.75, 14.50, 273.02, 25.00 Q 271.00, 35.50, 271.00, 46.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 272.30, 15.50, 271.99, 26.00 Q 272.00, 36.50, 272.00, 47.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.54, 16.50, 273.08, 27.00 Q 273.00, 37.50, 273.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 44.00 Q 14.35, 43.71, 24.69, 43.65 Q 35.04, 43.75, 45.38, 44.34 Q 55.73,\
                        43.97, 66.08, 44.08 Q 76.42, 44.37, 86.77, 43.47 Q 97.12, 44.75, 107.46, 43.28 Q 117.81, 42.62, 128.15, 44.56 Q 138.50, 45.05,\
                        148.85, 44.99 Q 159.19, 44.27, 169.54, 43.48 Q 179.88, 44.63, 190.23, 45.39 Q 200.58, 44.34, 210.92, 43.64 Q 221.27, 43.92,\
                        231.62, 43.10 Q 241.96, 43.79, 252.31, 44.81 Q 262.65, 44.00, 273.00, 44.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 45.00 Q 15.35, 43.62, 25.69, 43.58 Q 36.04, 43.44, 46.38, 43.36 Q 56.73,\
                        43.34, 67.08, 43.38 Q 77.42, 43.99, 87.77, 43.74 Q 98.12, 43.72, 108.46, 43.15 Q 118.81, 43.35, 129.15, 43.27 Q 139.50, 43.35,\
                        149.85, 44.01 Q 160.19, 43.90, 170.54, 44.05 Q 180.88, 43.75, 191.23, 43.94 Q 201.58, 44.03, 211.92, 43.19 Q 222.27, 43.54,\
                        232.62, 43.87 Q 242.96, 44.05, 253.31, 43.70 Q 263.65, 45.00, 274.00, 45.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 46.00 Q 16.35, 43.95, 26.69, 44.91 Q 37.04, 45.00, 47.38, 45.69 Q 57.73,\
                        45.79, 68.08, 45.35 Q 78.42, 45.54, 88.77, 45.79 Q 99.12, 45.86, 109.46, 46.10 Q 119.81, 45.58, 130.15, 45.27 Q 140.50, 44.36,\
                        150.85, 44.64 Q 161.19, 44.25, 171.54, 45.17 Q 181.88, 44.92, 192.23, 44.83 Q 202.58, 44.18, 212.92, 45.18 Q 223.27, 45.72,\
                        233.62, 45.20 Q 243.96, 45.50, 254.31, 45.40 Q 264.65, 46.00, 275.00, 46.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-2017854117button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-2017854117button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-2017854117button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:44px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Контакты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-2017854117\', \'1715641519\', {"button":"left","id":"8118238","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"2024017762","options":"reloadOnly","target":"132438759","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-268798418" style="position: absolute; left: 275px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="268798418" data-review-reference-id="268798418">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024">\
                     <svg:g width="601" height="1024"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.29, -0.18, 22.59, -0.34 Q 32.88, -0.31, 43.17, -0.36\
                        Q 53.47, -0.18, 63.76, -0.08 Q 74.05, 0.44, 84.34, -0.13 Q 94.64, -0.12, 104.93, -0.18 Q 115.22, 0.08, 125.52, 0.17 Q 135.81,\
                        1.37, 146.10, 1.55 Q 156.40, 0.93, 166.69, 0.24 Q 176.98, -0.12, 187.28, 0.25 Q 197.57, 0.30, 207.86, 0.40 Q 218.16, 0.55,\
                        228.45, 0.80 Q 238.74, 0.96, 249.03, 0.49 Q 259.33, 1.18, 269.62, 1.90 Q 279.91, 1.36, 290.21, 1.51 Q 300.50, 1.24, 310.79,\
                        0.62 Q 321.09, 0.55, 331.38, 0.41 Q 341.67, 0.26, 351.97, 0.12 Q 362.26, 0.09, 372.55, 0.42 Q 382.84, 0.41, 393.14, 0.12 Q\
                        403.43, -0.10, 413.72, 0.24 Q 424.02, 0.93, 434.31, 1.07 Q 444.60, 1.10, 454.90, 0.95 Q 465.19, 1.07, 475.48, 1.22 Q 485.78,\
                        1.41, 496.07, 0.82 Q 506.36, 0.19, 516.65, 2.29 Q 526.95, 3.01, 537.24, 2.90 Q 547.53, 2.70, 557.83, 1.94 Q 568.12, 2.10,\
                        578.41, 2.14 Q 588.71, 2.36, 598.82, 2.18 Q 599.18, 11.94, 599.39, 21.94 Q 600.04, 31.93, 600.00, 41.97 Q 600.09, 51.98, 600.36,\
                        61.99 Q 600.28, 71.99, 600.68, 82.00 Q 600.89, 92.00, 601.04, 102.00 Q 601.24, 112.00, 600.21, 122.00 Q 600.37, 132.00, 600.22,\
                        142.00 Q 600.12, 152.00, 600.27, 162.00 Q 600.08, 172.00, 600.17, 182.00 Q 600.90, 192.00, 599.80, 202.00 Q 600.73, 212.00,\
                        599.31, 222.00 Q 599.31, 232.00, 599.16, 242.00 Q 599.34, 252.00, 599.09, 262.00 Q 599.45, 272.00, 599.32, 282.00 Q 599.81,\
                        292.00, 599.63, 302.00 Q 599.10, 312.00, 599.81, 322.00 Q 600.30, 332.00, 600.22, 342.00 Q 600.18, 352.00, 599.87, 362.00\
                        Q 600.01, 372.00, 599.58, 382.00 Q 600.30, 392.00, 600.53, 402.00 Q 599.26, 412.00, 598.42, 422.00 Q 598.80, 432.00, 598.21,\
                        442.00 Q 598.62, 452.00, 598.83, 462.00 Q 598.10, 472.00, 598.78, 482.00 Q 599.30, 492.00, 599.16, 502.00 Q 598.86, 512.00,\
                        599.02, 522.00 Q 598.92, 532.00, 599.62, 542.00 Q 599.46, 552.00, 599.27, 562.00 Q 599.39, 572.00, 599.05, 582.00 Q 599.53,\
                        592.00, 598.61, 602.00 Q 598.85, 612.00, 598.61, 622.00 Q 599.17, 632.00, 599.51, 642.00 Q 600.48, 652.00, 599.91, 662.00\
                        Q 599.25, 672.00, 599.50, 682.00 Q 599.74, 692.00, 599.45, 702.00 Q 599.30, 712.00, 599.41, 722.00 Q 599.58, 732.00, 600.52,\
                        742.00 Q 600.65, 752.00, 600.72, 762.00 Q 600.88, 772.00, 600.92, 782.00 Q 601.11, 792.00, 601.36, 802.00 Q 600.80, 812.00,\
                        599.22, 822.00 Q 599.77, 832.00, 599.63, 842.00 Q 598.13, 852.00, 599.35, 862.00 Q 599.23, 872.00, 599.71, 882.00 Q 599.61,\
                        892.00, 599.67, 902.00 Q 598.91, 912.00, 598.74, 922.00 Q 598.63, 932.00, 598.78, 942.00 Q 600.06, 952.00, 600.53, 962.00\
                        Q 599.64, 972.00, 599.19, 982.00 Q 599.05, 992.00, 600.28, 1002.00 Q 599.70, 1012.00, 598.77, 1021.77 Q 588.56, 1021.57, 578.42,\
                        1022.02 Q 568.11, 1021.92, 557.84, 1022.47 Q 547.55, 1022.84, 537.25, 1022.60 Q 526.95, 1021.69, 516.65, 1021.45 Q 506.36,\
                        1022.13, 496.07, 1022.58 Q 485.78, 1022.91, 475.48, 1022.43 Q 465.19, 1022.30, 454.90, 1023.13 Q 444.60, 1023.18, 434.31,\
                        1022.68 Q 424.02, 1021.42, 413.72, 1021.35 Q 403.43, 1022.66, 393.14, 1023.05 Q 382.84, 1023.22, 372.55, 1023.77 Q 362.26,\
                        1023.34, 351.97, 1023.34 Q 341.67, 1023.38, 331.38, 1023.36 Q 321.09, 1023.15, 310.79, 1022.98 Q 300.50, 1022.75, 290.21,\
                        1022.64 Q 279.91, 1021.48, 269.62, 1022.07 Q 259.33, 1022.03, 249.03, 1023.34 Q 238.74, 1023.81, 228.45, 1023.79 Q 218.16,\
                        1023.23, 207.86, 1022.81 Q 197.57, 1022.06, 187.28, 1021.55 Q 176.98, 1021.70, 166.69, 1021.91 Q 156.40, 1021.84, 146.10,\
                        1021.33 Q 135.81, 1020.63, 125.52, 1021.40 Q 115.22, 1022.06, 104.93, 1021.81 Q 94.64, 1021.62, 84.34, 1021.68 Q 74.05, 1022.10,\
                        63.76, 1022.50 Q 53.47, 1022.27, 43.17, 1021.99 Q 32.88, 1022.39, 22.59, 1022.21 Q 12.29, 1022.60, 2.04, 1021.96 Q 1.65, 1012.12,\
                        1.84, 1002.02 Q 1.97, 992.00, 2.44, 981.99 Q 1.89, 972.00, 1.29, 962.01 Q 0.71, 952.01, 2.10, 942.00 Q 2.03, 932.00, 1.88,\
                        922.00 Q 1.68, 912.00, 1.66, 902.00 Q 2.23, 892.00, 2.18, 882.00 Q 1.01, 872.00, 1.79, 862.00 Q 2.90, 852.00, 2.68, 842.00\
                        Q 1.77, 832.00, 2.14, 822.00 Q 2.91, 812.00, 2.91, 802.00 Q 2.54, 792.00, 2.96, 782.00 Q 1.60, 772.00, 2.42, 762.00 Q 1.31,\
                        752.00, 0.05, 742.00 Q 0.96, 732.00, 1.38, 722.00 Q 2.20, 712.00, 2.17, 702.00 Q 1.91, 692.00, 1.82, 682.00 Q 0.94, 672.00,\
                        1.44, 662.00 Q 1.13, 652.00, 2.18, 642.00 Q 2.24, 632.00, 2.56, 622.00 Q 1.90, 612.00, 2.92, 602.00 Q 1.89, 592.00, 1.73,\
                        582.00 Q 2.10, 572.00, 0.39, 562.00 Q 0.03, 552.00, 0.52, 542.00 Q 0.48, 532.00, 0.34, 522.00 Q 0.25, 512.00, 0.05, 502.00\
                        Q 0.10, 492.00, 0.51, 482.00 Q 0.33, 472.00, 0.21, 462.00 Q -0.00, 452.00, -0.38, 442.00 Q -0.18, 432.00, 0.30, 422.00 Q 0.53,\
                        412.00, 2.10, 402.00 Q 2.32, 392.00, 1.17, 382.00 Q 0.40, 372.00, 0.95, 362.00 Q 2.31, 352.00, 3.15, 342.00 Q 1.93, 332.00,\
                        1.25, 322.00 Q 1.72, 312.00, 2.12, 302.00 Q 2.27, 292.00, 1.59, 282.00 Q 1.74, 272.00, 1.60, 262.00 Q 1.73, 252.00, 0.92,\
                        242.00 Q 1.14, 232.00, 1.94, 222.00 Q 1.88, 212.00, 2.16, 202.00 Q 1.48, 192.00, 2.82, 182.00 Q 3.07, 172.00, 2.57, 162.00\
                        Q 1.18, 152.00, 0.65, 142.00 Q 1.03, 132.00, 0.14, 122.00 Q 0.79, 112.00, 0.97, 102.00 Q 2.78, 92.00, 1.87, 82.00 Q 2.47,\
                        72.00, 2.65, 62.00 Q 1.64, 52.00, 0.99, 42.00 Q 0.42, 32.00, 1.08, 22.00 Q 2.00, 12.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 9.06, 9.47, 14.26, 18.03 Q 19.29, 26.70, 24.17, 35.44 Q 29.28,\
                        44.06, 34.26, 52.75 Q 39.01, 61.57, 44.11, 70.19 Q 49.44, 78.68, 54.50, 87.33 Q 59.30, 96.12, 63.89, 105.04 Q 68.28, 114.07,\
                        73.97, 122.35 Q 79.09, 130.96, 84.13, 139.61 Q 89.44, 148.11, 94.65, 156.67 Q 99.32, 165.54, 104.48, 174.12 Q 109.60, 182.73,\
                        114.29, 191.59 Q 118.84, 200.53, 123.30, 209.53 Q 128.96, 217.82, 133.37, 226.85 Q 139.27, 235.00, 144.79, 243.37 Q 150.33,\
                        251.74, 155.43, 260.36 Q 160.46, 269.02, 165.64, 277.59 Q 169.99, 286.65, 175.84, 294.83 Q 181.02, 303.40, 186.05, 312.07\
                        Q 190.79, 320.90, 195.68, 329.64 Q 200.36, 338.51, 205.56, 347.07 Q 210.79, 355.61, 215.92, 364.22 Q 220.90, 372.91, 225.39,\
                        381.88 Q 230.10, 390.73, 234.86, 399.55 Q 239.31, 408.55, 243.80, 417.53 Q 249.22, 425.96, 254.95, 434.21 Q 259.52, 443.14,\
                        264.29, 451.96 Q 269.59, 460.46, 275.63, 468.53 Q 281.02, 476.98, 286.10, 485.61 Q 291.09, 494.30, 296.77, 502.58 Q 301.91,\
                        511.18, 306.60, 520.03 Q 311.59, 528.72, 317.15, 537.07 Q 321.78, 545.96, 327.56, 554.19 Q 332.19, 563.09, 336.32, 572.27\
                        Q 341.74, 580.70, 346.87, 589.31 Q 352.07, 597.87, 357.20, 606.47 Q 362.27, 615.11, 367.83, 623.46 Q 372.63, 632.26, 377.33,\
                        641.11 Q 382.12, 649.91, 387.09, 658.61 Q 391.80, 667.46, 396.61, 676.25 Q 401.41, 685.05, 406.75, 693.52 Q 412.76, 701.61,\
                        417.36, 710.53 Q 422.31, 719.23, 427.48, 727.81 Q 432.54, 736.45, 438.11, 744.80 Q 443.20, 753.42, 448.12, 762.15 Q 453.13,\
                        770.83, 458.24, 779.44 Q 463.22, 788.13, 468.51, 796.64 Q 472.84, 805.71, 477.54, 814.56 Q 481.64, 823.77, 487.10, 832.18\
                        Q 492.04, 840.89, 496.81, 849.71 Q 503.67, 857.29, 508.92, 865.83 Q 514.27, 874.30, 519.04, 883.12 Q 523.87, 891.89, 528.65,\
                        900.70 Q 533.36, 909.55, 538.26, 918.29 Q 543.74, 926.69, 548.79, 935.33 Q 553.76, 944.03, 558.95, 952.60 Q 563.96, 961.27,\
                        569.32, 969.74 Q 573.78, 978.74, 579.41, 987.04 Q 585.16, 995.28, 590.18, 1003.95 Q 593.94, 1013.35, 599.00, 1022.00" style="\
                        fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 1022.00 Q 5.28, 1012.30, 10.50, 1003.74 Q 15.61, 995.12, 20.98, 986.65\
                        Q 26.17, 978.07, 31.19, 969.39 Q 36.54, 960.91, 42.42, 952.74 Q 47.41, 944.04, 52.44, 935.37 Q 57.27, 926.58, 62.29, 917.91\
                        Q 67.19, 909.16, 72.42, 900.60 Q 78.11, 892.32, 83.15, 883.65 Q 88.08, 874.93, 93.31, 866.37 Q 98.27, 857.66, 103.18, 848.92\
                        Q 107.73, 839.96, 112.96, 831.41 Q 118.16, 822.84, 123.19, 814.17 Q 128.15, 805.45, 133.59, 797.03 Q 138.37, 788.21, 143.91,\
                        779.83 Q 149.54, 771.52, 154.81, 762.98 Q 159.58, 754.16, 164.77, 745.58 Q 169.46, 736.72, 174.73, 728.18 Q 180.56, 719.98,\
                        185.20, 711.08 Q 189.81, 702.16, 195.47, 693.86 Q 200.39, 685.13, 204.97, 676.19 Q 209.71, 667.35, 214.46, 658.51 Q 219.61,\
                        649.91, 225.35, 641.66 Q 230.19, 632.88, 235.68, 624.48 Q 241.21, 616.10, 245.14, 606.78 Q 250.36, 598.22, 254.99, 589.31\
                        Q 260.60, 580.98, 266.19, 572.64 Q 270.72, 563.68, 276.10, 555.21 Q 281.39, 546.69, 286.44, 538.03 Q 290.82, 528.98, 295.94,\
                        520.36 Q 300.25, 511.27, 306.24, 503.16 Q 310.66, 494.13, 316.14, 485.72 Q 321.26, 477.11, 325.94, 468.23 Q 331.08, 459.62,\
                        335.64, 450.67 Q 341.50, 442.49, 346.22, 433.64 Q 352.50, 425.70, 357.75, 417.16 Q 362.70, 408.44, 367.44, 399.60 Q 371.90,\
                        390.60, 377.35, 382.17 Q 382.54, 373.59, 387.70, 364.99 Q 392.08, 355.94, 397.78, 347.67 Q 402.14, 338.60, 407.12, 329.90\
                        Q 412.25, 321.29, 418.03, 313.06 Q 423.27, 304.51, 427.85, 295.57 Q 433.37, 287.19, 438.02, 278.29 Q 443.71, 270.01, 448.64,\
                        261.28 Q 453.80, 252.69, 458.58, 243.87 Q 463.74, 235.27, 469.17, 226.84 Q 474.29, 218.22, 479.77, 209.81 Q 483.69, 200.49,\
                        488.35, 191.60 Q 493.70, 183.12, 498.88, 174.54 Q 503.68, 165.73, 508.88, 157.16 Q 514.30, 148.71, 519.65, 140.23 Q 524.84,\
                        131.66, 530.53, 123.37 Q 535.21, 114.49, 540.22, 105.81 Q 546.22, 97.71, 550.86, 88.81 Q 555.63, 79.98, 561.24, 71.65 Q 566.81,\
                        63.30, 571.76, 54.58 Q 574.99, 44.85, 580.74, 36.60 Q 585.97, 28.05, 590.76, 19.24 Q 595.92, 10.65, 601.00, 2.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-241496571" style="position: absolute; left: 276px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="241496571" data-review-reference-id="241496571">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:599px;" width="599" height="80">\
                     <svg:g width="599" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.26, -0.17, 22.52, -0.36 Q 32.78, -0.33, 43.03, -0.13 Q 53.29,\
                        -0.17, 63.55, -0.08 Q 73.81, 0.26, 84.07, 0.15 Q 94.33, -0.10, 104.59, -0.18 Q 114.84, 0.44, 125.10, 0.36 Q 135.36, 0.73,\
                        145.62, 0.81 Q 155.88, 0.72, 166.14, 0.66 Q 176.40, 0.36, 186.66, 0.28 Q 196.91, 0.62, 207.17, 0.49 Q 217.43, 0.55, 227.69,\
                        1.32 Q 237.95, 1.48, 248.21, 1.16 Q 258.47, 1.76, 268.72, 1.25 Q 278.98, 1.17, 289.24, 0.83 Q 299.50, 0.55, 309.76, 0.46 Q\
                        320.02, 0.53, 330.28, 0.28 Q 340.53, -0.05, 350.79, 0.02 Q 361.05, 0.25, 371.31, 0.19 Q 381.57, 0.57, 391.83, 0.61 Q 402.09,\
                        1.01, 412.34, 0.50 Q 422.60, 0.62, 432.86, 0.68 Q 443.12, 0.73, 453.38, 1.97 Q 463.64, 2.47, 473.90, 1.95 Q 484.15, 2.26,\
                        494.41, 1.41 Q 504.67, 1.99, 514.93, 2.54 Q 525.19, 1.92, 535.45, 1.67 Q 545.71, 1.19, 555.97, 1.68 Q 566.22, 1.85, 576.48,\
                        1.47 Q 586.74, 2.28, 597.36, 1.64 Q 597.75, 14.42, 597.89, 27.21 Q 598.33, 39.91, 598.01, 52.63 Q 597.75, 65.32, 597.85, 78.85\
                        Q 587.19, 79.35, 576.72, 79.69 Q 566.34, 79.79, 556.02, 79.67 Q 545.73, 79.75, 535.45, 78.74 Q 525.19, 79.18, 514.93, 79.36\
                        Q 504.67, 78.49, 494.41, 78.40 Q 484.15, 78.61, 473.90, 78.95 Q 463.64, 78.94, 453.38, 77.43 Q 443.12, 77.76, 432.86, 78.62\
                        Q 422.60, 78.26, 412.34, 78.61 Q 402.09, 78.78, 391.83, 78.68 Q 381.57, 78.80, 371.31, 78.72 Q 361.05, 78.76, 350.79, 78.25\
                        Q 340.53, 78.60, 330.28, 77.99 Q 320.02, 78.48, 309.76, 78.66 Q 299.50, 79.05, 289.24, 77.57 Q 278.98, 76.60, 268.72, 78.79\
                        Q 258.47, 78.68, 248.21, 78.92 Q 237.95, 77.53, 227.69, 78.21 Q 217.43, 78.19, 207.17, 79.03 Q 196.91, 78.90, 186.66, 78.16\
                        Q 176.40, 78.35, 166.14, 77.97 Q 155.88, 78.10, 145.62, 77.56 Q 135.36, 77.58, 125.10, 77.56 Q 114.84, 78.14, 104.59, 78.38\
                        Q 94.33, 77.82, 84.07, 77.29 Q 73.81, 77.87, 63.55, 78.42 Q 53.29, 78.66, 43.03, 78.64 Q 32.78, 78.22, 22.52, 78.76 Q 12.26,\
                        78.76, 1.42, 78.58 Q 1.49, 65.50, 1.53, 52.73 Q 1.36, 40.04, 1.72, 27.34 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-328457484" style="position: absolute; left: 275px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="328457484" data-review-reference-id="328457484">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:600px;" width="600" height="80">\
                     <svg:g width="600" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.79, 22.55, 1.13 Q 32.83, 1.78, 43.10, 1.12 Q 53.38, 1.69,\
                        63.66, 2.31 Q 73.93, 1.86, 84.21, 1.56 Q 94.48, 1.17, 104.76, 1.01 Q 115.03, 1.34, 125.31, 0.57 Q 135.59, 0.12, 145.86, 0.12\
                        Q 156.14, 0.53, 166.41, 1.05 Q 176.69, 0.56, 186.97, 1.58 Q 197.24, 1.39, 207.52, 1.20 Q 217.79, 1.34, 228.07, 1.57 Q 238.34,\
                        2.09, 248.62, 0.84 Q 258.90, 0.26, 269.17, 0.32 Q 279.45, 0.17, 289.72, 0.13 Q 300.00, 0.50, 310.28, 0.61 Q 320.55, 0.83,\
                        330.83, 1.17 Q 341.10, 1.42, 351.38, 0.81 Q 361.66, 1.35, 371.93, 2.29 Q 382.21, 2.51, 392.48, 2.46 Q 402.76, 2.12, 413.03,\
                        2.00 Q 423.31, 2.29, 433.59, 2.25 Q 443.86, 2.55, 454.14, 2.40 Q 464.41, 2.51, 474.69, 3.01 Q 484.97, 2.34, 495.24, 1.69 Q\
                        505.52, 1.42, 515.79, 0.91 Q 526.07, 0.83, 536.34, 0.86 Q 546.62, 0.82, 556.90, 0.98 Q 567.17, 0.25, 577.45, 0.99 Q 587.72,\
                        0.04, 598.81, 1.19 Q 599.14, 14.29, 599.52, 27.12 Q 599.53, 39.90, 599.92, 52.60 Q 600.11, 65.30, 599.22, 79.22 Q 588.33,\
                        79.85, 577.69, 79.69 Q 567.26, 79.28, 556.94, 79.32 Q 546.64, 78.97, 536.35, 78.84 Q 526.07, 78.99, 515.79, 78.48 Q 505.52,\
                        79.01, 495.24, 79.18 Q 484.97, 79.29, 474.69, 78.59 Q 464.41, 78.93, 454.14, 78.46 Q 443.86, 79.60, 433.59, 79.32 Q 423.31,\
                        79.87, 413.03, 79.26 Q 402.76, 78.97, 392.48, 79.08 Q 382.21, 79.04, 371.93, 78.81 Q 361.66, 78.86, 351.38, 79.47 Q 341.10,\
                        79.37, 330.83, 79.55 Q 320.55, 78.04, 310.28, 78.44 Q 300.00, 78.96, 289.72, 78.65 Q 279.45, 78.89, 269.17, 78.61 Q 258.90,\
                        77.91, 248.62, 78.20 Q 238.34, 78.50, 228.07, 78.31 Q 217.79, 78.48, 207.52, 78.59 Q 197.24, 78.55, 186.97, 78.64 Q 176.69,\
                        78.08, 166.41, 78.13 Q 156.14, 77.97, 145.86, 78.03 Q 135.59, 78.51, 125.31, 79.56 Q 115.03, 79.03, 104.76, 78.41 Q 94.48,\
                        79.33, 84.21, 79.69 Q 73.93, 79.77, 63.66, 79.30 Q 53.38, 79.88, 43.10, 79.15 Q 32.83, 79.24, 22.55, 78.98 Q 12.28, 78.97,\
                        1.60, 78.40 Q 1.42, 65.53, 0.68, 52.86 Q 1.08, 40.06, 0.59, 27.38 Q 2.00, 14.67, 2.00, 2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-1544648757" style="position: absolute; left: 425px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1544648757" data-review-reference-id="1544648757">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60">\
                     <svg:g width="200" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.89, 3.22, 23.78, 3.30 Q 34.67, 3.37, 45.56, 2.96 Q\
                        56.44, 1.90, 67.33, 2.93 Q 78.22, 2.69, 89.11, 2.60 Q 100.00, 1.09, 110.89, 1.19 Q 121.78, 2.36, 132.67, 2.52 Q 143.56, 1.76,\
                        154.44, 1.29 Q 165.33, 1.86, 176.22, 2.00 Q 187.11, 1.50, 198.25, 1.75 Q 198.56, 15.81, 198.41, 29.94 Q 197.73, 44.02, 197.92,\
                        57.92 Q 187.22, 58.35, 176.19, 57.80 Q 165.38, 58.70, 154.45, 58.25 Q 143.56, 58.33, 132.67, 58.30 Q 121.78, 58.17, 110.89,\
                        58.06 Q 100.00, 58.41, 89.11, 58.68 Q 78.22, 58.02, 67.33, 58.39 Q 56.44, 57.75, 45.56, 58.11 Q 34.67, 58.58, 23.78, 58.91\
                        Q 12.89, 57.72, 2.09, 57.91 Q 1.49, 44.17, 2.45, 29.94 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.32, 2.98, 22.17, 5.60 Q 31.95, 8.48, 41.68, 11.51 Q 51.48,\
                        14.32, 61.27, 17.16 Q 70.99, 20.23, 80.79, 23.05 Q 90.63, 25.70, 100.30, 28.95 Q 110.13, 31.66, 119.96, 34.35 Q 129.63, 37.60,\
                        139.47, 40.25 Q 149.02, 43.93, 159.01, 46.08 Q 168.72, 49.19, 178.54, 51.91 Q 188.20, 55.20, 198.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 11.64, 54.27, 21.60, 51.69 Q 31.56, 49.10, 41.36, 45.96 Q 51.56,\
                        44.22, 61.37, 41.11 Q 70.88, 36.90, 80.58, 33.41 Q 90.54, 30.81, 100.52, 28.30 Q 110.39, 25.39, 120.47, 23.24 Q 130.45, 20.72,\
                        140.18, 17.31 Q 149.99, 14.18, 159.89, 11.39 Q 169.79, 8.60, 179.73, 5.93 Q 190.10, 4.80, 200.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-1534176254" style="position: absolute; left: 377px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1534176254" data-review-reference-id="1534176254">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-2088991830" style="position: absolute; left: 275px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="2088991830" data-review-reference-id="2088991830">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 80px;width:86px;" width="86" height="80">\
                     <svg:g width="86" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 15.17, 1.27, 28.33, 1.63 Q 41.50, 1.45, 54.67, 2.35 Q 67.83, 3.00,\
                        81.21, 1.79 Q 80.49, 14.34, 80.33, 26.43 Q 80.00, 38.57, 80.71, 50.68 Q 81.94, 62.82, 81.46, 75.46 Q 67.81, 74.92, 54.60,\
                        74.52 Q 41.48, 74.63, 28.32, 74.63 Q 15.16, 74.81, 2.05, 74.95 Q 1.82, 62.89, 1.85, 50.69 Q 0.92, 38.57, 1.06, 26.36 Q 2.00,\
                        14.17, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 82.00, 4.00 Q 82.32, 16.33, 82.33, 28.67 Q 82.35, 41.00, 82.20, 53.33 Q 82.00,\
                        65.67, 82.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 83.00, 5.00 Q 80.78, 17.33, 81.62, 29.67 Q 81.72, 42.00, 83.00, 54.33 Q 83.00,\
                        66.67, 83.00, 79.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 84.00, 6.00 Q 84.96, 18.33, 84.54, 30.67 Q 84.40, 43.00, 84.48, 55.33 Q 84.00,\
                        67.67, 84.00, 80.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 76.00 Q 14.00, 76.32, 24.00, 75.84 Q 34.00, 75.62, 44.00, 75.15 Q 54.00,\
                        75.65, 64.00, 75.51 Q 74.00, 76.00, 84.00, 76.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 77.00 Q 15.00, 74.66, 25.00, 74.57 Q 35.00, 75.44, 45.00, 75.10 Q 55.00,\
                        74.96, 65.00, 76.56 Q 75.00, 77.00, 85.00, 77.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 78.00 Q 16.00, 79.06, 26.00, 78.66 Q 36.00, 78.42, 46.00, 78.46 Q 56.00,\
                        77.40, 66.00, 77.86 Q 76.00, 78.00, 86.00, 78.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page620342037-layer-2088991830button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page620342037-layer-2088991830button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page620342037-layer-2088991830button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:82px;height:76px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				=<br />  \
                     			</button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-77139831" style="position: absolute; left: 275px; top: 115px; width: 600px; height: 795px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="77139831" data-review-reference-id="77139831">\
            <div class="stencil-wrapper" style="width: 600px; height: 795px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 795px;width:600px;" width="600" height="795">\
                     <svg:g width="600" height="795"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.28, 1.32, 22.55, 1.65 Q 32.83, 1.43, 43.10, 1.99 Q 53.38, 1.16,\
                        63.66, 0.61 Q 73.93, 1.44, 84.21, 1.70 Q 94.48, 1.88, 104.76, 1.68 Q 115.03, 2.10, 125.31, 1.41 Q 135.59, 2.15, 145.86, 2.06\
                        Q 156.14, 1.96, 166.41, 2.16 Q 176.69, 2.28, 186.97, 2.02 Q 197.24, 1.09, 207.52, 1.57 Q 217.79, 1.93, 228.07, 2.18 Q 238.34,\
                        1.83, 248.62, 1.46 Q 258.90, 1.78, 269.17, 2.35 Q 279.45, 2.78, 289.72, 2.60 Q 300.00, 2.34, 310.28, 1.86 Q 320.55, 2.59,\
                        330.83, 2.14 Q 341.10, 2.27, 351.38, 2.63 Q 361.66, 3.21, 371.93, 2.76 Q 382.21, 1.52, 392.48, 1.49 Q 402.76, 0.74, 413.03,\
                        1.65 Q 423.31, 1.12, 433.59, 0.94 Q 443.86, 1.00, 454.14, 1.18 Q 464.41, 2.01, 474.69, 1.33 Q 484.97, 1.64, 495.24, 1.14 Q\
                        505.52, 1.92, 515.79, 0.70 Q 526.07, 0.23, 536.34, 1.03 Q 546.62, 2.73, 556.90, 2.01 Q 567.17, 1.36, 577.45, 1.44 Q 587.72,\
                        0.94, 598.37, 1.63 Q 599.13, 11.76, 599.36, 22.09 Q 598.62, 32.38, 598.09, 42.56 Q 598.24, 52.70, 598.36, 62.84 Q 597.63,\
                        72.99, 597.38, 83.13 Q 597.03, 93.27, 598.00, 103.41 Q 597.83, 113.55, 597.62, 123.69 Q 597.42, 133.83, 597.87, 143.97 Q 598.26,\
                        154.12, 598.47, 164.26 Q 598.67, 174.40, 598.12, 184.54 Q 598.83, 194.68, 599.52, 204.82 Q 599.35, 214.96, 598.58, 225.10\
                        Q 598.83, 235.24, 598.72, 245.38 Q 599.76, 255.53, 599.19, 265.67 Q 599.50, 275.81, 599.52, 285.95 Q 599.56, 296.09, 599.13,\
                        306.23 Q 597.05, 316.37, 597.95, 326.51 Q 599.23, 336.65, 598.34, 346.79 Q 598.20, 356.94, 598.39, 367.08 Q 598.96, 377.22,\
                        599.08, 387.36 Q 598.04, 397.50, 597.72, 407.64 Q 598.58, 417.78, 598.94, 427.92 Q 600.02, 438.06, 599.78, 448.20 Q 600.08,\
                        458.35, 599.13, 468.49 Q 596.57, 478.63, 596.59, 488.77 Q 597.16, 498.91, 597.93, 509.05 Q 598.75, 519.19, 598.55, 529.33\
                        Q 598.55, 539.47, 598.12, 549.62 Q 598.35, 559.76, 599.08, 569.90 Q 598.48, 580.04, 598.48, 590.18 Q 599.57, 600.32, 599.03,\
                        610.46 Q 599.83, 620.60, 599.38, 630.74 Q 599.66, 640.88, 599.08, 651.03 Q 599.25, 661.17, 599.17, 671.31 Q 599.12, 681.45,\
                        599.45, 691.59 Q 599.92, 701.73, 599.22, 711.87 Q 598.48, 722.01, 598.46, 732.15 Q 599.05, 742.30, 598.38, 752.44 Q 598.09,\
                        762.58, 597.43, 772.72 Q 598.01, 782.86, 598.36, 793.36 Q 587.65, 792.79, 577.39, 792.61 Q 567.15, 792.62, 556.87, 792.11\
                        Q 546.62, 792.94, 536.34, 792.27 Q 526.07, 793.39, 515.79, 793.54 Q 505.52, 793.77, 495.24, 793.45 Q 484.97, 793.31, 474.69,\
                        793.48 Q 464.41, 794.10, 454.14, 794.64 Q 443.86, 794.49, 433.59, 794.01 Q 423.31, 793.83, 413.03, 794.02 Q 402.76, 794.16,\
                        392.48, 793.89 Q 382.21, 794.18, 371.93, 793.64 Q 361.66, 794.27, 351.38, 794.05 Q 341.10, 793.89, 330.83, 794.40 Q 320.55,\
                        793.96, 310.28, 793.45 Q 300.00, 794.36, 289.72, 794.92 Q 279.45, 794.53, 269.17, 794.05 Q 258.90, 794.19, 248.62, 794.33\
                        Q 238.34, 793.93, 228.07, 793.89 Q 217.79, 794.21, 207.52, 794.20 Q 197.24, 793.89, 186.97, 794.22 Q 176.69, 793.54, 166.41,\
                        792.13 Q 156.14, 791.34, 145.86, 792.78 Q 135.59, 793.97, 125.31, 794.19 Q 115.03, 793.63, 104.76, 793.03 Q 94.48, 793.51,\
                        84.21, 793.96 Q 73.93, 793.87, 63.66, 794.11 Q 53.38, 793.74, 43.10, 793.42 Q 32.83, 793.55, 22.55, 793.72 Q 12.28, 793.60,\
                        1.70, 793.30 Q 1.71, 782.96, 1.06, 772.85 Q 1.17, 762.63, 0.71, 752.48 Q 1.87, 742.30, 1.82, 732.16 Q 1.23, 722.02, 1.15,\
                        711.87 Q 1.16, 701.73, 0.96, 691.59 Q 0.85, 681.45, 0.65, 671.31 Q 0.77, 661.17, 1.63, 651.03 Q 2.80, 640.88, 2.92, 630.74\
                        Q 3.00, 620.60, 2.09, 610.46 Q 1.38, 600.32, 1.22, 590.18 Q 1.61, 580.04, 1.26, 569.90 Q 1.36, 559.76, 2.14, 549.62 Q 2.84,\
                        539.47, 1.43, 529.33 Q 0.76, 519.19, 0.27, 509.05 Q 1.27, 498.91, 1.05, 488.77 Q 0.87, 478.63, 1.10, 468.49 Q 0.83, 458.35,\
                        0.38, 448.20 Q -0.13, 438.06, 1.13, 427.92 Q 1.76, 417.78, 0.85, 407.64 Q -0.05, 397.50, -0.27, 387.36 Q 0.02, 377.22, 0.89,\
                        367.08 Q -0.28, 356.94, -0.25, 346.79 Q 0.81, 336.65, 0.49, 326.51 Q 0.82, 316.37, 0.30, 306.23 Q 0.66, 296.09, 0.93, 285.95\
                        Q 0.72, 275.81, 1.44, 265.67 Q 1.02, 255.53, 0.81, 245.38 Q 0.73, 235.24, 0.69, 225.10 Q 0.69, 214.96, 0.59, 204.82 Q 0.69,\
                        194.68, 0.87, 184.54 Q 0.54, 174.40, 1.01, 164.26 Q 0.96, 154.12, 0.81, 143.97 Q 0.73, 133.83, 0.57, 123.69 Q 0.97, 113.55,\
                        0.74, 103.41 Q 1.41, 93.27, 2.32, 83.13 Q 2.00, 72.99, 2.26, 62.85 Q 1.63, 52.71, 2.31, 42.56 Q 2.26, 32.42, 1.65, 22.28 Q\
                        2.00, 12.14, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-337237750" style="position: absolute; left: 295px; top: 360px; width: 570px; height: 114px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="337237750" data-review-reference-id="337237750">\
            <div class="stencil-wrapper" style="width: 570px; height: 114px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textblock2"><p style="font-size: 14px;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt\
                        ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet\
                        clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing\
                        elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam\
                        et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est. Lorem ipsum dolor sit amet.</p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-313390980" style="position: absolute; left: 290px; top: 155px; width: 570px; height: 175px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="313390980" data-review-reference-id="313390980">\
            <div class="stencil-wrapper" style="width: 570px; height: 175px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 175px;width:570px;" width="570" height="175">\
                     <svg:g width="570" height="175"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.11, 1.84, 22.21, 1.79 Q 32.32, 1.64, 42.43, 1.58 Q\
                        52.54, 1.48, 62.64, 1.24 Q 72.75, 1.54, 82.86, 1.23 Q 92.96, 0.96, 103.07, 1.65 Q 113.18, 1.59, 123.29, 1.51 Q 133.39, 1.61,\
                        143.50, 1.83 Q 153.61, 2.02, 163.71, 0.69 Q 173.82, 1.31, 183.93, 0.86 Q 194.04, 2.03, 204.14, 1.81 Q 214.25, 1.13, 224.36,\
                        1.88 Q 234.46, 1.77, 244.57, 1.57 Q 254.68, 0.99, 264.79, 1.52 Q 274.89, 1.51, 285.00, 1.94 Q 295.11, 2.88, 305.21, 2.54 Q\
                        315.32, 1.24, 325.43, 0.15 Q 335.54, -0.14, 345.64, 1.01 Q 355.75, 1.36, 365.86, 0.40 Q 375.96, 1.37, 386.07, 2.57 Q 396.18,\
                        2.80, 406.29, 2.16 Q 416.39, 0.50, 426.50, 0.55 Q 436.61, 0.75, 446.71, 1.51 Q 456.82, 1.69, 466.93, 1.50 Q 477.04, 2.08,\
                        487.14, 1.95 Q 497.25, 2.14, 507.36, 1.03 Q 517.46, 1.55, 527.57, 1.27 Q 537.68, 1.34, 547.79, 1.13 Q 557.89, 1.23, 567.98,\
                        2.02 Q 568.41, 12.55, 568.01, 23.37 Q 568.38, 34.04, 568.48, 44.73 Q 569.01, 55.42, 569.80, 66.11 Q 569.15, 76.81, 569.41,\
                        87.50 Q 570.11, 98.19, 569.97, 108.87 Q 569.27, 119.56, 568.35, 130.25 Q 567.07, 140.94, 567.77, 151.62 Q 568.58, 162.31,\
                        568.35, 173.35 Q 558.13, 173.70, 547.81, 173.19 Q 537.72, 173.68, 527.58, 173.39 Q 517.48, 174.16, 507.36, 173.88 Q 497.25,\
                        173.98, 487.14, 173.78 Q 477.04, 173.13, 466.93, 174.59 Q 456.82, 174.92, 446.71, 174.19 Q 436.61, 173.05, 426.50, 174.29\
                        Q 416.39, 173.71, 406.29, 173.51 Q 396.18, 173.08, 386.07, 173.39 Q 375.96, 173.66, 365.86, 173.52 Q 355.75, 173.75, 345.64,\
                        173.57 Q 335.54, 173.95, 325.43, 174.12 Q 315.32, 173.50, 305.21, 173.60 Q 295.11, 174.06, 285.00, 174.93 Q 274.89, 174.68,\
                        264.79, 173.63 Q 254.68, 173.27, 244.57, 173.98 Q 234.46, 174.19, 224.36, 174.16 Q 214.25, 174.37, 204.14, 174.29 Q 194.04,\
                        174.31, 183.93, 174.10 Q 173.82, 174.37, 163.71, 173.58 Q 153.61, 173.38, 143.50, 174.54 Q 133.39, 173.23, 123.29, 173.32\
                        Q 113.18, 173.57, 103.07, 173.51 Q 92.96, 173.75, 82.86, 172.62 Q 72.75, 173.33, 62.64, 173.16 Q 52.54, 174.65, 42.43, 174.82\
                        Q 32.32, 174.61, 22.21, 173.70 Q 12.11, 173.70, 1.72, 173.28 Q 1.10, 162.61, 0.81, 151.79 Q 0.98, 141.01, 0.35, 130.30 Q 0.31,\
                        119.59, -0.23, 108.89 Q -0.25, 98.20, -0.40, 87.50 Q 0.09, 76.81, 1.30, 66.13 Q 1.39, 55.44, 0.66, 44.75 Q 0.06, 34.06, -0.09,\
                        23.38 Q 2.00, 12.69, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 11.99, 4.17, 21.57, 7.73 Q 31.39, 10.46, 40.91, 14.20 Q 51.12,\
                        15.64, 60.84, 18.75 Q 70.41, 22.30, 79.99, 25.86 Q 89.94, 28.15, 99.69, 31.15 Q 109.40, 34.25, 119.35, 36.55 Q 129.02, 39.81,\
                        138.74, 42.88 Q 148.59, 45.51, 158.26, 48.78 Q 167.98, 51.85, 177.94, 54.11 Q 187.77, 56.84, 197.31, 60.52 Q 207.16, 63.17,\
                        216.62, 67.09 Q 226.81, 68.61, 236.64, 71.33 Q 246.29, 74.63, 256.07, 77.52 Q 266.08, 79.61, 275.63, 83.25 Q 285.39, 86.20,\
                        295.39, 88.36 Q 304.90, 92.14, 314.73, 94.85 Q 324.44, 97.95, 334.14, 101.10 Q 343.89, 104.07, 353.82, 106.44 Q 363.61, 109.29,\
                        373.03, 113.35 Q 382.71, 116.59, 392.48, 119.47 Q 402.48, 121.62, 412.25, 124.53 Q 421.82, 128.11, 431.30, 131.98 Q 441.29,\
                        134.17, 451.29, 136.32 Q 461.12, 139.04, 470.86, 142.05 Q 480.50, 145.37, 490.56, 147.34 Q 500.16, 150.82, 510.05, 153.33\
                        Q 519.80, 156.29, 529.55, 159.28 Q 539.11, 162.89, 548.50, 167.06 Q 558.24, 170.05, 568.00, 173.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 173.00 Q 11.17, 167.98, 20.94, 164.97 Q 30.83, 162.35, 40.62, 159.36\
                        Q 50.44, 156.50, 60.32, 153.84 Q 70.08, 150.81, 79.80, 147.61 Q 89.60, 144.67, 99.44, 141.88 Q 109.30, 139.17, 119.33, 136.99\
                        Q 129.10, 133.97, 138.82, 130.80 Q 148.58, 127.73, 158.26, 124.39 Q 168.03, 121.38, 177.92, 118.73 Q 187.75, 115.91, 197.50,\
                        112.85 Q 207.22, 109.64, 216.99, 106.62 Q 226.86, 103.91, 236.82, 101.54 Q 246.72, 98.95, 256.38, 95.56 Q 266.11, 92.39, 275.98,\
                        89.70 Q 285.65, 86.32, 295.40, 83.23 Q 305.08, 79.94, 314.83, 76.84 Q 324.62, 73.88, 334.43, 70.97 Q 344.22, 68.04, 354.21,\
                        65.73 Q 363.99, 62.75, 373.81, 59.86 Q 383.58, 56.86, 393.42, 54.07 Q 403.03, 50.51, 412.86, 47.67 Q 422.64, 44.69, 432.44,\
                        41.78 Q 442.27, 38.92, 452.27, 36.66 Q 462.18, 34.13, 472.05, 31.41 Q 481.69, 27.96, 491.71, 25.77 Q 501.64, 23.29, 511.10,\
                        19.24 Q 520.92, 16.37, 530.67, 13.27 Q 540.70, 11.10, 550.41, 7.89 Q 560.21, 4.95, 570.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-1589803118" style="position: absolute; left: 290px; top: 525px; width: 183px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1589803118" data-review-reference-id="1589803118">\
            <div class="stencil-wrapper" style="width: 183px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Ваш вопрос</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-1743710341" style="position: absolute; left: 290px; top: 575px; width: 570px; height: 270px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1743710341" data-review-reference-id="1743710341">\
            <div class="stencil-wrapper" style="width: 570px; height: 270px">\
               <div>\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 270px;width:570px;" width="570" height="270">\
                     <svg:g id="__containerId__-page620342037-layer-1743710341svg" width="570" height="270"><svg:path id="__containerId__-page620342037-layer-1743710341_input_svg_border" class=" svg_unselected_element" d="M 2.00,\
                        2.00 Q 12.11, 3.62, 22.21, 3.39 Q 32.32, 2.56, 42.43, 2.79 Q 52.54, 2.63, 62.64, 1.47 Q 72.75, 1.85, 82.86, 1.70 Q 92.96,\
                        2.52, 103.07, 2.54 Q 113.18, 0.74, 123.29, 0.60 Q 133.39, 1.32, 143.50, 1.32 Q 153.61, 0.69, 163.71, 0.21 Q 173.82, 0.48,\
                        183.93, 1.83 Q 194.04, 1.57, 204.14, 2.45 Q 214.25, 0.39, 224.36, 0.34 Q 234.46, 0.30, 244.57, 0.12 Q 254.68, 0.00, 264.79,\
                        0.15 Q 274.89, 0.36, 285.00, 0.18 Q 295.11, 0.03, 305.21, -0.07 Q 315.32, 0.00, 325.43, 0.13 Q 335.54, -0.20, 345.64, -0.22\
                        Q 355.75, 0.72, 365.86, 1.36 Q 375.96, 0.70, 386.07, 1.43 Q 396.18, 2.65, 406.29, 3.32 Q 416.39, 2.50, 426.50, 0.75 Q 436.61,\
                        0.25, 446.71, 0.66 Q 456.82, 0.73, 466.93, 0.70 Q 477.04, 1.47, 487.14, 0.36 Q 497.25, 0.78, 507.36, 0.64 Q 517.46, 0.48,\
                        527.57, 0.88 Q 537.68, 1.28, 547.79, 0.61 Q 557.89, 0.67, 568.75, 1.25 Q 568.48, 12.07, 569.04, 22.31 Q 569.30, 32.61, 569.34,\
                        42.88 Q 569.44, 53.13, 569.22, 63.37 Q 568.09, 73.62, 569.61, 83.84 Q 568.70, 94.08, 569.82, 104.31 Q 569.85, 114.54, 569.20,\
                        124.77 Q 568.86, 135.00, 567.85, 145.23 Q 567.82, 155.46, 567.66, 165.69 Q 568.16, 175.92, 568.73, 186.15 Q 568.69, 196.38,\
                        568.46, 206.62 Q 567.71, 216.85, 567.62, 227.08 Q 567.67, 237.31, 567.69, 247.54 Q 567.30, 257.77, 568.30, 268.30 Q 558.15,\
                        268.77, 547.95, 269.13 Q 537.72, 268.61, 527.59, 268.67 Q 517.48, 268.80, 507.37, 269.26 Q 497.26, 269.45, 487.15, 269.55\
                        Q 477.04, 269.70, 466.93, 269.76 Q 456.82, 269.06, 446.71, 268.31 Q 436.61, 268.63, 426.50, 269.88 Q 416.39, 269.11, 406.29,\
                        269.36 Q 396.18, 269.00, 386.07, 269.62 Q 375.96, 269.56, 365.86, 269.73 Q 355.75, 269.31, 345.64, 269.04 Q 335.54, 268.91,\
                        325.43, 268.54 Q 315.32, 268.94, 305.21, 269.31 Q 295.11, 268.68, 285.00, 268.93 Q 274.89, 268.13, 264.79, 269.32 Q 254.68,\
                        269.20, 244.57, 267.46 Q 234.46, 267.80, 224.36, 268.19 Q 214.25, 269.35, 204.14, 268.94 Q 194.04, 269.16, 183.93, 269.30\
                        Q 173.82, 269.05, 163.71, 269.27 Q 153.61, 268.44, 143.50, 268.83 Q 133.39, 268.31, 123.29, 267.17 Q 113.18, 268.16, 103.07,\
                        269.08 Q 92.96, 268.33, 82.86, 267.26 Q 72.75, 267.68, 62.64, 268.93 Q 52.54, 269.01, 42.43, 269.09 Q 32.32, 269.58, 22.21,\
                        267.91 Q 12.11, 268.05, 2.13, 267.87 Q 2.06, 257.75, 2.51, 247.47 Q 1.84, 237.32, 1.37, 227.10 Q 1.29, 216.86, 0.88, 206.62\
                        Q 1.14, 196.39, 1.07, 186.16 Q 0.63, 175.92, 0.48, 165.69 Q 0.66, 155.46, 0.34, 145.23 Q 0.09, 135.00, 0.29, 124.77 Q 0.74,\
                        114.54, 1.72, 104.31 Q 2.12, 94.08, 1.86, 83.85 Q 1.53, 73.62, 1.48, 63.38 Q 0.63, 53.15, 0.85, 42.92 Q 0.93, 32.69, 1.53,\
                        22.46 Q 2.00, 12.23, 2.00, 2.00" style=" fill:white;"/><svg:path id="__containerId__-page620342037-layer-1743710341_line1" class=" svg_unselected_element" d="M 3.00, 3.00 Q 13.07,\
                        1.05, 23.14, 1.01 Q 33.21, 0.88, 43.29, 0.78 Q 53.36, 0.90, 63.43, 0.98 Q 73.50, 0.94, 83.57, 0.79 Q 93.64, 0.70, 103.71,\
                        1.03 Q 113.79, 1.02, 123.86, 0.96 Q 133.93, 1.35, 144.00, 1.35 Q 154.07, 1.59, 164.14, 0.94 Q 174.21, 0.88, 184.29, 0.72 Q\
                        194.36, 0.94, 204.43, 1.29 Q 214.50, 1.68, 224.57, 1.65 Q 234.64, 2.31, 244.71, 1.48 Q 254.79, 1.51, 264.86, 2.79 Q 274.93,\
                        1.83, 285.00, 2.74 Q 295.07, 2.33, 305.14, 2.74 Q 315.21, 1.60, 325.29, 1.60 Q 335.36, 1.55, 345.43, 1.45 Q 355.50, 1.28,\
                        365.57, 2.04 Q 375.64, 1.54, 385.71, 1.48 Q 395.79, 1.44, 405.86, 1.29 Q 415.93, 1.69, 426.00, 1.79 Q 436.07, 1.20, 446.14,\
                        1.62 Q 456.21, 1.67, 466.29, 2.09 Q 476.36, 1.98, 486.43, 1.42 Q 496.50, 1.51, 506.57, 1.60 Q 516.64, 2.56, 526.71, 4.05 Q\
                        536.79, 2.54, 546.86, 2.52 Q 556.93, 3.00, 567.00, 3.00" style=" fill:none;"/><svg:path id="__containerId__-page620342037-layer-1743710341_line2" class=" svg_unselected_element" d="M 3.00, 3.00 Q 2.98,\
                        13.15, 3.59, 23.31 Q 3.24, 33.46, 4.32, 43.62 Q 3.37, 53.77, 3.00, 63.92 Q 3.24, 74.08, 4.38, 84.23 Q 4.38, 94.38, 4.09, 104.54\
                        Q 4.36, 114.69, 3.93, 124.85 Q 4.57, 135.00, 3.68, 145.15 Q 2.98, 155.31, 3.62, 165.46 Q 4.14, 175.62, 3.91, 185.77 Q 3.65,\
                        195.92, 4.47, 206.08 Q 4.65, 216.23, 4.72, 226.38 Q 3.03, 236.54, 3.51, 246.69 Q 3.00, 256.85, 3.00, 267.00" style=" fill:none;"/><svg:path id="__containerId__-page620342037-layer-1743710341_line3" class=" svg_unselected_element" d="M 3.00, 3.00 Q 13.07,\
                        3.29, 23.14, 3.17 Q 33.21, 3.23, 43.29, 3.23 Q 53.36, 3.26, 63.43, 3.30 Q 73.50, 3.66, 83.57, 3.20 Q 93.64, 4.34, 103.71,\
                        3.72 Q 113.79, 4.45, 123.86, 3.92 Q 133.93, 3.78, 144.00, 3.83 Q 154.07, 3.22, 164.14, 2.53 Q 174.21, 2.72, 184.29, 4.03 Q\
                        194.36, 3.40, 204.43, 3.39 Q 214.50, 3.64, 224.57, 3.97 Q 234.64, 3.87, 244.71, 2.48 Q 254.79, 2.71, 264.86, 2.51 Q 274.93,\
                        3.97, 285.00, 2.43 Q 295.07, 2.46, 305.14, 2.13 Q 315.21, 2.08, 325.29, 1.82 Q 335.36, 1.79, 345.43, 2.01 Q 355.50, 2.40,\
                        365.57, 3.56 Q 375.64, 2.90, 385.71, 2.65 Q 395.79, 2.47, 405.86, 3.28 Q 415.93, 4.09, 426.00, 3.67 Q 436.07, 3.65, 446.14,\
                        3.14 Q 456.21, 3.06, 466.29, 3.06 Q 476.36, 3.49, 486.43, 3.01 Q 496.50, 3.09, 506.57, 3.55 Q 516.64, 2.91, 526.71, 2.53 Q\
                        536.79, 2.73, 546.86, 3.66 Q 556.93, 3.00, 567.00, 3.00" style=" fill:none;"/><svg:path id="__containerId__-page620342037-layer-1743710341_line4" class=" svg_unselected_element" d="M 3.00, 3.00 Q 4.99,\
                        13.15, 5.09, 23.31 Q 5.17, 33.46, 4.89, 43.62 Q 5.07, 53.77, 5.04, 63.92 Q 4.63, 74.08, 4.55, 84.23 Q 4.26, 94.38, 4.08, 104.54\
                        Q 4.29, 114.69, 4.50, 124.85 Q 4.38, 135.00, 4.20, 145.15 Q 4.11, 155.31, 4.08, 165.46 Q 3.91, 175.62, 3.63, 185.77 Q 4.08,\
                        195.92, 2.98, 206.08 Q 2.73, 216.23, 3.39, 226.38 Q 3.71, 236.54, 3.71, 246.69 Q 3.00, 256.85, 3.00, 267.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div style="position:absolute;left: 6px; top: 1px;" title=""><textarea id="__containerId__-page620342037-layer-1743710341input" onblur="rabbit.facade.raiseEvent(rabbit.events.svgBlur, new Array(\'__containerId__-page620342037-layer-1743710341_input_svg_border\',\'__containerId__-page620342037-layer-1743710341_line1\',\'__containerId__-page620342037-layer-1743710341_line2\',\'__containerId__-page620342037-layer-1743710341_line3\',\'__containerId__-page620342037-layer-1743710341_line4\'))" onfocus="rabbit.facade.raiseEvent(rabbit.events.svgFocus, new Array(\'__containerId__-page620342037-layer-1743710341_input_svg_border\',\'__containerId__-page620342037-layer-1743710341_line1\',\'__containerId__-page620342037-layer-1743710341_line2\',\'__containerId__-page620342037-layer-1743710341_line3\',\'__containerId__-page620342037-layer-1743710341_line4\'))" rows="" cols="" style="width:563px;height:264px;"></textarea></div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page620342037-layer-794527142" style="position: absolute; left: 275px; top: 870px; width: 600px; height: 40px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="794527142" data-review-reference-id="794527142">\
            <div class="stencil-wrapper" style="width: 600px; height: 40px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="position: absolute; left: -2px; top: -2px;height: 44px;width:604px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="helvetica-font" width="604" height="44" viewBox="-2 -2 604 44">\
                     <svg:a><svg:path class=" svg_unselected_element" d="M 4.00, 39.00 Q 3.08, 38.34, 2.47, 37.53 Q 1.96, 36.67, 1.38, 35.88 Q 0.11, 19.13,\
                        0.73, 1.95 Q 1.62, 1.04, 2.32, 0.29 Q 2.49, -1.17, 3.72, -1.87 Q 14.18, -1.38, 24.47, -1.22 Q 34.72, -1.23, 44.95, -1.85 Q\
                        55.20, -1.22, 65.45, -1.18 Q 75.69, -1.05, 85.93, -1.48 Q 96.17, -1.73, 106.41, -1.04 Q 116.66, -1.19, 126.90, -1.52 Q 137.14,\
                        -2.69, 147.38, -2.05 Q 157.62, -2.21, 167.86, -1.42 Q 178.10, -1.12, 188.34, -1.38 Q 198.59, -1.03, 208.83, -0.67 Q 219.07,\
                        -0.42, 229.31, -1.21 Q 239.55, -0.84, 249.79, -0.24 Q 260.03, 0.06, 270.28, -0.31 Q 280.52, -1.54, 290.76, -1.35 Q 301.00,\
                        -1.58, 311.24, -1.11 Q 321.48, -2.06, 331.72, -1.63 Q 341.97, -1.43, 352.21, -1.57 Q 362.45, -2.19, 372.69, -2.57 Q 382.93,\
                        -2.54, 393.17, -2.30 Q 403.41, -2.77, 413.66, -3.41 Q 423.90, -3.44, 434.14, -2.32 Q 444.38, -1.54, 454.62, -1.33 Q 464.86,\
                        -1.39, 475.10, -1.43 Q 485.35, -1.45, 495.59, -1.36 Q 505.83, -2.28, 516.07, -1.59 Q 526.31, -1.34, 536.55, -0.99 Q 546.79,\
                        -1.64, 557.03, -2.48 Q 567.28, -0.92, 577.52, -1.68 Q 587.76, -1.27, 597.92, -0.67 Q 598.69, 0.34, 599.59, 0.46 Q 599.55,\
                        1.71, 600.40, 2.19 Q 601.79, 18.88, 601.25, 36.04 Q 600.52, 37.01, 600.18, 38.16 Q 599.33, 38.94, 598.15, 39.48 Q 587.76,\
                        38.98, 577.51, 38.91 Q 567.32, 40.35, 557.06, 40.54 Q 546.80, 40.19, 536.56, 40.06 Q 526.31, 40.03, 516.07, 39.71 Q 505.83,\
                        39.94, 495.59, 40.14 Q 485.35, 40.33, 475.10, 40.47 Q 464.86, 40.69, 454.62, 40.47 Q 444.38, 40.87, 434.14, 40.81 Q 423.90,\
                        40.60, 413.66, 40.76 Q 403.41, 40.78, 393.17, 40.64 Q 382.93, 41.04, 372.69, 41.32 Q 362.45, 41.09, 352.21, 41.23 Q 341.97,\
                        41.11, 331.72, 40.26 Q 321.48, 39.67, 311.24, 39.57 Q 301.00, 38.72, 290.76, 38.81 Q 280.52, 39.08, 270.28, 38.86 Q 260.03,\
                        39.04, 249.79, 39.29 Q 239.55, 39.77, 229.31, 40.15 Q 219.07, 38.78, 208.83, 38.50 Q 198.59, 39.20, 188.34, 39.75 Q 178.10,\
                        39.76, 167.86, 39.55 Q 157.62, 40.04, 147.38, 40.57 Q 137.14, 40.51, 126.90, 39.92 Q 116.66, 39.48, 106.41, 40.07 Q 96.17,\
                        41.21, 85.93, 41.13 Q 75.69, 39.91, 65.45, 39.91 Q 55.21, 40.88, 44.97, 41.26 Q 34.72, 40.17, 24.48, 40.59 Q 14.24, 39.00,\
                        4.00, 39.00" style="fill-rule:evenodd;clip-rule:evenodd;fill:#808080;stroke:#666666;"/>\
                        <svg:text x="300" y="20" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">отправить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 600px; height: 40px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page620342037-layer-794527142\', \'497508653\', {"button":"left","id":"984793135","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1430332361","options":"reloadOnly","target":null,"transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page620342037-layer-720578732" style="position: absolute; left: 720px; top: 20px; width: 144px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="720578732" data-review-reference-id="720578732">\
            <div class="stencil-wrapper" style="width: 144px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Контакты</span></p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page620342037"] .border-wrapper, body[data-current-page-id="page620342037"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page620342037"] .border-wrapper, body.has-frame[data-current-page-id="page620342037"]\
         .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="page620342037"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page620342037"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page620342037",\
      			"name": "cont - menu",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-600-1024" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:657px;height:1048px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.17, 1.25, 52.33, 1.07 Q 62.50, 1.47, 72.67, 1.76 Q 82.83,\
            2.71, 93.00, 2.02 Q 103.17, 1.85, 113.33, 2.39 Q 123.50, 3.35, 133.67, 3.59 Q 143.83, 2.49, 154.00, 2.52 Q 164.17, 2.25, 174.33,\
            2.57 Q 184.50, 3.10, 194.67, 2.94 Q 204.83, 2.89, 215.00, 2.64 Q 225.17, 3.25, 235.33, 2.44 Q 245.50, 2.60, 255.67, 1.60 Q\
            265.83, 2.89, 276.00, 3.21 Q 286.17, 2.72, 296.33, 2.74 Q 306.50, 2.56, 316.67, 2.58 Q 326.83, 2.84, 337.00, 2.96 Q 347.17,\
            2.10, 357.33, 3.04 Q 367.50, 1.67, 377.67, 2.69 Q 387.83, 1.76, 398.00, 2.21 Q 408.17, 2.95, 418.33, 3.27 Q 428.50, 3.46,\
            438.67, 2.16 Q 448.83, 1.93, 459.00, 1.75 Q 469.17, 0.95, 479.33, 0.75 Q 489.50, 2.45, 499.67, 2.98 Q 509.83, 3.62, 520.00,\
            3.40 Q 530.17, 2.55, 540.33, 2.72 Q 550.50, 2.87, 560.67, 2.20 Q 570.83, 2.18, 581.00, 2.54 Q 591.17, 2.94, 601.33, 2.32 Q\
            611.50, 2.34, 621.67, 2.54 Q 631.83, 2.92, 642.20, 2.80 Q 642.63, 12.88, 643.14, 23.01 Q 642.23, 33.25, 641.94, 43.35 Q 642.99,\
            53.43, 644.00, 63.51 Q 642.52, 73.62, 642.36, 83.71 Q 642.21, 93.79, 643.58, 103.88 Q 643.72, 113.97, 643.04, 124.06 Q 642.24,\
            134.15, 642.63, 144.24 Q 643.34, 154.32, 643.06, 164.41 Q 642.90, 174.50, 642.41, 184.59 Q 643.70, 194.68, 643.46, 204.76\
            Q 644.20, 214.85, 643.61, 224.94 Q 643.47, 235.03, 643.67, 245.12 Q 644.10, 255.21, 643.91, 265.29 Q 643.56, 275.38, 644.04,\
            285.47 Q 642.89, 295.56, 642.09, 305.65 Q 641.88, 315.74, 641.35, 325.82 Q 641.44, 335.91, 641.58, 346.00 Q 641.31, 356.09,\
            642.03, 366.18 Q 642.79, 376.26, 642.67, 386.35 Q 641.80, 396.44, 642.13, 406.53 Q 641.65, 416.62, 642.18, 426.71 Q 642.09,\
            436.79, 643.13, 446.88 Q 642.15, 456.97, 642.79, 467.06 Q 642.40, 477.15, 642.54, 487.24 Q 641.88, 497.32, 641.92, 507.41\
            Q 642.75, 517.50, 642.57, 527.59 Q 642.55, 537.68, 643.39, 547.76 Q 643.25, 557.85, 643.48, 567.94 Q 643.13, 578.03, 642.56,\
            588.12 Q 642.25, 598.21, 642.72, 608.29 Q 642.26, 618.38, 643.57, 628.47 Q 643.85, 638.56, 643.76, 648.65 Q 642.93, 658.74,\
            642.35, 668.82 Q 643.10, 678.91, 641.66, 689.00 Q 641.48, 699.09, 642.76, 709.18 Q 643.34, 719.27, 644.31, 729.35 Q 644.35,\
            739.44, 642.27, 749.53 Q 641.50, 759.62, 642.47, 769.71 Q 641.54, 779.79, 642.63, 789.88 Q 642.83, 799.97, 643.54, 810.06\
            Q 643.35, 820.15, 643.34, 830.24 Q 642.63, 840.32, 642.98, 850.41 Q 643.30, 860.50, 642.77, 870.59 Q 642.63, 880.68, 642.84,\
            890.77 Q 642.92, 900.85, 643.17, 910.94 Q 643.36, 921.03, 642.55, 931.12 Q 642.60, 941.21, 642.78, 951.29 Q 642.49, 961.38,\
            642.27, 971.47 Q 642.84, 981.56, 643.28, 991.65 Q 642.91, 1001.74, 642.46, 1011.82 Q 643.13, 1021.91, 642.85, 1032.85 Q 632.04,\
            1032.63, 621.77, 1032.71 Q 611.57, 1033.08, 601.34, 1032.26 Q 591.20, 1033.87, 581.01, 1032.96 Q 570.84, 1033.45, 560.67,\
            1033.53 Q 550.50, 1033.27, 540.33, 1033.21 Q 530.17, 1033.06, 520.00, 1031.82 Q 509.83, 1033.13, 499.67, 1033.24 Q 489.50,\
            1033.13, 479.33, 1032.39 Q 469.17, 1031.77, 459.00, 1032.45 Q 448.83, 1033.02, 438.67, 1032.91 Q 428.50, 1032.65, 418.33,\
            1032.46 Q 408.17, 1032.36, 398.00, 1032.26 Q 387.83, 1031.65, 377.67, 1031.84 Q 367.50, 1031.88, 357.33, 1031.68 Q 347.17,\
            1032.20, 337.00, 1032.65 Q 326.83, 1033.00, 316.67, 1033.16 Q 306.50, 1032.76, 296.33, 1032.09 Q 286.17, 1031.26, 276.00,\
            1031.13 Q 265.83, 1031.63, 255.67, 1031.64 Q 245.50, 1031.39, 235.33, 1031.81 Q 225.17, 1032.18, 215.00, 1032.48 Q 204.83,\
            1032.61, 194.67, 1032.40 Q 184.50, 1033.45, 174.33, 1033.99 Q 164.17, 1033.06, 154.00, 1032.09 Q 143.83, 1032.17, 133.67,\
            1033.02 Q 123.50, 1032.61, 113.33, 1032.03 Q 103.17, 1032.31, 93.00, 1032.31 Q 82.83, 1032.61, 72.67, 1031.67 Q 62.50, 1031.57,\
            52.33, 1032.15 Q 42.17, 1031.74, 31.58, 1032.42 Q 31.15, 1022.20, 31.03, 1011.96 Q 30.69, 1001.82, 31.08, 991.68 Q 30.26,\
            981.59, 29.97, 971.49 Q 30.00, 961.39, 30.74, 951.30 Q 31.13, 941.21, 30.56, 931.12 Q 30.81, 921.03, 30.56, 910.94 Q 30.72,\
            900.85, 31.04, 890.77 Q 31.80, 880.68, 31.38, 870.59 Q 31.64, 860.50, 30.96, 850.41 Q 30.88, 840.32, 31.84, 830.24 Q 31.04,\
            820.15, 30.45, 810.06 Q 30.70, 799.97, 30.84, 789.88 Q 30.64, 779.79, 30.51, 769.71 Q 30.33, 759.62, 30.20, 749.53 Q 30.21,\
            739.44, 30.55, 729.35 Q 30.05, 719.27, 29.95, 709.18 Q 31.16, 699.09, 31.60, 689.00 Q 31.69, 678.91, 31.63, 668.82 Q 30.87,\
            658.74, 31.52, 648.65 Q 30.86, 638.56, 31.51, 628.47 Q 30.62, 618.38, 30.17, 608.29 Q 29.79, 598.21, 30.30, 588.12 Q 31.32,\
            578.03, 31.91, 567.94 Q 32.18, 557.85, 32.02, 547.76 Q 31.74, 537.68, 31.28, 527.59 Q 31.94, 517.50, 32.13, 507.41 Q 30.60,\
            497.32, 30.85, 487.24 Q 30.79, 477.15, 30.73, 467.06 Q 31.26, 456.97, 30.61, 446.88 Q 30.42, 436.79, 30.96, 426.71 Q 31.15,\
            416.62, 30.86, 406.53 Q 31.44, 396.44, 31.35, 386.35 Q 30.79, 376.26, 30.75, 366.18 Q 30.89, 356.09, 30.61, 346.00 Q 30.83,\
            335.91, 31.03, 325.82 Q 31.15, 315.74, 30.90, 305.65 Q 30.98, 295.56, 30.15, 285.47 Q 30.28, 275.38, 29.96, 265.29 Q 30.04,\
            255.21, 30.00, 245.12 Q 30.28, 235.03, 29.51, 224.94 Q 30.12, 214.85, 31.65, 204.76 Q 31.68, 194.68, 31.24, 184.59 Q 31.12,\
            174.50, 30.92, 164.41 Q 30.76, 154.32, 30.72, 144.24 Q 30.58, 134.15, 31.50, 124.06 Q 31.58, 113.97, 31.15, 103.88 Q 31.00,\
            93.79, 31.09, 83.71 Q 31.06, 73.62, 30.97, 63.53 Q 30.78, 53.44, 30.70, 43.35 Q 30.57, 33.26, 31.00, 23.18 Q 32.00, 13.09,\
            32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.17, 5.68, 43.33, 6.36 Q 53.50, 7.22, 63.67, 6.89 Q 73.83,\
            6.99, 84.00, 7.69 Q 94.17, 7.44, 104.33, 7.09 Q 114.50, 6.97, 124.67, 6.90 Q 134.83, 7.45, 145.00, 6.71 Q 155.17, 6.58, 165.33,\
            6.39 Q 175.50, 6.81, 185.67, 6.81 Q 195.83, 6.52, 206.00, 7.42 Q 216.17, 6.75, 226.33, 6.95 Q 236.50, 7.09, 246.67, 7.86 Q\
            256.83, 7.15, 267.00, 6.86 Q 277.17, 7.18, 287.33, 6.95 Q 297.50, 6.07, 307.67, 6.92 Q 317.83, 6.82, 328.00, 7.38 Q 338.17,\
            7.18, 348.33, 6.61 Q 358.50, 5.92, 368.67, 6.12 Q 378.83, 6.33, 389.00, 8.30 Q 399.17, 7.84, 409.33, 7.57 Q 419.50, 7.35,\
            429.67, 7.68 Q 439.83, 7.96, 450.00, 7.36 Q 460.17, 8.65, 470.33, 8.43 Q 480.50, 7.60, 490.67, 7.04 Q 500.83, 5.76, 511.00,\
            6.37 Q 521.17, 6.91, 531.33, 7.47 Q 541.50, 7.31, 551.67, 7.47 Q 561.83, 6.33, 572.00, 5.98 Q 582.17, 5.60, 592.33, 6.75 Q\
            602.50, 6.78, 612.67, 6.32 Q 622.83, 6.50, 632.93, 7.07 Q 632.18, 17.36, 633.07, 27.17 Q 634.08, 37.19, 634.36, 47.31 Q 634.65,\
            57.41, 634.88, 67.51 Q 634.35, 77.61, 633.58, 87.70 Q 633.84, 97.79, 634.00, 107.88 Q 633.05, 117.97, 632.39, 128.06 Q 633.09,\
            138.15, 633.49, 148.24 Q 632.91, 158.32, 632.59, 168.41 Q 632.26, 178.50, 633.15, 188.59 Q 633.34, 198.68, 633.54, 208.76\
            Q 633.16, 218.85, 632.93, 228.94 Q 633.94, 239.03, 634.20, 249.12 Q 633.72, 259.21, 632.40, 269.29 Q 632.47, 279.38, 632.64,\
            289.47 Q 633.33, 299.56, 633.19, 309.65 Q 632.65, 319.74, 632.33, 329.82 Q 632.51, 339.91, 633.17, 350.00 Q 633.13, 360.09,\
            633.87, 370.18 Q 634.25, 380.26, 634.33, 390.35 Q 634.29, 400.44, 634.09, 410.53 Q 634.09, 420.62, 634.20, 430.71 Q 633.39,\
            440.79, 633.09, 450.88 Q 633.44, 460.97, 633.57, 471.06 Q 633.32, 481.15, 633.24, 491.24 Q 633.54, 501.32, 633.56, 511.41\
            Q 632.90, 521.50, 632.48, 531.59 Q 632.45, 541.68, 633.15, 551.76 Q 634.00, 561.85, 634.27, 571.94 Q 634.06, 582.03, 634.31,\
            592.12 Q 634.40, 602.21, 633.40, 612.29 Q 632.21, 622.38, 631.88, 632.47 Q 632.38, 642.56, 633.50, 652.65 Q 634.35, 662.74,\
            634.90, 672.82 Q 634.56, 682.91, 634.39, 693.00 Q 635.05, 703.09, 634.59, 713.18 Q 634.68, 723.26, 635.02, 733.35 Q 634.85,\
            743.44, 635.00, 753.53 Q 634.66, 763.62, 634.74, 773.71 Q 634.90, 783.79, 634.96, 793.88 Q 634.33, 803.97, 634.25, 814.06\
            Q 633.51, 824.15, 633.26, 834.24 Q 632.93, 844.32, 633.29, 854.41 Q 633.85, 864.50, 634.52, 874.59 Q 633.44, 884.68, 633.78,\
            894.77 Q 633.28, 904.85, 633.00, 914.94 Q 633.25, 925.03, 632.71, 935.12 Q 633.06, 945.21, 632.24, 955.29 Q 632.47, 965.38,\
            633.24, 975.47 Q 633.93, 985.56, 633.47, 995.65 Q 633.24, 1005.74, 633.87, 1015.82 Q 633.73, 1025.91, 633.25, 1036.25 Q 622.90,\
            1036.21, 612.70, 1036.24 Q 602.55, 1036.79, 592.36, 1036.95 Q 582.18, 1036.69, 572.01, 1037.44 Q 561.84, 1038.06, 551.67,\
            1037.83 Q 541.50, 1037.59, 531.33, 1036.67 Q 521.17, 1036.61, 511.00, 1036.60 Q 500.83, 1036.57, 490.67, 1034.99 Q 480.50,\
            1035.99, 470.33, 1036.26 Q 460.17, 1036.39, 450.00, 1037.12 Q 439.83, 1037.26, 429.67, 1036.58 Q 419.50, 1036.06, 409.33,\
            1035.63 Q 399.17, 1034.90, 389.00, 1035.13 Q 378.83, 1035.96, 368.67, 1036.58 Q 358.50, 1036.67, 348.33, 1036.95 Q 338.17,\
            1036.76, 328.00, 1036.90 Q 317.83, 1037.99, 307.67, 1036.73 Q 297.50, 1037.42, 287.33, 1037.65 Q 277.17, 1036.98, 267.00,\
            1037.46 Q 256.83, 1037.27, 246.67, 1037.02 Q 236.50, 1037.04, 226.33, 1037.23 Q 216.17, 1037.10, 206.00, 1037.86 Q 195.83,\
            1037.50, 185.67, 1037.01 Q 175.50, 1036.08, 165.33, 1036.71 Q 155.17, 1036.61, 145.00, 1036.34 Q 134.83, 1037.08, 124.67,\
            1037.11 Q 114.50, 1037.02, 104.33, 1037.06 Q 94.17, 1036.25, 84.00, 1036.98 Q 73.83, 1037.53, 63.67, 1036.73 Q 53.50, 1037.30,\
            43.33, 1036.97 Q 33.17, 1036.71, 22.95, 1036.05 Q 22.84, 1025.97, 22.48, 1015.90 Q 22.71, 1005.76, 21.97, 995.68 Q 22.35,\
            985.57, 21.18, 975.49 Q 21.47, 965.39, 21.03, 955.30 Q 21.52, 945.21, 21.35, 935.12 Q 20.99, 925.03, 21.07, 914.94 Q 21.01,\
            904.85, 21.77, 894.77 Q 21.04, 884.68, 21.86, 874.59 Q 21.69, 864.50, 21.50, 854.41 Q 21.81, 844.32, 22.00, 834.24 Q 23.66,\
            824.15, 22.97, 814.06 Q 23.05, 803.97, 22.88, 793.88 Q 22.79, 783.79, 21.67, 773.71 Q 21.42, 763.62, 21.62, 753.53 Q 21.36,\
            743.44, 21.63, 733.35 Q 21.35, 723.26, 21.98, 713.18 Q 21.50, 703.09, 22.10, 693.00 Q 21.88, 682.91, 21.71, 672.82 Q 21.45,\
            662.74, 22.16, 652.65 Q 22.47, 642.56, 21.90, 632.47 Q 22.43, 622.38, 21.40, 612.29 Q 21.79, 602.21, 21.47, 592.12 Q 21.47,\
            582.03, 21.65, 571.94 Q 22.04, 561.85, 22.11, 551.76 Q 22.04, 541.68, 22.20, 531.59 Q 21.86, 521.50, 22.54, 511.41 Q 21.50,\
            501.32, 22.11, 491.24 Q 21.51, 481.15, 22.78, 471.06 Q 23.19, 460.97, 22.37, 450.88 Q 23.06, 440.79, 22.13, 430.71 Q 22.96,\
            420.62, 22.33, 410.53 Q 21.98, 400.44, 22.19, 390.35 Q 21.89, 380.26, 21.93, 370.18 Q 22.59, 360.09, 22.43, 350.00 Q 21.81,\
            339.91, 22.16, 329.82 Q 21.73, 319.74, 23.09, 309.65 Q 21.94, 299.56, 22.26, 289.47 Q 22.40, 279.38, 21.26, 269.29 Q 20.98,\
            259.21, 21.33, 249.12 Q 21.15, 239.03, 21.56, 228.94 Q 21.71, 218.85, 22.39, 208.76 Q 22.31, 198.68, 21.45, 188.59 Q 22.05,\
            178.50, 22.69, 168.41 Q 23.13, 158.32, 22.21, 148.24 Q 21.52, 138.15, 21.44, 128.06 Q 21.34, 117.97, 21.26, 107.88 Q 21.15,\
            97.79, 20.97, 87.71 Q 20.90, 77.62, 21.18, 67.53 Q 21.01, 57.44, 20.92, 47.35 Q 20.79, 37.26, 20.66, 27.18 Q 23.00, 17.09,\
            23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.17, 10.27, 60.33, 11.00 Q 70.50, 11.26, 80.67, 10.45 Q 90.83,\
            10.75, 101.00, 11.06 Q 111.17, 10.55, 121.33, 10.19 Q 131.50, 10.21, 141.67, 10.85 Q 151.83, 10.39, 162.00, 10.40 Q 172.17,\
            9.83, 182.33, 9.34 Q 192.50, 9.17, 202.67, 9.00 Q 212.83, 9.54, 223.00, 9.01 Q 233.17, 9.28, 243.33, 9.88 Q 253.50, 9.77,\
            263.67, 9.43 Q 273.83, 9.56, 284.00, 9.64 Q 294.17, 9.93, 304.33, 9.19 Q 314.50, 8.73, 324.67, 9.21 Q 334.83, 9.53, 345.00,\
            10.06 Q 355.17, 9.88, 365.33, 9.84 Q 375.50, 10.09, 385.67, 10.54 Q 395.83, 10.37, 406.00, 9.88 Q 416.17, 9.69, 426.33, 10.16\
            Q 436.50, 9.95, 446.67, 9.32 Q 456.83, 9.07, 467.00, 9.04 Q 477.17, 9.34, 487.33, 9.20 Q 497.50, 8.87, 507.67, 10.07 Q 517.83,\
            9.98, 528.00, 9.46 Q 538.17, 9.47, 548.33, 10.12 Q 558.50, 9.31, 568.67, 9.30 Q 578.83, 9.14, 589.00, 9.22 Q 599.17, 9.52,\
            609.33, 10.31 Q 619.50, 10.50, 629.67, 10.39 Q 639.83, 10.98, 649.97, 11.03 Q 650.55, 20.91, 650.61, 31.09 Q 650.76, 41.21,\
            650.50, 51.34 Q 650.44, 61.43, 650.51, 71.53 Q 650.48, 81.62, 650.89, 91.70 Q 650.53, 101.79, 651.12, 111.88 Q 650.89, 121.97,\
            650.81, 132.06 Q 650.30, 142.15, 650.56, 152.24 Q 649.65, 162.32, 650.52, 172.41 Q 651.32, 182.50, 650.96, 192.59 Q 651.60,\
            202.68, 651.85, 212.76 Q 651.51, 222.85, 651.55, 232.94 Q 651.23, 243.03, 651.51, 253.12 Q 651.10, 263.21, 651.65, 273.29\
            Q 651.38, 283.38, 651.90, 293.47 Q 652.28, 303.56, 651.69, 313.65 Q 650.82, 323.74, 650.23, 333.82 Q 649.86, 343.91, 650.38,\
            354.00 Q 651.24, 364.09, 650.92, 374.18 Q 650.83, 384.26, 650.86, 394.35 Q 650.62, 404.44, 650.02, 414.53 Q 650.98, 424.62,\
            651.24, 434.71 Q 651.13, 444.79, 651.33, 454.88 Q 651.58, 464.97, 651.49, 475.06 Q 650.72, 485.15, 651.16, 495.24 Q 650.73,\
            505.32, 651.15, 515.41 Q 651.30, 525.50, 650.71, 535.59 Q 650.43, 545.68, 650.35, 555.76 Q 651.44, 565.85, 650.54, 575.94\
            Q 650.20, 586.03, 649.32, 596.12 Q 649.91, 606.21, 649.70, 616.29 Q 650.74, 626.38, 650.58, 636.47 Q 650.68, 646.56, 650.57,\
            656.65 Q 650.28, 666.74, 650.81, 676.82 Q 649.63, 686.91, 650.13, 697.00 Q 650.36, 707.09, 650.47, 717.18 Q 650.20, 727.27,\
            650.33, 737.35 Q 650.06, 747.44, 649.70, 757.53 Q 649.94, 767.62, 650.19, 777.71 Q 650.45, 787.79, 650.26, 797.88 Q 650.89,\
            807.97, 651.06, 818.06 Q 650.98, 828.15, 650.31, 838.24 Q 650.06, 848.32, 650.28, 858.41 Q 649.57, 868.50, 649.72, 878.59\
            Q 650.13, 888.68, 649.67, 898.77 Q 649.50, 908.85, 648.40, 918.94 Q 649.31, 929.03, 649.85, 939.12 Q 650.88, 949.21, 650.89,\
            959.29 Q 650.59, 969.38, 650.03, 979.47 Q 649.99, 989.56, 651.20, 999.65 Q 651.02, 1009.74, 651.02, 1019.82 Q 651.04, 1029.91,\
            650.47, 1040.47 Q 639.82, 1039.96, 629.79, 1040.86 Q 619.50, 1040.07, 609.33, 1039.99 Q 599.16, 1039.78, 589.00, 1040.24 Q\
            578.83, 1039.96, 568.67, 1040.13 Q 558.50, 1041.23, 548.33, 1041.67 Q 538.17, 1041.92, 528.00, 1042.14 Q 517.83, 1042.11,\
            507.67, 1041.45 Q 497.50, 1040.82, 487.33, 1041.76 Q 477.17, 1041.07, 467.00, 1040.95 Q 456.83, 1040.43, 446.67, 1040.45 Q\
            436.50, 1040.19, 426.33, 1040.20 Q 416.17, 1040.37, 406.00, 1039.79 Q 395.83, 1039.81, 385.67, 1040.51 Q 375.50, 1040.93,\
            365.33, 1041.30 Q 355.17, 1041.33, 345.00, 1040.96 Q 334.83, 1040.70, 324.67, 1040.50 Q 314.50, 1040.71, 304.33, 1040.73 Q\
            294.17, 1040.82, 284.00, 1040.20 Q 273.83, 1039.66, 263.67, 1039.28 Q 253.50, 1039.70, 243.33, 1040.67 Q 233.17, 1041.20,\
            223.00, 1041.15 Q 212.83, 1040.94, 202.67, 1041.05 Q 192.50, 1041.33, 182.33, 1041.41 Q 172.17, 1041.13, 162.00, 1041.47 Q\
            151.83, 1041.44, 141.67, 1041.34 Q 131.50, 1040.66, 121.33, 1039.33 Q 111.17, 1039.93, 101.00, 1040.39 Q 90.83, 1040.47, 80.67,\
            1039.90 Q 70.50, 1040.10, 60.33, 1040.67 Q 50.17, 1040.52, 39.50, 1040.50 Q 40.55, 1029.73, 40.35, 1019.77 Q 40.15, 1009.73,\
            40.72, 999.62 Q 41.00, 989.54, 40.66, 979.47 Q 40.16, 969.38, 39.66, 959.30 Q 39.64, 949.21, 41.04, 939.12 Q 41.22, 929.03,\
            40.80, 918.94 Q 40.38, 908.85, 41.45, 898.77 Q 40.10, 888.68, 40.45, 878.59 Q 40.30, 868.50, 39.94, 858.41 Q 39.80, 848.32,\
            38.39, 838.24 Q 39.21, 828.15, 39.41, 818.06 Q 39.06, 807.97, 39.24, 797.88 Q 39.43, 787.79, 39.88, 777.71 Q 40.13, 767.62,\
            40.16, 757.53 Q 39.79, 747.44, 38.93, 737.35 Q 39.55, 727.27, 40.87, 717.18 Q 39.91, 707.09, 39.73, 697.00 Q 39.14, 686.91,\
            39.38, 676.82 Q 40.27, 666.74, 39.53, 656.65 Q 39.79, 646.56, 39.85, 636.47 Q 39.45, 626.38, 39.59, 616.29 Q 40.52, 606.21,\
            40.09, 596.12 Q 40.90, 586.03, 40.79, 575.94 Q 41.12, 565.85, 40.42, 555.76 Q 40.33, 545.68, 39.92, 535.59 Q 39.22, 525.50,\
            39.40, 515.41 Q 39.20, 505.32, 40.16, 495.24 Q 40.11, 485.15, 39.16, 475.06 Q 39.14, 464.97, 38.99, 454.88 Q 39.26, 444.79,\
            39.07, 434.71 Q 39.59, 424.62, 38.85, 414.53 Q 38.94, 404.44, 39.39, 394.35 Q 39.48, 384.26, 39.44, 374.18 Q 39.69, 364.09,\
            40.15, 354.00 Q 40.39, 343.91, 39.53, 333.82 Q 39.01, 323.74, 38.89, 313.65 Q 39.71, 303.56, 40.27, 293.47 Q 40.28, 283.38,\
            41.04, 273.29 Q 40.93, 263.21, 41.66, 253.12 Q 41.07, 243.03, 39.57, 232.94 Q 38.88, 222.85, 38.64, 212.76 Q 38.27, 202.68,\
            38.30, 192.59 Q 38.59, 182.50, 38.40, 172.41 Q 38.70, 162.32, 38.04, 152.24 Q 39.43, 142.15, 38.75, 132.06 Q 38.64, 121.97,\
            39.05, 111.88 Q 39.05, 101.79, 38.52, 91.71 Q 38.73, 81.62, 38.80, 71.53 Q 39.13, 61.44, 38.58, 51.35 Q 38.46, 41.26, 38.38,\
            31.18 Q 40.00, 21.09, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');