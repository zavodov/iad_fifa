rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1985076138-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1985076138" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1985076138-layer-2121026156" style="position: absolute; left: -1px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="2121026156" data-review-reference-id="2121026156">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1198659627" style="position: absolute; left: 1px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1198659627" data-review-reference-id="1198659627">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-713698962" style="position: absolute; left: 0px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="713698962" data-review-reference-id="713698962">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-162561487" style="position: absolute; left: 150px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="162561487" data-review-reference-id="162561487">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-402414116" style="position: absolute; left: 95px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="402414116" data-review-reference-id="402414116">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1667415127" style="position: absolute; left: 0px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="1667415127" data-review-reference-id="1667415127">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1334157620" style="position: absolute; left: 422px; top: 25px; width: 163px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1334157620" data-review-reference-id="1334157620">\
            <div class="stencil-wrapper" style="width: 163px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Настройки</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-365452919" style="position: absolute; left: 0px; top: 115px; width: 600px; height: 795px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="365452919" data-review-reference-id="365452919">\
            <div class="stencil-wrapper" style="width: 600px; height: 795px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 795px; width:600px;" width="600" height="795" viewBox="0 0 600 795">\
                     <svg:g width="600" height="795">\
                        <svg:rect x="0" y="0" width="600" height="795" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1971033065" style="position: absolute; left: 85px; top: 162px; width: 169px; height: 168px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1971033065" data-review-reference-id="1971033065">\
            <div class="stencil-wrapper" style="width: 169px; height: 168px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 168px;width:169px;" width="169" height="168" viewBox="0 0 169 168">\
                     <svg:g width="169" height="168">\
                        <svg:ellipse cx="84.5" cy="84" rx="84.5" ry="84" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-914628339" style="position: absolute; left: 85px; top: 365px; width: 420px; height: 55px" data-interactive-element-type="default.iphoneButton" class="iphoneButton stencil mobile-interaction-potential-trigger " data-stencil-id="914628339" data-review-reference-id="914628339">\
            <div class="stencil-wrapper" style="width: 420px; height: 55px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 55px;width:420px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="420" height="55" viewBox="0 0 420 55">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,54.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-45 0.5,-2 1,-1 1,-1 2,-0.5 410,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,45 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="210" y="27.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">изменить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1976373577" style="position: absolute; left: 295px; top: 160px; width: 210px; height: 55px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1976373577" data-review-reference-id="1976373577">\
            <div class="stencil-wrapper" style="width: 210px; height: 55px">\
               <div title=""><textarea id="__containerId__-1985076138-layer-1976373577input" rows="" cols="" style="width:208px;height:51px;padding: 0px;border-width:1px;">NAME</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-2017063371" style="position: absolute; left: 295px; top: 270px; width: 210px; height: 55px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="2017063371" data-review-reference-id="2017063371">\
            <div class="stencil-wrapper" style="width: 210px; height: 55px">\
               <div title=""><textarea id="__containerId__-1985076138-layer-2017063371input" rows="" cols="" style="width:208px;height:51px;padding: 0px;border-width:1px;">SURNAME</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-122182700" style="position: absolute; left: 0px; top: 855px; width: 600px; height: 55px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="122182700" data-review-reference-id="122182700">\
            <div class="stencil-wrapper" style="width: 600px; height: 55px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 55px;width:600px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="600" height="55" viewBox="0 0 600 55">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,54.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-45 0.5,-2 1,-1 1,-1 2,-0.5 590,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,45 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="300" y="27.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.8333333333333333em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">сохранить</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 600px; height: 55px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1985076138-layer-122182700\', \'1418801818\', {"button":"left","id":"749062074","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"863147785","options":"reloadOnly","target":null,"transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1985076138-layer-247580193" style="position: absolute; left: 85px; top: 480px; width: 109px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="247580193" data-review-reference-id="247580193">\
            <div class="stencil-wrapper" style="width: 109px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Язык - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-2115090058" style="position: absolute; left: 195px; top: 470px; width: 310px; height: 55px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="2115090058" data-review-reference-id="2115090058">\
            <div class="stencil-wrapper" style="width: 310px; height: 55px">\
               <div title=""><select id="__containerId__-1985076138-layer-2115090058select" style="width:310px;height:55px;" title="">\
                     <option title="">Русский</option>\
                     <option title="">English</option></select></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1683224742" style="position: absolute; left: 185px; top: 575px; width: 228px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1683224742" data-review-reference-id="1683224742">\
            <div class="stencil-wrapper" style="width: 228px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Предпочтения </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1375164288" style="position: absolute; left: 85px; top: 655px; width: 106px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1375164288" data-review-reference-id="1375164288">\
            <div class="stencil-wrapper" style="width: 106px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Клуб - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-1488880575" style="position: absolute; left: 85px; top: 740px; width: 102px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1488880575" data-review-reference-id="1488880575">\
            <div class="stencil-wrapper" style="width: 102px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Лига - </span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-580971955" style="position: absolute; left: 195px; top: 645px; width: 310px; height: 55px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="580971955" data-review-reference-id="580971955">\
            <div class="stencil-wrapper" style="width: 310px; height: 55px">\
               <div title=""><select id="__containerId__-1985076138-layer-580971955select" style="width:310px;height:55px;" title="">\
                     <option title="">First entry</option>\
                     <option title="">Second entry</option>\
                     <option title="">Third entry</option></select></div>\
            </div>\
         </div>\
         <div id="__containerId__-1985076138-layer-166227965" style="position: absolute; left: 195px; top: 730px; width: 310px; height: 55px" data-interactive-element-type="default.combobox" class="combobox stencil mobile-interaction-potential-trigger " data-stencil-id="166227965" data-review-reference-id="166227965">\
            <div class="stencil-wrapper" style="width: 310px; height: 55px">\
               <div title=""><select id="__containerId__-1985076138-layer-166227965select" style="width:310px;height:55px;" title="">\
                     <option title="">First entry</option>\
                     <option title="">Second entry</option>\
                     <option title="">Third entry</option></select></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1985076138"] .border-wrapper, body[data-current-page-id="1985076138"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1985076138"] .border-wrapper, body.has-frame[data-current-page-id="1985076138"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1985076138"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1985076138"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1985076138",\
      			"name": "settings",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');