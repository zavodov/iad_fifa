rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__1166916087-layer" class="layer" name="__containerId__pageLayer" data-layer-id="1166916087" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-1166916087-layer-761721860" style="position: absolute; left: 0px; top: 0px; width: 600px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="761721860" data-review-reference-id="761721860">\
            <div class="stencil-wrapper" style="width: 600px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:600px;" width="600" height="1024" viewBox="0 0 600 1024">\
                     <svg:g width="600" height="1024">\
                        <svg:rect x="0" y="0" width="600" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="600" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="600" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1166916087-layer-326773" style="position: absolute; left: 0px; top: 255px; width: 600px; height: 515px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="326773" data-review-reference-id="326773">\
            <div class="stencil-wrapper" style="width: 600px; height: 515px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 515px; width:600px;" width="600" height="515" viewBox="0 0 600 515">\
                     <svg:g width="600" height="515">\
                        <svg:rect x="0" y="0" width="600" height="515" style="stroke-width:1;stroke:black;fill:white;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1166916087-layer-1480963956" style="position: absolute; left: 175px; top: 510px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1480963956" data-review-reference-id="1480963956">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-1166916087-layer-1480963956input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">email</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-1166916087-layer-1096038006" style="position: absolute; left: 175px; top: 580px; width: 265px; height: 35px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1096038006" data-review-reference-id="1096038006">\
            <div class="stencil-wrapper" style="width: 265px; height: 35px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 35px;width:265px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="265" height="35" viewBox="0 0 265 35">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,34.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-25 0.5,-2 1,-1 1,-1 2,-0.5 255,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,25 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="132.5" y="17.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.6666666666666667em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">sent my password</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 265px; height: 35px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-1166916087-layer-1096038006\', \'1434062901\', {"button":"left","id":"150148159","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1683488280","options":"reloadOnly","target":"1168452877","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-1166916087-layer-1952827834" style="position: absolute; left: 85px; top: 295px; width: 435px; height: 105px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1952827834" data-review-reference-id="1952827834">\
            <div class="stencil-wrapper" style="width: 435px; height: 105px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 105px; width:435px;" width="435" height="105" viewBox="0 0 435 105">\
                     <svg:g width="435" height="105">\
                        <svg:rect x="0" y="0" width="435" height="105" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-1166916087-layer-1315506605" style="position: absolute; left: 235px; top: 330px; width: 140px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1315506605" data-review-reference-id="1315506605">\
            <div class="stencil-wrapper" style="width: 140px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">__logo__ </p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="1166916087"] .border-wrapper, body[data-current-page-id="1166916087"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="1166916087"] .border-wrapper, body.has-frame[data-current-page-id="1166916087"] .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="1166916087"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="1166916087"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "1166916087",\
      			"name": "forget password",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');