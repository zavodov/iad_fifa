rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page106900939-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page106900939" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page106900939-layer-368883164" style="position: absolute; left: 275px; top: 0px; width: 601px; height: 1024px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="368883164" data-review-reference-id="368883164">\
            <div class="stencil-wrapper" style="width: 601px; height: 1024px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 1024px;width:601px;" width="601" height="1024" viewBox="0 0 601 1024">\
                     <svg:g width="601" height="1024">\
                        <svg:rect x="0" y="0" width="601" height="1024" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="601" y2="1024" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="1024" x2="601" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1254413406" style="position: absolute; left: 275px; top: 240px; width: 600px; height: 80px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1254413406" data-review-reference-id="1254413406">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div id="1254413406-1891757621" style="position: absolute; left: 0px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1891757621" data-review-reference-id="1891757621">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-1891757621_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-1891757621div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-1891757621\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-1891757621\', \'result\');">\
                           				RFPL\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-665663306" style="position: absolute; left: 100px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="665663306" data-review-reference-id="665663306">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-665663306_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-665663306div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-665663306\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-665663306\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-584488950" style="position: absolute; left: 200px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="584488950" data-review-reference-id="584488950">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-584488950_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-584488950div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-584488950\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-584488950\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-1298398196" style="position: absolute; left: 400px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1298398196" data-review-reference-id="1298398196">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-1298398196_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-1298398196div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-1298398196\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-1298398196\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-1360158057" style="position: absolute; left: 300px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1360158057" data-review-reference-id="1360158057">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-1360158057_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-1360158057div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-1360158057\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-1360158057\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="1254413406-322290237" style="position: absolute; left: 500px; top: 0px; width: 100px; height: 80px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="322290237" data-review-reference-id="322290237">\
                  <div class="stencil-wrapper" style="width: 100px; height: 80px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 90px;width:110px;" width="100" height="85">\
                           <svg:g id="target" x="-5" y="0" width="100" height="80" name="target" class="">\
                              <svg:path id="1254413406-322290237_small_path" width="100" height="80" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 95,5 C 105,5 105,15 105,15 L 105,85 L 5,85 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="1254413406-322290237div" class="helvetica-font" style="position: absolute; top: 4px; height: 80px;width:100px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:30px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1254413406-322290237\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1254413406-322290237\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-245548156" style="position: absolute; left: 275px; top: 320px; width: 600px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="245548156" data-review-reference-id="245548156">\
            <div class="stencil-wrapper" style="width: 600px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:600px;" width="600" height="465" viewBox="0 0 600 465">\
                     <svg:g width="600" height="465">\
                        <svg:rect x="0" y="0" width="600" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1001972155" style="position: absolute; left: 276px; top: 0px; width: 599px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1001972155" data-review-reference-id="1001972155">\
            <div class="stencil-wrapper" style="width: 599px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:599px;" width="599" height="80" viewBox="0 0 599 80">\
                     <svg:g width="599" height="80">\
                        <svg:rect x="0" y="0" width="599" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-110308917" style="position: absolute; left: 275px; top: 944px; width: 600px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="110308917" data-review-reference-id="110308917">\
            <div class="stencil-wrapper" style="width: 600px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:600px;" width="600" height="80" viewBox="0 0 600 80">\
                     <svg:g width="600" height="80">\
                        <svg:rect x="0" y="0" width="600" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-231704857" style="position: absolute; left: 395px; top: 10px; width: 200px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="231704857" data-review-reference-id="231704857">\
            <div class="stencil-wrapper" style="width: 200px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:200px;" width="200" height="60" viewBox="0 0 200 60">\
                     <svg:g width="200" height="60">\
                        <svg:rect x="0" y="0" width="200" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="200" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="200" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-69641287" style="position: absolute; left: 625px; top: 20px; width: 242px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="69641287" data-review-reference-id="69641287">\
            <div class="stencil-wrapper" style="width: 242px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Коэффициенты </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-858768943" style="position: absolute; left: 377px; top: 965px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="858768943" data-review-reference-id="858768943">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1556518736" style="position: absolute; left: 275px; top: 0px; width: 86px; height: 80px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1556518736" data-review-reference-id="1556518736">\
            <div class="stencil-wrapper" style="width: 86px; height: 80px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:86px;height:80px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">=<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 86px; height: 80px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1556518736\', \'interaction95907894\', {"button":"left","id":"action739538240","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction9868519","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1684263033" style="position: absolute; left: 275px; top: 350px; width: 600px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="1684263033" data-review-reference-id="1684263033">\
            <div class="stencil-wrapper" style="width: 600px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page106900939-layer-1684263033-accordion" title="">\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; width: 600px; height:395px; font-size: 1em; line-height: 1.2em;border: 2px solid black; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page106900939-layer-1684263033-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="overflow:auto;border:1px solid black;background-color:white;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page106900939-layer-1684263033-accordion", "600", "395", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1707740378" style="position: absolute; left: 277px; top: 380px; width: 598px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="1707740378" data-review-reference-id="1707740378">\
            <div class="stencil-wrapper" style="width: 598px; height: 273px">\
               <div title=""><table width=\'588.0\' height=\'273.0\' cellspacing=\'0\' class=\'tableStyle\'><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span\
                  style=\'\'>BK</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>1</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>X</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>2</span><br\
                  /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\'\
                  class=\'tableCells\'><span style=\'\'>1X ставка</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Winline</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Лига ставок</span><br /></td><td\
                  style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\'\
                  class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\' class=\'tableCells\'><span\
                  style=\'\'>Фонбет</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:163px;\'\
                  class=\'tableCells\'><span style=\'\'>Леон</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:163px;\' class=\'tableCells\'><span style=\'\'>Pinnacle</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:78px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:51px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr></table>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-727200144" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 1024px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="727200144" data-review-reference-id="727200144">\
            <div class="stencil-wrapper" style="width: 275px; height: 1024px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 1024px; width:275px;" width="275" height="1024" viewBox="0 0 275 1024">\
                     <svg:g width="275" height="1024">\
                        <svg:rect x="0" y="0" width="275" height="1024" style="stroke-width:1;stroke:black;fill:#C1C1C1;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-983065824" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="983065824" data-review-reference-id="983065824">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px; width:275px;" width="275" height="160" viewBox="0 0 275 160">\
                     <svg:g width="275" height="160">\
                        <svg:rect x="0" y="0" width="275" height="160" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-1130817064" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1130817064" data-review-reference-id="1130817064">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="1130817064-1812207366" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="1812207366" data-review-reference-id="1812207366">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85" viewBox="0 0 85 85">\
                           <svg:g width="85" height="85">\
                              <svg:ellipse cx="42.5" cy="42.5" rx="42.5" ry="42.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="1130817064-1896090289" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1896090289" data-review-reference-id="1896090289">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="1130817064-61490957" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="61490957" data-review-reference-id="61490957">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page106900939-layer-423162585" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="423162585" data-review-reference-id="423162585">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#a7a77c;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Коэффициенты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-423162585\', \'interaction18279135\', {"button":"left","id":"action123693521","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction938923229","options":"reloadOnly","target":"1524600402","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1360226194" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1360226194" data-review-reference-id="1360226194">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Изменения<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1360226194\', \'1029926925\', {"button":"left","id":"1505091817","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1938511453","options":"reloadOnly","target":"492670737","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1734217449" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1734217449" data-review-reference-id="1734217449">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Статистика<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1734217449\', \'438952614\', {"button":"left","id":"39975667","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"527260149","options":"reloadOnly","target":"1497021823","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-683091116" style="position: absolute; left: 0px; top: 974px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="683091116" data-review-reference-id="683091116">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Выход<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-683091116\', \'1331777761\', {"button":"left","id":"1263484831","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1763273653","options":"reloadOnly","target":"1168452877","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-2018754584" style="position: absolute; left: 0px; top: 928px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="2018754584" data-review-reference-id="2018754584">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Настройки<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-2018754584\', \'1248403003\', {"button":"left","id":"1081421981","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"1576978564","options":"reloadOnly","target":"1985076138","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page106900939-layer-1791868487" style="position: absolute; left: 0px; top: 880px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1791868487" data-review-reference-id="1791868487">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:48px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Контакты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page106900939-layer-1791868487\', \'503721507\', {"button":"left","id":"307307194","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"640307367","options":"reloadOnly","target":"132438759","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page106900939"] .border-wrapper, body[data-current-page-id="page106900939"] .simulation-container{\
         			width:600px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page106900939"] .border-wrapper, body.has-frame[data-current-page-id="page106900939"]\
         .simulation-container{\
         			height:1024px;\
         		}\
         		\
         		body[data-current-page-id="page106900939"] .svg-border-600-1024{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page106900939"] .border-wrapper .border-div{\
         			width:600px;\
         			height:1024px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page106900939",\
      			"name": "cf - menu",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":600,\
      			"height":1024,\
      			"parentFolder": "",\
      			"frame": "android7",\
      			"frameOrientation": "portrait"\
      		}\
      	\
   </div>\
</div>');