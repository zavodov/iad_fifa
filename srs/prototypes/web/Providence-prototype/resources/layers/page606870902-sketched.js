rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page606870902-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page606870902" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page606870902-layer-image432401129" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image432401129" data-review-reference-id="image432401129">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768">\
                     <svg:g width="1092" height="768"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 4.01, 22.15, 3.09 Q 32.22, 2.42, 42.30, 1.52 Q\
                        52.37, 2.21, 62.44, 1.17 Q 72.52, 0.27, 82.59, 1.05 Q 92.67, 1.10, 102.74, 1.56 Q 112.81, 1.65, 122.89, 1.29 Q 132.96, 1.35,\
                        143.04, 0.83 Q 153.11, 0.95, 163.19, 1.38 Q 173.26, 1.55, 183.33, 0.48 Q 193.41, 0.06, 203.48, -0.07 Q 213.56, -0.27, 223.63,\
                        -0.30 Q 233.70, -0.43, 243.78, -0.01 Q 253.85, 0.80, 263.93, 0.82 Q 274.00, 0.52, 284.07, 0.41 Q 294.15, 0.67, 304.22, 1.71\
                        Q 314.30, 2.16, 324.37, 2.66 Q 334.44, 2.90, 344.52, 2.83 Q 354.59, 2.25, 364.67, 1.13 Q 374.74, 0.70, 384.81, 0.75 Q 394.89,\
                        0.92, 404.96, 0.68 Q 415.04, 0.43, 425.11, 0.40 Q 435.18, 0.38, 445.26, 0.24 Q 455.33, 0.23, 465.41, -0.21 Q 475.48, 0.15,\
                        485.56, 0.40 Q 495.63, 0.82, 505.70, 0.87 Q 515.78, 1.01, 525.85, 0.40 Q 535.93, 0.51, 546.00, 0.33 Q 556.07, 0.50, 566.15,\
                        0.62 Q 576.22, 1.48, 586.30, 1.48 Q 596.37, 1.53, 606.44, 2.05 Q 616.52, 1.77, 626.59, 1.38 Q 636.67, 1.71, 646.74, 1.16 Q\
                        656.81, 1.53, 666.89, 1.57 Q 676.96, 1.57, 687.04, 1.52 Q 697.11, 2.16, 707.19, 1.11 Q 717.26, 0.70, 727.33, 0.71 Q 737.41,\
                        0.43, 747.48, 1.05 Q 757.56, 1.24, 767.63, 0.76 Q 777.70, -0.04, 787.78, 0.85 Q 797.85, 0.69, 807.93, 0.52 Q 818.00, 0.79,\
                        828.07, 0.53 Q 838.15, 0.49, 848.22, 0.92 Q 858.30, 0.94, 868.37, 0.24 Q 878.44, -0.01, 888.52, -0.05 Q 898.59, 0.07, 908.67,\
                        -0.10 Q 918.74, 0.03, 928.82, -0.21 Q 938.89, -0.37, 948.96, 0.28 Q 959.04, -0.17, 969.11, 0.95 Q 979.19, 1.41, 989.26, 1.49\
                        Q 999.33, 1.39, 1009.41, 1.49 Q 1019.48, 2.12, 1029.56, 1.90 Q 1039.63, 1.73, 1049.70, 0.86 Q 1059.78, 1.11, 1069.85, 1.17\
                        Q 1079.93, 0.73, 1090.75, 1.25 Q 1091.43, 11.58, 1091.47, 21.90 Q 1091.38, 32.07, 1091.61, 42.16 Q 1091.60, 52.24, 1090.87,\
                        62.31 Q 1091.19, 72.36, 1090.90, 82.42 Q 1090.50, 92.47, 1090.54, 102.53 Q 1090.11, 112.58, 1089.82, 122.63 Q 1089.79, 132.68,\
                        1090.33, 142.74 Q 1090.09, 152.79, 1090.94, 162.84 Q 1090.88, 172.89, 1090.55, 182.95 Q 1090.49, 193.00, 1091.02, 203.05 Q\
                        1091.06, 213.11, 1090.50, 223.16 Q 1090.82, 233.21, 1090.09, 243.26 Q 1090.72, 253.32, 1089.83, 263.37 Q 1089.61, 273.42,\
                        1090.16, 283.47 Q 1089.69, 293.53, 1089.36, 303.58 Q 1089.12, 313.63, 1090.94, 323.68 Q 1091.33, 333.74, 1091.22, 343.79 Q\
                        1090.39, 353.84, 1091.12, 363.89 Q 1092.01, 373.95, 1091.70, 384.00 Q 1091.35, 394.05, 1090.65, 404.11 Q 1090.97, 414.16,\
                        1090.24, 424.21 Q 1090.73, 434.26, 1090.44, 444.32 Q 1090.95, 454.37, 1091.18, 464.42 Q 1091.28, 474.47, 1090.06, 484.53 Q\
                        1090.10, 494.58, 1091.20, 504.63 Q 1091.30, 514.68, 1091.10, 524.74 Q 1090.38, 534.79, 1089.95, 544.84 Q 1090.53, 554.89,\
                        1090.30, 564.95 Q 1091.28, 575.00, 1090.48, 585.05 Q 1090.58, 595.11, 1090.06, 605.16 Q 1090.86, 615.21, 1089.87, 625.26 Q\
                        1090.60, 635.32, 1090.58, 645.37 Q 1091.56, 655.42, 1091.79, 665.47 Q 1091.84, 675.53, 1091.88, 685.58 Q 1091.34, 695.63,\
                        1091.41, 705.68 Q 1090.58, 715.74, 1091.68, 725.79 Q 1091.84, 735.84, 1091.58, 745.89 Q 1091.79, 755.95, 1090.84, 766.84 Q\
                        1080.31, 767.14, 1070.08, 767.57 Q 1059.84, 766.98, 1049.73, 766.91 Q 1039.65, 767.24, 1029.57, 767.73 Q 1019.49, 767.60,\
                        1009.41, 766.56 Q 999.33, 765.59, 989.26, 766.06 Q 979.19, 766.35, 969.11, 766.38 Q 959.04, 766.47, 948.96, 766.45 Q 938.89,\
                        766.59, 928.82, 766.49 Q 918.74, 766.05, 908.67, 765.27 Q 898.59, 765.03, 888.52, 765.68 Q 878.44, 765.43, 868.37, 765.97\
                        Q 858.30, 765.75, 848.22, 767.30 Q 838.15, 767.30, 828.07, 766.64 Q 818.00, 766.37, 807.93, 766.54 Q 797.85, 767.38, 787.78,\
                        766.86 Q 777.70, 766.51, 767.63, 767.32 Q 757.56, 767.86, 747.48, 768.08 Q 737.41, 767.77, 727.33, 767.84 Q 717.26, 767.25,\
                        707.19, 767.01 Q 697.11, 766.83, 687.04, 767.14 Q 676.96, 767.41, 666.89, 767.53 Q 656.81, 766.86, 646.74, 766.23 Q 636.67,\
                        766.17, 626.59, 766.71 Q 616.52, 766.60, 606.44, 766.21 Q 596.37, 765.61, 586.30, 765.69 Q 576.22, 765.87, 566.15, 765.67\
                        Q 556.07, 765.24, 546.00, 765.58 Q 535.93, 765.36, 525.85, 766.19 Q 515.78, 766.61, 505.70, 766.67 Q 495.63, 766.17, 485.56,\
                        766.89 Q 475.48, 766.36, 465.41, 766.31 Q 455.33, 766.81, 445.26, 767.37 Q 435.18, 767.59, 425.11, 768.09 Q 415.04, 766.92,\
                        404.96, 766.91 Q 394.89, 767.16, 384.81, 766.99 Q 374.74, 767.38, 364.67, 767.69 Q 354.59, 767.71, 344.52, 767.78 Q 334.44,\
                        767.60, 324.37, 766.67 Q 314.30, 766.93, 304.22, 767.18 Q 294.15, 766.65, 284.07, 766.21 Q 274.00, 766.53, 263.93, 767.14\
                        Q 253.85, 766.92, 243.78, 765.51 Q 233.70, 765.83, 223.63, 765.51 Q 213.56, 766.22, 203.48, 766.32 Q 193.41, 766.18, 183.33,\
                        766.02 Q 173.26, 766.61, 163.19, 765.55 Q 153.11, 764.64, 143.04, 765.29 Q 132.96, 766.71, 122.89, 767.21 Q 112.81, 767.00,\
                        102.74, 766.89 Q 92.67, 767.19, 82.59, 767.32 Q 72.52, 767.50, 62.44, 766.79 Q 52.37, 766.73, 42.30, 767.25 Q 32.22, 767.83,\
                        22.15, 767.88 Q 12.07, 767.28, 1.45, 766.55 Q 1.36, 756.16, 1.25, 746.00 Q 2.47, 735.81, 1.74, 725.80 Q 1.54, 715.74, 1.36,\
                        705.69 Q 2.13, 695.63, 2.73, 685.58 Q 2.45, 675.53, 1.89, 665.47 Q 1.37, 655.42, 1.41, 645.37 Q 1.06, 635.32, 1.12, 625.26\
                        Q 0.94, 615.21, 1.79, 605.16 Q 1.73, 595.11, 0.92, 585.05 Q 1.00, 575.00, 1.40, 564.95 Q 1.60, 554.89, 0.87, 544.84 Q 0.11,\
                        534.79, -0.08, 524.74 Q 0.69, 514.68, 0.64, 504.63 Q 0.30, 494.58, 1.02, 484.53 Q 1.16, 474.47, 1.17, 464.42 Q 1.73, 454.37,\
                        1.99, 444.32 Q 1.21, 434.26, 1.58, 424.21 Q 2.83, 414.16, 2.06, 404.11 Q 1.99, 394.05, 0.55, 384.00 Q 1.59, 373.95, 1.41,\
                        363.89 Q 1.99, 353.84, 1.25, 343.79 Q 1.66, 333.74, 2.53, 323.68 Q 1.53, 313.63, 0.85, 303.58 Q 1.13, 293.53, 2.28, 283.47\
                        Q 2.14, 273.42, 1.53, 263.37 Q 1.03, 253.32, 1.46, 243.26 Q 1.17, 233.21, 0.03, 223.16 Q -0.18, 213.11, -0.40, 203.05 Q -0.15,\
                        193.00, -0.03, 182.95 Q 0.03, 172.89, -0.22, 162.84 Q 0.08, 152.79, 0.48, 142.74 Q -0.39, 132.68, -0.04, 122.63 Q 0.88, 112.58,\
                        1.39, 102.53 Q 0.69, 92.47, 0.09, 82.42 Q -0.04, 72.37, 0.23, 62.32 Q 0.70, 52.26, 1.39, 42.21 Q 0.52, 32.16, 0.58, 22.11\
                        Q 2.00, 12.05, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 11.73, 5.67, 19.97, 11.46 Q 28.02, 17.52, 36.30, 23.26 Q 44.36,\
                        29.31, 52.56, 35.15 Q 60.86, 40.86, 69.04, 46.74 Q 77.07, 52.83, 85.15, 58.84 Q 93.27, 64.81, 101.54, 70.55 Q 109.52, 76.72,\
                        117.79, 82.46 Q 126.14, 88.09, 134.48, 93.75 Q 142.75, 99.50, 150.81, 105.55 Q 159.07, 111.30, 167.42, 116.94 Q 175.73, 122.64,\
                        183.99, 128.40 Q 192.02, 134.48, 200.38, 140.10 Q 208.75, 145.71, 216.54, 152.15 Q 224.89, 157.79, 233.01, 163.75 Q 240.94,\
                        169.97, 249.23, 175.70 Q 257.22, 181.85, 265.82, 187.12 Q 274.16, 192.77, 282.68, 198.17 Q 290.88, 204.01, 298.90, 210.11\
                        Q 307.12, 215.94, 315.15, 222.03 Q 323.49, 227.67, 331.70, 233.51 Q 340.13, 239.04, 348.30, 244.92 Q 356.90, 250.20, 365.06,\
                        256.11 Q 373.40, 261.76, 381.19, 268.19 Q 389.28, 274.19, 397.53, 279.97 Q 406.17, 285.20, 414.63, 290.67 Q 422.05, 297.63,\
                        430.33, 303.36 Q 438.63, 309.07, 447.10, 314.53 Q 455.47, 320.13, 463.47, 326.27 Q 471.45, 332.43, 479.76, 338.13 Q 488.25,\
                        343.55, 496.02, 350.02 Q 504.39, 355.63, 512.43, 361.71 Q 520.97, 367.07, 529.52, 372.42 Q 537.70, 378.30, 546.15, 383.78\
                        Q 554.36, 389.62, 563.00, 394.85 Q 571.08, 400.86, 579.53, 406.36 Q 587.72, 412.22, 595.82, 418.21 Q 604.03, 424.04, 612.14,\
                        430.02 Q 620.50, 435.63, 628.31, 442.04 Q 637.01, 447.18, 644.99, 453.34 Q 653.42, 458.86, 661.44, 464.96 Q 669.79, 470.61,\
                        678.23, 476.11 Q 686.55, 481.78, 694.60, 487.84 Q 702.72, 493.81, 711.50, 498.84 Q 719.54, 504.91, 727.91, 510.52 Q 736.22,\
                        516.20, 744.22, 522.33 Q 752.27, 528.40, 760.17, 534.67 Q 768.33, 540.59, 776.38, 546.64 Q 785.40, 551.32, 793.11, 557.87\
                        Q 801.50, 563.44, 809.76, 569.21 Q 818.56, 574.21, 827.22, 579.39 Q 835.42, 585.25, 843.00, 591.98 Q 851.10, 597.97, 859.55,\
                        603.46 Q 867.78, 609.27, 876.20, 614.79 Q 884.42, 620.62, 892.77, 626.25 Q 901.03, 632.02, 909.14, 637.99 Q 917.36, 643.81,\
                        925.62, 649.57 Q 933.99, 655.18, 942.16, 661.08 Q 950.07, 667.33, 958.48, 672.89 Q 966.53, 678.94, 974.70, 684.84 Q 982.05,\
                        691.89, 990.09, 697.97 Q 998.66, 703.30, 1007.51, 708.22 Q 1015.49, 714.38, 1023.89, 719.94 Q 1031.78, 726.23, 1040.36, 731.54\
                        Q 1048.07, 738.08, 1056.85, 743.10 Q 1065.06, 748.94, 1073.52, 754.42 Q 1081.76, 760.21, 1090.00, 766.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 766.00 Q 9.72, 759.44, 17.77, 753.36 Q 26.31, 747.97, 34.31, 741.83 Q\
                        42.67, 736.18, 51.27, 730.88 Q 59.78, 725.45, 67.18, 718.44 Q 75.68, 713.00, 83.84, 707.07 Q 92.48, 701.83, 101.10, 696.56\
                        Q 109.71, 691.27, 118.23, 685.87 Q 125.54, 678.72, 133.75, 672.87 Q 142.50, 667.78, 150.21, 661.20 Q 159.07, 656.28, 166.30,\
                        649.02 Q 175.02, 643.89, 183.43, 638.33 Q 191.65, 632.49, 200.19, 627.10 Q 208.43, 621.30, 216.54, 615.29 Q 225.28, 610.19,\
                        233.47, 604.30 Q 240.87, 597.29, 248.91, 591.20 Q 257.13, 585.36, 265.83, 580.19 Q 273.65, 573.79, 282.69, 569.12 Q 291.41,\
                        563.99, 298.51, 556.55 Q 307.32, 551.55, 316.28, 546.76 Q 323.95, 540.14, 331.16, 532.86 Q 340.25, 528.25, 348.30, 522.17\
                        Q 357.63, 517.91, 365.49, 511.56 Q 373.42, 505.31, 381.80, 499.69 Q 390.00, 493.82, 398.12, 487.84 Q 406.51, 482.24, 414.57,\
                        476.17 Q 422.42, 469.80, 430.64, 463.95 Q 439.70, 459.31, 447.49, 452.85 Q 455.73, 447.04, 463.91, 441.15 Q 471.94, 435.03,\
                        480.00, 428.96 Q 488.75, 423.88, 496.69, 417.63 Q 505.78, 413.04, 513.58, 406.60 Q 522.04, 401.09, 530.51, 395.60 Q 538.14,\
                        388.93, 545.92, 382.47 Q 554.00, 376.42, 562.56, 371.07 Q 571.75, 366.60, 579.93, 360.71 Q 587.42, 353.82, 595.33, 347.54\
                        Q 603.65, 341.83, 612.75, 337.26 Q 621.38, 331.99, 630.11, 326.89 Q 638.68, 321.54, 646.53, 315.17 Q 653.58, 307.66, 661.62,\
                        301.56 Q 670.56, 296.74, 679.07, 291.32 Q 686.90, 284.92, 695.07, 279.01 Q 703.94, 274.09, 712.67, 268.98 Q 720.60, 262.72,\
                        728.49, 256.41 Q 736.59, 250.40, 745.26, 245.20 Q 753.63, 239.58, 761.90, 233.81 Q 769.92, 227.68, 777.92, 221.52 Q 785.89,\
                        215.33, 794.34, 209.81 Q 802.60, 204.03, 811.37, 198.98 Q 819.64, 193.20, 827.60, 186.99 Q 835.27, 180.37, 843.17, 174.06\
                        Q 851.32, 168.12, 859.89, 162.79 Q 868.89, 158.05, 877.67, 153.00 Q 885.50, 146.61, 893.31, 140.19 Q 901.19, 133.86, 909.59,\
                        128.28 Q 917.77, 122.37, 926.01, 116.57 Q 933.94, 110.30, 942.26, 104.60 Q 950.23, 98.40, 958.70, 92.92 Q 967.48, 87.88, 976.23,\
                        82.80 Q 983.72, 75.91, 992.02, 70.19 Q 1000.68, 64.97, 1009.68, 60.24 Q 1017.78, 54.24, 1025.78, 48.08 Q 1034.04, 42.29, 1042.78,\
                        37.20 Q 1050.88, 31.18, 1059.19, 25.47 Q 1067.06, 19.13, 1074.44, 12.08 Q 1083.74, 7.79, 1092.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect889260063" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect889260063" data-review-reference-id="rect889260063">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px;width:275px;" width="275" height="768">\
                     <svg:g width="275" height="768"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.31, 22.85, 0.31 Q 33.27, 0.17, 43.69, 0.51 Q 54.12, 0.54,\
                        64.54, 0.30 Q 74.96, 0.23, 85.38, 0.62 Q 95.81, 0.87, 106.23, 1.28 Q 116.65, 1.17, 127.08, 1.22 Q 137.50, 0.88, 147.92, 0.55\
                        Q 158.35, 0.69, 168.77, 1.46 Q 179.19, 1.28, 189.62, 1.63 Q 200.04, 1.79, 210.46, 2.57 Q 220.88, 1.63, 231.31, 2.15 Q 241.73,\
                        2.02, 252.15, 2.20 Q 262.58, 1.74, 273.04, 1.96 Q 273.12, 12.01, 273.37, 22.05 Q 273.84, 32.10, 273.63, 42.19 Q 272.54, 52.27,\
                        273.19, 62.31 Q 273.91, 72.36, 273.59, 82.42 Q 274.18, 92.47, 273.95, 102.53 Q 273.88, 112.58, 273.59, 122.63 Q 272.92, 132.68,\
                        272.15, 142.74 Q 272.34, 152.79, 273.20, 162.84 Q 272.51, 172.89, 272.28, 182.95 Q 272.22, 193.00, 272.92, 203.05 Q 272.51,\
                        213.11, 272.76, 223.16 Q 271.28, 233.21, 273.16, 243.26 Q 273.28, 253.32, 273.07, 263.37 Q 273.40, 273.42, 273.27, 283.47\
                        Q 273.58, 293.53, 273.21, 303.58 Q 274.25, 313.63, 274.07, 323.68 Q 274.27, 333.74, 273.79, 343.79 Q 274.12, 353.84, 272.00,\
                        363.89 Q 271.48, 373.95, 272.45, 384.00 Q 273.52, 394.05, 273.89, 404.11 Q 273.50, 414.16, 273.57, 424.21 Q 273.88, 434.26,\
                        274.58, 444.32 Q 274.00, 454.37, 273.94, 464.42 Q 273.92, 474.47, 273.80, 484.53 Q 272.95, 494.58, 273.39, 504.63 Q 273.63,\
                        514.68, 273.00, 524.74 Q 273.47, 534.79, 272.63, 544.84 Q 271.99, 554.89, 272.75, 564.95 Q 272.73, 575.00, 273.42, 585.05\
                        Q 272.80, 595.11, 272.67, 605.16 Q 272.95, 615.21, 273.03, 625.26 Q 273.25, 635.32, 273.46, 645.37 Q 273.13, 655.42, 273.71,\
                        665.47 Q 273.45, 675.53, 272.93, 685.58 Q 273.42, 695.63, 274.12, 705.68 Q 273.35, 715.74, 271.22, 725.79 Q 271.17, 735.84,\
                        272.83, 745.89 Q 274.26, 755.95, 273.54, 766.54 Q 262.77, 766.59, 252.27, 766.81 Q 241.81, 767.24, 231.35, 767.28 Q 220.90,\
                        767.03, 210.47, 767.30 Q 200.04, 767.42, 189.62, 767.38 Q 179.19, 766.59, 168.77, 765.70 Q 158.35, 766.09, 147.92, 766.29\
                        Q 137.50, 766.47, 127.08, 765.81 Q 116.65, 766.09, 106.23, 766.90 Q 95.81, 766.63, 85.38, 766.24 Q 74.96, 767.40, 64.54, 767.62\
                        Q 54.12, 766.45, 43.69, 765.76 Q 33.27, 765.88, 22.85, 765.35 Q 12.42, 765.22, 1.88, 766.12 Q 2.09, 755.92, 1.32, 745.99 Q\
                        0.92, 735.91, 1.38, 725.81 Q 1.44, 715.75, 1.90, 705.68 Q 2.01, 695.63, 1.62, 685.58 Q 1.02, 675.53, 0.65, 665.47 Q -0.16,\
                        655.42, 0.30, 645.37 Q 0.84, 635.32, 0.69, 625.26 Q 0.31, 615.21, 0.26, 605.16 Q 0.17, 595.11, 0.04, 585.05 Q 0.28, 575.00,\
                        0.86, 564.95 Q 1.29, 554.89, 1.83, 544.84 Q 1.26, 534.79, 0.55, 524.74 Q 0.22, 514.68, 0.71, 504.63 Q 1.32, 494.58, 2.39,\
                        484.53 Q 1.60, 474.47, 1.39, 464.42 Q 0.91, 454.37, 1.21, 444.32 Q 2.14, 434.26, 1.85, 424.21 Q 1.00, 414.16, 1.63, 404.11\
                        Q 1.96, 394.05, 2.58, 384.00 Q 2.25, 373.95, 1.10, 363.89 Q 0.69, 353.84, 1.57, 343.79 Q 1.82, 333.74, 1.26, 323.68 Q 0.92,\
                        313.63, 1.69, 303.58 Q 2.09, 293.53, 1.95, 283.47 Q 1.59, 273.42, 2.27, 263.37 Q 1.88, 253.32, 1.58, 243.26 Q 1.59, 233.21,\
                        1.85, 223.16 Q 1.58, 213.11, 0.99, 203.05 Q 1.95, 193.00, 1.37, 182.95 Q 2.04, 172.89, 2.19, 162.84 Q 2.51, 152.79, 1.68,\
                        142.74 Q 0.86, 132.68, 0.56, 122.63 Q 0.23, 112.58, 1.73, 102.53 Q 2.04, 92.47, 3.83, 82.42 Q 3.58, 72.37, 3.36, 62.32 Q 2.68,\
                        52.26, 2.29, 42.21 Q 1.62, 32.16, 0.48, 22.11 Q 2.00, 12.05, 2.00, 2.00" style=" fill:#C1C1C1;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-group510150739" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group510150739" data-review-reference-id="group510150739">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="group510150739-group848874950" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group848874950" data-review-reference-id="group848874950">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="group848874950-tabbutton346631860" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton346631860" data-review-reference-id="tabbutton346631860">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                                 <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                                    <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 7.86, 25.50, 6.82, 19.00 Q 6.37, 17.50, 6.10, 15.79 Q 6.82, 14.75,\
                                       6.97, 13.55 Q 8.45, 12.98, 9.11, 12.11 Q 9.91, 11.38, 10.70, 10.50 Q 11.07, 8.81, 12.20, 8.18 Q 14.03, 8.29, 15.94, 8.66 Q\
                                       27.18, 9.17, 38.39, 10.21 Q 49.51, 9.45, 60.67, 9.60 Q 71.84, 9.54, 83.00, 8.69 Q 94.16, 7.06, 105.33, 6.58 Q 116.50, 6.68,\
                                       127.67, 6.47 Q 138.83, 7.82, 150.19, 7.84 Q 151.61, 9.05, 153.12, 9.68 Q 154.19, 10.06, 155.16, 10.66 Q 155.67, 12.19, 155.83,\
                                       13.19 Q 156.27, 13.88, 157.18, 14.49 Q 158.28, 15.12, 158.45, 16.24 Q 158.87, 17.74, 160.19, 18.97 Q 160.26, 25.48, 160.59,\
                                       32.54 Q 149.32, 33.15, 138.07, 33.52 Q 126.89, 33.65, 115.77, 33.86 Q 104.67, 33.87, 93.59, 33.89 Q 82.51, 33.79, 71.43, 33.71\
                                       Q 60.36, 33.11, 49.29, 32.36 Q 38.21, 32.69, 27.14, 33.95 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                                    </svg:g>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'tabbutton346631860\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'tabbutton346631860\', \'result\');" class="selected">\
                                 <div class="smallSkechtedTab">\
                                    <div id="tabbutton346631860_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                                 <div class="bigSkechtedTab">\
                                    <div id="tabbutton346631860_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">RFPL\
                                       							\
                                       <addMouseOverListener></addMouseOverListener>\
                                       							\
                                       <addMouseOutListener></addMouseOutListener>\
                                       						\
                                    </div>\
                                 </div>\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-tabbutton103208529" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton103208529" data-review-reference-id="tabbutton103208529">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 5.89, 25.50, 6.88, 19.00 Q 6.64, 17.50, 7.19, 16.05 Q 6.55, 14.65,\
                                 6.81, 13.49 Q 7.64, 12.60, 8.74, 11.74 Q 9.63, 10.99, 10.31, 9.86 Q 11.17, 9.00, 12.30, 8.41 Q 13.77, 7.60, 15.63, 6.97 Q\
                                 26.98, 6.97, 38.26, 7.29 Q 49.47, 7.64, 60.65, 7.71 Q 71.83, 7.57, 83.00, 7.41 Q 94.16, 7.21, 105.33, 7.18 Q 116.50, 7.45,\
                                 127.67, 7.43 Q 138.83, 7.41, 150.32, 7.05 Q 151.99, 7.50, 153.72, 8.05 Q 154.76, 8.75, 155.52, 9.89 Q 156.48, 10.50, 157.56,\
                                 11.44 Q 158.36, 12.38, 158.68, 13.59 Q 159.10, 14.67, 159.83, 15.64 Q 160.52, 17.11, 161.04, 18.81 Q 160.27, 25.48, 160.22,\
                                 32.21 Q 149.20, 32.81, 137.99, 32.95 Q 126.86, 33.07, 115.76, 33.31 Q 104.66, 33.15, 93.58, 32.81 Q 82.50, 32.85, 71.43, 32.37\
                                 Q 60.36, 33.07, 49.29, 32.47 Q 38.21, 33.26, 27.14, 33.38 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'tabbutton103208529\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'tabbutton103208529\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="tabbutton103208529_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="tabbutton103208529_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Premier League\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-tabbutton550900072" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton550900072" data-review-reference-id="tabbutton550900072">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.39, 25.50, 6.22, 19.00 Q 6.13, 17.50, 6.32, 15.84 Q 7.27, 14.92,\
                                 7.43, 13.76 Q 7.72, 12.64, 8.59, 11.60 Q 10.18, 11.76, 10.99, 10.99 Q 11.39, 9.38, 12.86, 9.69 Q 14.34, 9.09, 15.82, 8.01\
                                 Q 27.13, 8.56, 38.32, 8.74 Q 49.51, 9.51, 60.68, 9.98 Q 71.84, 9.93, 83.00, 9.74 Q 94.17, 8.95, 105.33, 8.14 Q 116.50, 8.05,\
                                 127.67, 8.85 Q 138.83, 8.94, 150.00, 8.99 Q 151.69, 8.74, 153.14, 9.61 Q 154.03, 10.44, 155.19, 10.60 Q 156.27, 10.94, 157.53,\
                                 11.46 Q 157.94, 12.68, 158.47, 13.72 Q 158.46, 15.02, 159.13, 15.94 Q 159.85, 17.37, 159.93, 19.01 Q 160.12, 25.49, 159.95,\
                                 31.96 Q 148.88, 31.86, 137.90, 32.32 Q 126.80, 32.16, 115.72, 32.16 Q 104.65, 32.17, 93.58, 32.97 Q 82.51, 34.02, 71.43, 33.18\
                                 Q 60.36, 31.76, 49.29, 32.78 Q 38.21, 31.56, 27.14, 32.06 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'tabbutton550900072\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'tabbutton550900072\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="tabbutton550900072_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="tabbutton550900072_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">BundesLiga\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-763240328" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="763240328" data-review-reference-id="763240328">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 6.28, 25.50, 6.49, 19.00 Q 6.89, 17.50, 6.87, 15.97 Q 7.57, 15.02,\
                                 8.31, 14.13 Q 8.65, 13.07, 8.85, 11.85 Q 9.48, 10.79, 11.07, 11.12 Q 12.25, 10.95, 13.18, 10.42 Q 14.80, 10.29, 16.20, 10.08\
                                 Q 27.29, 10.39, 38.36, 9.69 Q 49.51, 9.27, 60.66, 8.29 Q 71.83, 8.29, 83.00, 9.16 Q 94.17, 9.62, 105.33, 9.44 Q 116.50, 8.94,\
                                 127.67, 8.84 Q 138.83, 9.16, 149.72, 10.71 Q 151.24, 10.54, 152.63, 11.01 Q 153.69, 11.22, 154.63, 11.80 Q 155.76, 11.99,\
                                 156.54, 12.47 Q 157.11, 13.28, 157.16, 14.50 Q 157.72, 15.43, 159.16, 15.93 Q 159.35, 17.56, 160.76, 18.86 Q 160.37, 25.47,\
                                 160.52, 32.48 Q 149.15, 32.65, 137.97, 32.79 Q 126.81, 32.36, 115.72, 32.32 Q 104.66, 33.35, 93.58, 33.35 Q 82.51, 33.62,\
                                 71.43, 34.02 Q 60.36, 33.78, 49.29, 33.47 Q 38.21, 30.50, 27.14, 30.32 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'763240328\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'763240328\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="763240328_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="763240328_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue 1\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-1193404445" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1193404445" data-review-reference-id="1193404445">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 7.77, 25.50, 7.81, 19.00 Q 8.18, 17.50, 7.45, 16.11 Q 8.16, 15.24,\
                                 8.70, 14.30 Q 8.37, 12.94, 8.81, 11.81 Q 9.63, 10.98, 11.13, 11.21 Q 12.01, 10.52, 12.80, 9.55 Q 13.95, 8.07, 15.74, 7.59\
                                 Q 27.10, 8.25, 38.31, 8.45 Q 49.50, 8.89, 60.67, 8.90 Q 71.84, 9.45, 83.00, 9.14 Q 94.17, 9.06, 105.33, 8.02 Q 116.50, 7.61,\
                                 127.67, 7.95 Q 138.83, 7.31, 150.22, 7.66 Q 151.97, 7.61, 153.77, 7.92 Q 154.84, 8.56, 155.70, 9.51 Q 156.57, 10.32, 157.30,\
                                 11.70 Q 157.48, 13.01, 157.37, 14.38 Q 158.09, 15.22, 158.32, 16.30 Q 158.39, 17.93, 158.42, 19.29 Q 158.19, 25.66, 159.48,\
                                 31.52 Q 148.59, 31.00, 137.82, 31.73 Q 126.83, 32.61, 115.76, 33.40 Q 104.67, 33.55, 93.59, 34.08 Q 82.51, 33.60, 71.43, 33.79\
                                 Q 60.36, 33.63, 49.29, 34.15 Q 38.21, 33.75, 27.14, 32.90 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1193404445\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1193404445\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1193404445_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1193404445_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Ligue BBVA\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-1755474258" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1755474258" data-review-reference-id="1755474258">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute; left: -3px; top: -6px; height: 41px;width:166px;" width="160" height="36">\
                           <svg:g id="target" width="163" height="30" name="target" class="iosTab">\
                              <svg:g class="smallSkechtedTab"><svg:path class=" svg_unselected_element" d="M 7.00, 32.00 Q 5.39, 25.50, 6.04, 19.00 Q 5.66, 17.50, 6.94, 15.99 Q 6.79, 14.74,\
                                 7.02, 13.58 Q 8.00, 12.77, 8.97, 11.97 Q 9.68, 11.06, 10.59, 10.32 Q 11.69, 9.93, 12.69, 9.30 Q 14.18, 8.67, 15.81, 7.99 Q\
                                 27.06, 7.79, 38.30, 8.16 Q 49.48, 7.97, 60.65, 7.77 Q 71.83, 7.71, 83.00, 7.78 Q 94.17, 7.96, 105.33, 7.91 Q 116.50, 7.86,\
                                 127.67, 7.87 Q 138.83, 8.07, 150.21, 7.73 Q 151.76, 8.45, 153.43, 8.82 Q 154.55, 9.24, 155.66, 9.59 Q 156.57, 10.32, 157.63,\
                                 11.36 Q 158.19, 12.50, 158.91, 13.45 Q 159.65, 14.37, 160.23, 15.46 Q 160.86, 16.98, 161.14, 18.79 Q 161.40, 25.37, 160.55,\
                                 32.51 Q 149.27, 33.01, 138.00, 33.00 Q 126.86, 33.09, 115.73, 32.61 Q 104.64, 32.04, 93.58, 32.78 Q 82.50, 32.32, 71.43, 32.71\
                                 Q 60.36, 32.13, 49.29, 32.48 Q 38.21, 32.41, 27.14, 32.22 Q 16.07, 32.00, 5.00, 32.00" style=" fill:white;"/>\
                              </svg:g>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="target" name="target" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1755474258\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1755474258\', \'result\');" class="selected">\
                           <div class="smallSkechtedTab">\
                              <div id="1755474258_div_small" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; height: 24px;width:159px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:6px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                           <div class="bigSkechtedTab">\
                              <div id="1755474258_div_big" class="ClickableSketch helvetica-font" style="line-height: 17px;position: absolute; left: 2px; top: -6px; height: 30px;width:162px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:13px;" xml:space="preserve">Serie A\
                                 							\
                                 <addMouseOverListener></addMouseOverListener>\
                                 							\
                                 <addMouseOutListener></addMouseOutListener>\
                                 						\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect977337239" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect977337239" data-review-reference-id="rect977337239">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px;width:1091px;" width="1091" height="465">\
                     <svg:g width="1091" height="465"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 1.79, 22.13, 1.38 Q 32.19, 1.08, 42.26, 0.69 Q 52.32, 0.44,\
                        62.39, 0.83 Q 72.45, 0.85, 82.52, 0.75 Q 92.58, 0.55, 102.65, 0.38 Q 112.71, 0.93, 122.78, 0.92 Q 132.84, 1.11, 142.91, 0.91\
                        Q 152.97, 0.83, 163.04, 1.04 Q 173.10, 0.81, 183.17, 0.67 Q 193.23, 0.53, 203.30, 0.35 Q 213.36, 0.60, 223.43, 1.15 Q 233.49,\
                        1.70, 243.56, 1.24 Q 253.62, 1.87, 263.69, 1.64 Q 273.75, 1.96, 283.81, 1.73 Q 293.88, 1.56, 303.94, 1.50 Q 314.01, 1.30,\
                        324.07, 1.62 Q 334.14, 1.25, 344.20, 1.45 Q 354.27, 1.62, 364.33, 1.53 Q 374.40, 1.48, 384.46, 1.44 Q 394.53, 1.10, 404.59,\
                        0.71 Q 414.66, 0.99, 424.72, 1.10 Q 434.79, 2.05, 444.85, 2.25 Q 454.92, 2.39, 464.98, 2.74 Q 475.05, 2.35, 485.11, 0.96 Q\
                        495.18, 1.25, 505.24, 1.82 Q 515.31, 2.03, 525.37, 1.26 Q 535.44, 0.62, 545.50, 1.59 Q 555.56, 1.74, 565.63, 1.18 Q 575.69,\
                        1.13, 585.76, 1.43 Q 595.82, 1.75, 605.89, 1.44 Q 615.95, 1.72, 626.02, 1.27 Q 636.08, 1.57, 646.15, 1.12 Q 656.21, 1.56,\
                        666.28, 0.84 Q 676.34, 1.43, 686.41, 1.37 Q 696.47, 0.80, 706.54, 1.57 Q 716.60, 1.02, 726.67, 1.35 Q 736.73, 1.60, 746.80,\
                        1.58 Q 756.86, 1.68, 766.93, 1.54 Q 776.99, 1.35, 787.06, 0.90 Q 797.12, 1.39, 807.19, 0.36 Q 817.25, 0.49, 827.32, 0.61 Q\
                        837.38, 1.04, 847.44, 1.09 Q 857.51, 1.49, 867.57, 1.89 Q 877.64, 1.38, 887.70, 1.44 Q 897.77, 0.89, 907.83, 1.66 Q 917.90,\
                        1.95, 927.96, 1.90 Q 938.03, 1.45, 948.09, 1.95 Q 958.16, 1.12, 968.22, 0.93 Q 978.29, 2.39, 988.35, 2.07 Q 998.42, 2.33,\
                        1008.48, 1.52 Q 1018.55, 1.37, 1028.61, 1.49 Q 1038.68, 1.19, 1048.74, 1.33 Q 1058.81, 1.14, 1068.87, 0.47 Q 1078.94, 0.87,\
                        1088.96, 2.04 Q 1089.23, 11.95, 1089.16, 22.02 Q 1089.85, 32.01, 1089.58, 42.07 Q 1089.20, 52.11, 1089.17, 62.13 Q 1087.79,\
                        72.16, 1088.42, 82.18 Q 1088.12, 92.20, 1088.72, 102.22 Q 1089.27, 112.24, 1088.73, 122.26 Q 1089.43, 132.28, 1089.14, 142.30\
                        Q 1088.96, 152.33, 1089.48, 162.35 Q 1089.78, 172.37, 1089.44, 182.39 Q 1089.78, 192.41, 1090.05, 202.43 Q 1090.79, 212.46,\
                        1089.95, 222.48 Q 1089.97, 232.50, 1090.17, 242.52 Q 1088.84, 252.54, 1089.50, 262.57 Q 1089.82, 272.59, 1089.78, 282.61 Q\
                        1090.31, 292.63, 1090.71, 302.65 Q 1089.41, 312.67, 1089.04, 322.70 Q 1088.99, 332.72, 1089.77, 342.74 Q 1089.96, 352.76,\
                        1089.16, 362.78 Q 1088.30, 372.80, 1088.64, 382.83 Q 1088.86, 392.85, 1088.42, 402.87 Q 1088.50, 412.89, 1089.15, 422.91 Q\
                        1089.33, 432.93, 1089.17, 442.96 Q 1089.44, 452.98, 1088.84, 462.84 Q 1079.10, 463.49, 1069.00, 463.93 Q 1058.84, 463.58,\
                        1048.75, 463.38 Q 1038.69, 463.93, 1028.62, 464.47 Q 1018.55, 464.77, 1008.48, 463.93 Q 998.42, 464.14, 988.35, 463.58 Q 978.29,\
                        464.29, 968.22, 463.82 Q 958.16, 463.69, 948.09, 464.36 Q 938.03, 464.87, 927.96, 464.37 Q 917.90, 464.36, 907.83, 464.18\
                        Q 897.77, 464.29, 887.70, 464.46 Q 877.64, 464.17, 867.57, 463.12 Q 857.51, 462.93, 847.44, 463.24 Q 837.38, 463.55, 827.32,\
                        462.60 Q 817.25, 462.32, 807.19, 462.98 Q 797.12, 463.50, 787.06, 463.37 Q 776.99, 462.99, 766.93, 463.17 Q 756.86, 463.37,\
                        746.80, 462.34 Q 736.73, 462.00, 726.67, 463.56 Q 716.60, 465.19, 706.54, 464.52 Q 696.47, 463.04, 686.41, 462.38 Q 676.34,\
                        462.42, 666.28, 462.75 Q 656.21, 463.07, 646.15, 462.22 Q 636.08, 462.68, 626.02, 463.82 Q 615.95, 464.38, 605.89, 463.62\
                        Q 595.82, 463.32, 585.76, 463.42 Q 575.69, 463.87, 565.63, 463.91 Q 555.56, 463.69, 545.50, 463.91 Q 535.44, 463.71, 525.37,\
                        464.12 Q 515.31, 464.20, 505.24, 464.71 Q 495.18, 464.14, 485.11, 464.05 Q 475.05, 463.96, 464.98, 463.88 Q 454.92, 464.55,\
                        444.85, 465.12 Q 434.79, 464.99, 424.72, 464.06 Q 414.66, 464.11, 404.59, 464.62 Q 394.53, 464.82, 384.46, 464.30 Q 374.40,\
                        464.11, 364.33, 464.12 Q 354.27, 465.00, 344.20, 464.14 Q 334.14, 463.54, 324.07, 462.54 Q 314.01, 462.37, 303.94, 463.72\
                        Q 293.88, 464.00, 283.81, 463.53 Q 273.75, 462.98, 263.69, 464.29 Q 253.62, 463.47, 243.56, 464.15 Q 233.49, 463.53, 223.43,\
                        462.87 Q 213.36, 463.41, 203.30, 464.46 Q 193.23, 465.16, 183.17, 463.85 Q 173.10, 462.85, 163.04, 463.45 Q 152.97, 463.73,\
                        142.91, 463.68 Q 132.84, 464.19, 122.78, 464.42 Q 112.71, 464.48, 102.65, 464.61 Q 92.58, 464.34, 82.52, 464.95 Q 72.45, 464.49,\
                        62.39, 464.29 Q 52.32, 464.85, 42.26, 464.53 Q 32.19, 464.08, 22.13, 463.11 Q 12.06, 462.94, 1.71, 463.29 Q 0.77, 453.39,\
                        0.99, 443.10 Q 1.68, 432.96, 2.39, 422.90 Q 2.70, 412.88, 1.98, 402.87 Q 1.10, 392.85, 0.82, 382.83 Q 0.64, 372.81, 0.84,\
                        362.78 Q 0.75, 352.76, 0.64, 342.74 Q 0.69, 332.72, 0.31, 322.70 Q 0.82, 312.67, 2.03, 302.65 Q 2.01, 292.63, 1.54, 282.61\
                        Q 1.57, 272.59, 0.84, 262.57 Q 1.13, 252.54, 0.90, 242.52 Q 0.46, 232.50, 0.84, 222.48 Q 0.78, 212.46, 1.38, 202.43 Q 1.41,\
                        192.41, 2.22, 182.39 Q 2.07, 172.37, 2.24, 162.35 Q 2.43, 152.33, 2.35, 142.30 Q 2.20, 132.28, 2.77, 122.26 Q 2.40, 112.24,\
                        1.24, 102.22 Q 0.46, 92.20, 0.42, 82.17 Q 0.27, 72.15, 0.15, 62.13 Q 1.04, 52.11, 1.12, 42.09 Q 0.62, 32.07, 0.69, 22.04 Q\
                        2.00, 12.02, 2.00, 2.00" style=" fill:#a7a77c;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect313157706" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect313157706" data-review-reference-id="rect313157706">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px;width:275px;" width="275" height="160">\
                     <svg:g width="275" height="160"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.42, 0.76, 22.85, 0.74 Q 33.27, 0.59, 43.69, 0.75 Q 54.12, 1.08,\
                        64.54, 0.71 Q 74.96, 0.71, 85.38, 0.86 Q 95.81, 0.84, 106.23, 1.28 Q 116.65, 0.79, 127.08, 0.71 Q 137.50, 0.61, 147.92, 0.74\
                        Q 158.35, 1.12, 168.77, 1.13 Q 179.19, 1.22, 189.62, 0.96 Q 200.04, 1.44, 210.46, 1.95 Q 220.88, 1.69, 231.31, 1.17 Q 241.73,\
                        1.24, 252.15, 1.29 Q 262.58, 0.88, 273.22, 1.78 Q 273.91, 12.84, 273.70, 24.19 Q 273.14, 35.42, 273.21, 46.56 Q 273.55, 57.71,\
                        273.63, 68.85 Q 273.66, 80.00, 273.76, 91.14 Q 273.57, 102.29, 273.47, 113.43 Q 273.87, 124.57, 274.04, 135.71 Q 273.52, 146.86,\
                        273.44, 158.44 Q 262.64, 158.19, 252.21, 158.40 Q 241.82, 159.41, 231.33, 158.63 Q 220.91, 159.33, 210.47, 159.28 Q 200.04,\
                        159.65, 189.62, 159.06 Q 179.19, 159.04, 168.77, 159.56 Q 158.35, 159.94, 147.92, 158.03 Q 137.50, 158.21, 127.08, 159.10\
                        Q 116.65, 160.08, 106.23, 159.08 Q 95.81, 157.34, 85.38, 158.15 Q 74.96, 157.88, 64.54, 157.85 Q 54.12, 158.96, 43.69, 158.93\
                        Q 33.27, 159.00, 22.85, 158.73 Q 12.42, 159.00, 1.64, 158.36 Q 1.25, 147.11, 1.10, 135.84 Q 1.04, 124.64, 1.35, 113.45 Q 0.53,\
                        102.31, 2.02, 91.14 Q 1.61, 80.00, 1.46, 68.86 Q 1.25, 57.72, 1.14, 46.57 Q 2.30, 35.43, 1.81, 24.29 Q 2.00, 13.14, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-group935811796" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group935811796" data-review-reference-id="group935811796">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="group935811796-ellipse554337119" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="ellipse554337119" data-review-reference-id="ellipse554337119">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85">\
                           <svg:g width="85" height="85"><svg:path class=" svg_unselected_element" d="M 82.00, 44.00 Q 84.46, 45.03, 82.70, 55.10 Q 78.36, 64.50, 71.22, 72.04 Q 63.17,\
                              78.29, 53.40, 81.43 Q 43.36, 82.24, 33.51, 80.60 Q 24.00, 77.53, 16.23, 70.93 Q 10.40, 62.68, 7.25, 53.15 Q 5.56, 43.38, 7.43,\
                              33.52 Q 12.22, 24.79, 17.74, 16.78 Q 24.99, 9.95, 34.40, 6.03 Q 44.73, 6.67, 54.46, 7.58 Q 63.57, 11.44, 71.01, 18.00 Q 76.94,\
                              25.79, 80.36, 35.00 Q 81.99, 44.66, 80.53, 54.47" style=" fill:white;"/>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="group935811796-text927814406" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text927814406" data-review-reference-id="text927814406">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="group935811796-text604129404" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text604129404" data-review-reference-id="text604129404">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect214085128" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect214085128" data-review-reference-id="rect214085128">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 0.64, 22.11, 0.45 Q 32.17, 0.17, 42.22, 0.02 Q 52.28, -0.05,\
                        62.33, -0.05 Q 72.39, -0.00, 82.44, -0.17 Q 92.50, -0.23, 102.56, -0.01 Q 112.61, 0.22, 122.67, 0.51 Q 132.72, 0.44, 142.78,\
                        0.42 Q 152.83, 0.29, 162.89, 0.11 Q 172.94, 0.87, 183.00, -0.35 Q 193.06, 0.51, 203.11, 0.79 Q 213.17, 0.59, 223.22, 0.60\
                        Q 233.28, 1.28, 243.33, 0.93 Q 253.39, 0.61, 263.44, 1.03 Q 273.50, 1.14, 283.56, 1.06 Q 293.61, 1.08, 303.67, 0.95 Q 313.72,\
                        0.66, 323.78, 0.98 Q 333.83, 1.39, 343.89, 2.13 Q 353.94, 1.70, 364.00, 0.21 Q 374.06, 0.48, 384.11, 0.93 Q 394.17, 2.49,\
                        404.22, 1.08 Q 414.28, 1.05, 424.33, 1.03 Q 434.39, 0.72, 444.44, 0.28 Q 454.50, 0.43, 464.56, 0.71 Q 474.61, 0.97, 484.67,\
                        1.86 Q 494.72, 1.57, 504.78, 1.04 Q 514.83, 1.69, 524.89, 1.34 Q 534.94, 0.99, 545.00, 1.58 Q 555.06, 1.60, 565.11, 0.90 Q\
                        575.17, 1.82, 585.22, 0.79 Q 595.28, 1.53, 605.33, 0.90 Q 615.39, 1.12, 625.44, 1.15 Q 635.50, 0.85, 645.56, 1.60 Q 655.61,\
                        1.54, 665.67, 2.30 Q 675.72, 3.21, 685.78, 2.77 Q 695.83, 3.42, 705.89, 2.40 Q 715.94, 1.20, 726.00, 0.54 Q 736.05, 0.17,\
                        746.11, 0.51 Q 756.17, 1.17, 766.22, 1.02 Q 776.28, 2.64, 786.33, 3.46 Q 796.39, 2.88, 806.44, 1.36 Q 816.50, 0.63, 826.55,\
                        1.30 Q 836.61, 1.87, 846.67, 1.32 Q 856.72, 2.19, 866.78, 0.85 Q 876.83, 1.37, 886.89, 0.67 Q 896.94, 0.26, 907.00, 0.11 Q\
                        917.05, 0.02, 927.11, 0.49 Q 937.17, 0.11, 947.22, 0.28 Q 957.28, 0.54, 967.33, 1.38 Q 977.39, 0.80, 987.44, 0.97 Q 997.50,\
                        1.29, 1007.55, 0.66 Q 1017.61, 1.11, 1027.67, 1.61 Q 1037.72, 1.63, 1047.78, 0.88 Q 1057.83, -0.07, 1067.89, 0.31 Q 1077.94,\
                        0.64, 1088.49, 1.51 Q 1088.32, 14.56, 1088.48, 27.26 Q 1089.01, 39.93, 1089.45, 52.62 Q 1088.69, 65.32, 1088.10, 78.10 Q 1078.08,\
                        78.40, 1068.01, 78.85 Q 1057.92, 79.33, 1047.79, 78.41 Q 1037.72, 77.82, 1027.68, 79.26 Q 1017.62, 79.69, 1007.56, 78.98 Q\
                        997.50, 78.80, 987.44, 78.54 Q 977.39, 78.79, 967.33, 79.16 Q 957.28, 78.81, 947.22, 78.26 Q 937.17, 78.27, 927.11, 78.69\
                        Q 917.05, 79.54, 907.00, 78.24 Q 896.94, 77.94, 886.89, 77.21 Q 876.83, 77.49, 866.78, 78.04 Q 856.72, 78.28, 846.67, 77.70\
                        Q 836.61, 78.07, 826.55, 78.97 Q 816.50, 78.27, 806.44, 78.30 Q 796.39, 78.23, 786.33, 77.79 Q 776.28, 77.88, 766.22, 78.49\
                        Q 756.17, 79.43, 746.11, 79.09 Q 736.05, 78.81, 726.00, 78.83 Q 715.94, 79.61, 705.89, 79.29 Q 695.83, 78.75, 685.78, 77.73\
                        Q 675.72, 77.04, 665.67, 77.80 Q 655.61, 77.96, 645.56, 78.05 Q 635.50, 78.33, 625.44, 79.32 Q 615.39, 80.18, 605.33, 79.98\
                        Q 595.28, 79.29, 585.22, 78.55 Q 575.17, 77.51, 565.11, 77.99 Q 555.06, 78.19, 545.00, 79.01 Q 534.94, 78.41, 524.89, 77.19\
                        Q 514.83, 78.29, 504.78, 79.49 Q 494.72, 79.49, 484.67, 78.33 Q 474.61, 78.27, 464.56, 78.66 Q 454.50, 79.34, 444.44, 79.43\
                        Q 434.39, 79.57, 424.33, 79.67 Q 414.28, 78.77, 404.22, 79.55 Q 394.17, 79.48, 384.11, 78.69 Q 374.06, 78.84, 364.00, 79.72\
                        Q 353.94, 79.29, 343.89, 79.32 Q 333.83, 78.69, 323.78, 78.24 Q 313.72, 78.06, 303.67, 77.72 Q 293.61, 77.99, 283.56, 78.47\
                        Q 273.50, 78.53, 263.44, 77.84 Q 253.39, 77.37, 243.33, 77.45 Q 233.28, 77.92, 223.22, 78.50 Q 213.17, 77.98, 203.11, 78.27\
                        Q 193.06, 78.78, 183.00, 78.60 Q 172.94, 78.64, 162.89, 78.64 Q 152.83, 78.72, 142.78, 78.98 Q 132.72, 79.31, 122.67, 79.82\
                        Q 112.61, 79.39, 102.56, 78.99 Q 92.50, 79.08, 82.44, 79.06 Q 72.39, 78.98, 62.33, 78.83 Q 52.28, 78.18, 42.22, 77.42 Q 32.17,\
                        77.38, 22.11, 77.75 Q 12.06, 78.76, 1.56, 78.44 Q 0.74, 65.75, 0.95, 52.82 Q 1.64, 40.02, 2.18, 27.33 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect994032070" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect994032070" data-review-reference-id="rect994032070">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px;width:1090px;" width="1090" height="80">\
                     <svg:g width="1090" height="80"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.06, 0.72, 22.11, 0.67 Q 32.17, 0.68, 42.22, 1.26 Q 52.28, 1.30,\
                        62.33, 0.96 Q 72.39, 1.26, 82.44, 1.80 Q 92.50, 0.94, 102.56, 1.71 Q 112.61, 1.72, 122.67, 1.39 Q 132.72, 1.19, 142.78, 2.22\
                        Q 152.83, 2.99, 162.89, 3.02 Q 172.94, 2.37, 183.00, 1.15 Q 193.06, 1.69, 203.11, 1.89 Q 213.17, 1.88, 223.22, 2.00 Q 233.28,\
                        1.96, 243.33, 2.34 Q 253.39, 2.12, 263.44, 2.55 Q 273.50, 1.58, 283.56, 2.53 Q 293.61, 2.62, 303.67, 2.69 Q 313.72, 2.79,\
                        323.78, 3.17 Q 333.83, 2.67, 343.89, 1.86 Q 353.94, 1.38, 364.00, 0.38 Q 374.06, 1.00, 384.11, 0.94 Q 394.17, 1.47, 404.22,\
                        1.31 Q 414.28, 1.34, 424.33, 1.35 Q 434.39, 1.16, 444.44, 1.77 Q 454.50, 0.78, 464.56, 1.63 Q 474.61, 1.74, 484.67, 2.14 Q\
                        494.72, 1.85, 504.78, 1.75 Q 514.83, 2.95, 524.89, 3.31 Q 534.94, 1.77, 545.00, 0.99 Q 555.06, 1.38, 565.11, 1.76 Q 575.17,\
                        1.55, 585.22, 1.62 Q 595.28, 2.05, 605.33, 2.67 Q 615.39, 3.00, 625.44, 3.36 Q 635.50, 2.54, 645.56, 3.40 Q 655.61, 2.33,\
                        665.67, 1.62 Q 675.72, 0.80, 685.78, 0.52 Q 695.83, 1.33, 705.89, 1.23 Q 715.94, 0.72, 726.00, 0.45 Q 736.05, 0.54, 746.11,\
                        1.09 Q 756.17, 0.78, 766.22, 0.37 Q 776.28, 0.51, 786.33, 1.23 Q 796.39, 0.46, 806.44, 1.09 Q 816.50, 0.63, 826.55, 1.25 Q\
                        836.61, 0.98, 846.67, 1.72 Q 856.72, 1.23, 866.78, 1.48 Q 876.83, 2.00, 886.89, 2.36 Q 896.94, 2.32, 907.00, 1.75 Q 917.05,\
                        1.98, 927.11, 1.91 Q 937.17, 2.15, 947.22, 1.27 Q 957.28, 2.12, 967.33, 2.36 Q 977.39, 2.56, 987.44, 2.10 Q 997.50, 1.81,\
                        1007.55, 1.58 Q 1017.61, 2.15, 1027.67, 2.58 Q 1037.72, 2.40, 1047.78, 3.06 Q 1057.83, 3.46, 1067.89, 2.55 Q 1077.94, 1.72,\
                        1088.34, 1.66 Q 1089.10, 14.30, 1089.52, 27.12 Q 1089.95, 39.87, 1089.84, 52.61 Q 1089.52, 65.31, 1089.03, 79.04 Q 1078.43,\
                        79.49, 1068.10, 79.49 Q 1057.87, 78.60, 1047.80, 78.69 Q 1037.74, 79.30, 1027.67, 78.20 Q 1017.61, 78.15, 1007.55, 77.63 Q\
                        997.50, 77.11, 987.44, 76.83 Q 977.39, 77.53, 967.33, 78.33 Q 957.28, 77.88, 947.22, 77.54 Q 937.17, 78.29, 927.11, 79.53\
                        Q 917.05, 80.03, 907.00, 80.13 Q 896.94, 80.13, 886.89, 80.23 Q 876.83, 80.06, 866.78, 79.87 Q 856.72, 79.50, 846.67, 79.50\
                        Q 836.61, 79.61, 826.55, 79.94 Q 816.50, 79.86, 806.44, 79.17 Q 796.39, 78.96, 786.33, 79.18 Q 776.28, 79.89, 766.22, 80.05\
                        Q 756.17, 79.48, 746.11, 79.76 Q 736.05, 79.82, 726.00, 79.57 Q 715.94, 79.23, 705.89, 79.32 Q 695.83, 79.33, 685.78, 79.11\
                        Q 675.72, 78.44, 665.67, 77.62 Q 655.61, 78.93, 645.56, 79.41 Q 635.50, 78.77, 625.44, 78.65 Q 615.39, 79.49, 605.33, 79.57\
                        Q 595.28, 79.19, 585.22, 78.99 Q 575.17, 79.48, 565.11, 79.27 Q 555.06, 78.46, 545.00, 77.79 Q 534.94, 77.90, 524.89, 78.10\
                        Q 514.83, 78.51, 504.78, 78.23 Q 494.72, 78.23, 484.67, 78.18 Q 474.61, 78.54, 464.56, 77.87 Q 454.50, 77.91, 444.44, 77.74\
                        Q 434.39, 78.84, 424.33, 78.48 Q 414.28, 78.08, 404.22, 78.48 Q 394.17, 78.81, 384.11, 78.98 Q 374.06, 78.68, 364.00, 78.04\
                        Q 353.94, 79.31, 343.89, 79.58 Q 333.83, 79.46, 323.78, 78.91 Q 313.72, 79.58, 303.67, 78.46 Q 293.61, 78.61, 283.56, 78.12\
                        Q 273.50, 78.32, 263.44, 79.50 Q 253.39, 79.16, 243.33, 79.90 Q 233.28, 79.93, 223.22, 79.94 Q 213.17, 78.93, 203.11, 79.08\
                        Q 193.06, 78.22, 183.00, 77.73 Q 172.94, 77.80, 162.89, 77.91 Q 152.83, 77.56, 142.78, 77.23 Q 132.72, 77.63, 122.67, 77.59\
                        Q 112.61, 78.90, 102.56, 78.25 Q 92.50, 78.28, 82.44, 77.93 Q 72.39, 78.16, 62.33, 77.76 Q 52.28, 78.79, 42.22, 79.41 Q 32.17,\
                        78.88, 22.11, 79.42 Q 12.06, 79.78, 1.18, 78.82 Q 1.14, 65.62, 1.36, 52.76 Q 1.41, 40.04, 1.81, 27.34 Q 2.00, 14.67, 2.00,\
                        2.00" style=" fill:grey;opacity:0.5;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-image974156449" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image974156449" data-review-reference-id="image974156449">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60">\
                     <svg:g width="230" height="60"><svg:path id="id" class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.27, 0.84, 22.55, 1.05 Q 32.82, 1.33, 43.09, 1.16 Q\
                        53.36, 1.76, 63.64, 1.76 Q 73.91, 1.68, 84.18, 1.56 Q 94.45, 1.89, 104.73, 2.34 Q 115.00, 2.55, 125.27, 1.93 Q 135.55, 1.69,\
                        145.82, 1.63 Q 156.09, 2.21, 166.36, 1.00 Q 176.64, 0.46, 186.91, 1.47 Q 197.18, 1.41, 207.45, 1.80 Q 217.73, 2.75, 227.79,\
                        2.21 Q 228.26, 15.91, 228.45, 29.94 Q 228.51, 43.97, 228.59, 58.59 Q 217.99, 58.81, 207.60, 59.05 Q 197.19, 58.08, 186.92,\
                        58.28 Q 176.64, 58.43, 166.37, 59.27 Q 156.10, 59.59, 145.82, 59.22 Q 135.55, 59.86, 125.27, 57.77 Q 115.00, 58.95, 104.73,\
                        59.10 Q 94.45, 58.31, 84.18, 58.64 Q 73.91, 58.18, 63.64, 58.11 Q 53.36, 57.82, 43.09, 58.35 Q 32.82, 59.40, 22.55, 58.68\
                        Q 12.27, 59.02, 1.73, 58.27 Q 1.38, 44.21, 1.36, 30.09 Q 2.00, 16.00, 2.00, 2.00" style="fill:white;stroke-width:1.5;"/><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.27, 4.56, 22.53, 7.15 Q 32.80, 9.69, 43.27, 11.47 Q 53.53,\
                        14.07, 63.95, 16.01 Q 74.19, 18.66, 84.44, 21.32 Q 94.64, 24.17, 104.73, 27.46 Q 115.10, 29.60, 125.42, 31.95 Q 135.66, 34.62,\
                        145.88, 37.37 Q 156.18, 39.84, 166.54, 42.02 Q 176.76, 44.78, 187.14, 46.89 Q 197.45, 49.29, 207.52, 52.66 Q 217.73, 55.45,\
                        228.00, 58.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 2.00, 58.00 Q 12.65, 56.64, 22.87, 53.49 Q 33.22, 50.91, 43.34, 47.36 Q 53.91,\
                        45.65, 64.36, 43.44 Q 74.54, 40.16, 84.84, 37.34 Q 95.14, 34.55, 105.61, 32.46 Q 115.89, 29.57, 126.36, 27.43 Q 136.74, 24.98,\
                        146.99, 21.95 Q 157.65, 20.61, 167.96, 17.84 Q 178.19, 14.77, 188.43, 11.71 Q 199.07, 10.31, 209.24, 6.95 Q 219.64, 4.55,\
                        230.00, 2.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-button31216625" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="button31216625" data-review-reference-id="button31216625">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, -0.24, 22.62, -0.26 Q 32.92, 0.08, 43.23, -0.00 Q 53.54,\
                        0.26, 63.85, 0.39 Q 74.15, 0.36, 84.46, 0.74 Q 94.77, 0.90, 105.08, 1.17 Q 115.38, 1.15, 125.69, 1.28 Q 136.00, 1.14, 146.31,\
                        1.02 Q 156.62, 1.06, 166.92, 2.00 Q 177.23, 2.13, 187.54, 1.69 Q 197.85, 1.84, 208.15, 1.46 Q 218.46, 1.27, 228.77, 0.99 Q\
                        239.08, 1.01, 249.38, 1.34 Q 259.69, 1.56, 270.31, 1.69 Q 270.78, 12.49, 270.45, 23.44 Q 270.78, 34.20, 270.39, 45.39 Q 259.96,\
                        45.84, 249.34, 44.70 Q 239.03, 44.32, 228.76, 44.85 Q 218.45, 44.29, 208.15, 44.50 Q 197.84, 44.14, 187.54, 44.45 Q 177.23,\
                        44.96, 166.92, 45.22 Q 156.62, 45.54, 146.31, 45.50 Q 136.00, 45.25, 125.69, 45.35 Q 115.38, 45.27, 105.08, 45.22 Q 94.77,\
                        45.70, 84.46, 45.23 Q 74.15, 45.97, 63.85, 45.30 Q 53.54, 45.62, 43.23, 45.37 Q 32.92, 45.01, 22.62, 45.82 Q 12.31, 45.68,\
                        1.91, 45.09 Q 1.96, 34.26, 1.39, 23.59 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#a7a77c;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.35, 15.00, 272.34, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.47, 16.00, 273.51, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.20, 17.00, 273.67, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.06, 24.69, 44.97 Q 35.04, 44.84, 45.38, 45.40 Q 55.73,\
                        45.36, 66.08, 44.93 Q 76.42, 44.91, 86.77, 45.66 Q 97.12, 45.89, 107.46, 46.06 Q 117.81, 45.55, 128.15, 45.61 Q 138.50, 44.63,\
                        148.85, 45.34 Q 159.19, 45.68, 169.54, 46.21 Q 179.88, 46.44, 190.23, 46.44 Q 200.58, 46.32, 210.92, 47.18 Q 221.27, 46.95,\
                        231.62, 46.75 Q 241.96, 47.09, 252.31, 46.39 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 44.54, 25.69, 45.41 Q 36.04, 45.95, 46.38, 46.45 Q 56.73,\
                        47.49, 67.08, 47.40 Q 77.42, 46.61, 87.77, 47.67 Q 98.12, 46.69, 108.46, 47.49 Q 118.81, 48.40, 129.15, 47.26 Q 139.50, 45.74,\
                        149.85, 46.14 Q 160.19, 47.17, 170.54, 47.07 Q 180.88, 45.73, 191.23, 45.26 Q 201.58, 46.50, 211.92, 46.91 Q 222.27, 47.88,\
                        232.62, 48.03 Q 242.96, 47.85, 253.31, 46.88 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.13, 26.69, 47.01 Q 37.04, 46.93, 47.38, 46.83 Q 57.73,\
                        47.15, 68.08, 47.24 Q 78.42, 47.62, 88.77, 47.03 Q 99.12, 46.94, 109.46, 47.18 Q 119.81, 47.75, 130.15, 47.84 Q 140.50, 47.35,\
                        150.85, 47.73 Q 161.19, 47.08, 171.54, 46.91 Q 181.88, 46.43, 192.23, 46.81 Q 202.58, 47.29, 212.92, 47.56 Q 223.27, 47.94,\
                        233.62, 48.27 Q 243.96, 48.54, 254.31, 48.67 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button31216625button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button31216625button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button31216625button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Коэффициенты<br />  \
                     			</button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-text901840057" style="position: absolute; left: 1052px; top: 20px; width: 239px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text901840057" data-review-reference-id="text901840057">\
            <div class="stencil-wrapper" style="width: 239px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Коэффициенты</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-button828406697" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button828406697" data-review-reference-id="button828406697">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 2.74, 22.62, 2.93 Q 32.92, 2.39, 43.23, 3.03 Q 53.54, 1.51,\
                        63.85, 0.82 Q 74.15, 1.00, 84.46, 1.71 Q 94.77, 1.28, 105.08, 1.04 Q 115.38, 0.90, 125.69, 0.70 Q 136.00, 0.62, 146.31, 0.37\
                        Q 156.62, 0.07, 166.92, 0.75 Q 177.23, 2.23, 187.54, 1.01 Q 197.85, 0.66, 208.15, 0.35 Q 218.46, 0.40, 228.77, 0.22 Q 239.08,\
                        0.06, 249.38, -0.09 Q 259.69, 0.16, 270.84, 1.16 Q 271.37, 12.29, 271.77, 23.25 Q 272.06, 34.11, 271.06, 46.06 Q 260.09, 46.24,\
                        249.59, 46.50 Q 239.09, 45.28, 228.78, 45.24 Q 218.46, 45.23, 208.16, 46.38 Q 197.85, 46.91, 187.54, 45.58 Q 177.23, 45.69,\
                        166.92, 45.70 Q 156.62, 46.28, 146.31, 46.36 Q 136.00, 46.55, 125.69, 46.64 Q 115.38, 46.79, 105.08, 46.37 Q 94.77, 46.44,\
                        84.46, 46.60 Q 74.15, 46.69, 63.85, 46.33 Q 53.54, 46.26, 43.23, 45.53 Q 32.92, 45.88, 22.62, 46.19 Q 12.31, 46.23, 1.28,\
                        45.72 Q 0.90, 34.62, 0.53, 23.71 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 273.76, 15.00, 273.05, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 270.48, 16.00, 270.42, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 273.14, 17.00, 272.79, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.04, 24.69, 45.00 Q 35.04, 45.59, 45.38, 45.35 Q 55.73,\
                        45.92, 66.08, 46.22 Q 76.42, 45.20, 86.77, 45.14 Q 97.12, 45.23, 107.46, 46.10 Q 117.81, 46.97, 128.15, 47.04 Q 138.50, 46.17,\
                        148.85, 45.28 Q 159.19, 45.06, 169.54, 45.10 Q 179.88, 44.90, 190.23, 44.80 Q 200.58, 44.90, 210.92, 44.94 Q 221.27, 45.28,\
                        231.62, 45.59 Q 241.96, 45.96, 252.31, 46.03 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 48.18, 25.69, 48.18 Q 36.04, 47.31, 46.38, 47.65 Q 56.73,\
                        46.71, 67.08, 46.65 Q 77.42, 46.78, 87.77, 46.96 Q 98.12, 47.45, 108.46, 46.74 Q 118.81, 46.28, 129.15, 46.74 Q 139.50, 46.38,\
                        149.85, 45.83 Q 160.19, 45.36, 170.54, 45.55 Q 180.88, 46.38, 191.23, 47.23 Q 201.58, 46.59, 211.92, 47.50 Q 222.27, 46.69,\
                        232.62, 46.96 Q 242.96, 46.11, 253.31, 46.59 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 47.12, 26.69, 47.65 Q 37.04, 48.94, 47.38, 48.37 Q 57.73,\
                        47.48, 68.08, 49.95 Q 78.42, 49.03, 88.77, 47.76 Q 99.12, 45.76, 109.46, 45.95 Q 119.81, 47.78, 130.15, 49.60 Q 140.50, 49.01,\
                        150.85, 48.98 Q 161.19, 48.84, 171.54, 48.24 Q 181.88, 47.03, 192.23, 45.97 Q 202.58, 46.76, 212.92, 46.52 Q 223.27, 46.95,\
                        233.62, 48.78 Q 243.96, 47.96, 254.31, 48.88 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button828406697button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button828406697button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button828406697button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Изменения<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button828406697\', \'interaction241219712\', {"button":"left","id":"action520718713","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction205821286","options":"reloadOnly","target":"page189476043","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button164524961" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button164524961" data-review-reference-id="button164524961">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.34, 22.62, 1.15 Q 32.92, 1.06, 43.23, 0.90 Q 53.54, 1.34,\
                        63.85, 1.46 Q 74.15, 1.18, 84.46, 0.90 Q 94.77, 1.03, 105.08, 1.55 Q 115.38, 1.31, 125.69, 1.86 Q 136.00, 2.11, 146.31, 1.77\
                        Q 156.62, 1.25, 166.92, 1.08 Q 177.23, 1.17, 187.54, 1.25 Q 197.85, 1.58, 208.15, 1.84 Q 218.46, 2.19, 228.77, 2.00 Q 239.08,\
                        1.72, 249.38, 2.83 Q 259.69, 2.96, 269.41, 2.59 Q 269.79, 12.82, 269.52, 23.57 Q 269.74, 34.27, 270.31, 45.31 Q 259.87, 45.56,\
                        249.42, 45.26 Q 239.08, 45.08, 228.76, 44.81 Q 218.47, 45.34, 208.16, 45.50 Q 197.85, 45.61, 187.54, 45.32 Q 177.23, 45.43,\
                        166.92, 45.37 Q 156.62, 45.37, 146.31, 45.07 Q 136.00, 45.64, 125.69, 45.16 Q 115.38, 45.87, 105.08, 45.91 Q 94.77, 45.78,\
                        84.46, 45.10 Q 74.15, 44.95, 63.85, 44.36 Q 53.54, 44.88, 43.23, 44.76 Q 32.92, 44.29, 22.62, 45.84 Q 12.31, 45.31, 2.08,\
                        44.92 Q 2.52, 34.08, 2.58, 23.42 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.36, 15.00, 272.39, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 271.98, 16.00, 272.29, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 271.22, 17.00, 271.89, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 44.13, 24.69, 43.94 Q 35.04, 44.72, 45.38, 44.32 Q 55.73,\
                        44.40, 66.08, 44.91 Q 76.42, 43.92, 86.77, 44.33 Q 97.12, 44.10, 107.46, 44.59 Q 117.81, 45.38, 128.15, 45.98 Q 138.50, 45.61,\
                        148.85, 44.63 Q 159.19, 44.41, 169.54, 45.06 Q 179.88, 45.62, 190.23, 45.02 Q 200.58, 45.14, 210.92, 45.17 Q 221.27, 45.18,\
                        231.62, 44.56 Q 241.96, 44.17, 252.31, 44.55 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 47.04, 25.69, 46.24 Q 36.04, 45.75, 46.38, 45.79 Q 56.73,\
                        45.79, 67.08, 45.75 Q 77.42, 45.36, 87.77, 45.69 Q 98.12, 45.42, 108.46, 45.02 Q 118.81, 44.95, 129.15, 45.07 Q 139.50, 45.56,\
                        149.85, 45.31 Q 160.19, 45.17, 170.54, 45.30 Q 180.88, 45.17, 191.23, 45.33 Q 201.58, 45.34, 211.92, 45.67 Q 222.27, 45.63,\
                        232.62, 45.81 Q 242.96, 44.84, 253.31, 45.09 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 49.06, 26.69, 48.05 Q 37.04, 47.84, 47.38, 47.33 Q 57.73,\
                        46.80, 68.08, 47.12 Q 78.42, 45.64, 88.77, 46.38 Q 99.12, 45.87, 109.46, 46.49 Q 119.81, 47.38, 130.15, 48.14 Q 140.50, 47.59,\
                        150.85, 46.21 Q 161.19, 46.60, 171.54, 47.13 Q 181.88, 46.65, 192.23, 46.18 Q 202.58, 46.13, 212.92, 46.24 Q 223.27, 47.00,\
                        233.62, 46.48 Q 243.96, 46.75, 254.31, 47.47 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button164524961button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button164524961button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button164524961button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Статистика<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button164524961\', \'interaction851416437\', {"button":"left","id":"action986234028","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction791259442","options":"reloadOnly","target":"page995954813","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button961258309" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button961258309" data-review-reference-id="button961258309">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.61, 22.62, 0.70 Q 32.92, 0.72, 43.23, 0.49 Q 53.54, 1.67,\
                        63.85, 0.77 Q 74.15, 0.57, 84.46, 0.32 Q 94.77, 1.71, 105.08, 2.00 Q 115.38, 1.59, 125.69, 0.47 Q 136.00, 0.41, 146.31, 1.08\
                        Q 156.62, 0.23, 166.92, 0.50 Q 177.23, 1.91, 187.54, 1.62 Q 197.85, 0.53, 208.15, 1.75 Q 218.46, 1.24, 228.77, 1.19 Q 239.08,\
                        0.48, 249.38, 0.27 Q 259.69, 0.25, 270.71, 1.29 Q 270.46, 12.60, 270.48, 23.43 Q 270.29, 34.23, 270.52, 45.52 Q 259.91, 45.69,\
                        249.48, 45.71 Q 239.00, 43.76, 228.75, 44.21 Q 218.45, 43.92, 208.15, 44.60 Q 197.85, 45.24, 187.54, 45.19 Q 177.23, 45.06,\
                        166.92, 44.17 Q 156.62, 45.22, 146.31, 44.97 Q 136.00, 44.97, 125.69, 44.67 Q 115.38, 45.13, 105.08, 45.55 Q 94.77, 45.95,\
                        84.46, 46.02 Q 74.15, 46.02, 63.85, 46.26 Q 53.54, 46.43, 43.23, 46.64 Q 32.92, 46.73, 22.62, 46.68 Q 12.31, 46.55, 1.41,\
                        45.59 Q 1.74, 34.34, 2.37, 23.45 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.50, 15.00, 270.99, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 274.44, 16.00, 274.46, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.34, 17.00, 274.57, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 43.66, 24.69, 43.77 Q 35.04, 44.12, 45.38, 44.21 Q 55.73,\
                        44.67, 66.08, 44.61 Q 76.42, 44.05, 86.77, 43.83 Q 97.12, 44.25, 107.46, 44.15 Q 117.81, 44.46, 128.15, 44.90 Q 138.50, 44.91,\
                        148.85, 44.68 Q 159.19, 44.39, 169.54, 44.25 Q 179.88, 44.38, 190.23, 44.73 Q 200.58, 45.05, 210.92, 44.99 Q 221.27, 44.88,\
                        231.62, 44.85 Q 241.96, 44.72, 252.31, 44.84 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.66, 25.69, 46.43 Q 36.04, 45.91, 46.38, 45.80 Q 56.73,\
                        46.19, 67.08, 46.32 Q 77.42, 46.36, 87.77, 46.33 Q 98.12, 45.88, 108.46, 45.95 Q 118.81, 45.43, 129.15, 46.02 Q 139.50, 44.95,\
                        149.85, 45.86 Q 160.19, 45.83, 170.54, 46.03 Q 180.88, 46.21, 191.23, 45.88 Q 201.58, 45.20, 211.92, 45.71 Q 222.27, 45.37,\
                        232.62, 45.14 Q 242.96, 46.76, 253.31, 46.87 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 46.45, 26.69, 46.41 Q 37.04, 46.26, 47.38, 46.19 Q 57.73,\
                        46.27, 68.08, 46.54 Q 78.42, 46.65, 88.77, 46.48 Q 99.12, 46.40, 109.46, 46.77 Q 119.81, 47.42, 130.15, 46.58 Q 140.50, 46.88,\
                        150.85, 47.56 Q 161.19, 46.60, 171.54, 46.76 Q 181.88, 46.35, 192.23, 46.54 Q 202.58, 46.59, 212.92, 47.16 Q 223.27, 47.62,\
                        233.62, 48.51 Q 243.96, 48.07, 254.31, 47.22 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button961258309button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button961258309button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button961258309button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Выход<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button961258309\', \'interaction486932185\', {"button":"left","id":"action456130266","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction157429537","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button856441484" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button856441484" data-review-reference-id="button856441484">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 50px;width:275px;" width="275" height="50">\
                     <svg:g width="275" height="50"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 1.53, 22.62, 1.26 Q 32.92, 1.43, 43.23, 1.50 Q 53.54, 1.46,\
                        63.85, 1.82 Q 74.15, 1.33, 84.46, 1.75 Q 94.77, 0.94, 105.08, 1.44 Q 115.38, 1.53, 125.69, 1.56 Q 136.00, 1.34, 146.31, 0.96\
                        Q 156.62, 1.93, 166.92, 2.72 Q 177.23, 3.05, 187.54, 1.21 Q 197.85, 1.62, 208.15, 1.89 Q 218.46, 1.98, 228.77, 1.84 Q 239.08,\
                        1.29, 249.38, 1.93 Q 259.69, 2.34, 269.99, 2.01 Q 270.26, 12.66, 270.54, 23.42 Q 270.55, 34.21, 270.02, 45.02 Q 259.82, 45.41,\
                        249.40, 45.12 Q 239.01, 43.89, 228.75, 44.31 Q 218.45, 44.52, 208.16, 45.96 Q 197.85, 45.42, 187.54, 45.67 Q 177.23, 45.73,\
                        166.92, 45.95 Q 156.62, 45.71, 146.31, 45.36 Q 136.00, 45.80, 125.69, 45.66 Q 115.38, 46.48, 105.08, 46.22 Q 94.77, 47.11,\
                        84.46, 45.86 Q 74.15, 45.55, 63.85, 45.81 Q 53.54, 45.84, 43.23, 46.26 Q 32.92, 45.06, 22.62, 46.39 Q 12.31, 47.04, 1.19,\
                        45.81 Q 0.62, 34.71, 0.47, 23.72 Q 2.00, 12.75, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 271.88, 15.00, 271.32, 26.00 Q 271.00, 37.00, 271.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 271.79, 16.00, 272.32, 27.00 Q 272.00, 38.00, 272.00, 49.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.26, 17.00, 273.97, 28.00 Q 273.00, 39.00, 273.00, 50.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 46.00 Q 14.35, 45.10, 24.69, 45.59 Q 35.04, 45.59, 45.38, 45.69 Q 55.73,\
                        45.51, 66.08, 45.51 Q 76.42, 45.75, 86.77, 46.07 Q 97.12, 45.84, 107.46, 45.78 Q 117.81, 45.39, 128.15, 45.59 Q 138.50, 46.25,\
                        148.85, 45.69 Q 159.19, 45.87, 169.54, 46.18 Q 179.88, 46.87, 190.23, 46.18 Q 200.58, 45.86, 210.92, 45.79 Q 221.27, 45.63,\
                        231.62, 45.13 Q 241.96, 45.32, 252.31, 45.62 Q 262.65, 46.00, 273.00, 46.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 47.00 Q 15.35, 46.43, 25.69, 45.75 Q 36.04, 45.38, 46.38, 45.10 Q 56.73,\
                        45.07, 67.08, 45.47 Q 77.42, 45.30, 87.77, 45.21 Q 98.12, 45.06, 108.46, 45.00 Q 118.81, 45.00, 129.15, 45.35 Q 139.50, 45.25,\
                        149.85, 45.07 Q 160.19, 45.35, 170.54, 45.37 Q 180.88, 45.29, 191.23, 45.19 Q 201.58, 45.00, 211.92, 44.93 Q 222.27, 45.31,\
                        232.62, 45.39 Q 242.96, 45.69, 253.31, 45.81 Q 263.65, 47.00, 274.00, 47.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 48.00 Q 16.35, 48.08, 26.69, 48.54 Q 37.04, 48.15, 47.38, 48.23 Q 57.73,\
                        47.54, 68.08, 47.20 Q 78.42, 47.46, 88.77, 48.03 Q 99.12, 47.89, 109.46, 46.23 Q 119.81, 46.25, 130.15, 45.84 Q 140.50, 46.19,\
                        150.85, 46.59 Q 161.19, 47.23, 171.54, 47.42 Q 181.88, 47.37, 192.23, 47.17 Q 202.58, 45.92, 212.92, 45.84 Q 223.27, 46.17,\
                        233.62, 46.82 Q 243.96, 46.94, 254.31, 45.99 Q 264.65, 48.00, 275.00, 48.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button856441484button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button856441484button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button856441484button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:46px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Настройки<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button856441484\', \'interaction593811501\', {"button":"left","id":"action290429271","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction975357200","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button668127624" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button668127624" data-review-reference-id="button668127624">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" style="position:absolute; left:0; top:-2px;" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="position: absolute;top: 2px;height: 48px;width:275px;" width="275" height="48">\
                     <svg:g width="275" height="48"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.31, 2.02, 22.62, 1.69 Q 32.92, 1.53, 43.23, 1.43 Q 53.54, 1.41,\
                        63.85, 1.07 Q 74.15, 0.85, 84.46, 0.95 Q 94.77, 1.21, 105.08, 1.17 Q 115.38, 0.90, 125.69, 0.61 Q 136.00, 0.55, 146.31, 0.36\
                        Q 156.62, 0.85, 166.92, 0.88 Q 177.23, 1.01, 187.54, 1.49 Q 197.85, 0.67, 208.15, 1.59 Q 218.46, 0.78, 228.77, 0.84 Q 239.08,\
                        1.07, 249.38, 0.74 Q 259.69, 0.78, 270.56, 1.44 Q 270.92, 11.94, 271.38, 22.30 Q 271.28, 32.66, 270.27, 43.27 Q 259.79, 43.30,\
                        249.50, 43.84 Q 239.11, 43.57, 228.78, 43.50 Q 218.46, 43.16, 208.15, 42.31 Q 197.85, 44.38, 187.54, 43.35 Q 177.23, 43.70,\
                        166.92, 43.09 Q 156.62, 43.59, 146.31, 44.13 Q 136.00, 43.37, 125.69, 43.79 Q 115.38, 43.17, 105.08, 42.64 Q 94.77, 43.21,\
                        84.46, 43.14 Q 74.15, 43.13, 63.85, 43.80 Q 53.54, 43.37, 43.23, 43.48 Q 32.92, 43.64, 22.62, 43.03 Q 12.31, 43.98, 1.36,\
                        43.64 Q 0.88, 33.12, 1.02, 22.64 Q 2.00, 12.25, 2.00, 2.00" style=" fill:#d9d9d9;"/><svg:path class=" svg_unselected_element" d="M 271.00, 4.00 Q 272.09, 14.50, 272.54, 25.00 Q 271.00, 35.50, 271.00, 46.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 272.00, 5.00 Q 273.74, 15.50, 272.71, 26.00 Q 272.00, 36.50, 272.00, 47.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 273.00, 6.00 Q 274.39, 16.50, 273.79, 27.00 Q 273.00, 37.50, 273.00, 48.00"\
                        style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 4.00, 44.00 Q 14.35, 42.04, 24.69, 42.96 Q 35.04, 42.79, 45.38, 44.43 Q 55.73,\
                        43.75, 66.08, 43.72 Q 76.42, 44.51, 86.77, 45.32 Q 97.12, 44.56, 107.46, 43.05 Q 117.81, 41.96, 128.15, 42.79 Q 138.50, 42.56,\
                        148.85, 43.08 Q 159.19, 43.61, 169.54, 43.59 Q 179.88, 43.68, 190.23, 43.44 Q 200.58, 43.03, 210.92, 43.92 Q 221.27, 42.91,\
                        231.62, 42.38 Q 241.96, 43.19, 252.31, 43.56 Q 262.65, 44.00, 273.00, 44.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 5.00, 45.00 Q 15.35, 43.18, 25.69, 43.38 Q 36.04, 43.42, 46.38, 43.76 Q 56.73,\
                        43.85, 67.08, 43.79 Q 77.42, 43.91, 87.77, 44.19 Q 98.12, 44.25, 108.46, 44.16 Q 118.81, 44.15, 129.15, 44.11 Q 139.50, 44.39,\
                        149.85, 44.20 Q 160.19, 43.90, 170.54, 43.93 Q 180.88, 43.82, 191.23, 44.48 Q 201.58, 44.18, 211.92, 43.79 Q 222.27, 43.92,\
                        232.62, 43.76 Q 242.96, 43.41, 253.31, 43.70 Q 263.65, 45.00, 274.00, 45.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 6.00, 46.00 Q 16.35, 46.65, 26.69, 46.43 Q 37.04, 46.47, 47.38, 46.88 Q 57.73,\
                        47.11, 68.08, 46.86 Q 78.42, 46.32, 88.77, 46.85 Q 99.12, 47.03, 109.46, 46.17 Q 119.81, 47.06, 130.15, 47.08 Q 140.50, 46.51,\
                        150.85, 45.97 Q 161.19, 45.72, 171.54, 45.94 Q 181.88, 45.48, 192.23, 45.70 Q 202.58, 46.30, 212.92, 45.34 Q 223.27, 45.22,\
                        233.62, 46.37 Q 243.96, 46.63, 254.31, 45.90 Q 264.65, 46.00, 275.00, 46.00" style=" fill:none;"/>\
                     </svg:g>\
                  </svg:svg><button id="__containerId__-page606870902-layer-button668127624button" type="button" onmouseover="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOver, \'__containerId__-page606870902-layer-button668127624button\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.buttonMouseOut, &#34;__containerId__-page606870902-layer-button668127624button&#34;);" title="" class="ClickableSketch" style="position: absolute; width:271px;height:44px;font-size:1.8333333333333333em;cursor:pointer;color:black" xml:space="preserve">\
                     				Контакты<br />  \
                     			</button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button668127624\', \'interaction567474680\', {"button":"left","id":"action76226304","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction234973819","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-text434713023" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text434713023" data-review-reference-id="text434713023">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-accordion411519902" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="accordion411519902" data-review-reference-id="accordion411519902">\
            <div class="stencil-wrapper" style="width: 930px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page606870902-layer-accordion411519902-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:930px;" width="930" height="395">\
                     <svg:g id="__containerId__-page606870902-layer-accordion411519902svg" width="930" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 0.53, 22.13, 0.67 Q 32.20, 0.41, 42.26, 1.20 Q 52.33, 1.17,\
                        62.39, 0.67 Q 72.46, 1.07, 82.52, 1.80 Q 92.59, 2.16, 102.65, 1.83 Q 112.72, 1.68, 122.78, 0.96 Q 132.85, 1.28, 142.91, 1.21\
                        Q 152.98, 1.15, 163.04, 1.59 Q 173.11, 1.63, 183.17, 2.07 Q 193.24, 1.73, 203.30, 1.62 Q 213.37, 1.97, 223.43, 2.02 Q 233.50,\
                        1.86, 243.57, 1.06 Q 253.63, 1.48, 263.70, 1.26 Q 273.76, 1.79, 283.83, 0.97 Q 293.89, 0.82, 303.96, 1.06 Q 314.02, 0.84,\
                        324.09, 0.74 Q 334.15, -0.00, 344.22, 0.13 Q 354.28, -0.04, 364.35, 1.30 Q 374.41, 1.37, 384.48, 1.84 Q 394.54, 1.63, 404.61,\
                        1.51 Q 414.67, 1.98, 424.74, 1.81 Q 434.80, 1.52, 444.87, 1.76 Q 454.93, 1.91, 465.00, 2.27 Q 475.07, 1.93, 485.13, 0.92 Q\
                        495.20, 0.71, 505.26, 1.30 Q 515.33, 1.14, 525.39, 0.80 Q 535.46, 0.62, 545.52, 2.20 Q 555.59, 1.87, 565.65, 1.25 Q 575.72,\
                        0.71, 585.78, 1.18 Q 595.85, 1.72, 605.91, 1.43 Q 615.98, 0.99, 626.04, 0.82 Q 636.11, 1.12, 646.17, 0.56 Q 656.24, 0.06,\
                        666.30, -0.03 Q 676.37, 0.40, 686.44, 0.72 Q 696.50, 0.76, 706.57, 0.24 Q 716.63, 0.29, 726.70, 0.82 Q 736.76, 0.69, 746.83,\
                        1.11 Q 756.89, 2.06, 766.96, 2.28 Q 777.02, 2.52, 787.09, 2.43 Q 797.15, 1.84, 807.22, 1.32 Q 817.28, 1.90, 827.35, 1.44 Q\
                        837.41, 1.92, 847.48, 1.65 Q 857.54, 1.66, 867.61, 1.00 Q 877.67, 1.09, 887.74, 1.15 Q 897.81, 1.10, 907.87, 1.36 Q 917.94,\
                        1.71, 928.04, 1.96 Q 928.74, 12.04, 927.76, 22.61 Q 927.81, 32.88, 929.14, 43.12 Q 929.42, 53.42, 929.77, 63.72 Q 930.11,\
                        74.02, 929.24, 84.31 Q 929.04, 94.60, 929.16, 104.89 Q 928.72, 115.18, 929.04, 125.47 Q 929.40, 135.76, 929.41, 146.05 Q 929.06,\
                        156.34, 927.92, 166.63 Q 928.20, 176.92, 929.14, 187.21 Q 929.10, 197.50, 928.00, 207.79 Q 927.98, 218.08, 928.26, 228.37\
                        Q 928.91, 238.66, 928.57, 248.95 Q 928.52, 259.24, 928.49, 269.53 Q 928.63, 279.82, 927.92, 290.11 Q 928.20, 300.39, 928.38,\
                        310.68 Q 928.64, 320.97, 928.97, 331.26 Q 928.98, 341.55, 928.66, 351.84 Q 928.68, 362.13, 929.41, 372.42 Q 929.71, 382.71,\
                        928.88, 393.88 Q 918.34, 394.21, 908.08, 394.49 Q 897.91, 394.62, 887.80, 394.75 Q 877.70, 394.59, 867.62, 394.86 Q 857.55,\
                        394.09, 847.48, 394.12 Q 837.42, 394.40, 827.35, 394.35 Q 817.28, 394.55, 807.22, 394.73 Q 797.15, 394.80, 787.09, 394.83\
                        Q 777.02, 394.87, 766.96, 394.61 Q 756.89, 394.59, 746.83, 394.13 Q 736.76, 393.92, 726.70, 393.71 Q 716.63, 393.15, 706.57,\
                        393.32 Q 696.50, 392.22, 686.44, 393.01 Q 676.37, 392.19, 666.30, 392.78 Q 656.24, 392.91, 646.17, 393.12 Q 636.11, 393.09,\
                        626.04, 393.50 Q 615.98, 393.72, 605.91, 393.54 Q 595.85, 393.91, 585.78, 394.36 Q 575.72, 394.36, 565.65, 393.33 Q 555.59,\
                        393.43, 545.52, 393.63 Q 535.46, 393.76, 525.39, 393.64 Q 515.33, 393.89, 505.26, 394.55 Q 495.20, 394.28, 485.13, 394.17\
                        Q 475.07, 394.37, 465.00, 394.08 Q 454.93, 394.27, 444.87, 394.63 Q 434.80, 394.59, 424.74, 394.37 Q 414.67, 394.53, 404.61,\
                        393.27 Q 394.54, 393.22, 384.48, 392.53 Q 374.41, 392.40, 364.35, 393.41 Q 354.28, 392.68, 344.22, 392.23 Q 334.15, 391.79,\
                        324.09, 392.80 Q 314.02, 392.76, 303.96, 393.63 Q 293.89, 393.57, 283.83, 393.30 Q 273.76, 393.67, 263.70, 394.14 Q 253.63,\
                        394.52, 243.57, 394.61 Q 233.50, 394.29, 223.43, 393.48 Q 213.37, 393.76, 203.30, 393.45 Q 193.24, 393.31, 183.17, 393.77\
                        Q 173.11, 393.64, 163.04, 393.50 Q 152.98, 393.46, 142.91, 394.31 Q 132.85, 393.50, 122.78, 393.88 Q 112.72, 393.99, 102.65,\
                        393.96 Q 92.59, 394.03, 82.52, 393.92 Q 72.46, 393.79, 62.39, 393.08 Q 52.33, 393.75, 42.26, 393.83 Q 32.20, 394.09, 22.13,\
                        394.52 Q 12.07, 394.38, 1.29, 393.71 Q 1.41, 382.91, 1.74, 372.46 Q 2.12, 362.12, 1.18, 351.87 Q 1.11, 341.57, 1.21, 331.27\
                        Q 0.56, 320.98, 0.41, 310.69 Q 0.38, 300.40, 0.28, 290.11 Q 0.41, 279.82, 0.14, 269.53 Q 0.07, 259.24, 0.13, 248.95 Q -0.04,\
                        238.66, 0.50, 228.37 Q 0.74, 218.08, 1.55, 207.79 Q 1.54, 197.50, 1.20, 187.21 Q 0.07, 176.92, 0.67, 166.63 Q 1.10, 156.34,\
                        0.53, 146.05 Q -0.21, 135.76, 0.56, 125.47 Q 0.49, 115.18, -0.08, 104.89 Q -0.15, 94.61, -0.08, 84.32 Q 0.59, 74.03, 0.54,\
                        63.74 Q 0.88, 53.45, 1.04, 43.16 Q 1.36, 32.87, 1.37, 22.58 Q 2.00, 12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page606870902-layer-accordion411519902-accordion", "926", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page606870902-layer-table574065384" style="position: absolute; left: 355px; top: 225px; width: 929px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="table574065384" data-review-reference-id="table574065384">\
            <div class="stencil-wrapper" style="width: 929px; height: 273px">\
               <div title="">\
                  <svg:svg xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 273px;width:929px;" width="929" height="273">\
                     <svg:g x="0" y="0" width="929" height="273" style="stroke:black;stroke-width:1px;fill:white;"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.05, 2.65, 22.11, 2.64 Q 32.16, 3.13, 42.22, 1.84 Q 52.27, 1.91,\
                        62.33, 1.78 Q 72.38, 0.94, 82.43, 0.84 Q 92.49, 1.43, 102.54, 1.64 Q 112.60, 2.55, 122.65, 2.04 Q 132.71, 2.89, 142.76, 1.31\
                        Q 152.82, 0.60, 162.87, 1.12 Q 172.92, 0.91, 182.98, 0.71 Q 193.03, 0.28, 203.09, 0.15 Q 213.14, 0.41, 223.20, 0.94 Q 233.25,\
                        0.42, 243.30, -0.09 Q 253.36, 1.04, 263.41, 1.50 Q 273.47, 1.40, 283.52, -0.05 Q 293.58, -0.15, 303.63, 0.75 Q 313.68, 2.35,\
                        323.74, 2.23 Q 333.79, 2.08, 343.85, 1.96 Q 353.90, 1.39, 363.96, 1.07 Q 374.01, 2.17, 384.07, 0.97 Q 394.12, 0.51, 404.17,\
                        0.62 Q 414.23, 0.68, 424.28, 0.95 Q 434.34, 1.55, 444.39, 1.64 Q 454.45, 1.66, 464.50, 1.23 Q 474.55, 1.11, 484.61, 0.37 Q\
                        494.66, 0.72, 504.72, 1.71 Q 514.77, 2.08, 524.83, 2.26 Q 534.88, 2.30, 544.93, 1.93 Q 554.99, 1.13, 565.04, 0.22 Q 575.10,\
                        -0.13, 585.15, 0.34 Q 595.21, 1.07, 605.26, 1.27 Q 615.32, 2.83, 625.37, 2.50 Q 635.42, 2.07, 645.48, 1.72 Q 655.53, 1.57,\
                        665.59, 2.10 Q 675.64, 1.74, 685.70, 2.05 Q 695.75, 1.99, 705.80, 1.17 Q 715.86, 1.01, 725.91, 1.05 Q 735.97, 1.94, 746.02,\
                        2.52 Q 756.08, 2.58, 766.13, 1.67 Q 776.18, 0.40, 786.24, 0.74 Q 796.29, 0.59, 806.35, 0.41 Q 816.40, -0.11, 826.46, -0.06\
                        Q 836.51, 0.01, 846.56, 0.46 Q 856.62, 0.47, 866.67, 0.64 Q 876.73, 0.10, 886.78, -0.07 Q 896.84, -0.21, 906.89, 0.45 Q 916.94,\
                        0.41, 927.89, 1.11 Q 928.22, 11.94, 928.15, 22.53 Q 928.53, 32.94, 928.08, 43.35 Q 928.43, 53.71, 928.19, 64.07 Q 928.38,\
                        74.42, 928.44, 84.77 Q 928.41, 95.11, 928.11, 105.46 Q 928.16, 115.81, 927.69, 126.15 Q 927.05, 136.50, 928.41, 146.85 Q 928.46,\
                        157.19, 928.53, 167.54 Q 928.83, 177.88, 928.61, 188.23 Q 928.61, 198.58, 927.85, 208.92 Q 928.14, 219.27, 927.90, 229.62\
                        Q 928.28, 239.96, 928.24, 250.31 Q 927.66, 260.65, 927.35, 271.35 Q 917.22, 271.84, 906.91, 271.14 Q 896.81, 270.62, 886.78,\
                        270.91 Q 876.74, 271.66, 866.68, 272.07 Q 856.62, 272.09, 846.57, 272.02 Q 836.51, 271.48, 826.46, 271.68 Q 816.40, 271.63,\
                        806.35, 271.42 Q 796.29, 271.01, 786.24, 270.47 Q 776.18, 271.79, 766.13, 271.30 Q 756.08, 271.13, 746.02, 271.22 Q 735.97,\
                        270.89, 725.91, 270.93 Q 715.86, 271.05, 705.80, 272.37 Q 695.75, 273.00, 685.70, 272.52 Q 675.64, 272.84, 665.59, 272.63\
                        Q 655.53, 272.04, 645.48, 271.37 Q 635.42, 271.13, 625.37, 270.88 Q 615.32, 269.92, 605.26, 270.43 Q 595.21, 270.96, 585.15,\
                        270.97 Q 575.10, 271.79, 565.04, 272.16 Q 554.99, 271.95, 544.93, 271.83 Q 534.88, 271.73, 524.83, 271.24 Q 514.77, 271.64,\
                        504.72, 272.04 Q 494.66, 272.01, 484.61, 272.65 Q 474.55, 272.74, 464.50, 272.44 Q 454.45, 272.29, 444.39, 272.56 Q 434.34,\
                        271.95, 424.28, 272.41 Q 414.23, 272.24, 404.17, 272.22 Q 394.12, 271.84, 384.07, 271.39 Q 374.01, 272.01, 363.96, 272.05\
                        Q 353.90, 271.75, 343.85, 272.09 Q 333.79, 271.45, 323.74, 272.89 Q 313.68, 271.28, 303.63, 271.80 Q 293.58, 271.95, 283.52,\
                        271.65 Q 273.47, 271.83, 263.41, 272.21 Q 253.36, 272.58, 243.30, 272.07 Q 233.25, 271.12, 223.20, 271.41 Q 213.14, 271.88,\
                        203.09, 272.05 Q 193.03, 271.83, 182.98, 271.72 Q 172.92, 271.93, 162.87, 271.78 Q 152.82, 271.42, 142.76, 271.74 Q 132.71,\
                        271.70, 122.65, 272.55 Q 112.60, 273.07, 102.54, 272.76 Q 92.49, 272.79, 82.43, 272.44 Q 72.38, 272.12, 62.33, 272.44 Q 52.27,\
                        273.04, 42.22, 271.67 Q 32.16, 272.60, 22.11, 271.39 Q 12.05, 270.96, 2.12, 270.88 Q 1.97, 260.66, 0.51, 250.52 Q -0.25, 240.11,\
                        1.26, 229.64 Q 1.41, 219.28, 0.72, 208.93 Q -0.21, 198.59, 0.16, 188.23 Q 0.19, 177.89, 0.09, 167.54 Q 0.01, 157.19, 0.05,\
                        146.85 Q 0.46, 136.50, 1.60, 126.15 Q 1.04, 115.81, 1.23, 105.46 Q 1.28, 95.12, 1.41, 84.77 Q 1.27, 74.42, 0.74, 64.08 Q 1.30,\
                        53.73, 0.55, 43.38 Q 0.17, 33.04, 0.05, 22.69 Q 2.00, 12.35, 2.00, 2.00" style=" fill:white;"/><svg:path class=" svg_unselected_element"\
                        d="M 299.00, 0.00 Q 299.06, 10.50, 298.57, 21.00 Q 298.14, 31.50, 298.60, 42.00 Q 299.36, 52.50, 298.47, 63.00 Q 298.10, 73.50,\
                        298.20, 84.00 Q 299.76, 94.50, 299.09, 105.00 Q 298.86, 115.50, 298.51, 126.00 Q 299.48, 136.50, 298.63, 147.00 Q 299.26,\
                        157.50, 298.87, 168.00 Q 299.11, 178.50, 300.47, 189.00 Q 299.90, 199.50, 299.58, 210.00 Q 299.23, 220.50, 299.74, 231.00\
                        Q 299.04, 241.50, 298.54, 252.00 Q 299.00, 262.50, 299.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 466.00, 0.00 Q 464.82, 10.50, 465.61, 21.00 Q 465.53, 31.50, 466.18, 42.00 Q 465.35, 52.50, 464.93, 63.00 Q 466.28, 73.50,\
                        466.19, 84.00 Q 466.49, 94.50, 466.42, 105.00 Q 466.28, 115.50, 465.53, 126.00 Q 466.32, 136.50, 465.98, 147.00 Q 464.30,\
                        157.50, 465.18, 168.00 Q 465.52, 178.50, 465.72, 189.00 Q 466.51, 199.50, 466.12, 210.00 Q 465.54, 220.50, 466.19, 231.00\
                        Q 465.80, 241.50, 465.59, 252.00 Q 466.00, 262.50, 466.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 633.00, 0.00 Q 634.86, 10.50, 634.91, 21.00 Q 635.05, 31.50, 635.15, 42.00 Q 634.98, 52.50, 634.93, 63.00 Q 634.97, 73.50,\
                        635.13, 84.00 Q 635.17, 94.50, 634.83, 105.00 Q 634.60, 115.50, 634.40, 126.00 Q 634.50, 136.50, 634.52, 147.00 Q 634.69,\
                        157.50, 634.98, 168.00 Q 635.00, 178.50, 635.12, 189.00 Q 634.91, 199.50, 634.56, 210.00 Q 633.91, 220.50, 633.84, 231.00\
                        Q 633.62, 241.50, 633.62, 252.00 Q 633.00, 262.50, 633.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 800.00, 0.00 Q 801.44, 10.50, 801.48, 21.00 Q 801.62, 31.50, 801.23, 42.00 Q 801.36, 52.50, 801.51, 63.00 Q 801.36, 73.50,\
                        801.01, 84.00 Q 800.44, 94.50, 800.79, 105.00 Q 801.30, 115.50, 801.08, 126.00 Q 801.33, 136.50, 800.69, 147.00 Q 800.20,\
                        157.50, 800.05, 168.00 Q 799.83, 178.50, 799.18, 189.00 Q 799.63, 199.50, 800.01, 210.00 Q 800.52, 220.50, 800.60, 231.00\
                        Q 800.41, 241.50, 800.33, 252.00 Q 800.00, 262.50, 800.00, 273.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 0.00, 39.00 Q 10.10, 37.10, 20.20, 37.00 Q 30.29, 37.33, 40.39, 37.35 Q 50.49, 37.47, 60.59, 37.70 Q 70.68, 37.62, 80.78,\
                        37.57 Q 90.88, 38.10, 100.98, 38.36 Q 111.08, 38.81, 121.17, 38.52 Q 131.27, 38.35, 141.37, 38.33 Q 151.47, 38.31, 161.57,\
                        38.09 Q 171.66, 38.79, 181.76, 38.74 Q 191.86, 39.41, 201.96, 39.16 Q 212.05, 39.29, 222.15, 39.13 Q 232.25, 38.57, 242.35,\
                        38.26 Q 252.45, 38.37, 262.54, 38.10 Q 272.64, 38.51, 282.74, 38.53 Q 292.84, 38.71, 302.93, 38.56 Q 313.03, 38.50, 323.13,\
                        37.85 Q 333.23, 38.26, 343.33, 38.62 Q 353.42, 39.11, 363.52, 39.10 Q 373.62, 39.99, 383.72, 39.92 Q 393.82, 40.18, 403.91,\
                        39.72 Q 414.01, 39.67, 424.11, 39.47 Q 434.21, 39.72, 444.30, 39.45 Q 454.40, 39.39, 464.50, 39.39 Q 474.60, 39.17, 484.70,\
                        38.92 Q 494.79, 38.32, 504.89, 38.18 Q 514.99, 38.57, 525.09, 37.74 Q 535.19, 38.37, 545.28, 38.63 Q 555.38, 39.29, 565.48,\
                        39.44 Q 575.58, 39.76, 585.67, 39.46 Q 595.77, 38.32, 605.87, 38.51 Q 615.97, 37.86, 626.07, 38.84 Q 636.16, 38.39, 646.26,\
                        38.49 Q 656.36, 38.14, 666.46, 37.84 Q 676.55, 37.42, 686.65, 37.15 Q 696.75, 37.82, 706.85, 37.76 Q 716.95, 38.30, 727.04,\
                        38.12 Q 737.14, 38.67, 747.24, 39.81 Q 757.34, 39.45, 767.44, 39.18 Q 777.53, 39.09, 787.63, 39.29 Q 797.73, 38.25, 807.83,\
                        38.97 Q 817.92, 38.68, 828.02, 38.62 Q 838.12, 38.38, 848.22, 37.92 Q 858.32, 38.19, 868.41, 38.54 Q 878.51, 38.91, 888.61,\
                        38.23 Q 898.71, 39.01, 908.81, 38.79 Q 918.90, 39.00, 929.00, 39.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 0.00, 78.00 Q 10.10, 78.19, 20.20, 78.76 Q 30.29, 78.71, 40.39, 79.44 Q 50.49, 78.50, 60.59, 78.24 Q 70.68, 78.42, 80.78,\
                        79.26 Q 90.88, 78.93, 100.98, 78.62 Q 111.08, 78.73, 121.17, 78.57 Q 131.27, 78.13, 141.37, 77.70 Q 151.47, 77.35, 161.57,\
                        78.28 Q 171.66, 78.36, 181.76, 78.32 Q 191.86, 77.50, 201.96, 77.14 Q 212.05, 76.83, 222.15, 77.42 Q 232.25, 77.40, 242.35,\
                        77.36 Q 252.45, 77.19, 262.54, 77.07 Q 272.64, 77.23, 282.74, 77.42 Q 292.84, 77.66, 302.93, 76.20 Q 313.03, 76.58, 323.13,\
                        77.47 Q 333.23, 77.57, 343.33, 77.52 Q 353.42, 77.52, 363.52, 77.84 Q 373.62, 76.99, 383.72, 77.66 Q 393.82, 77.58, 403.91,\
                        77.97 Q 414.01, 77.71, 424.11, 77.25 Q 434.21, 77.37, 444.30, 77.40 Q 454.40, 77.23, 464.50, 77.33 Q 474.60, 77.40, 484.70,\
                        77.41 Q 494.79, 78.58, 504.89, 77.23 Q 514.99, 77.30, 525.09, 77.56 Q 535.19, 77.96, 545.28, 77.90 Q 555.38, 77.16, 565.48,\
                        78.87 Q 575.58, 78.63, 585.67, 78.78 Q 595.77, 77.61, 605.87, 76.22 Q 615.97, 76.01, 626.07, 76.13 Q 636.16, 76.55, 646.26,\
                        76.73 Q 656.36, 78.70, 666.46, 77.35 Q 676.55, 77.99, 686.65, 77.15 Q 696.75, 76.88, 706.85, 77.08 Q 716.95, 78.08, 727.04,\
                        78.26 Q 737.14, 77.75, 747.24, 77.45 Q 757.34, 77.79, 767.44, 77.49 Q 777.53, 78.04, 787.63, 76.73 Q 797.73, 76.38, 807.83,\
                        76.95 Q 817.92, 77.18, 828.02, 76.65 Q 838.12, 76.83, 848.22, 76.36 Q 858.32, 76.44, 868.41, 76.32 Q 878.51, 76.75, 888.61,\
                        76.53 Q 898.71, 76.35, 908.81, 76.30 Q 918.90, 78.00, 929.00, 78.00" style=" fill:none;"/><svg:path class=" svg_unselected_element"\
                        d="M 0.00, 117.00 Q 10.10, 116.19, 20.20, 116.38 Q 30.29, 116.25, 40.39, 116.18 Q 50.49, 115.65, 60.59, 116.02 Q 70.68, 115.77,\
                        80.78, 115.97 Q 90.88, 116.19, 100.98, 115.69 Q 111.08, 115.89, 121.17, 116.27 Q 131.27, 116.07, 141.37, 115.08 Q 151.47,\
                        116.01, 161.57, 114.83 Q 171.66, 115.27, 181.76, 115.72 Q 191.86, 115.35, 201.96, 115.17 Q 212.05, 115.16, 222.15, 115.11\
                        Q 232.25, 114.93, 242.35, 115.16 Q 252.45, 115.13, 262.54, 115.08 Q 272.64, 115.00, 282.74, 114.75 Q 292.84, 115.30, 302.93,\
                        116.57 Q 313.03, 115.47, 323.13, 114.87 Q 333.23, 116.25, 343.33, 115.73 Q 353.42, 116.15, 363.52, 116.62 Q 373.62, 116.47,\
                        383.72, 116.49 Q 393.82, 115.85, 403.91, 115.49 Q 414.01, 115.31, 424.11, 115.31 Q 434.21, 115.30, 444.30, 115.18 Q 454.40,\
                        115.43, 464.50, 115.25 Q 474.60, 115.23, 484.70, 115.67 Q 494.79, 115.74, 504.89, 116.01 Q 514.99, 115.84, 525.09, 115.90\
                        Q 535.19, 115.80, 545.28, 115.63 Q 555.38, 115.24, 565.48, 114.98 Q 575.58, 114.98, 585.67, 115.03 Q 595.77, 115.70, 605.87,\
                        116.45 Q 615.97, 116.89, 626.07, 115.75 Q 636.16, 116.06, 646.26, 115.47 Q 656.36, 116.95, 666.46, 117.95 Q 676.55, 117.49,\
                        686.65, 116.68 Q 696.75, 115.95, 706.85, 116.78 Q 716.95, 117.09, 727.04, 116.65 Q 737.14, 117.52, 747.24, 116.52 Q 757.34,\
                        116.99, 767.44, 116.82 Q 777.53, 116.71, 787.63, 115.87 Q 797.73, 115.44, 807.83, 115.50 Q 817.92, 115.87, 828.02, 115.49\
                        Q 838.12, 115.94, 848.22, 115.59 Q 858.32, 115.02, 868.41, 115.54 Q 878.51, 115.10, 888.61, 115.28 Q 898.71, 115.88, 908.81,\
                        115.23 Q 918.90, 117.00, 929.00, 117.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 156.00\
                        Q 10.10, 155.00, 20.20, 155.15 Q 30.29, 155.58, 40.39, 155.29 Q 50.49, 154.10, 60.59, 154.74 Q 70.68, 154.52, 80.78, 155.52\
                        Q 90.88, 155.44, 100.98, 154.31 Q 111.08, 154.63, 121.17, 155.05 Q 131.27, 154.83, 141.37, 154.41 Q 151.47, 156.05, 161.57,\
                        156.11 Q 171.66, 156.92, 181.76, 155.96 Q 191.86, 154.66, 201.96, 154.02 Q 212.05, 154.06, 222.15, 153.93 Q 232.25, 153.79,\
                        242.35, 153.66 Q 252.45, 153.64, 262.54, 154.03 Q 272.64, 153.94, 282.74, 154.26 Q 292.84, 155.19, 302.93, 156.44 Q 313.03,\
                        156.97, 323.13, 155.49 Q 333.23, 155.96, 343.33, 155.97 Q 353.42, 156.58, 363.52, 155.22 Q 373.62, 154.72, 383.72, 154.62\
                        Q 393.82, 154.29, 403.91, 154.25 Q 414.01, 154.66, 424.11, 154.62 Q 434.21, 154.45, 444.30, 154.38 Q 454.40, 154.19, 464.50,\
                        154.55 Q 474.60, 154.38, 484.70, 154.62 Q 494.79, 154.65, 504.89, 154.37 Q 514.99, 155.30, 525.09, 154.66 Q 535.19, 154.82,\
                        545.28, 154.98 Q 555.38, 154.44, 565.48, 154.49 Q 575.58, 154.77, 585.67, 154.53 Q 595.77, 154.77, 605.87, 155.55 Q 615.97,\
                        156.84, 626.07, 156.92 Q 636.16, 156.32, 646.26, 156.00 Q 656.36, 155.63, 666.46, 156.75 Q 676.55, 156.81, 686.65, 154.93\
                        Q 696.75, 155.72, 706.85, 155.74 Q 716.95, 155.58, 727.04, 154.79 Q 737.14, 154.35, 747.24, 155.22 Q 757.34, 155.64, 767.44,\
                        154.79 Q 777.53, 154.90, 787.63, 153.95 Q 797.73, 154.95, 807.83, 154.34 Q 817.92, 154.55, 828.02, 155.23 Q 838.12, 154.84,\
                        848.22, 154.99 Q 858.32, 155.15, 868.41, 154.25 Q 878.51, 154.21, 888.61, 154.20 Q 898.71, 154.06, 908.81, 153.97 Q 918.90,\
                        156.00, 929.00, 156.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 195.00 Q 10.10, 192.48,\
                        20.20, 192.50 Q 30.29, 192.50, 40.39, 192.61 Q 50.49, 192.84, 60.59, 192.72 Q 70.68, 192.51, 80.78, 192.90 Q 90.88, 193.88,\
                        100.98, 193.51 Q 111.08, 193.05, 121.17, 194.04 Q 131.27, 192.84, 141.37, 193.48 Q 151.47, 193.09, 161.57, 193.14 Q 171.66,\
                        193.96, 181.76, 194.21 Q 191.86, 193.83, 201.96, 193.75 Q 212.05, 193.38, 222.15, 193.33 Q 232.25, 194.44, 242.35, 194.75\
                        Q 252.45, 193.42, 262.54, 193.77 Q 272.64, 194.24, 282.74, 194.52 Q 292.84, 194.38, 302.93, 193.98 Q 313.03, 193.68, 323.13,\
                        193.28 Q 333.23, 193.51, 343.33, 193.12 Q 353.42, 192.91, 363.52, 192.75 Q 373.62, 192.73, 383.72, 192.65 Q 393.82, 192.86,\
                        403.91, 193.20 Q 414.01, 193.73, 424.11, 193.36 Q 434.21, 193.26, 444.30, 193.19 Q 454.40, 193.62, 464.50, 194.80 Q 474.60,\
                        196.00, 484.70, 195.54 Q 494.79, 194.63, 504.89, 193.67 Q 514.99, 193.68, 525.09, 194.00 Q 535.19, 194.03, 545.28, 194.51\
                        Q 555.38, 194.54, 565.48, 194.98 Q 575.58, 195.35, 585.67, 195.08 Q 595.77, 194.92, 605.87, 194.51 Q 615.97, 194.14, 626.07,\
                        194.09 Q 636.16, 193.67, 646.26, 193.52 Q 656.36, 193.34, 666.46, 194.36 Q 676.55, 195.05, 686.65, 193.74 Q 696.75, 193.25,\
                        706.85, 193.04 Q 716.95, 193.56, 727.04, 192.87 Q 737.14, 193.25, 747.24, 193.11 Q 757.34, 193.05, 767.44, 192.90 Q 777.53,\
                        193.07, 787.63, 192.85 Q 797.73, 192.56, 807.83, 192.83 Q 817.92, 193.53, 828.02, 194.17 Q 838.12, 194.42, 848.22, 194.61\
                        Q 858.32, 194.46, 868.41, 194.27 Q 878.51, 193.38, 888.61, 193.46 Q 898.71, 193.37, 908.81, 193.58 Q 918.90, 195.00, 929.00,\
                        195.00" style=" fill:none;"/><svg:path class=" svg_unselected_element" d="M 0.00, 234.00 Q 10.10, 233.47, 20.20, 234.98 Q\
                        30.29, 234.88, 40.39, 235.54 Q 50.49, 234.57, 60.59, 234.14 Q 70.68, 234.33, 80.78, 235.07 Q 90.88, 234.88, 100.98, 234.07\
                        Q 111.08, 233.92, 121.17, 233.43 Q 131.27, 234.06, 141.37, 233.47 Q 151.47, 233.70, 161.57, 234.31 Q 171.66, 233.83, 181.76,\
                        234.00 Q 191.86, 233.10, 201.96, 233.86 Q 212.05, 233.48, 222.15, 234.34 Q 232.25, 233.73, 242.35, 233.59 Q 252.45, 233.28,\
                        262.54, 234.27 Q 272.64, 234.22, 282.74, 232.73 Q 292.84, 232.65, 302.93, 233.34 Q 313.03, 233.86, 323.13, 234.28 Q 333.23,\
                        234.97, 343.33, 234.06 Q 353.42, 234.30, 363.52, 234.35 Q 373.62, 233.68, 383.72, 233.03 Q 393.82, 234.01, 403.91, 234.82\
                        Q 414.01, 234.39, 424.11, 233.44 Q 434.21, 233.04, 444.30, 233.69 Q 454.40, 233.55, 464.50, 233.00 Q 474.60, 232.87, 484.70,\
                        233.52 Q 494.79, 233.60, 504.89, 232.88 Q 514.99, 232.57, 525.09, 233.42 Q 535.19, 233.37, 545.28, 233.14 Q 555.38, 232.04,\
                        565.48, 231.91 Q 575.58, 232.21, 585.67, 232.26 Q 595.77, 232.26, 605.87, 232.30 Q 615.97, 232.12, 626.07, 232.23 Q 636.16,\
                        231.84, 646.26, 232.55 Q 656.36, 233.28, 666.46, 234.59 Q 676.55, 234.22, 686.65, 233.08 Q 696.75, 233.03, 706.85, 233.41\
                        Q 716.95, 233.02, 727.04, 232.90 Q 737.14, 232.47, 747.24, 232.54 Q 757.34, 232.48, 767.44, 232.88 Q 777.53, 233.41, 787.63,\
                        233.57 Q 797.73, 233.14, 807.83, 232.85 Q 817.92, 232.80, 828.02, 232.64 Q 838.12, 232.79, 848.22, 232.65 Q 858.32, 232.94,\
                        868.41, 233.03 Q 878.51, 232.87, 888.61, 233.03 Q 898.71, 232.33, 908.81, 231.94 Q 918.90, 234.00, 929.00, 234.00" style="\
                        fill:none;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div style="height: 273px;width:929px; position:absolute; top: 0px; left: 0px;"><span style=\'position: absolute; top: 5px;left: 10px;width:269px;\'><span style=\'position: relative;\'>BK</span></span><span\
                     style=\'position: absolute; top: 5px;left: 309px;width:137px;\'><span style=\'position: relative;\'>1</span></span><span style=\'position:\
                     absolute; top: 5px;left: 476px;width:137px;\'><span style=\'position: relative;\'>X</span></span><span style=\'position: absolute;\
                     top: 5px;left: 643px;width:137px;\'><span style=\'position: relative;\'>2</span></span><span style=\'position: absolute; top:\
                     5px;left: 810px;width:99px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 44px;left:\
                     10px;width:269px;\'><span style=\'position: relative;\'>1X ставка</span></span><span style=\'position: absolute; top: 44px;left:\
                     309px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 476px;width:137px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 643px;width:137px;\'><span style=\'position:\
                     relative;\'>_cf_</span></span><span style=\'position: absolute; top: 44px;left: 810px;width:99px;\'><span style=\'position: relative;\'>...</span></span><span\
                     style=\'position: absolute; top: 83px;left: 10px;width:269px;\'><span style=\'position: relative;\'>Winline</span></span><span\
                     style=\'position: absolute; top: 83px;left: 309px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 83px;left: 476px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute;\
                     top: 83px;left: 643px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top:\
                     83px;left: 810px;width:99px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 122px;left:\
                     10px;width:269px;\'><span style=\'position: relative;\'>Лига ставок</span></span><span style=\'position: absolute; top: 122px;left:\
                     309px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left:\
                     476px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left:\
                     643px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 122px;left:\
                     810px;width:99px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position: absolute; top: 161px;left: 10px;width:269px;\'><span\
                     style=\'position: relative;\'>Фонбет</span></span><span style=\'position: absolute; top: 161px;left: 309px;width:137px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 161px;left: 476px;width:137px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 161px;left: 643px;width:137px;\'><span\
                     style=\'position: relative;\'>_cf_</span></span><span style=\'position: absolute; top: 161px;left: 810px;width:99px;\'><span style=\'position:\
                     relative;\'>...</span></span><span style=\'position: absolute; top: 200px;left: 10px;width:269px;\'><span style=\'position: relative;\'>Леон</span></span><span\
                     style=\'position: absolute; top: 200px;left: 309px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 200px;left: 476px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 200px;left: 643px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span\
                     style=\'position: absolute; top: 200px;left: 810px;width:99px;\'><span style=\'position: relative;\'>...</span></span><span style=\'position:\
                     absolute; top: 239px;left: 10px;width:269px;\'><span style=\'position: relative;\'>Pinnacle</span></span><span style=\'position:\
                     absolute; top: 239px;left: 309px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 239px;left: 476px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 239px;left: 643px;width:137px;\'><span style=\'position: relative;\'>_cf_</span></span><span style=\'position:\
                     absolute; top: 239px;left: 810px;width:99px;\'><span style=\'position: relative;\'>...</span></span>\
                  </div>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page606870902"] .border-wrapper, body[data-current-page-id="page606870902"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page606870902"] .border-wrapper, body.has-frame[data-current-page-id="page606870902"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page606870902"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page606870902"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page606870902",\
      			"name": "coefficients",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
   <div id="border-wrapper">\
      <div xmlns="http://www.w3.org/1999/xhtml" xmlns:json="http://json.org/" class="svg-border svg-border-1366-768" style="display: none;">\
         <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" class="svg_border" style="position:absolute;left:-43px;top:-13px;width:1423px;height:792px;"><svg:path class=" svg_unselected_element" d="M 32.00, 3.00 Q 42.12, 0.35, 52.24, 0.54 Q 62.35, 1.56, 72.47, 1.38 Q 82.59,\
            2.49, 92.71, 3.03 Q 102.82, 2.01, 112.94, 1.80 Q 123.06, 1.80, 133.18, 2.77 Q 143.29, 2.57, 153.41, 2.71 Q 163.53, 2.29, 173.65,\
            1.77 Q 183.76, 2.24, 193.88, 1.92 Q 204.00, 1.52, 214.12, 2.21 Q 224.24, 4.00, 234.35, 3.28 Q 244.47, 3.40, 254.59, 2.59 Q\
            264.71, 2.77, 274.82, 2.74 Q 284.94, 2.61, 295.06, 2.02 Q 305.18, 1.26, 315.29, 1.21 Q 325.41, 1.06, 335.53, 1.00 Q 345.65,\
            0.90, 355.76, 0.82 Q 365.88, 0.47, 376.00, 1.48 Q 386.12, 1.60, 396.24, 2.21 Q 406.35, 3.28, 416.47, 3.56 Q 426.59, 2.74,\
            436.71, 2.12 Q 446.82, 1.80, 456.94, 3.05 Q 467.06, 2.67, 477.18, 2.03 Q 487.29, 1.92, 497.41, 1.88 Q 507.53, 1.48, 517.65,\
            1.58 Q 527.76, 1.54, 537.88, 1.99 Q 548.00, 1.03, 558.12, 1.38 Q 568.24, 0.88, 578.35, 1.38 Q 588.47, 2.02, 598.59, 2.07 Q\
            608.71, 2.05, 618.82, 1.99 Q 628.94, 1.40, 639.06, 1.10 Q 649.18, 1.35, 659.29, 1.06 Q 669.41, 1.14, 679.53, 1.72 Q 689.65,\
            1.72, 699.77, 1.07 Q 709.88, 2.07, 720.00, 2.71 Q 730.12, 2.80, 740.24, 2.45 Q 750.35, 2.34, 760.47, 2.29 Q 770.59, 2.54,\
            780.71, 2.02 Q 790.82, 1.70, 800.94, 2.84 Q 811.06, 2.83, 821.18, 2.36 Q 831.29, 2.65, 841.41, 2.47 Q 851.53, 1.96, 861.65,\
            1.66 Q 871.77, 2.45, 881.88, 3.45 Q 892.00, 1.92, 902.12, 2.80 Q 912.24, 2.23, 922.35, 1.90 Q 932.47, 2.00, 942.59, 1.93 Q\
            952.71, 1.46, 962.82, 2.14 Q 972.94, 1.16, 983.06, 1.02 Q 993.18, 1.06, 1003.30, 1.33 Q 1013.41, 1.68, 1023.53, 1.88 Q 1033.65,\
            1.85, 1043.77, 2.09 Q 1053.88, 1.31, 1064.00, 0.65 Q 1074.12, 1.11, 1084.24, 1.12 Q 1094.35, 1.53, 1104.47, 1.06 Q 1114.59,\
            1.36, 1124.71, 1.75 Q 1134.83, 2.60, 1144.94, 2.19 Q 1155.06, 1.55, 1165.18, 2.09 Q 1175.30, 1.98, 1185.41, 2.20 Q 1195.53,\
            2.72, 1205.65, 2.50 Q 1215.77, 2.05, 1225.88, 1.59 Q 1236.00, 2.28, 1246.12, 1.10 Q 1256.24, 2.02, 1266.35, 1.74 Q 1276.47,\
            1.58, 1286.59, 1.34 Q 1296.71, 1.32, 1306.83, 1.32 Q 1316.94, 1.13, 1327.06, 2.12 Q 1337.18, 2.75, 1347.30, 2.45 Q 1357.41,\
            1.38, 1367.53, 1.46 Q 1377.65, 1.92, 1387.77, 2.49 Q 1397.88, 3.24, 1407.90, 3.10 Q 1408.05, 13.15, 1408.43, 23.28 Q 1408.19,\
            33.50, 1408.94, 43.65 Q 1407.67, 53.86, 1408.43, 64.02 Q 1408.12, 74.20, 1408.44, 84.37 Q 1407.96, 94.54, 1408.72, 104.71\
            Q 1409.12, 114.88, 1409.15, 125.05 Q 1409.51, 135.22, 1409.85, 145.39 Q 1408.58, 155.57, 1407.62, 165.74 Q 1407.71, 175.91,\
            1407.77, 186.08 Q 1407.82, 196.25, 1408.75, 206.42 Q 1408.38, 216.59, 1408.23, 226.76 Q 1407.75, 236.93, 1408.08, 247.11 Q\
            1409.11, 257.28, 1409.37, 267.45 Q 1409.26, 277.62, 1409.44, 287.79 Q 1408.74, 297.96, 1408.48, 308.13 Q 1407.08, 318.30,\
            1406.82, 328.47 Q 1407.72, 338.64, 1408.55, 348.82 Q 1407.93, 358.99, 1407.26, 369.16 Q 1407.21, 379.33, 1408.55, 389.50 Q\
            1408.14, 399.67, 1408.90, 409.84 Q 1408.93, 420.01, 1408.65, 430.18 Q 1408.48, 440.36, 1408.36, 450.53 Q 1408.84, 460.70,\
            1409.24, 470.87 Q 1408.75, 481.04, 1408.29, 491.21 Q 1408.80, 501.38, 1409.77, 511.55 Q 1409.81, 521.72, 1409.50, 531.89 Q\
            1408.73, 542.07, 1408.90, 552.24 Q 1409.48, 562.41, 1409.35, 572.58 Q 1408.24, 582.75, 1409.01, 592.92 Q 1409.76, 603.09,\
            1408.58, 613.26 Q 1407.91, 623.43, 1408.22, 633.61 Q 1407.81, 643.78, 1407.96, 653.95 Q 1408.70, 664.12, 1408.89, 674.29 Q\
            1409.36, 684.46, 1409.83, 694.63 Q 1409.77, 704.80, 1409.12, 714.97 Q 1409.01, 725.15, 1409.23, 735.32 Q 1408.18, 745.49,\
            1408.68, 755.66 Q 1409.66, 765.83, 1408.52, 776.52 Q 1398.26, 777.13, 1387.86, 776.65 Q 1377.68, 776.46, 1367.55, 776.50 Q\
            1357.42, 776.10, 1347.29, 775.23 Q 1337.18, 776.13, 1327.06, 776.60 Q 1316.94, 777.19, 1306.83, 777.46 Q 1296.71, 776.95,\
            1286.59, 777.18 Q 1276.47, 777.20, 1266.35, 777.47 Q 1256.24, 776.70, 1246.12, 776.38 Q 1236.00, 775.82, 1225.88, 775.66 Q\
            1215.77, 775.70, 1205.65, 776.00 Q 1195.53, 776.58, 1185.41, 776.34 Q 1175.30, 777.81, 1165.18, 776.95 Q 1155.06, 776.73,\
            1144.94, 776.05 Q 1134.83, 776.23, 1124.71, 777.68 Q 1114.59, 777.32, 1104.47, 777.04 Q 1094.35, 777.02, 1084.24, 777.46 Q\
            1074.12, 777.61, 1064.00, 777.60 Q 1053.88, 777.48, 1043.77, 776.76 Q 1033.65, 777.94, 1023.53, 777.35 Q 1013.41, 776.83,\
            1003.30, 777.35 Q 993.18, 777.56, 983.06, 777.63 Q 972.94, 777.12, 962.82, 775.95 Q 952.71, 776.43, 942.59, 776.19 Q 932.47,\
            776.45, 922.35, 776.14 Q 912.24, 776.01, 902.12, 776.71 Q 892.00, 777.15, 881.88, 776.95 Q 871.77, 776.98, 861.65, 776.83\
            Q 851.53, 776.59, 841.41, 775.96 Q 831.29, 776.33, 821.18, 775.56 Q 811.06, 775.89, 800.94, 775.63 Q 790.82, 776.23, 780.71,\
            776.96 Q 770.59, 776.95, 760.47, 778.14 Q 750.35, 777.29, 740.24, 776.80 Q 730.12, 777.37, 720.00, 777.53 Q 709.88, 776.03,\
            699.77, 775.81 Q 689.65, 776.95, 679.53, 776.55 Q 669.41, 777.11, 659.29, 776.28 Q 649.18, 776.26, 639.06, 776.76 Q 628.94,\
            776.83, 618.82, 776.15 Q 608.71, 775.49, 598.59, 777.06 Q 588.47, 777.26, 578.35, 777.50 Q 568.24, 777.64, 558.12, 777.68\
            Q 548.00, 777.73, 537.88, 777.95 Q 527.76, 777.57, 517.65, 777.86 Q 507.53, 777.73, 497.41, 776.57 Q 487.29, 776.53, 477.18,\
            775.77 Q 467.06, 775.50, 456.94, 775.32 Q 446.82, 775.33, 436.71, 775.33 Q 426.59, 775.40, 416.47, 776.22 Q 406.35, 776.44,\
            396.24, 777.81 Q 386.12, 777.31, 376.00, 777.41 Q 365.88, 776.68, 355.76, 776.82 Q 345.65, 776.84, 335.53, 776.72 Q 325.41,\
            776.46, 315.29, 776.75 Q 305.18, 777.81, 295.06, 777.93 Q 284.94, 777.07, 274.82, 777.28 Q 264.71, 777.22, 254.59, 776.12\
            Q 244.47, 775.71, 234.35, 776.75 Q 224.24, 776.52, 214.12, 777.49 Q 204.00, 777.15, 193.88, 777.94 Q 183.76, 777.15, 173.65,\
            775.85 Q 163.53, 774.73, 153.41, 774.83 Q 143.29, 774.89, 133.18, 775.22 Q 123.06, 776.81, 112.94, 776.75 Q 102.82, 775.84,\
            92.71, 775.67 Q 82.59, 774.91, 72.47, 774.22 Q 62.35, 774.98, 52.24, 775.09 Q 42.12, 775.66, 31.49, 776.51 Q 31.50, 766.00,\
            31.93, 755.67 Q 31.92, 745.49, 31.43, 735.33 Q 31.72, 725.15, 31.47, 714.98 Q 30.77, 704.81, 30.94, 694.63 Q 30.91, 684.46,\
            31.22, 674.29 Q 32.19, 664.12, 31.95, 653.95 Q 30.93, 643.78, 31.13, 633.61 Q 30.55, 623.43, 30.37, 613.26 Q 31.32, 603.09,\
            31.47, 592.92 Q 31.14, 582.75, 31.95, 572.58 Q 32.20, 562.41, 31.20, 552.24 Q 30.32, 542.07, 29.74, 531.89 Q 29.55, 521.72,\
            29.62, 511.55 Q 30.01, 501.38, 30.44, 491.21 Q 30.38, 481.04, 31.39, 470.87 Q 31.23, 460.70, 30.98, 450.53 Q 30.53, 440.36,\
            30.28, 430.18 Q 30.20, 420.01, 30.22, 409.84 Q 30.73, 399.67, 31.89, 389.50 Q 32.11, 379.33, 31.46, 369.16 Q 30.14, 358.99,\
            30.23, 348.82 Q 31.01, 338.64, 31.38, 328.47 Q 31.86, 318.30, 31.68, 308.13 Q 31.04, 297.96, 31.00, 287.79 Q 31.04, 277.62,\
            31.01, 267.45 Q 30.57, 257.28, 30.91, 247.11 Q 30.48, 236.93, 30.59, 226.76 Q 30.88, 216.59, 31.22, 206.42 Q 31.06, 196.25,\
            31.23, 186.08 Q 30.91, 175.91, 30.59, 165.74 Q 30.56, 155.57, 30.45, 145.39 Q 31.11, 135.22, 30.91, 125.05 Q 30.61, 114.88,\
            30.51, 104.71 Q 30.37, 94.54, 30.15, 84.37 Q 31.46, 74.20, 32.19, 64.03 Q 32.50, 53.86, 31.32, 43.68 Q 30.69, 33.51, 30.52,\
            23.34 Q 32.00, 13.17, 32.00, 3.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 23.00, 7.00 Q 33.12, 5.94, 43.24, 5.44 Q 53.35, 5.30, 63.47, 5.26 Q 73.59,\
            5.26, 83.71, 5.52 Q 93.82, 5.41, 103.94, 5.23 Q 114.06, 5.49, 124.18, 5.73 Q 134.29, 5.76, 144.41, 5.47 Q 154.53, 5.43, 164.65,\
            5.66 Q 174.76, 4.92, 184.88, 5.41 Q 195.00, 5.42, 205.12, 5.66 Q 215.24, 5.19, 225.35, 5.94 Q 235.47, 6.47, 245.59, 6.62 Q\
            255.71, 6.66, 265.82, 6.08 Q 275.94, 5.76, 286.06, 6.35 Q 296.18, 6.29, 306.29, 6.32 Q 316.41, 6.54, 326.53, 5.31 Q 336.65,\
            5.16, 346.76, 5.74 Q 356.88, 5.89, 367.00, 5.70 Q 377.12, 6.29, 387.24, 6.28 Q 397.35, 6.00, 407.47, 5.61 Q 417.59, 5.77,\
            427.71, 6.52 Q 437.82, 6.73, 447.94, 7.22 Q 458.06, 6.75, 468.18, 6.09 Q 478.29, 5.86, 488.41, 5.79 Q 498.53, 5.46, 508.65,\
            5.37 Q 518.76, 5.24, 528.88, 5.23 Q 539.00, 5.21, 549.12, 6.10 Q 559.24, 6.26, 569.35, 5.55 Q 579.47, 6.03, 589.59, 6.11 Q\
            599.71, 8.03, 609.82, 7.24 Q 619.94, 6.22, 630.06, 5.21 Q 640.18, 5.39, 650.29, 4.97 Q 660.41, 5.69, 670.53, 5.96 Q 680.65,\
            5.65, 690.77, 6.08 Q 700.88, 5.71, 711.00, 5.52 Q 721.12, 5.56, 731.24, 6.10 Q 741.35, 5.38, 751.47, 5.44 Q 761.59, 6.24,\
            771.71, 6.16 Q 781.82, 5.70, 791.94, 6.18 Q 802.06, 5.91, 812.18, 5.53 Q 822.29, 5.27, 832.41, 5.25 Q 842.53, 5.66, 852.65,\
            6.01 Q 862.77, 6.37, 872.88, 7.38 Q 883.00, 7.76, 893.12, 8.24 Q 903.24, 7.53, 913.35, 6.59 Q 923.47, 7.19, 933.59, 6.35 Q\
            943.71, 6.64, 953.82, 6.88 Q 963.94, 6.17, 974.06, 6.58 Q 984.18, 6.33, 994.30, 6.01 Q 1004.41, 6.42, 1014.53, 5.54 Q 1024.65,\
            5.42, 1034.77, 5.39 Q 1044.88, 5.36, 1055.00, 5.36 Q 1065.12, 6.43, 1075.24, 7.10 Q 1085.35, 7.39, 1095.47, 7.34 Q 1105.59,\
            6.89, 1115.71, 6.04 Q 1125.83, 6.58, 1135.94, 6.70 Q 1146.06, 6.91, 1156.18, 6.80 Q 1166.30, 6.35, 1176.41, 7.24 Q 1186.53,\
            6.98, 1196.65, 6.18 Q 1206.77, 5.77, 1216.88, 6.70 Q 1227.00, 6.80, 1237.12, 6.10 Q 1247.24, 6.09, 1257.35, 6.20 Q 1267.47,\
            6.21, 1277.59, 5.55 Q 1287.71, 5.10, 1297.83, 6.05 Q 1307.94, 6.35, 1318.06, 6.07 Q 1328.18, 5.00, 1338.30, 5.37 Q 1348.41,\
            5.30, 1358.53, 5.84 Q 1368.65, 5.69, 1378.77, 5.75 Q 1388.88, 5.54, 1399.73, 6.27 Q 1400.05, 16.82, 1400.25, 27.16 Q 1399.77,\
            37.46, 1399.23, 47.68 Q 1399.65, 57.84, 1400.42, 68.02 Q 1400.43, 78.19, 1400.41, 88.37 Q 1400.13, 98.54, 1399.01, 108.71\
            Q 1398.40, 118.88, 1398.85, 129.05 Q 1399.94, 139.22, 1399.54, 149.39 Q 1399.58, 159.57, 1399.24, 169.74 Q 1399.54, 179.91,\
            1399.77, 190.08 Q 1399.77, 200.25, 1399.69, 210.42 Q 1399.32, 220.59, 1400.02, 230.76 Q 1399.13, 240.93, 1400.21, 251.11 Q\
            1400.61, 261.28, 1399.84, 271.45 Q 1400.34, 281.62, 1400.02, 291.79 Q 1400.67, 301.96, 1400.61, 312.13 Q 1400.40, 322.30,\
            1399.39, 332.47 Q 1400.07, 342.64, 1399.58, 352.82 Q 1399.20, 362.99, 1398.80, 373.16 Q 1399.05, 383.33, 1399.40, 393.50 Q\
            1399.96, 403.67, 1399.56, 413.84 Q 1398.92, 424.01, 1398.45, 434.18 Q 1398.45, 444.36, 1400.17, 454.53 Q 1400.60, 464.70,\
            1399.91, 474.87 Q 1398.96, 485.04, 1399.60, 495.21 Q 1400.33, 505.38, 1400.12, 515.55 Q 1399.75, 525.72, 1399.49, 535.89 Q\
            1399.79, 546.07, 1399.39, 556.24 Q 1399.53, 566.41, 1400.09, 576.58 Q 1399.75, 586.75, 1399.65, 596.92 Q 1401.23, 607.09,\
            1400.87, 617.26 Q 1400.93, 627.43, 1400.38, 637.61 Q 1400.43, 647.78, 1401.03, 657.95 Q 1401.31, 668.12, 1401.34, 678.29 Q\
            1401.40, 688.46, 1401.50, 698.63 Q 1400.87, 708.80, 1399.93, 718.97 Q 1399.73, 729.15, 1399.88, 739.32 Q 1399.92, 749.49,\
            1400.03, 759.66 Q 1400.34, 769.83, 1399.71, 780.71 Q 1389.20, 780.95, 1378.91, 781.00 Q 1368.71, 780.84, 1358.58, 781.53 Q\
            1348.44, 781.65, 1338.31, 781.27 Q 1328.18, 780.40, 1318.06, 781.73 Q 1307.94, 781.32, 1297.83, 780.84 Q 1287.71, 780.52,\
            1277.59, 780.31 Q 1267.47, 780.61, 1257.35, 781.24 Q 1247.24, 779.79, 1237.12, 780.97 Q 1227.00, 780.96, 1216.88, 780.51 Q\
            1206.77, 781.07, 1196.65, 781.00 Q 1186.53, 780.89, 1176.41, 780.94 Q 1166.30, 780.72, 1156.18, 779.92 Q 1146.06, 779.85,\
            1135.94, 780.16 Q 1125.83, 780.70, 1115.71, 781.27 Q 1105.59, 781.38, 1095.47, 781.09 Q 1085.35, 779.95, 1075.24, 780.88 Q\
            1065.12, 780.63, 1055.00, 780.45 Q 1044.88, 780.16, 1034.77, 780.90 Q 1024.65, 780.82, 1014.53, 781.25 Q 1004.41, 781.30,\
            994.30, 780.15 Q 984.18, 779.74, 974.06, 780.07 Q 963.94, 779.16, 953.82, 779.95 Q 943.71, 781.24, 933.59, 781.18 Q 923.47,\
            781.47, 913.35, 780.68 Q 903.24, 780.91, 893.12, 781.56 Q 883.00, 780.61, 872.88, 780.67 Q 862.77, 780.63, 852.65, 780.07\
            Q 842.53, 780.54, 832.41, 780.61 Q 822.29, 780.75, 812.18, 780.89 Q 802.06, 780.93, 791.94, 780.77 Q 781.82, 780.34, 771.71,\
            780.28 Q 761.59, 780.54, 751.47, 780.36 Q 741.35, 780.96, 731.24, 780.65 Q 721.12, 780.49, 711.00, 780.33 Q 700.88, 780.01,\
            690.77, 780.18 Q 680.65, 780.30, 670.53, 780.56 Q 660.41, 780.17, 650.29, 781.03 Q 640.18, 781.04, 630.06, 781.13 Q 619.94,\
            781.48, 609.82, 781.12 Q 599.71, 780.35, 589.59, 779.75 Q 579.47, 779.76, 569.35, 779.02 Q 559.24, 779.92, 549.12, 779.83\
            Q 539.00, 780.32, 528.88, 780.90 Q 518.76, 780.93, 508.65, 780.31 Q 498.53, 780.12, 488.41, 780.33 Q 478.29, 779.28, 468.18,\
            779.49 Q 458.06, 779.99, 447.94, 780.12 Q 437.82, 780.08, 427.71, 780.30 Q 417.59, 780.50, 407.47, 780.33 Q 397.35, 780.74,\
            387.24, 779.72 Q 377.12, 780.20, 367.00, 779.65 Q 356.88, 780.18, 346.76, 780.69 Q 336.65, 780.29, 326.53, 779.27 Q 316.41,\
            779.90, 306.29, 780.56 Q 296.18, 780.10, 286.06, 781.17 Q 275.94, 780.00, 265.82, 779.58 Q 255.71, 780.65, 245.59, 780.79\
            Q 235.47, 780.13, 225.35, 780.69 Q 215.24, 782.01, 205.12, 781.56 Q 195.00, 781.69, 184.88, 782.19 Q 174.76, 781.61, 164.65,\
            782.01 Q 154.53, 782.30, 144.41, 780.97 Q 134.29, 781.68, 124.18, 780.98 Q 114.06, 780.05, 103.94, 779.93 Q 93.82, 779.23,\
            83.71, 779.66 Q 73.59, 780.17, 63.47, 780.17 Q 53.35, 780.42, 43.24, 780.22 Q 33.12, 780.55, 22.66, 780.34 Q 22.01, 770.16,\
            21.67, 759.85 Q 21.39, 749.59, 22.08, 739.35 Q 22.50, 729.15, 23.04, 718.97 Q 23.49, 708.80, 23.60, 698.63 Q 24.97, 688.46,\
            24.15, 678.29 Q 23.01, 668.12, 22.01, 657.95 Q 21.06, 647.78, 20.95, 637.61 Q 21.18, 627.43, 21.66, 617.26 Q 22.50, 607.09,\
            22.90, 596.92 Q 22.57, 586.75, 22.88, 576.58 Q 22.34, 566.41, 21.71, 556.24 Q 21.75, 546.07, 22.09, 535.89 Q 22.20, 525.72,\
            22.53, 515.55 Q 22.77, 505.38, 23.38, 495.21 Q 22.92, 485.04, 23.05, 474.87 Q 22.44, 464.70, 23.09, 454.53 Q 23.16, 444.36,\
            22.88, 434.18 Q 22.46, 424.01, 22.18, 413.84 Q 23.53, 403.67, 22.86, 393.50 Q 23.42, 383.33, 22.86, 373.16 Q 21.68, 362.99,\
            21.07, 352.82 Q 20.96, 342.64, 21.55, 332.47 Q 21.75, 322.30, 21.52, 312.13 Q 21.39, 301.96, 22.00, 291.79 Q 21.96, 281.62,\
            21.88, 271.45 Q 21.93, 261.28, 21.43, 251.11 Q 21.10, 240.93, 21.09, 230.76 Q 21.00, 220.59, 20.98, 210.42 Q 21.42, 200.25,\
            21.65, 190.08 Q 21.23, 179.91, 20.97, 169.74 Q 20.73, 159.57, 21.42, 149.39 Q 22.26, 139.22, 22.14, 129.05 Q 22.62, 118.88,\
            21.78, 108.71 Q 22.65, 98.54, 22.02, 88.37 Q 21.81, 78.20, 21.89, 68.03 Q 21.83, 57.86, 21.80, 47.68 Q 21.88, 37.51, 21.64,\
            27.34 Q 23.00, 17.17, 23.00, 7.00" style=" fill:white;"/><svg:path class=" svg_unselected_element" d="M 40.00, 11.00 Q 50.12, 10.41, 60.24, 10.29 Q 70.35, 10.51, 80.47, 10.72 Q 90.59,\
            11.38, 100.71, 11.95 Q 110.82, 11.70, 120.94, 11.00 Q 131.06, 11.57, 141.18, 11.41 Q 151.29, 11.08, 161.41, 11.43 Q 171.53,\
            10.21, 181.65, 10.27 Q 191.76, 10.32, 201.88, 9.79 Q 212.00, 12.14, 222.12, 11.22 Q 232.24, 11.38, 242.35, 10.84 Q 252.47,\
            10.86, 262.59, 10.14 Q 272.71, 10.30, 282.82, 11.17 Q 292.94, 12.16, 303.06, 10.10 Q 313.18, 10.96, 323.29, 10.83 Q 333.41,\
            10.63, 343.53, 10.35 Q 353.65, 9.39, 363.76, 9.19 Q 373.88, 10.43, 384.00, 12.75 Q 394.12, 11.52, 404.24, 11.32 Q 414.35,\
            11.29, 424.47, 11.24 Q 434.59, 11.18, 444.71, 10.58 Q 454.82, 10.19, 464.94, 10.83 Q 475.06, 11.28, 485.18, 10.23 Q 495.29,\
            9.78, 505.41, 9.94 Q 515.53, 10.06, 525.65, 9.92 Q 535.76, 10.75, 545.88, 11.49 Q 556.00, 10.23, 566.12, 9.34 Q 576.24, 10.23,\
            586.35, 10.42 Q 596.47, 10.48, 606.59, 10.10 Q 616.71, 9.35, 626.82, 9.45 Q 636.94, 9.50, 647.06, 9.23 Q 657.18, 8.95, 667.29,\
            8.82 Q 677.41, 8.78, 687.53, 8.56 Q 697.65, 8.91, 707.77, 9.39 Q 717.88, 10.71, 728.00, 11.66 Q 738.12, 11.26, 748.24, 9.66\
            Q 758.35, 9.25, 768.47, 9.70 Q 778.59, 9.65, 788.71, 10.08 Q 798.82, 10.13, 808.94, 10.18 Q 819.06, 10.06, 829.18, 10.16 Q\
            839.29, 9.98, 849.41, 9.98 Q 859.53, 10.10, 869.65, 9.72 Q 879.77, 9.66, 889.88, 10.16 Q 900.00, 10.32, 910.12, 9.95 Q 920.24,\
            9.95, 930.35, 9.87 Q 940.47, 9.70, 950.59, 9.76 Q 960.71, 9.86, 970.82, 9.45 Q 980.94, 8.89, 991.06, 8.87 Q 1001.18, 8.83,\
            1011.30, 8.82 Q 1021.41, 9.65, 1031.53, 10.75 Q 1041.65, 11.29, 1051.77, 11.32 Q 1061.88, 10.75, 1072.00, 9.79 Q 1082.12,\
            9.33, 1092.24, 9.67 Q 1102.35, 9.50, 1112.47, 11.22 Q 1122.59, 11.92, 1132.71, 11.59 Q 1142.83, 11.58, 1152.94, 11.25 Q 1163.06,\
            10.27, 1173.18, 10.97 Q 1183.30, 9.76, 1193.41, 9.87 Q 1203.53, 10.01, 1213.65, 9.99 Q 1223.77, 10.12, 1233.88, 9.92 Q 1244.00,\
            9.75, 1254.12, 9.48 Q 1264.24, 10.47, 1274.35, 9.46 Q 1284.47, 9.78, 1294.59, 9.32 Q 1304.71, 9.46, 1314.83, 9.06 Q 1324.94,\
            8.74, 1335.06, 9.37 Q 1345.18, 8.49, 1355.30, 9.58 Q 1365.41, 9.82, 1375.53, 9.60 Q 1385.65, 9.92, 1395.77, 10.05 Q 1405.88,\
            10.22, 1416.21, 10.79 Q 1416.34, 21.06, 1416.38, 31.29 Q 1416.36, 41.49, 1417.06, 51.65 Q 1416.46, 61.85, 1416.74, 72.02 Q\
            1416.67, 82.19, 1417.08, 92.37 Q 1416.79, 102.54, 1417.23, 112.71 Q 1417.49, 122.88, 1416.82, 133.05 Q 1417.42, 143.22, 1416.90,\
            153.39 Q 1417.00, 163.57, 1416.11, 173.74 Q 1415.90, 183.91, 1416.54, 194.08 Q 1416.41, 204.25, 1416.18, 214.42 Q 1415.43,\
            224.59, 1416.55, 234.76 Q 1417.12, 244.93, 1416.66, 255.11 Q 1415.96, 265.28, 1416.87, 275.45 Q 1416.04, 285.62, 1417.15,\
            295.79 Q 1416.75, 305.96, 1416.70, 316.13 Q 1416.91, 326.30, 1416.10, 336.47 Q 1416.24, 346.64, 1415.74, 356.82 Q 1416.32,\
            366.99, 1416.13, 377.16 Q 1416.33, 387.33, 1416.25, 397.50 Q 1416.84, 407.67, 1416.28, 417.84 Q 1417.16, 428.01, 1416.72,\
            438.18 Q 1416.57, 448.36, 1417.07, 458.53 Q 1415.98, 468.70, 1416.67, 478.87 Q 1416.99, 489.04, 1417.33, 499.21 Q 1416.94,\
            509.38, 1417.95, 519.55 Q 1417.14, 529.72, 1417.00, 539.89 Q 1416.78, 550.07, 1416.99, 560.24 Q 1417.29, 570.41, 1417.69,\
            580.58 Q 1417.93, 590.75, 1417.19, 600.92 Q 1417.16, 611.09, 1415.97, 621.26 Q 1415.67, 631.43, 1416.24, 641.61 Q 1416.21,\
            651.78, 1417.07, 661.95 Q 1417.36, 672.12, 1417.27, 682.29 Q 1417.02, 692.46, 1417.10, 702.63 Q 1417.32, 712.80, 1417.41,\
            722.97 Q 1417.08, 733.15, 1416.85, 743.32 Q 1417.34, 753.49, 1417.78, 763.66 Q 1417.88, 773.83, 1416.59, 784.59 Q 1406.19,\
            784.92, 1395.85, 784.56 Q 1385.71, 784.86, 1375.56, 785.01 Q 1365.43, 784.85, 1355.30, 784.92 Q 1345.18, 784.52, 1335.06,\
            784.91 Q 1324.95, 785.85, 1314.83, 786.16 Q 1304.71, 784.68, 1294.59, 783.80 Q 1284.47, 784.40, 1274.35, 785.11 Q 1264.24,\
            785.54, 1254.12, 784.70 Q 1244.00, 784.43, 1233.88, 784.45 Q 1223.77, 785.18, 1213.65, 784.65 Q 1203.53, 784.06, 1193.41,\
            784.27 Q 1183.30, 784.53, 1173.18, 784.61 Q 1163.06, 784.50, 1152.94, 784.56 Q 1142.83, 784.80, 1132.71, 784.74 Q 1122.59,\
            783.95, 1112.47, 783.19 Q 1102.35, 784.10, 1092.24, 785.17 Q 1082.12, 785.13, 1072.00, 784.67 Q 1061.88, 784.43, 1051.77,\
            784.66 Q 1041.65, 785.24, 1031.53, 784.88 Q 1021.41, 784.91, 1011.30, 785.57 Q 1001.18, 786.02, 991.06, 785.51 Q 980.94, 785.79,\
            970.82, 784.53 Q 960.71, 785.99, 950.59, 786.02 Q 940.47, 785.04, 930.35, 784.80 Q 920.24, 784.30, 910.12, 784.05 Q 900.00,\
            784.60, 889.88, 784.27 Q 879.77, 784.05, 869.65, 784.24 Q 859.53, 784.81, 849.41, 784.80 Q 839.29, 785.24, 829.18, 784.58\
            Q 819.06, 784.66, 808.94, 784.19 Q 798.82, 784.29, 788.71, 784.04 Q 778.59, 784.45, 768.47, 784.45 Q 758.35, 784.44, 748.24,\
            784.43 Q 738.12, 784.53, 728.00, 785.52 Q 717.88, 785.41, 707.77, 783.90 Q 697.65, 783.89, 687.53, 784.69 Q 677.41, 785.56,\
            667.29, 784.75 Q 657.18, 784.82, 647.06, 785.31 Q 636.94, 784.73, 626.82, 784.23 Q 616.71, 784.20, 606.59, 784.77 Q 596.47,\
            785.56, 586.35, 785.67 Q 576.24, 785.49, 566.12, 784.93 Q 556.00, 785.28, 545.88, 785.71 Q 535.76, 785.46, 525.65, 784.56\
            Q 515.53, 784.34, 505.41, 784.77 Q 495.29, 785.04, 485.18, 783.64 Q 475.06, 783.19, 464.94, 783.00 Q 454.82, 783.14, 444.71,\
            783.75 Q 434.59, 783.80, 424.47, 784.04 Q 414.35, 784.45, 404.24, 784.40 Q 394.12, 783.26, 384.00, 783.84 Q 373.88, 783.47,\
            363.76, 784.65 Q 353.65, 784.99, 343.53, 785.32 Q 333.41, 785.61, 323.29, 784.72 Q 313.18, 785.13, 303.06, 784.47 Q 292.94,\
            785.32, 282.82, 785.96 Q 272.71, 784.98, 262.59, 784.15 Q 252.47, 784.11, 242.35, 785.92 Q 232.24, 785.46, 222.12, 784.82\
            Q 212.00, 784.70, 201.88, 784.99 Q 191.76, 784.39, 181.65, 785.13 Q 171.53, 784.76, 161.41, 783.10 Q 151.29, 783.11, 141.18,\
            783.60 Q 131.06, 783.79, 120.94, 783.30 Q 110.82, 784.66, 100.71, 784.29 Q 90.59, 783.51, 80.47, 784.14 Q 70.35, 784.69, 60.24,\
            784.91 Q 50.12, 784.06, 39.68, 784.32 Q 40.10, 773.80, 39.68, 763.70 Q 40.46, 753.46, 40.32, 743.31 Q 40.11, 733.14, 39.20,\
            722.98 Q 39.41, 712.81, 39.32, 702.63 Q 40.08, 692.46, 40.08, 682.29 Q 39.01, 672.12, 39.59, 661.95 Q 40.01, 651.78, 40.51,\
            641.61 Q 40.62, 631.43, 39.37, 621.26 Q 39.92, 611.09, 38.96, 600.92 Q 38.93, 590.75, 38.79, 580.58 Q 39.08, 570.41, 39.37,\
            560.24 Q 38.80, 550.07, 37.76, 539.89 Q 38.46, 529.72, 39.47, 519.55 Q 39.42, 509.38, 39.87, 499.21 Q 38.75, 489.04, 39.60,\
            478.87 Q 39.38, 468.70, 39.37, 458.53 Q 39.08, 448.36, 39.33, 438.18 Q 39.65, 428.01, 38.63, 417.84 Q 38.25, 407.67, 38.22,\
            397.50 Q 38.76, 387.33, 39.50, 377.16 Q 38.88, 366.99, 38.44, 356.82 Q 38.76, 346.64, 39.87, 336.47 Q 40.51, 326.30, 40.24,\
            316.13 Q 39.05, 305.96, 39.25, 295.79 Q 39.75, 285.62, 39.50, 275.45 Q 39.35, 265.28, 38.55, 255.11 Q 38.19, 244.93, 38.04,\
            234.76 Q 38.18, 224.59, 38.54, 214.42 Q 38.77, 204.25, 39.00, 194.08 Q 38.63, 183.91, 38.57, 173.74 Q 38.50, 163.57, 38.40,\
            153.39 Q 38.11, 143.22, 38.48, 133.05 Q 38.17, 122.88, 38.59, 112.71 Q 38.96, 102.54, 38.78, 92.37 Q 38.42, 82.20, 38.69,\
            72.03 Q 38.17, 61.86, 37.92, 51.68 Q 37.84, 41.51, 37.79, 31.34 Q 40.00, 21.17, 40.00, 11.00" style=" fill:white;"/>\
         </svg:svg>\
      </div>\
   </div>\
</div>');