rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page606870902-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page606870902" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page606870902-layer-image432401129" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image432401129" data-review-reference-id="image432401129">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768" viewBox="0 0 1092 768">\
                     <svg:g width="1092" height="768">\
                        <svg:rect x="0" y="0" width="1092" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1092" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1092" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect889260063" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect889260063" data-review-reference-id="rect889260063">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px; width:275px;" width="275" height="768" viewBox="0 0 275 768">\
                     <svg:g width="275" height="768">\
                        <svg:rect x="0" y="0" width="275" height="768" style="stroke-width:1;stroke:black;fill:#C1C1C1;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-group510150739" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group510150739" data-review-reference-id="group510150739">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="group510150739-group848874950" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group848874950" data-review-reference-id="group848874950">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="group848874950-tabbutton346631860" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton346631860" data-review-reference-id="tabbutton346631860">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                                 <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                                    <svg:path id="group848874950-tabbutton346631860_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="group848874950-tabbutton346631860div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group848874950-tabbutton346631860\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group848874950-tabbutton346631860\', \'result\');">\
                                 				RFPL\
                                 				\
                                 <addMouseOverListener></addMouseOverListener>\
                                 				\
                                 <addMouseOutListener></addMouseOutListener>\
                                 			\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-tabbutton103208529" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton103208529" data-review-reference-id="tabbutton103208529">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="group510150739-tabbutton103208529_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="group510150739-tabbutton103208529div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group510150739-tabbutton103208529\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group510150739-tabbutton103208529\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-tabbutton550900072" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="tabbutton550900072" data-review-reference-id="tabbutton550900072">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="group510150739-tabbutton550900072_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="group510150739-tabbutton550900072div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group510150739-tabbutton550900072\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group510150739-tabbutton550900072\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-763240328" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="763240328" data-review-reference-id="763240328">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="group510150739-763240328_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="group510150739-763240328div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group510150739-763240328\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group510150739-763240328\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-1193404445" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1193404445" data-review-reference-id="1193404445">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="group510150739-1193404445_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="group510150739-1193404445div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group510150739-1193404445\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group510150739-1193404445\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="group510150739-1755474258" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1755474258" data-review-reference-id="1755474258">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="group510150739-1755474258_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="group510150739-1755474258div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'group510150739-1755474258\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'group510150739-1755474258\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect977337239" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect977337239" data-review-reference-id="rect977337239">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:1091px;" width="1091" height="465" viewBox="0 0 1091 465">\
                     <svg:g width="1091" height="465">\
                        <svg:rect x="0" y="0" width="1091" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect313157706" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect313157706" data-review-reference-id="rect313157706">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px; width:275px;" width="275" height="160" viewBox="0 0 275 160">\
                     <svg:g width="275" height="160">\
                        <svg:rect x="0" y="0" width="275" height="160" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-group935811796" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="group935811796" data-review-reference-id="group935811796">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="group935811796-ellipse554337119" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="ellipse554337119" data-review-reference-id="ellipse554337119">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85" viewBox="0 0 85 85">\
                           <svg:g width="85" height="85">\
                              <svg:ellipse cx="42.5" cy="42.5" rx="42.5" ry="42.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="group935811796-text927814406" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text927814406" data-review-reference-id="text927814406">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="group935811796-text604129404" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text604129404" data-review-reference-id="text604129404">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect214085128" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect214085128" data-review-reference-id="rect214085128">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-rect994032070" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect994032070" data-review-reference-id="rect994032070">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-image974156449" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image974156449" data-review-reference-id="image974156449">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60" viewBox="0 0 230 60">\
                     <svg:g width="230" height="60">\
                        <svg:rect x="0" y="0" width="230" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="230" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="230" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-button31216625" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="button31216625" data-review-reference-id="button31216625">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#a7a77c;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Коэффициенты<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-text901840057" style="position: absolute; left: 1052px; top: 20px; width: 239px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text901840057" data-review-reference-id="text901840057">\
            <div class="stencil-wrapper" style="width: 239px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Коэффициенты</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-button828406697" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button828406697" data-review-reference-id="button828406697">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Изменения<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button828406697\', \'interaction241219712\', {"button":"left","id":"action520718713","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction205821286","options":"reloadOnly","target":"page189476043","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button164524961" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button164524961" data-review-reference-id="button164524961">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Статистика<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button164524961\', \'interaction851416437\', {"button":"left","id":"action986234028","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction791259442","options":"reloadOnly","target":"page995954813","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button961258309" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button961258309" data-review-reference-id="button961258309">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Выход<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button961258309\', \'interaction486932185\', {"button":"left","id":"action456130266","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction157429537","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button856441484" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button856441484" data-review-reference-id="button856441484">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Настройки<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button856441484\', \'interaction593811501\', {"button":"left","id":"action290429271","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction975357200","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-button668127624" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="button668127624" data-review-reference-id="button668127624">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:48px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Контакты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page606870902-layer-button668127624\', \'interaction567474680\', {"button":"left","id":"action76226304","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction234973819","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page606870902-layer-text434713023" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text434713023" data-review-reference-id="text434713023">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page606870902-layer-accordion411519902" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 395px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="accordion411519902" data-review-reference-id="accordion411519902">\
            <div class="stencil-wrapper" style="width: 930px; height: 395px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page606870902-layer-accordion411519902-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 395px;width:930px;" width="930" height="395">\
                     <svg:g id="__containerId__-page606870902-layer-accordion411519902svg" width="930" height="395"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 0.37, 22.13, 0.34 Q 32.20, 0.21, 42.26, 0.11 Q 52.33, 0.23,\
                        62.39, 0.32 Q 72.46, 0.27, 82.52, 0.12 Q 92.59, 0.02, 102.65, 0.43 Q 112.72, 0.50, 122.78, 1.34 Q 132.85, 0.60, 142.91, 0.64\
                        Q 152.98, 0.47, 163.04, 0.27 Q 173.11, 0.20, 183.17, 0.04 Q 193.24, 0.27, 203.30, 0.73 Q 213.37, 2.01, 223.43, 1.20 Q 233.50,\
                        2.34, 243.57, 2.31 Q 253.63, 1.56, 263.70, 1.19 Q 273.76, 1.19, 283.83, 1.51 Q 293.89, 1.40, 303.96, 1.89 Q 314.02, 0.49,\
                        324.09, 1.52 Q 334.15, 0.99, 344.22, 0.94 Q 354.28, 1.05, 364.35, 0.55 Q 374.41, 0.95, 384.48, 1.10 Q 394.54, 0.77, 404.61,\
                        0.77 Q 414.67, 2.07, 424.74, 2.40 Q 434.80, 1.37, 444.87, 1.11 Q 454.93, 2.48, 465.00, 2.92 Q 475.07, 2.42, 485.13, 3.38 Q\
                        495.20, 1.81, 505.26, 2.12 Q 515.33, 1.38, 525.39, 0.92 Q 535.46, 1.12, 545.52, 0.78 Q 555.59, 2.15, 565.65, 1.62 Q 575.72,\
                        2.45, 585.78, 2.19 Q 595.85, 2.12, 605.91, 2.91 Q 615.98, 1.05, 626.04, -0.01 Q 636.11, 1.30, 646.17, 2.13 Q 656.24, 1.42,\
                        666.30, 1.50 Q 676.37, 0.70, 686.44, 0.76 Q 696.50, 0.93, 706.57, 1.00 Q 716.63, 1.03, 726.70, 0.72 Q 736.76, 1.46, 746.83,\
                        1.20 Q 756.89, 1.38, 766.96, 1.25 Q 777.02, 1.51, 787.09, 1.23 Q 797.15, 0.99, 807.22, 0.67 Q 817.28, 0.87, 827.35, 1.58 Q\
                        837.41, 1.55, 847.48, 1.68 Q 857.54, 3.26, 867.61, 2.45 Q 877.67, 1.24, 887.74, 1.32 Q 897.81, 1.86, 907.87, 1.49 Q 917.94,\
                        1.86, 928.17, 1.83 Q 928.73, 12.05, 928.88, 22.45 Q 927.80, 32.88, 927.76, 43.17 Q 929.01, 53.43, 929.67, 63.72 Q 929.36,\
                        74.02, 928.63, 84.31 Q 928.36, 94.60, 929.33, 104.89 Q 929.24, 115.18, 929.36, 125.47 Q 929.14, 135.76, 928.56, 146.05 Q 928.81,\
                        156.34, 928.92, 166.63 Q 928.69, 176.92, 927.40, 187.21 Q 927.49, 197.50, 928.41, 207.79 Q 929.19, 218.08, 926.87, 228.37\
                        Q 927.30, 238.66, 927.54, 248.95 Q 927.86, 259.24, 927.79, 269.53 Q 927.10, 279.82, 926.57, 290.11 Q 927.49, 300.39, 929.04,\
                        310.68 Q 930.70, 320.97, 929.68, 331.26 Q 928.89, 341.55, 928.27, 351.84 Q 928.15, 362.13, 927.11, 372.42 Q 927.95, 382.71,\
                        927.88, 392.87 Q 917.93, 392.97, 907.88, 393.10 Q 897.87, 394.02, 887.77, 393.93 Q 877.68, 393.42, 867.62, 393.76 Q 857.55,\
                        393.52, 847.48, 394.07 Q 837.41, 394.02, 827.35, 393.54 Q 817.28, 393.94, 807.22, 393.17 Q 797.15, 393.44, 787.09, 393.67\
                        Q 777.02, 393.19, 766.96, 392.65 Q 756.89, 392.41, 746.83, 393.34 Q 736.76, 393.84, 726.70, 393.35 Q 716.63, 393.08, 706.57,\
                        392.88 Q 696.50, 392.75, 686.44, 392.74 Q 676.37, 393.71, 666.30, 393.28 Q 656.24, 393.58, 646.17, 393.73 Q 636.11, 393.86,\
                        626.04, 394.18 Q 615.98, 393.39, 605.91, 393.65 Q 595.85, 393.12, 585.78, 393.72 Q 575.72, 393.46, 565.65, 394.12 Q 555.59,\
                        394.60, 545.52, 394.44 Q 535.46, 392.56, 525.39, 392.16 Q 515.33, 393.06, 505.26, 392.68 Q 495.20, 393.41, 485.13, 393.79\
                        Q 475.07, 393.85, 465.00, 393.80 Q 454.93, 393.63, 444.87, 393.22 Q 434.80, 393.63, 424.74, 393.93 Q 414.67, 393.75, 404.61,\
                        393.47 Q 394.54, 393.55, 384.48, 393.56 Q 374.41, 393.59, 364.35, 394.18 Q 354.28, 394.69, 344.22, 394.37 Q 334.15, 393.42,\
                        324.09, 392.02 Q 314.02, 391.68, 303.96, 392.35 Q 293.89, 392.80, 283.83, 394.00 Q 273.76, 394.86, 263.70, 395.09 Q 253.63,\
                        394.59, 243.57, 394.16 Q 233.50, 393.64, 223.43, 393.39 Q 213.37, 393.05, 203.30, 393.11 Q 193.24, 393.40, 183.17, 394.68\
                        Q 173.11, 394.56, 163.04, 394.51 Q 152.98, 394.10, 142.91, 394.09 Q 132.85, 393.98, 122.78, 394.30 Q 112.72, 394.23, 102.65,\
                        394.85 Q 92.59, 394.76, 82.52, 394.93 Q 72.46, 394.76, 62.39, 394.86 Q 52.33, 394.47, 42.26, 393.83 Q 32.20, 394.20, 22.13,\
                        392.97 Q 12.07, 393.13, 1.69, 393.31 Q 1.69, 382.81, 1.28, 372.52 Q 1.06, 362.19, 0.70, 351.88 Q 1.63, 341.56, 2.84, 331.26\
                        Q 2.74, 320.97, 1.77, 310.68 Q 2.39, 300.39, 2.47, 290.10 Q 2.50, 279.82, 1.74, 269.53 Q 1.28, 259.24, 0.99, 248.95 Q 1.80,\
                        238.66, 1.50, 228.37 Q 1.48, 218.08, 2.07, 207.79 Q 2.84, 197.50, 2.17, 187.21 Q 1.95, 176.92, 0.99, 166.63 Q 0.38, 156.34,\
                        -0.37, 146.05 Q 0.42, 135.76, 1.56, 125.47 Q 1.92, 115.18, 2.36, 104.89 Q 1.64, 94.61, 1.58, 84.32 Q 1.20, 74.03, 0.99, 63.74\
                        Q 2.10, 53.45, 2.05, 43.16 Q 0.93, 32.87, 0.43, 22.58 Q 2.00, 12.29, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:391px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page606870902-layer-accordion411519902-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:385px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page606870902-layer-accordion411519902-accordion", "926", "391", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page606870902-layer-table574065384" style="position: absolute; left: 355px; top: 225px; width: 929px; height: 273px" data-interactive-element-type="default.table" class="table stencil mobile-interaction-potential-trigger " data-stencil-id="table574065384" data-review-reference-id="table574065384">\
            <div class="stencil-wrapper" style="width: 929px; height: 273px">\
               <div title=""><table width=\'919.0\' height=\'273.0\' cellspacing=\'0\' class=\'tableStyle\'><tr style=\'height: 39px\'><td style=\'width:269px;\' class=\'tableCells\'><span\
                  style=\'\'>BK</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>1</span><br /></td><td style=\'width:137px;\'\
                  class=\'tableCells\'><span style=\'\'>X</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>2</span><br\
                  /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:269px;\'\
                  class=\'tableCells\'><span style=\'\'>1X ставка</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:269px;\' class=\'tableCells\'><span style=\'\'>Winline</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr><tr style=\'height: 39px\'><td style=\'width:269px;\' class=\'tableCells\'><span style=\'\'>Лига ставок</span><br /></td><td\
                  style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:99px;\'\
                  class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:269px;\' class=\'tableCells\'><span\
                  style=\'\'>Фонбет</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height: 39px\'><td style=\'width:269px;\'\
                  class=\'tableCells\'><span style=\'\'>Леон</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br\
                  /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br /></td></tr><tr style=\'height:\
                  39px\'><td style=\'width:269px;\' class=\'tableCells\'><span style=\'\'>Pinnacle</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span\
                  style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\' class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:137px;\'\
                  class=\'tableCells\'><span style=\'\'>_cf_</span><br /></td><td style=\'width:99px;\' class=\'tableCells\'><span style=\'\'>...</span><br\
                  /></td></tr></table>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page606870902"] .border-wrapper, body[data-current-page-id="page606870902"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page606870902"] .border-wrapper, body.has-frame[data-current-page-id="page606870902"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page606870902"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page606870902"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page606870902",\
      			"name": "coefficients",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');