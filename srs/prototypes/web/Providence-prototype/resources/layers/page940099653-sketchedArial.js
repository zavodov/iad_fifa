rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page940099653-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page940099653" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page940099653-layer-image755549093" style="position: absolute; left: 0px; top: 0px; width: 1366px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image755549093" data-review-reference-id="image755549093">\
            <div class="stencil-wrapper" style="width: 1366px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1366px;" width="1366" height="768" viewBox="0 0 1366 768">\
                     <svg:g width="1366" height="768">\
                        <svg:rect x="0" y="0" width="1366" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1366" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1366" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-rect614169215" style="position: absolute; left: 425px; top: 130px; width: 513px; height: 515px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect614169215" data-review-reference-id="rect614169215">\
            <div class="stencil-wrapper" style="width: 513px; height: 515px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 515px; width:513px;" width="513" height="515" viewBox="0 0 513 515">\
                     <svg:g width="513" height="515">\
                        <svg:rect x="0" y="0" width="513" height="515" style="stroke-width:1;stroke:black;fill:white;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-image416501245" style="position: absolute; left: 565px; top: 545px; width: 230px; height: 50px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="image416501245" data-review-reference-id="image416501245">\
            <div class="stencil-wrapper" style="width: 230px; height: 50px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 50px;width:230px;" width="230" height="50" viewBox="0 0 230 50">\
                     <svg:g width="230" height="50">\
                        <svg:svg x="0" y="0" width="230" height="50">\
                           <svg:image width="230" height="50" xlink:href="../repoimages/636388.png" preserveAspectRatio="none" transform="scale(1,1) translate(-0,-0)  "></svg:image>\
                        </svg:svg>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-textinput873276094" style="position: absolute; left: 550px; top: 310px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput873276094" data-review-reference-id="textinput873276094">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page940099653-layer-textinput873276094input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">email</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-textinput672238500" style="position: absolute; left: 550px; top: 370px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput672238500" data-review-reference-id="textinput672238500">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page940099653-layer-textinput672238500input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">password</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-iphoneButton858359124" style="position: absolute; left: 550px; top: 480px; width: 265px; height: 35px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneButton858359124" data-review-reference-id="iphoneButton858359124">\
            <div class="stencil-wrapper" style="width: 265px; height: 35px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 35px;width:265px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="265" height="35" viewBox="0 0 265 35">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,34.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-25 0.5,-2 1,-1 1,-1 2,-0.5 255,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,25 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="132.5" y="17.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.6666666666666667em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">sign in</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 265px; height: 35px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page940099653-layer-iphoneButton858359124\', \'interaction564688114\', {"button":"left","id":"action25463876","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction892157991","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page940099653-layer-text999558792" style="position: absolute; left: 680px; top: 425px; width: 140px; height: 17px" data-interactive-element-type="default.text2" class="text2 pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="text999558792" data-review-reference-id="text999558792">\
            <div class="stencil-wrapper" style="width: 140px; height: 17px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.link2"><p style="font-size: 14px;"><span class="underline">forget you password?</span></p></span></span></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 140px; height: 17px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page940099653-layer-text999558792\', \'interaction355809480\', {"button":"left","id":"action742072533","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction350050809","options":"reloadOnly","target":"page632393824","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page940099653-layer-text364845486" style="position: absolute; left: 550px; top: 425px; width: 51px; height: 17px" data-interactive-element-type="default.text2" class="text2 pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="text364845486" data-review-reference-id="text364845486">\
            <div class="stencil-wrapper" style="width: 51px; height: 17px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.link2"><p style="font-size: 14px;"><span class="underline">sign up</span></p></span></span></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 51px; height: 17px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page940099653-layer-text364845486\', \'interaction388373850\', {"button":"left","id":"action487560513","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction191833605","options":"reloadOnly","target":"page303691222","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page940099653-layer-rect457791153" style="position: absolute; left: 465px; top: 165px; width: 435px; height: 105px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="rect457791153" data-review-reference-id="rect457791153">\
            <div class="stencil-wrapper" style="width: 435px; height: 105px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 105px; width:435px;" width="435" height="105" viewBox="0 0 435 105">\
                     <svg:g width="435" height="105">\
                        <svg:rect x="0" y="0" width="435" height="105" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page940099653-layer-text808315854" style="position: absolute; left: 615px; top: 200px; width: 140px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="text808315854" data-review-reference-id="text808315854">\
            <div class="stencil-wrapper" style="width: 140px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">__logo__ </p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page940099653"] .border-wrapper, body[data-current-page-id="page940099653"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page940099653"] .border-wrapper, body.has-frame[data-current-page-id="page940099653"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page940099653"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page940099653"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page940099653",\
      			"name": "sign in",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');