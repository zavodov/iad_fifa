rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page189476043-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page189476043" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page189476043-layer-1564809249" style="position: absolute; left: 274px; top: 0px; width: 1092px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1564809249" data-review-reference-id="1564809249">\
            <div class="stencil-wrapper" style="width: 1092px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1092px;" width="1092" height="768" viewBox="0 0 1092 768">\
                     <svg:g width="1092" height="768">\
                        <svg:rect x="0" y="0" width="1092" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1092" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1092" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-598702032" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 768px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="598702032" data-review-reference-id="598702032">\
            <div class="stencil-wrapper" style="width: 275px; height: 768px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 768px; width:275px;" width="275" height="768" viewBox="0 0 275 768">\
                     <svg:g width="275" height="768">\
                        <svg:rect x="0" y="0" width="275" height="768" style="stroke-width:1;stroke:black;fill:#C1C1C1;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-606212298" style="position: absolute; left: 355px; top: 130px; width: 930px; height: 30px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="606212298" data-review-reference-id="606212298">\
            <div class="stencil-wrapper" style="width: 930px; height: 30px">\
               <div id="606212298-1510572249" style="position: absolute; left: 0px; top: 0px; width: 1335px; height: 665px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="1510572249" data-review-reference-id="1510572249">\
                  <div class="stencil-wrapper" style="width: 1335px; height: 665px">\
                     <div id="1510572249-1186813712" style="position: absolute; left: 0px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1186813712" data-review-reference-id="1186813712">\
                        <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                           <div title="">\
                              <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                                 <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                                    <svg:path id="1510572249-1186813712_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                                 </svg:g>\
                              </svg:svg>\
                              <div id="1510572249-1186813712div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'1510572249-1186813712\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'1510572249-1186813712\', \'result\');">\
                                 				RFPL\
                                 				\
                                 <addMouseOverListener></addMouseOverListener>\
                                 				\
                                 <addMouseOutListener></addMouseOutListener>\
                                 			\
                              </div>\
                           </div>\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-882477332" style="position: absolute; left: 155px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="882477332" data-review-reference-id="882477332">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="606212298-882477332_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="606212298-882477332div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'606212298-882477332\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'606212298-882477332\', \'result\');">\
                           				Premier League\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-746476258" style="position: absolute; left: 310px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="746476258" data-review-reference-id="746476258">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="606212298-746476258_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="606212298-746476258div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'606212298-746476258\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'606212298-746476258\', \'result\');">\
                           				BundesLiga\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1085355756" style="position: absolute; left: 465px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1085355756" data-review-reference-id="1085355756">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="606212298-1085355756_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="606212298-1085355756div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'606212298-1085355756\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'606212298-1085355756\', \'result\');">\
                           				Ligue 1\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1128455295" style="position: absolute; left: 620px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1128455295" data-review-reference-id="1128455295">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="606212298-1128455295_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="606212298-1128455295div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'606212298-1128455295\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'606212298-1128455295\', \'result\');">\
                           				Ligue BBVA\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
               <div id="606212298-1377395495" style="position: absolute; left: 775px; top: 0px; width: 155px; height: 30px" data-interactive-element-type="default.tabbutton" class="tabbutton stencil mobile-interaction-potential-trigger " data-stencil-id="1377395495" data-review-reference-id="1377395495">\
                  <div class="stencil-wrapper" style="width: 155px; height: 30px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" style="position: absolute; left: -5px; top: -5px; height: 40px;width:165px;" width="155" height="35">\
                           <svg:g id="target" x="-5" y="0" width="155" height="30" name="target" class="">\
                              <svg:path id="606212298-1377395495_small_path" width="155" height="30" style="stroke:black; stroke-width:1; fill:white" class="smallTab" d="M 5,15 C 5,5 15,5 15,5 L 150,5 C 160,5 160,15 160,15 L 160,35 L 5,35 L 5,15"></svg:path>\
                           </svg:g>\
                        </svg:svg>\
                        <div id="606212298-1377395495div" class="helvetica-font" style="position: absolute; top: 4px; height: 30px;width:155px;text-align:center;font-size:1em;fill:none;cursor:pointer;padding-top:5px;" xml:space="preserve" onmouseover="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOver, \'606212298-1377395495\', \'result\');" onmouseout="rabbit.facade.raiseEvent(rabbit.events.tabButtonMouseOut, \'606212298-1377395495\', \'result\');">\
                           				Serie A\
                           				\
                           <addMouseOverListener></addMouseOverListener>\
                           				\
                           <addMouseOutListener></addMouseOutListener>\
                           			\
                        </div>\
                     </div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-1840063710" style="position: absolute; left: 275px; top: 160px; width: 1091px; height: 465px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1840063710" data-review-reference-id="1840063710">\
            <div class="stencil-wrapper" style="width: 1091px; height: 465px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 465px; width:1091px;" width="1091" height="465" viewBox="0 0 1091 465">\
                     <svg:g width="1091" height="465">\
                        <svg:rect x="0" y="0" width="1091" height="465" style="stroke-width:1;stroke:black;fill:#a7a77c;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-784305066" style="position: absolute; left: 0px; top: 0px; width: 275px; height: 160px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="784305066" data-review-reference-id="784305066">\
            <div class="stencil-wrapper" style="width: 275px; height: 160px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 160px; width:275px;" width="275" height="160" viewBox="0 0 275 160">\
                     <svg:g width="275" height="160">\
                        <svg:rect x="0" y="0" width="275" height="160" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-991307636" style="position: absolute; left: 15px; top: 35px; width: 240px; height: 87px" data-interactive-element-type="group" class="group stencil mobile-interaction-potential-trigger " data-stencil-id="991307636" data-review-reference-id="991307636">\
            <div class="stencil-wrapper" style="width: 240px; height: 87px">\
               <div id="991307636-832991126" style="position: absolute; left: 0px; top: 0px; width: 85px; height: 85px" data-interactive-element-type="static.ellipse" class="ellipse stencil mobile-interaction-potential-trigger " data-stencil-id="832991126" data-review-reference-id="832991126">\
                  <div class="stencil-wrapper" style="width: 85px; height: 85px">\
                     <div title="">\
                        <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 85px;width:85px;" width="85" height="85" viewBox="0 0 85 85">\
                           <svg:g width="85" height="85">\
                              <svg:ellipse cx="42.5" cy="42.5" rx="42.5" ry="42.5" style="stroke-width:1;stroke:black;fill:white;"></svg:ellipse>\
                           </svg:g>\
                        </svg:svg>\
                     </div>\
                  </div>\
               </div>\
               <div id="991307636-1982813214" style="position: absolute; left: 100px; top: 0px; width: 91px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1982813214" data-review-reference-id="1982813214">\
                  <div class="stencil-wrapper" style="width: 91px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Name</span></p></span></span></div>\
                  </div>\
               </div>\
               <div id="991307636-477098393" style="position: absolute; left: 100px; top: 50px; width: 145px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="477098393" data-review-reference-id="477098393">\
                  <div class="stencil-wrapper" style="width: 145px; height: 37px">\
                     <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">Surname </p></span></span></div>\
                  </div>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-1191584763" style="position: absolute; left: 276px; top: 0px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="1191584763" data-review-reference-id="1191584763">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-63224330" style="position: absolute; left: 276px; top: 688px; width: 1090px; height: 80px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="63224330" data-review-reference-id="63224330">\
            <div class="stencil-wrapper" style="width: 1090px; height: 80px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 80px; width:1090px;" width="1090" height="80" viewBox="0 0 1090 80">\
                     <svg:g width="1090" height="80">\
                        <svg:rect x="0" y="0" width="1090" height="80" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-352178416" style="position: absolute; left: 355px; top: 10px; width: 230px; height: 60px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="352178416" data-review-reference-id="352178416">\
            <div class="stencil-wrapper" style="width: 230px; height: 60px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 60px;width:230px;" width="230" height="60" viewBox="0 0 230 60">\
                     <svg:g width="230" height="60">\
                        <svg:rect x="0" y="0" width="230" height="60" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="230" y2="60" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="60" x2="230" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-987791261" style="position: absolute; left: 0px; top: 180px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="987791261" data-review-reference-id="987791261">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Коэффициенты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-987791261\', \'interaction277522265\', {"button":"left","id":"action389694926","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction508902595","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-68618477" style="position: absolute; left: 1117px; top: 20px; width: 172px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="68618477" data-review-reference-id="68618477">\
            <div class="stencil-wrapper" style="width: 172px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p><span style="font-size: 32px;">Изменения</span></p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-35730108" style="position: absolute; left: 0px; top: 250px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button stencil mobile-interaction-potential-trigger " data-stencil-id="35730108" data-review-reference-id="35730108">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#a7a77c;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Изменения<br /></button></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-421256168" style="position: absolute; left: 0px; top: 320px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="421256168" data-review-reference-id="421256168">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Статистика<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-421256168\', \'interaction494869075\', {"button":"left","id":"action378196588","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction685374820","options":"reloadOnly","target":"page995954813","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-685009201" style="position: absolute; left: 0px; top: 718px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="685009201" data-review-reference-id="685009201">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Выход<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-685009201\', \'interaction173339785\', {"button":"left","id":"action78889276","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction170441484","options":"reloadOnly","target":"page940099653","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1207690582" style="position: absolute; left: 0px; top: 673px; width: 275px; height: 50px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1207690582" data-review-reference-id="1207690582">\
            <div class="stencil-wrapper" style="width: 275px; height: 50px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:50px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Настройки<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 50px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-1207690582\', \'interaction638595480\', {"button":"left","id":"action293296677","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction374237093","options":"reloadOnly","target":"page47785501","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1634000025" style="position: absolute; left: 0px; top: 625px; width: 275px; height: 48px" data-interactive-element-type="default.button" class="button pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1634000025" data-review-reference-id="1634000025">\
            <div class="stencil-wrapper" style="width: 275px; height: 48px">\
               <div xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" title=""><button type="button" style="width:275px;height:48px;font-size:1.8333333333333333em;background-color:#d9d9d9;padding-left: 0px; padding-right: 0px;" xml:space="preserve" title="">Контакты<br /></button></div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 275px; height: 48px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page189476043-layer-1634000025\', \'interaction327886842\', {"button":"left","id":"action131477880","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"reaction656799605","options":"reloadOnly","target":"page607113527","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page189476043-layer-1832345745" style="position: absolute; left: 355px; top: 710px; width: 413px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1832345745" data-review-reference-id="1832345745">\
            <div class="stencil-wrapper" style="width: 413px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">footer and some information </p></span></span></div>\
            </div>\
         </div>\
         <div id="__containerId__-page189476043-layer-816793362" style="position: absolute; left: 355px; top: 195px; width: 930px; height: 400px" data-interactive-element-type="default.accordion" class="accordion stencil mobile-interaction-potential-trigger " data-stencil-id="816793362" data-review-reference-id="816793362">\
            <div class="stencil-wrapper" style="width: 930px; height: 400px">\
               <div xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" id="__containerId__-page189476043-layer-816793362-accordion" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 400px;width:930px;" width="930" height="400">\
                     <svg:g id="__containerId__-page189476043-layer-816793362svg" width="930" height="400"><svg:path class=" svg_unselected_element" d="M 2.00, 2.00 Q 12.07, 2.50, 22.13, 3.23 Q 32.20, 3.52, 42.26, 3.70 Q 52.33, 2.76,\
                        62.39, 3.15 Q 72.46, 3.13, 82.52, 3.79 Q 92.59, 2.87, 102.65, 3.00 Q 112.72, 3.03, 122.78, 3.07 Q 132.85, 2.22, 142.91, 2.50\
                        Q 152.98, 2.88, 163.04, 2.99 Q 173.11, 2.91, 183.17, 1.80 Q 193.24, 2.19, 203.30, 1.57 Q 213.37, 2.90, 223.43, 2.46 Q 233.50,\
                        2.87, 243.57, 2.42 Q 253.63, 1.28, 263.70, 1.10 Q 273.76, 0.88, 283.83, 1.58 Q 293.89, 1.19, 303.96, 2.55 Q 314.02, 2.27,\
                        324.09, 2.18 Q 334.15, 2.20, 344.22, 2.12 Q 354.28, 2.44, 364.35, 1.10 Q 374.41, 1.65, 384.48, 2.21 Q 394.54, 2.60, 404.61,\
                        1.32 Q 414.67, 2.13, 424.74, 2.60 Q 434.80, 2.47, 444.87, 2.02 Q 454.93, 2.59, 465.00, 3.22 Q 475.07, 2.92, 485.13, 2.53 Q\
                        495.20, 1.62, 505.26, 0.92 Q 515.33, 0.72, 525.39, 0.81 Q 535.46, 0.69, 545.52, 0.35 Q 555.59, 0.98, 565.65, 1.77 Q 575.72,\
                        1.51, 585.78, 0.64 Q 595.85, 1.45, 605.91, 1.84 Q 615.98, 1.48, 626.04, 2.21 Q 636.11, 1.57, 646.17, 2.23 Q 656.24, 1.86,\
                        666.30, 1.56 Q 676.37, 1.49, 686.44, 0.83 Q 696.50, 1.20, 706.57, 2.23 Q 716.63, 1.88, 726.70, 1.02 Q 736.76, 1.02, 746.83,\
                        1.66 Q 756.89, 2.61, 766.96, 0.99 Q 777.02, 2.20, 787.09, 1.29 Q 797.15, 0.70, 807.22, 0.32 Q 817.28, 0.95, 827.35, 1.97 Q\
                        837.41, 2.01, 847.48, 1.91 Q 857.54, 1.40, 867.61, 1.57 Q 877.67, 1.10, 887.74, 1.44 Q 897.81, 1.79, 907.87, 2.17 Q 917.94,\
                        1.76, 927.94, 2.06 Q 927.45, 12.61, 928.45, 22.78 Q 928.60, 33.22, 928.71, 43.66 Q 928.96, 54.09, 928.97, 64.52 Q 929.08,\
                        74.94, 929.13, 85.37 Q 930.00, 95.79, 930.10, 106.21 Q 930.31, 116.63, 929.95, 127.05 Q 929.99, 137.47, 929.54, 147.89 Q 930.06,\
                        158.32, 930.33, 168.74 Q 930.40, 179.16, 929.97, 189.58 Q 929.79, 200.00, 929.74, 210.42 Q 930.10, 220.84, 929.79, 231.26\
                        Q 929.68, 241.68, 929.37, 252.11 Q 929.47, 262.53, 928.52, 272.95 Q 927.94, 283.37, 928.02, 293.79 Q 928.17, 304.21, 928.14,\
                        314.63 Q 927.84, 325.05, 927.34, 335.47 Q 927.46, 345.89, 928.57, 356.32 Q 927.67, 366.74, 927.44, 377.16 Q 927.50, 387.58,\
                        928.17, 398.17 Q 918.22, 398.86, 908.02, 399.04 Q 897.83, 398.32, 887.76, 398.68 Q 877.69, 398.84, 867.62, 399.41 Q 857.55,\
                        398.68, 847.48, 399.16 Q 837.42, 399.26, 827.35, 398.97 Q 817.28, 399.42, 807.22, 399.37 Q 797.15, 398.96, 787.09, 398.93\
                        Q 777.02, 399.13, 766.96, 399.56 Q 756.89, 399.36, 746.83, 399.37 Q 736.76, 398.84, 726.70, 398.22 Q 716.63, 399.28, 706.57,\
                        399.67 Q 696.50, 399.46, 686.44, 397.61 Q 676.37, 397.06, 666.30, 397.91 Q 656.24, 398.06, 646.17, 398.08 Q 636.11, 398.74,\
                        626.04, 398.76 Q 615.98, 398.72, 605.91, 398.73 Q 595.85, 399.35, 585.78, 398.83 Q 575.72, 399.08, 565.65, 398.81 Q 555.59,\
                        399.10, 545.52, 398.78 Q 535.46, 399.29, 525.39, 399.64 Q 515.33, 399.27, 505.26, 399.39 Q 495.20, 399.25, 485.13, 399.64\
                        Q 475.07, 398.92, 465.00, 398.83 Q 454.93, 399.11, 444.87, 398.15 Q 434.80, 398.06, 424.74, 397.89 Q 414.67, 398.82, 404.61,\
                        398.43 Q 394.54, 398.67, 384.48, 398.28 Q 374.41, 398.44, 364.35, 397.24 Q 354.28, 398.26, 344.22, 399.05 Q 334.15, 399.22,\
                        324.09, 399.40 Q 314.02, 398.14, 303.96, 398.82 Q 293.89, 398.07, 283.83, 397.96 Q 273.76, 398.03, 263.70, 397.67 Q 253.63,\
                        398.38, 243.57, 398.44 Q 233.50, 398.87, 223.43, 398.50 Q 213.37, 399.38, 203.30, 399.25 Q 193.24, 399.23, 183.17, 400.13\
                        Q 173.11, 400.40, 163.04, 400.46 Q 152.98, 400.32, 142.91, 399.88 Q 132.85, 399.19, 122.78, 399.80 Q 112.72, 399.18, 102.65,\
                        399.25 Q 92.59, 399.98, 82.52, 400.19 Q 72.46, 399.55, 62.39, 400.45 Q 52.33, 399.98, 42.26, 399.94 Q 32.20, 399.80, 22.13,\
                        399.68 Q 12.07, 399.56, 1.22, 398.78 Q 0.83, 387.97, 0.98, 377.30 Q 1.15, 366.79, 0.56, 356.36 Q 1.38, 345.90, 0.85, 335.48\
                        Q 1.30, 325.06, 0.85, 314.63 Q 1.21, 304.21, 1.61, 293.79 Q 0.94, 283.37, 0.66, 272.95 Q 0.46, 262.53, 0.20, 252.11 Q 0.13,\
                        241.68, 0.04, 231.26 Q 0.05, 220.84, 0.24, 210.42 Q 0.35, 200.00, 0.73, 189.58 Q 1.40, 179.16, 1.41, 168.74 Q 1.64, 158.32,\
                        1.79, 147.89 Q 1.61, 137.47, 1.55, 127.05 Q 1.37, 116.63, 1.75, 106.21 Q 1.26, 95.79, 1.41, 85.37 Q 1.69, 74.95, 2.70, 64.53\
                        Q 1.73, 54.11, 1.65, 43.68 Q 1.49, 33.26, 1.54, 22.84 Q 2.00, 12.42, 2.00, 2.00" style=" fill:#DDDDDD;"/>\
                     </svg:g>\
                  </svg:svg>\
                  <div xml:space="preserve" style="&#xA;&#x9;&#x9;&#x9;&#x9;overflow: hidden; position: absolute; left: 2px; top: 2px; width: 926px; height:396px; font-size: 1em; line-height: 1.2em;border: none; background: #DDD&#xA;&#x9;&#x9;&#x9;">\
                     				\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-2">\
                        							First Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-4">\
                        							Second Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-6">\
                        							Third Match (some information)\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     						\
                     						\
                     						\
                     		\
                     						\
                     						\
                     						\
                     <div class="accordion-header &#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&#xA;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;" id="__containerId__-page189476043-layer-816793362-8">\
                        							...\
                        						\
                     </div>\
                     						\
                     <div class="accordion-content">\
                        							\
                        								\
                        										\
                        <div class="renderEmptyPage" style="position:absolute;left:4px;top:4px;width:920px;height:390px;overflow:auto;border:none;">Frame without target</div>\
                        									\
                        							\
                        						\
                     </div>\
                     					\
                     			\
                  </div>\
               </div><script xmlns:accordionHelper="java:it.rapidrabb.editor.stencils.helpers.AccordionHelper" xmlns:helper="java:it.rapidrabb.editor.stencils.helpers.StencilHelper" type="text/javascript">\
				rabbit.stencils.accordion.setupAccordion("__containerId__-page189476043-layer-816793362-accordion", "926", "396", 1);\
			</script></div>\
         </div>\
         <div id="__containerId__-page189476043-layer-iphoneListBackground528657481" style="position: absolute; left: 355px; top: 225px; width: 930px; height: 280px" data-interactive-element-type="static.iphoneListBackground" class="iphoneListBackground stencil mobile-interaction-potential-trigger " data-stencil-id="iphoneListBackground528657481" data-review-reference-id="iphoneListBackground528657481">\
            <div class="stencil-wrapper" style="width: 930px; height: 280px">\
               <div title="" style="height: 280px; width:930px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="930" height="280" viewBox="0 0 930 280">\
                     <svg:rect x="1" y="1" width="928" rx="10" height="278" style="stroke-width:1;fill:white;;stroke:black;"></svg:rect>\
                     <svg:line x1="1" y1="40" x2="928" y2="40" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="80" x2="928" y2="80" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="120" x2="928" y2="120" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="160" x2="928" y2="160" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="200" x2="928" y2="200" style="stroke-width:1;stroke:black;"></svg:line>\
                     <svg:line x1="1" y1="240" x2="928" y2="240" style="stroke-width:1;stroke:black;"></svg:line>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page189476043"] .border-wrapper, body[data-current-page-id="page189476043"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page189476043"] .border-wrapper, body.has-frame[data-current-page-id="page189476043"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page189476043"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page189476043"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page189476043",\
      			"name": "changes",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');