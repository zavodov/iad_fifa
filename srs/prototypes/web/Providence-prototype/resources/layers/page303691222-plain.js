rabbit.data.layerStore.addLayerFromHtml('<div xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sketchedHelper="java:it.rapidrabb.editor.stencils.helpers.SketchedHelper" xmlns:fn="http://www.w3.org/2005/xpath-functions">\
   <div id="result">\
      <div xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" id="__containerId__page303691222-layer" class="layer" name="__containerId__pageLayer" data-layer-id="page303691222" data-layer-type="pageLayer" style="position:absolute;left:0px;top:0px;">\
         <div id="__containerId__-page303691222-layer-1309838245" style="position: absolute; left: 0px; top: 0px; width: 1366px; height: 768px" data-interactive-element-type="default.image" class="image stencil mobile-interaction-potential-trigger " data-stencil-id="1309838245" data-review-reference-id="1309838245">\
            <div class="stencil-wrapper" style="width: 1366px; height: 768px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" class="image-cropper" style="height: 768px;width:1366px;" width="1366" height="768" viewBox="0 0 1366 768">\
                     <svg:g width="1366" height="768">\
                        <svg:rect x="0" y="0" width="1366" height="768" style="stroke:black; stroke-width:1;fill:white;"></svg:rect>\
                        <svg:line x1="0" y1="0" x2="1366" y2="768" style="stroke:black; stroke-width:0.5;"></svg:line>\
                        <svg:line x1="0" y1="768" x2="1366" y2="0" style="stroke:black; stroke-width:0.5;"></svg:line>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-22530544" style="position: absolute; left: 425px; top: 130px; width: 513px; height: 515px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="22530544" data-review-reference-id="22530544">\
            <div class="stencil-wrapper" style="width: 513px; height: 515px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 515px; width:513px;" width="513" height="515" viewBox="0 0 513 515">\
                     <svg:g width="513" height="515">\
                        <svg:rect x="0" y="0" width="513" height="515" style="stroke-width:1;stroke:black;fill:white;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-356261905" style="position: absolute; left: 550px; top: 440px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="356261905" data-review-reference-id="356261905">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page303691222-layer-356261905input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">email</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-textinput997296309" style="position: absolute; left: 550px; top: 355px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput997296309" data-review-reference-id="textinput997296309">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page303691222-layer-textinput997296309input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">Surname</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-textinput96662595" style="position: absolute; left: 550px; top: 305px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="textinput96662595" data-review-reference-id="textinput96662595">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page303691222-layer-textinput96662595input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">Name</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-1693479693" style="position: absolute; left: 550px; top: 490px; width: 265px; height: 40px" data-interactive-element-type="default.textinput" class="textinput stencil mobile-interaction-potential-trigger " data-stencil-id="1693479693" data-review-reference-id="1693479693">\
            <div class="stencil-wrapper" style="width: 265px; height: 40px">\
               <div title=""><textarea id="__containerId__-page303691222-layer-1693479693input" rows="" cols="" style="width:263px;height:36px;padding: 0px;border-width:1px;">password</textarea></div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-1259690632" style="position: absolute; left: 550px; top: 570px; width: 265px; height: 35px" data-interactive-element-type="default.iphoneButton" class="iphoneButton pidoco-clickable-element stencil mobile-interaction-potential-trigger " data-stencil-id="1259690632" data-review-reference-id="1259690632">\
            <div class="stencil-wrapper" style="width: 265px; height: 35px">\
               <div xmlns:pidoco="http://www.pidoco.com/util" title="" style="height: 35px;width:265px;">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" width="265" height="35" viewBox="0 0 265 35">\
                     <svg:a>\
                        <svg:path fill-rule="evenodd" clip-rule="evenodd" fill="#808080" stroke="#666666" d="M5,34.5 l-2,-0.5 -1,-1 -1,-1.5 -0.5,-1.5 0,-25 0.5,-2 1,-1 1,-1 2,-0.5 255,0 1.5,0.5 1.5,1 1,1.5 0.5,1.5 0,25 -0.5,1.5 -1,1.5 -1.5,1 -1.5,0.5 z"></svg:path>\
                        <svg:text x="132.5" y="17.5" dy="0.3em" fill="#FFFFFF" style="font-size:1.6666666666666667em;stroke-width:0pt;" font-family="\'HelveticaNeue-Bold\'" text-anchor="middle" xml:space="preserve">sign up</svg:text>\
                     </svg:a>\
                  </svg:svg>\
               </div>\
            </div>\
            <div class="interactive-stencil-highlighter" style="width: 265px; height: 35px"></div><script xmlns:json="http://json.org/" type="text/javascript">\
			$(document).ready(function(){\
				rabbit.interaction.manager.registerInteraction(\'__containerId__-page303691222-layer-1259690632\', \'322725809\', {"button":"left","id":"427385172","numberOfFinger":"1","type":"click"},  \
					[\
						{"delay":"0","id":"887552519","options":"reloadOnly","target":"page606870902","transition":"none","type":"showPage"}\
					]\
				);\
			});\
		</script></div>\
         <div id="__containerId__-page303691222-layer-629932482" style="position: absolute; left: 465px; top: 165px; width: 435px; height: 105px" data-interactive-element-type="static.rect" class="rect stencil mobile-interaction-potential-trigger " data-stencil-id="629932482" data-review-reference-id="629932482">\
            <div class="stencil-wrapper" style="width: 435px; height: 105px">\
               <div title="">\
                  <svg:svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" overflow="hidden" style="height: 105px; width:435px;" width="435" height="105" viewBox="0 0 435 105">\
                     <svg:g width="435" height="105">\
                        <svg:rect x="0" y="0" width="435" height="105" style="stroke-width:1;stroke:black;fill:grey;opacity:0.5;"></svg:rect>\
                     </svg:g>\
                  </svg:svg>\
               </div>\
            </div>\
         </div>\
         <div id="__containerId__-page303691222-layer-1702158707" style="position: absolute; left: 615px; top: 200px; width: 140px; height: 37px" data-interactive-element-type="default.text2" class="text2 stencil mobile-interaction-potential-trigger " data-stencil-id="1702158707" data-review-reference-id="1702158707">\
            <div class="stencil-wrapper" style="width: 140px; height: 37px">\
               <div title=""><span class="default-text2-container-wrapper" title=""><span class="default-text2-container" data-child-type="default.textHeadline12"><p class="none" style="font-size: 32px;">__logo__ </p></span></span></div>\
            </div>\
         </div>\
      </div>\
   </div>\
   <div id="styles">\
      <style type="text/css">\
         	\
         		body[data-current-page-id="page303691222"] .border-wrapper, body[data-current-page-id="page303691222"] .simulation-container{\
         			width:1366px;\
         		}\
         		\
         		body.has-frame[data-current-page-id="page303691222"] .border-wrapper, body.has-frame[data-current-page-id="page303691222"]\
         .simulation-container{\
         			height:768px;\
         		}\
         		\
         		body[data-current-page-id="page303691222"] .svg-border-1366-768{\
         			display: block !important;\
         		}\
         		\
         		body[data-current-page-id="page303691222"] .border-wrapper .border-div{\
         			width:1366px;\
         			height:768px;\
         		}\
         	\
      </style>\
   </div>\
   <div id="json">\
      		{\
      			"id": "page303691222",\
      			"name": "sign up",\
      			"layers": {\
      				\
      			},\
      			"image":"",\
      			"width":1366,\
      			"height":768,\
      			"parentFolder": "",\
      			"frame": "desktop",\
      			"frameOrientation": "landscape"\
      		}\
      	\
   </div>\
</div>');