const TelegramBot = require('node-telegram-bot-api');
const token = process.env.BOT_TOKEN;
const bot = new TelegramBot(token, {polling: true});
const normalizer = require('../util/normalizer');

module.exports = (synchronizer) => {
    const dateOptions = {day: 'numeric', month: 'numeric', hour: '2-digit', minute: '2-digit'};

    bot.onText(/\/start/, async (msg) => {
        bot.sendMessage(msg.chat.id, "Дарова!");
    });

    bot.onText(/\/matches/, async (msg) => {
        let response = 'Список ближайших матчей:\n';
        const matches = await synchronizer.sequelize.models.Match.findAll({
            where: {
                match_status: 'TIMED'
            },
            include: [
                {
                    model: synchronizer.sequelize.models.Team,
                    as: 'home_team',
                    attributes: {
                        exclude: ['team_id', 'team_logo']
                    }
                },
                {
                    model: synchronizer.sequelize.models.Team,
                    as: 'away_team',
                    attributes: {
                        exclude: ['team_id', 'team_logo']
                    }
                },
                {
                    model: synchronizer.sequelize.models.League,
                    as: 'league',
                    attributes: {
                        exclude: ['league_id', 'league_code', 'league_teams_number']
                    }
                },
            ],
            attributes: {
                exclude: ['team_logo', 'home_team_id', 'away_team_id', 'league_id', 'away_team_goals', 'home_team_goals', 'match_status']
            },
            order: [['match_date']],
            limit: 5
        });
        for (const match of matches) {
            const date = new Date(match.get('match_date'));
            response += `${date.toLocaleDateString('ru', dateOptions)} по Гринвичу - ${match.get('home_team').get('team_name')} против ${match.get('away_team').get('team_name')} в ${match.get('league').get('league_name')}\n`;
        }
        bot.sendMessage(msg.chat.id, response);
    });

    bot.onText(/\/teaminfo (.+)/, async (msg, match) => {
        const teamId = synchronizer.cachedTeams.get(normalizer.normalize(match[1]));
        if (teamId !== undefined) {
            let response = `Найденная информация о команде ${match[1]}:\n`;
            response += 'Команда учавствует в следующих лигах:\n';
            const leagueParticipations = await synchronizer.sequelize.models.LeagueParticipation.findAll({
                where: {
                    team_id: teamId
                }, include: [
                    {
                        model: synchronizer.sequelize.models.League,
                        as: 'league',
                    }
                ], attributes: {
                    exclude: ['league_participation_id', 'league_id', 'league_teams_number']
                }
            });
            for (const leagueParticipation of leagueParticipations) {
                response += (`${leagueParticipation.get('league').get('league_name')}:\n -побед - ${leagueParticipation.get('wins')}\n -поражений - ${leagueParticipation.get('loses')}\n -ничьих - ${leagueParticipation.get('draws')}\n`);
            }
            response += 'Ближайшие матчи:\n';
            const matches = await synchronizer.sequelize.models.Match.findAll({
                where: {
                    match_status: 'TIMED',
                    $or: [
                        {
                            home_team_id: teamId
                        },
                        {
                            away_team_id: teamId
                        }
                    ]
                },
                include: [
                    {
                        model: synchronizer.sequelize.models.Team,
                        as: 'home_team',
                        attributes: {
                            exclude: ['team_id', 'team_logo']
                        }
                    },
                    {
                        model: synchronizer.sequelize.models.Team,
                        as: 'away_team',
                        attributes: {
                            exclude: ['team_id', 'team_logo']
                        }
                    },
                    {
                        model: synchronizer.sequelize.models.League,
                        as: 'league',
                        attributes: {
                            exclude: ['league_id', 'league_code', 'league_teams_number']
                        }
                    }
                ],
                attributes: {
                    exclude: ['team_logo', 'home_team_id', 'away_team_id', 'league_id', 'away_team_goals', 'home_team_goals', 'match_status']
                },
                order: [['match_date']],
                limit: 5
            });
            for (const match of matches) {
                const date = new Date(match.get('match_date'));
                response += `${date.toLocaleDateString('ru', dateOptions)} по Гринвичу - ${match.get('home_team').get('team_name')} против ${match.get('away_team').get('team_name')} в ${match.get('league').get('league_name')}\n`;
            }
            bot.sendMessage(msg.chat.id, response);
        } else
            bot.sendMessage(msg.chat.id, 'Команда не найдена :(');
    });

    bot.onText(/\/matchinfo (.+) vs (.+)/, async (msg, match) => {
        const firstTeamId = synchronizer.cachedTeams.get(normalizer.normalize(match[1]));
        const secondTeamId = synchronizer.cachedTeams.get(normalizer.normalize(match[2]));
        if (firstTeamId !== undefined && secondTeamId !== undefined) {
            const matches = await synchronizer.sequelize.models.Match.findAll({
                where: {
                    $and: [
                        {
                            $or: [
                                {
                                    home_team_id: secondTeamId,
                                    away_team_id: firstTeamId
                                },
                                {
                                    home_team_id: firstTeamId,
                                    away_team_id: secondTeamId
                                }
                            ]
                        },
                        {
                            $or: [
                                {
                                    match_status: 'FINISHED'
                                },
                                {
                                    match_status: 'TIMED'
                                }
                            ]
                        }
                    ]
                },
                include: [
                    {
                        model: synchronizer.sequelize.models.Team,
                        as: 'home_team',
                        attributes: {
                            exclude: ['team_id', 'team_logo']
                        }
                    },
                    {
                        model: synchronizer.sequelize.models.Team,
                        as: 'away_team',
                        attributes: {
                            exclude: ['team_id', 'team_logo']
                        }
                    },
                    {
                        model: synchronizer.sequelize.models.League,
                        as: 'league',
                        attributes: {
                            exclude: ['league_id', 'league_code', 'league_teams_number']
                        }
                    }
                ],
                attributes: {
                    exclude: ['team_logo', 'home_team_id', 'away_team_id', 'league_id']
                },
                order: [['match_date']]
            });
            if (matches.length === 0)
                bot.sendMessage(msg.chat.id, 'Матчи не найдены :(');
            else {
                let response = `Матчи между командами ${match[1]} и ${match[2]}:\n`;
                for (const match of matches) {
                    const date = new Date(match.get('match_date'));
                    response += (`${date.toLocaleDateString('ru', dateOptions)} по Гринвичу - ${match.get('home_team').get('team_name')} против ${match.get('away_team').get('team_name')} в ${match.get('league').get('league_name')}`);
                    if (match.get('match_status') === 'FINISHED')
                        response += ` со счётом ${match.get('home_team_goals')} - ${match.get('away_team_goals')}\n`;
                    else
                        response += '\n';
                }
                bot.sendMessage(msg.chat.id, response);
            }
        } else
            bot.sendMessage(msg.chat.id, 'Похоже, что какая-то команда не найдена :(');
    });

};
