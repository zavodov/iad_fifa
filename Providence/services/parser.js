const cheerio = require('cheerio');
const request = require('async-request');
const phantom = require('phantom');
const n = require('../util/normalizer');

module.exports = (synchronizer) => {

    const bookmakersData = new Map();

    const pariMatch = async (uri) => {
        let result = [];
        const response = await request(uri, {
            method: 'GET'
        });
        const $ = cheerio.load(response.body, {decodeEntities: false});
        $('tbody.row1, tbody.row2').not('.props').each(function (index, tableRow) {
            const temp = $('td a', tableRow);
            const tempTeam = temp.eq(0).html().split('<br>');
            result.push({
                home_team: tempTeam[0],
                away_team: tempTeam[1],
                home: temp.eq(5).text(),
                draw: temp.eq(6).text(),
                away: temp.eq(7).text()
            })
        });
        return result.filter((element) => {
            return Object.keys(element).length > 0;
        });
    };

    const pinnacle = async (uri) => {
        const instance = await phantom.create();
        const page = await instance.createPage();
        const status = await page.open(uri);
        const content = await page.property('content');
        const $ = cheerio.load(content, {decodeEntities: false});
        let result = [];
        $('tbody.ng-scope[ng-repeat="event in events = (league.events | filter: linesDataExists)"]').each((tableIndex, dataTable) => {
            const names = $('td.game-name.name', dataTable);
            const odds = $('td.oddTip.game-moneyline', dataTable);
            result.push({
                home_team: names.eq(0).text().trim(),
                away_team: names.eq(1).text().trim(),
                home: odds.eq(0).text().trim(),
                draw: odds.eq(2).text().trim(),
                away: odds.eq(1).text().trim()
            })
        });
        await instance.exit();
        return result.filter((element) => {
            return element['home'] !== '' && element['home'] !== '' && element['away'] !== '';
        });
    };

    const palmerBet = async (uri) => {
        let result = [];
        const response = await request(uri, {
            method: 'GET'
        });
        const $ = cheerio.load(response.body, {decodeEntities: false});
        $('table.odds-grid.markets-grid tbody tr').each(function (higherIndex, tableRow) {
            const names = $('td.nam', tableRow).text().split('vs');
            result.push({
                home_team: names[0].trim(),
                away_team: names[1].trim(),
                home: $('a', tableRow).eq(1).text(),
                away: $('a', tableRow).eq(2).text(),
                draw: $('td', tableRow).eq(3).text()
            })
        });
        return result;
    };

    bookmakersData.set('Pari Match', pariMatch);
    bookmakersData.set('Pinnacle', pinnacle);
    bookmakersData.set('Palmer Bet', palmerBet);

    module.load = async () => {
        const bookmakers = await synchronizer.sequelize.models.Bookmaker.findAll({
            include: [
                {
                    model: synchronizer.sequelize.models.BookmakerLine,
                    as: 'lines',
                    attributes: {
                        exclude: ['bookmaker_id']
                    }
                }
            ],
            attributes: {
                exclude: ['bookmaker_url']
            }
        });
        for (const bookmaker of bookmakers) {
            for (const line of bookmaker.get('lines')) {
                const matches = await bookmakersData.get(bookmaker.get('bookmaker_name'))(line.get('line_url'));
                for (let match of matches) {
                    const matchFoundValue = await synchronizer.sequelize.models.Match.findOne({
                        where: {
                            league_id: line.get('league_id'),
                            home_team_id: synchronizer.cachedTeams.get(n.normalize(match['home_team'])),
                            away_team_id: synchronizer.cachedTeams.get(n.normalize(match['away_team']))
                        }
                    });
                    if (matchFoundValue === null)
                        console.log('ATTENTION, TEAM/S ARE NOT FOUND:', bookmaker.get('bookmaker_id'), n.normalize(match['home_team']), n.normalize(match['away_team']));
                    else {
                        await synchronizer.sequelize.models.Offer.create({
                            bookmaker_id: bookmaker.get('bookmaker_id'),
                            event_id: synchronizer.cachedMatches.get(matchFoundValue.get('match_id'))['home'],
                            offer_coefficient: match['home']
                        });
                        await synchronizer.sequelize.models.Offer.create({
                            bookmaker_id: bookmaker.get('bookmaker_id'),
                            event_id: synchronizer.cachedMatches.get(matchFoundValue.get('match_id'))['draw'],
                            offer_coefficient: match['draw']
                        });
                        await synchronizer.sequelize.models.Offer.create({
                            bookmaker_id: bookmaker.get('bookmaker_id'),
                            event_id: synchronizer.cachedMatches.get(matchFoundValue.get('match_id'))['away'],
                            offer_coefficient: match['away']
                        })
                    }
                }
            }
        }
    };

    return module;

};