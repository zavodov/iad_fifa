let odds = new Vue ({
    el : '#odds',
    data: {
        bookmakers: null,
        actual_odds: null,
        current_better_bookmaker: null
    },
    methods:{
        //ПЕРЕПИСАТЬ НАФИГ ЭТО ДЕРЬМО!
        getBetterCf: function (events, event_type){
            let offers = null;
            events.forEach(function(item, i, arr) {
                if (item.event_type === event_type)
                    offers = item.offers;
            });
            if (offers.length === 0) return " ";

            let betterCf = Number(offers[0].offer_coefficient);
            let betterBookmaker = offers[0].bookmaker_id;
            offers.forEach(function(item, i, arr){
                if (Number(item.offer_coefficient) > betterCf){
                    betterCf = Number(item.offer_coefficient);
                    betterBookmaker = item.bookmaker_id;
                }
            });
            this.current_better_bookmaker = this.bookmakers[Number(betterBookmaker)-1].bookmaker_url;
            return betterCf;
        }
    }
});

function loadBookmakers() {
    let bookmaker = [];

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/bookmakers', false);
    xhr.send();

    if (xhr.status != 200) {
        Console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
    } else {
        bookmaker = JSON.parse(xhr.responseText);
    }

    odds.bookmakers = bookmaker
}

function loadOdds() {
    let actual_odds = [];

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/odds/actual', false);
    xhr.send();

        if (xhr.status != 200) {
            Console.log('Ошибка ' + xhr.status + ': ' + xhr.statusText);
        } else {
            actual_odds = JSON.parse(xhr.responseText);
        }

    odds.actual_odds = actual_odds
}

loadBookmakers();
loadOdds();