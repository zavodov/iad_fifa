
$(document).ready(function() {

    app.leagues.forEach(function(item, i, arr) {
        $('#leagues-table-' + item.league_code)
            .DataTable({
                    info: false,
                    paging: false,
                    searching: false
                })
                .column('6:visible')
                .order( 'desc' )
                .draw();
    });

} );