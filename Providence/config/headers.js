//Заголовки запроса для обращения к различным RESTful API
module.exports = {
    footballDataHeaders: {
        //minified - не содержит ненужной мета-информации
        'X-Response-Control': 'minified',
        //Токен football-data, записанный в переменную окуржения API_TOKEN_FD
        'X-Auth-Token': process.env.API_TOKEN_FD
    }
};
