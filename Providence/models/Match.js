module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Match', {
        match_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        home_team_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        away_team_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        league_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        match_status: {
            //https://api.football-data.org/docs/v1/state_diagram_fixturestatus.png
            //CANCELED - отменён
            //POSTPONED - перенесён
            //SCHEDULED - есть дата
            //TIMED - есть время
            type: DataTypes.ENUM('CANCELED', 'POSTPONED', 'SCHEDULED', 'TIMED', 'IN_PLAY', 'FINISHED'),
            allowNull: false
        },
        match_date: {
            type: "TIMESTAMP",
            allowNull: false
        },
        home_team_goals: {
            type: "SMALLINT",
        },
        away_team_goals: {
            type: "SMALLINT",
        }
    }, {
        timestamps: false,
        freezeTableName: true
    })
};