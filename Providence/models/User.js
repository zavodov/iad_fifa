module.exports = function (sequelize, DataTypes) {
    return sequelize.define('User', {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING(20),
            unique: true,
            allowNull: false
        },
        //Not Null не установлен, т.к. возможна авторизация через вк
        password: {
            type: DataTypes.STRING(128),
        }
    }, {
        //timestamp - время создания, время обновления
        timestamps: false,
        //freezeTableName - sequelize пытается обзывать таблицы во множественном числе
        freezeTableName: true
    })
};