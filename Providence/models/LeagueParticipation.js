module.exports = function (sequelize, DataTypes) {
    return sequelize.define('LeagueParticipation', {
        league_participation_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        team_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        league_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        wins: {
            type: "SMALLINT",
            allowNull: false,
            defaultValue: "0"
        },
        loses: {
            type: "SMALLINT",
            allowNull: false,
            defaultValue: "0"
        },
        draws: {
            type: "SMALLINT",
            allowNull: false,
            defaultValue: "0"
        }
    }, {
        timestamps: false,
        freezeTableName: true
    })
};