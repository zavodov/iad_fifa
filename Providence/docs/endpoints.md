# Спецификация endpoint'ов

### Список endpoint'ов

| Ресурс | Действие | URI |
| ------ | ------ | ------ |
| League | Список всех лиг | /api/leagues |
| League | Информация о лиге и список всех командах лиги | /api/leagues/{code} |
| Match | Список всех матчей лиги | /api/leagues/{code}/matches |
| Match | Список всех предстоящих матчей лиги с установленным временем | /api/leagues/{code}/matches/timed |
| Team | Информация о команде | /api/teams/{id} |
| Match | Список матчей команды | /api/teams/{id}/matches |
| Match | Список всех предстоящих матчей с установленным временем | /api/matches |
| Match | Подробная информаци о матче, содержащая список событий и предложения от букмекеров | /api/matches/{id} |
| Match | Список предстоящих матчей, содержащий список событий и предложений от букмекеров  | /api/odds/actual |
| Bookmakers | Список букмекеров | /api/bookmakers |

### Примеры запросов
#### Запрос:
`GET /api/leagues`
#### Ответ:
```json
[
    {
        "league_id":1,
        "league_code":"PL",
        "league_name":"Premier League 2017/18",
        "league_teams_number":20
    }
...
]
```
#### Запрос:
`GET /api/leagues/PL`
#### Ответ:
```json
{
    "league_id":1,
    "league_code":"PL",
    "league_name":"Premier League 2017/18",
    "league_teams_number":20,
    "league_participation":
    [
        {
            "league_participation_id":64,
            "wins":4,
            "loses":7,
            "draws":2,
            "team":
            {
                "team_id":66,
                "team_name":"AFC Bournemouth",
                "team_logo":"https://upload.wikimedia.org/wikipedia/de/4/41/Afc_bournemouth.svg"
            }
        }
        ...
    ]
}
```
#### Запрос:
`GET /api/matches`
#### Ответ:
```json
[
    {
        "match_id":1063,
        "league_id":1,
        "match_status":"SCHEDULED",
        "match_date":"2018-05-13T11:00:00.000Z",
        "home_team_goals":null,
        "away_team_goals":null,
        "home_team":
        {
            "team_name":"Swansea City FC",
            "team_logo":"http://upload.wikimedia.org/wikipedia/de/a/ab/Swansea_City_Logo.svg"
        },
        "away_team":
        {
            "team_name":"Stoke City FC",
            "team_logo":"http://upload.wikimedia.org/wikipedia/de/a/a3/Stoke_City.svg"
        }
    }
    ...
]
```
#### Запрос:
`GET /api/teams/{id}`
#### Ответ:
```json
{
    "team_id":1,
    "team_name":"Bayer Leverkusen",
    "team_logo":"https://upload.wikimedia.org/wikipedia/en/5/59/Bayer_04_Leverkusen_logo.svg"
}
```
#### Запрос:
`GET /api/odds/actual`
#### Ответ:
```json
[
    {
        "match_id":1588,
        "league_id":2,
        "match_status":"TIMED",
        "match_date":"2017-11-28T15:00:00.000Z",
        "home_team_goals":null,
        "away_team_goals":null, 
        "home_team":
        {
            "team_name":"Amiens SC",
            "team_logo":"https://upload.wikimedia.org/wikipedia/de/0/0d/SC_Amiens_Logo.svg"
        }, 
        "away_team":
        {
            "team_name":"Dijon FCO",
            "team_logo":"http://upload.wikimedia.org/wikipedia/de/e/e1/FCO_Dijon.svg"
        }, 
        "events":
        [
            {
                "event_id":3276,
                "event_type":"AWAY",
                "offers":
                [
                        {
                        "offer_id":1359,
                        "bookmaker_id":3,
                        "offer_coefficient":3.25,
                        "offer_time":"2017-11-28T17:15:15.790Z"
                   },
                ...
                ]
            }
            ...
        ]
    }
    ...
]
```
#### Запрос:
`GET /api/bookmakers`
#### Ответ:
```json
[
    {
        "bookmaker_id":1,
        "bookmaker_name":"Pari Match",
        "bookmaker_url":"https://www.parimatch.com/"
    }
    ...
]
```

### Дополнительная информация

#### Список лиг

| Код | Страна | Название |
| ------ | ------ | ------ |
| PL | Англия | Premiere League |
| BL1 | Германия | 1. Bundesliga |
| SA | Италия | Serie A |
| PD | Испания | Primera Division |
| FL1 | Франция | Ligue 1 |
